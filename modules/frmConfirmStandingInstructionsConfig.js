var frmConfirmStandingInstructionsConfig = {
  "formid": "frmConfirmStandingInstructions",
  "frmConfirmStandingInstructions": {
    "entity": "StandingInstructions",
    "objectServiceName": "RBObjects",
    "objectServiceOptions" : {"access":"online"},
  },
  "segSIDetails": {
    "fieldprops": {
      "widgettype": "Segment",
      "entity": "StandingInstructions",
      "additionalFields": ["transfer_type","instruction_number","status","txn_frequency","frequency_code","ccy_code","period","credit_branch_code","credit_bracnh_desc","credit_acc","debit_acc"],
      "field": {
        "lblTransactiondate": {
          "widgettype": "Label",
          "field": "start_date"
        },
        "lblTransactionDesc":{
          "widgettype": "Label",
          "field": "transfer_typedesc"
        },
        "lblAmount":{
          "widgettype": "Label",   
          "field": "amount"
        }
      }
    }
  },
  "lblInstructionNumber":{
    "fieldprops": {
      "entity":"StandingInstructions",   
      "widgettype":"Label",  
      "field":"InstructionNum"
    }		  
  },
  "lblCustID":{
    "fieldprops": {
      "entity":"StandingInstructions",   
      "widgettype":"Label",  
      "field":"custId"
    }		  
  },
  "lblfcdb":{
    "fieldprops": {
      "entity":"StandingInstructions",   
      "widgettype":"Label",  
      "field":"p_fcdb_ref_no"
    }		  
  },
  "lblUsr":{
    "fieldprops": {
      "entity":"StandingInstructions",   
      "widgettype":"Label",  
      "field":"usr"
    }		  
  },
  "lblPass":{
    "fieldprops": {
      "entity":"StandingInstructions",   
      "widgettype":"Label",  
      "field":"pass"
    }		  
  }
};

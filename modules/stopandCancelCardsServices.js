function stopCardsFunction(cardNum, action)
{
  if(kony.sdk.isNetworkAvailable()){
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Cards", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Cards"); 

    dataObject.addField("custId",custid);
    dataObject.addField("Month","");
    dataObject.addField("Year","");
    //     dataObject.addField("usr","konyuat");
    //     dataObject.addField("pass","konyuat_1");
    dataObject.addField("CardNum",cardNum);
    dataObject.addField("p_fcdb_ref_no","");
    dataObject.addField("p_action",action);

    var serviceOptions = {"dataObject":dataObject,"headers":headers};
    kony.print("Input params getBillDetailsPostpaidBills-->"+ serviceOptions );
    kony.print("Input params getBillDetailsPostpaidBills-->"+ JSON.stringify(serviceOptions) );
    popupCommonAlertDimiss();
    ShowLoadingScreen();
    modelObj.customVerb("stopCard",serviceOptions, stopCardsSuccess, stopCardsError);
  }
  else{
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}


function stopCardsSuccess(response)
{
  kony.application.dismissLoadingScreen();
  if(response.ErrorCode == 0){
    if(CARDLIST_DETAILS.operationType == CARD_COSNTANTS.ACTIVATE){
      frmManageCardsKA["flxStopCardSwitchOff"].setVisibility(false);
      frmManageCardsKA["flxStopCardSwitchOn"].setVisibility(true);
      frmManageCardsKA.lblCreditCardStatus.text = geti18Value("i18n.card.active");
      kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_status = "Activated";
    }else if(CARDLIST_DETAILS.operationType == CARD_COSNTANTS.DEACTIVATE){
      frmManageCardsKA["flxStopCardSwitchOff"].setVisibility(true);
      frmManageCardsKA["flxStopCardSwitchOn"].setVisibility(false);
      frmManageCardsKA.lblCreditCardStatus.text = geti18Value("i18n.card.notactive");
      kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_status = "Deactivated";
      customAlertPopup(geti18Value("i18n.common.success"), geti18nkey("i18n.card.deactivatesuccess"),popupCommonAlertDimiss,"");
    }
    //customAlertPopup(geti18Value("i18n.common.success"), geti18nkey("i18n.cards.reqsubsucc"),popupCommonAlertDimiss,"");
  }else{
    var Message = getServiceErrorMessage(response.ErrorCode,"stopCardsSuccess");
    customAlertPopup(geti18Value("i18n.Bene.Failed"), Message,popupCommonAlertDimiss,"");
  }
}


function stopCardsError(response)
{
  kony.application.dismissLoadingScreen();
  var Message = geti18Value("i18n.common.somethingwentwrong");
  customAlertPopup(geti18Value("i18n.Bene.Failed"), Message,popupCommonAlertDimiss,"");
  //popupCommonAlertDimiss();
}

function cancelCardsFunction(cardNum, reason, cardType)
{
  if(kony.sdk.isNetworkAvailable()){
    ShowLoadingScreen();

    //     alert(JSON.stringify(CARDLIST_DETAILS));
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Cards", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Cards"); 

    dataObject.addField("custId",custid);
    dataObject.addField("CardNum",cardNum);
    dataObject.addField("cardType", cardType);
    dataObject.addField("Reason", reason);

    var serviceOptions = {"dataObject":dataObject,"headers":headers};
    kony.print("Input params cancelCard-->"+ JSON.stringify(serviceOptions) );
    modelObj.customVerb("CancelCard",serviceOptions, cancelCardsSuccess, cancelCardsError);
  }else{
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}


function cancelCardsSuccess(response)
{
  kony.application.dismissLoadingScreen();
  if(response.ErrorCode == 0){
    customAlertPopup(geti18Value("i18n.common.success"), geti18nkey("i18n.cards.reqsubsucc"),function(){frmManageCardsKA.flxcancelSwitchOff.isVisible = false;
                                                                                                        frmManageCardsKA.flxcancelSwitchOn.isVisible = true;
                                                                                                        popupCommonAlertDimiss();
                                                                                                        frmManageCardsKA.show();},"");
    kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_status = "Deactivated";
  }
  else{
    var Message = getErrorMessage(response.ErrorCode);
    customAlertPopup(geti18Value("i18n.Bene.Failed"), Message,popupCommonAlertDimiss,"");
  }
}


function cancelCardsError(response)
{
  kony.application.dismissLoadingScreen();
  var Message = geti18Value("i18n.common.somethingwentwrong");
  customAlertPopup(geti18Value("i18n.Bene.Failed"),Message,popupCommonAlertDimiss,"");
  popupStopCard.dismiss();
}
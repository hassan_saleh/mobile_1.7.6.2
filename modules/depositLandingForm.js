//Setting Deposit Data

//used For visiting FirstTime Deposit
function firstTimeVisitDepositForm()
{
  if(kony.store.getItem("depositForm") === null || kony.store.getItem("depositForm") === false)
  {
     //alert("hi entering in to deposit header skin");
    frmTermsAndConditionsKA.titleBarWrapper.skin=skncontainerBkgNone;
    //kony.print("frmTermsAndConditionsKA.titleBarWrapper.skin"+frmTermsAndConditionsKA.titleBarWrapper.skin);
    frmTermsAndConditionsKA.show();
  }
  else
  { 
     getDeposits("frmDepositPayLandingKA");
  }
}

function checkDepositIphone(){
  userAgent = kony.os.userAgent();
  	if (userAgent === "iPhone"){
 		if(kony.retailBanking.globalData.globals.configPreferences.rdc === "OFF" ){
    		frmMoreLandingKA.footers[0].FlxDeposits.setVisibility(false);
  		}
 	 } 
}

//Selecting Scheduled Transactions
function scheduledDepositTabSelected(){
   frmDepositPayLandingKA.transactionListsContainer.animate(
        kony.ui.createAnimation({100:
        	{ 
             "left": '-100%',
             "stepConfig":{"timingFunction": easeOut}}}),
        {fillMode: forwards ,duration:0.3},
        {animationEnd: function() {} });
  
    frmDepositPayLandingKA.tabSelectedIndicator.animate(
        kony.ui.createAnimation({100:
        	{ 
             "left": '50%',
             "stepConfig":{"timingFunction": easeOut}}}),
        {fillMode: forwards ,duration:0.3},
        {animationEnd: function() {} });
  
    frmDepositPayLandingKA.scheduledTabButton.skin = skntabSelected;
    frmDepositPayLandingKA.recentTabButton.skin = skntabDeselected;
}

//Selecting Recent Transactions
function recentDepositTabSelected(){
   frmDepositPayLandingKA.transactionListsContainer.animate(
        kony.ui.createAnimation({100:
          { 
             "left": '0%',
             "stepConfig":{"timingFunction": easeOut}}}),
        {fillMode: forwards ,duration:0.3},
        {animationEnd: function() {} });
  
  frmDepositPayLandingKA.tabSelectedIndicator.animate(
        kony.ui.createAnimation({100:
          { 
             "left": '0%',
             "stepConfig":{"timingFunction": easeOut}}}),
        {fillMode: forwards ,duration:0.3},
        {animationEnd: function() {} }); 
  
  frmDepositPayLandingKA.recentTabButton.skin = skntabSelected;
  frmDepositPayLandingKA.scheduledTabButton.skin = skntabDeselected;
}

//
function scheduledDepositTransactionsOnRowClick() {
  selectedAccountColor = checkingColor;
}
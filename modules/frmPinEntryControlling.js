var enter_count = 0;
var pass = "";
var firstPass = "";

function pinEntry1PreShow() {
    enter_count = 0;
    for (var i = 6; i >= 1; i--) {
        frmPinEntryStep1["flxProgressButton" + i].skin = "sknFlxProgressButtonEmpty";
    }
}

function check(i) {
    //store the number as string and matches entered and re-entered pin
    enter_count++;
    var formName = kony.application.getCurrentForm().id;
    if (enter_count <= 6) //store  umber till count is less than 6
    {
        frmPinEntryStep2.DidNotMatchLabel.isVisible = false;
        pass = pass + i;
        if (formName === "frmPinEntryStep1")
            frmPinEntryStep1["flxProgressButton" + enter_count].skin = "sknFlxProgressButtonFill";
        else if (formName === "frmPinEntryStep2")
            frmPinEntryStep2["flxProgressButton" + enter_count].skin = "sknFlxProgressButtonFill";

    }

    if (firstPass !== "" && enter_count == 6) {
        //if the pin is entered in the second form 
        if (firstPass === pass) { //first pass will contain the final pin
            gblToOTPFrom = "Pin";
           kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            callRequestOTP();
            //EnableDisablePinLogin();
        } else {
            recheck_pass();
            //alert("PIN Confirmation failed!Please Re-Enter");
            frmPinEntryStep2.DidNotMatchLabel.setVisibility(true);

            pass = "";
            enter_count = 0;
        }
    }


    if (enter_count == 6 && formName === "frmPinEntryStep1") {
        firstPass = pass;
        recheck_pass();
    }
}


function recheck_pass() { //change the previous form to second step form

    frmPinEntryStep2.show();
    enter_count = 0;
    pass = "";
    clearAll();
}



function CancelClick() { //on click of clear button
    kony.print("CancelClick diffFlowFlag :: " + diffFlowFlag);
    clearAll();
    if (diffFlowFlag == "LOGINFLOW") {
        frmAuthorizationAlternatives.show();
    } else {
        frmSettingsKA.show();
    }

}

function DeleteClick() {
    var formName = kony.application.getCurrentForm().id;
    if (enter_count > 0) {
        pass = pass.substring(0, pass.length - 1);
        if (formName === "frmPinEntryStep1")
            frmPinEntryStep1["flxProgressButton" + enter_count].skin = "sknFlxProgressButtonEmpty";
        else if (formName === "frmPinEntryStep2")
            frmPinEntryStep2["flxProgressButton" + enter_count].skin = "sknFlxProgressButtonEmpty";

        enter_count--;
    }
}

function clearAll() { //changes all filled skin to empty skins
    var formName = kony.application.getCurrentForm().id;
    for (var i = 6; i >= 1; i--) {
        if (formName === "frmPinEntryStep1")
            frmPinEntryStep1["flxProgressButton" + i].skin = "sknFlxProgressButtonEmpty";
        else if (formName === "frmPinEntryStep2")
            frmPinEntryStep2["flxProgressButton" + i].skin = "sknFlxProgressButtonEmpty";

    }
}

function pinEntry2PreShow() {

    for (var i = 6; i >= 1; i--) {
        frmPinEntryStep2["flxProgressButton" + i].skin = "sknFlxProgressButtonEmpty";
    }

}


function clickNoThanks()//on click of back button on frmPinEntry
{
  enter_count=0;
  pass="";
  firstPass="";
  
  updateFlags("isPinEnabledFlag",false);
  if(fromSettings==true){
   
    fromSettings=false;
  }
  else{
    navigationOnSetPinMode();
  }
}


function callback_ofanim() {
        frmPinEntryStep2.flxProgressButtons.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "35%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_BACKWARDS,
            "duration": 0.07
        }, {
          
        });
    }
function animation()
{
  for(var i=0;i<5;i++){
  	frmPinEntryStep2.flxProgressButtons.animate(
    kony.ui.createAnimation({
        "100": {
            "left": "20%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": "1",
        "fillMode": kony.anim.FILL_MODE_BACKWARDS,
        "duration": 0.07
    }, {
        "animationEnd": callback_ofanim
    });
}
}



function callback_ofanimation_invalidPin() {
        frmPinEntryStep1.flxProgressButtons.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "35%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_BACKWARDS,
            "duration": 0.07
        }, {
          
        });
    }
function animation_invalidPin()
{
  //for(var i=0;i<5;i++){
  	frmPinEntryStep1.flxProgressButtons.animate(
    kony.ui.createAnimation({
        "100": {
            "left": "20%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_BACKWARDS,
        "duration": 0.12
    }, {
        "animationEnd": callback_ofanimation_invalidPin
    });
//}
}





/*function checkUniquenss(str){
   var splitString = str.split(""); 
   var reverseArray = splitString.reverse();
   var joinArray = reverseArray.join("");
   var result=Math.abs(Number(str)-Number(joinArray));
  if(result===Number("530865")){
        return false;
 	}
 	return true;
}

function checkConsicutives(str){
   var splitString = str.split(""); 
   var reverseArray = splitString.reverse();
   var joinArray = reverseArray.join("");
   var result=Math.abs(Number(str)-Number(joinArray));
   if(result===Number("0")){
        return false;
 	}
 	return true;
}

// clearClicked -- renamed as button_clickPINLogin
 function button_clickPINLogin(action){on click of clear button
 	if(action === "Delete"){
       if(enter_count>0)
         {
           pass = pass.substring(0, pass.length - 1);
            if(formName==="frmPinEntryStep1")
                frmPinEntryStep1["flxProgressButton"+enter_count].skin="sknFlxProgressButtonEmpty";
            else if(formName==="frmPinEntryStep2")
           		 frmPinEntryStep2["flxProgressButton"+enter_count].skin="sknFlxProgressButtonEmpty";

           enter_count--;        
         }

     if(enter_count>0){
           if(formName==="frmPinEntryStep1")
 		frmPinEntryStep1.flxClear.isVisible="true";
          else if(formName==="frmPinEntryStep2")
             frmPinEntryStep2.flxClear.isVisible="true";
       }
     else{
        if(formName==="frmPinEntryStep1")
 		frmPinEntryStep1.flxClear.isVisible="false";
     	if(firstPass !== ""){
         	frmPinEntryStep1.btnCancel.text = "Cancel";
         }
          else if(formName==="frmPinEntryStep2")
             frmPinEntryStep2.flxClear.isVisible="false";
     }
     }else if(action === "Cancel"){
     	frmPinSetEnable.show();
     	clearAll();
     	frmPinEntryStep1.StepNumberCount.text = "1";
         frmPinEntryStep1.btnDelete.text = "Delete";
     	frmPinEntryStep1.btnCancel.text = "Cancel";
     }else if(action === "Confirm"){
    	
     }
 }

*/
//Type your code here

function set_CASA_ACCOUNT_LOGIN(){
  try{
    kony.print("CASA ACCOUNT USER LOGIN");
  	hasCasaAccounts = true;
    onClickCardsMenu();
  }catch(e){
  	kony.print("Exception_set_CASA_ACCOUNT_LOGIN ::"+e);
  }
}

function set_UI_FOR_CASA_USER(form){
	try{
      kony.print("Form ::"+form.id);
      switch(form.id){
        case "frmCardsLandingKA":
        	form.footerBack.setVisibility(!hasCasaAccounts);
        	form.btnApplyCards.setVisibility(!hasCasaAccounts);
      		form.flxBack.setVisibility(!hasCasaAccounts);
          break;
        case "frmManageCardsKA":
        	form.footerBack.setVisibility(!hasCasaAccounts);
          	form.flxWebPay.setVisibility(hasCasaAccounts);
            form.flxCreditCardPay.setVisibility(!hasCasaAccounts);
          break;
        case "frmSettingsKA":
        	form.flxAlertandNotifications.setVisibility(!hasCasaAccounts);
            form.flxAccountPreferences.setVisibility(true);//Show Account prefernces section when user does'nt have accounts or hide all accounts.
          break;
      }
      
    }catch(e){
    	kony.print("Exception_set_UI_FOR_CASA_USER"+e);
    }
}
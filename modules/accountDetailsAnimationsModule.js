//Type your code here

// Marzia Zain       6th-April-2021            Account Details Header Animations

var lastScroll = 0;

function accountsHeaderAnimate_init()
{
	frmAccountDetailKA.transactionSegment.onScrollStart=onScrollingStart;
  	frmAccountDetailKA.transactionSegment.onScrolling=onScrolling;
  	frmAccountDetailKA.transactionSegment.onScrollEnd=onScrollEnd;
}
    
function onScrollingStart(a)
{
  var offset = frmAccountDetailKA.transactionSegment.contentOffsetMeasured;
  var y=offset.y; var x=offset.x;  
  
  if(lastScroll < y)
  {
    kony.print("Upwards: Seg 1 x:"+x+" y:"+y+" last y:"+lastScroll);
  	scrollUpwards();    
  }
  lastScroll = y;
}

function onScrollEnd(a)
{
  var offset = frmAccountDetailKA.transactionSegment.contentOffsetMeasured;
  var y=offset.y; var x=offset.x;  
  lastScroll = y;
}

function onScrolling()
{
   	var offset = frmAccountDetailKA.transactionSegment.contentOffsetMeasured;
   	var y=offset.y; var x=offset.x; 
  
	if(y < 5)
    {
        scrollDownwards();
    }
}


function scrollUpwards()
{   frmAccountDetailKA.flxBody.height = "100%";
    frmAccountDetailKA.flxBody.animate(
    kony.ui.createAnimation({
        "100": {
            "top": "2%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 1
    }, {
        "animationEnd": function(){  }
    });    
    
}

function scrollDownwards()
{   
  	kony.print("Downwards");
  	frmAccountDetailKA.flxBody.height = "82%";
    frmAccountDetailKA.flxBody.animate(
    kony.ui.createAnimation({
        "100": {
            "top": "18%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 1
    }, {
        "animationEnd": function(){  }
    });  
    
}

function defaultAccountDetailHeaderState()
{
  //alert("defaultAccountDetailHeaderState");
  frmAccountDetailKA.flxBody.height = "82%";
  frmAccountDetailKA.flxBody.top = "18%";
}






function getCreditCards(){
  try{
  var creditCards = [], cardFlag = "C", isPrimaryCard = false;
  var allCards = null;//kony.retailBanking.globalData.prCardLandinList;
//   allCards = kony.retailBanking.globalData.prCreditCardPayList;
  if(kony.newCARD.applyCardsOption === true){
    if(kony.newCARD.maincardTYPE === "Debit"){
      cardFlag = "D";
      if(kony.newCARD.cardTYPE === "Sticker"){
      	isPrimaryCard = true;
      }
    }else if(kony.newCARD.maincardTYPE === "Prepaid"){
      cardFlag = "W";
      if(kony.newCARD.cardTYPE === "Wearable"){
      	isPrimaryCard = true;
      }
    }else if(kony.newCARD.maincardTYPE === "Credit" && kony.newCARD.cardTYPE === "Wearable"){
      	isPrimaryCard = true;
    }
    if(kony.newCARD.cardTYPE === "SuplementryCreditCard" || kony.newCARD.cardTYPE === "SuplementryDebitCard"){
    	isPrimaryCard = true;
    }
  }

  for(var i in kony.retailBanking.globalData.prCreditCardPayList){
    allCards = kony.retailBanking.globalData.prCreditCardPayList[i];
    if(allCards.cardTypeFlag == cardFlag){
      var cardNum = allCards.card_num;
      allCards.hiddenCardNum = cardNum.substring(0, 6) + "******"+ cardNum.substring(cardNum.length-4, cardNum.length);
      if(!isEmpty(allCards["nickname"])){
        allCards.name_on_card = allCards["nickname"];
      }
      if(kony.store.getItem("langPrefObj") === "ar"){
          allCards.card_type_category = allCards.hiddenCardNum+"  "+allCards.card_type;
      }else{
          allCards.card_type_category = allCards.card_type+"  "+allCards.hiddenCardNum;
      }
      kony.print("all cards after ::"+JSON.stringify(allCards));
      if(isPrimaryCard === true && (allCards.is_primary === "Primary Card" || allCards.is_primary.toUpperCase() === "P")){
      	creditCards.push(allCards);
      }else if(isPrimaryCard === false){
      	creditCards.push(allCards);
      }
	  allCards = null;
    }
  }
  frmCreditCardNumber.segCreditCardNumber.removeAll();
  frmCreditCardNumber.segCreditCardNumber.widgetDataMap = {
    lblCardNum : "card_num",
    lblHiddenCardNum : "hiddenCardNum",
    lblCardHolderName : "name_on_card",
    lblCardType:"card_type_category"
  };

  frmCreditCardNumber.segCreditCardNumber.setData(creditCards);
  }catch(e){
  	exceptionLogCall("::getCreditCards::","Exception while assigning value to segment","UI",e);
  }
}



function payFromCreditCard(){
  if(kony.sdk.isNetworkAvailable()){
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    frmCreditCardPayment.lblPaymentFlag.text = "C";
    frmCreditCardPayment.lblDum.text = "123";
    var cardNUM = "";
	if(frmCreditCardPayment.btnOtherCard.text === "t"){
    	cardNUM = frmCreditCardPayment.txtBoxOtherCard.text;
    }else{
    	cardNUM = frmCreditCardPayment.hiddenCardNumber.text;
    }
//      frmCreditCardPayment.lblHiddenAmount.text = "";
//      frmCreditCardPayment.lblHiddenAmount.text = amt;
   var teddxtt = frmCreditCardPayment.lblPaymentFlag.text;
    kony.print("Flag :: "+ frmCreditCardPayment.lblPaymentFlag.text);
    kony.print("Card num :: "+  cardNUM);
    kony.print("account num :: "+  frmCreditCardPayment.hiddenLblAccountNum.text);
    kony.print("Branch num :: "+  frmCreditCardPayment.lblBranchNum.text);
    kony.print("Amount:: "+  frmCreditCardPayment.lblHiddenAmount.text);
    
//     var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
//     var controller = INSTANCE.getFormController("frmCreditCardPayment");
//     var controllerContextData = controller.getContextData() || new kony.sdk.mvvm.NavigationObject();
//     controllerContextData.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
//     controller.setContextData(controllerContextData);
//     controller.performAction("saveData");
		var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Cards", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Cards");
        dataObject.addField("fromAccountNumber", frmCreditCardPayment.hiddenLblAccountNum.text);
		dataObject.addField("SourceBranchCode", frmCreditCardPayment.lblBranchNum.text);
		dataObject.addField("CardNum", cardNUM);
		dataObject.addField("amount", frmCreditCardPayment.lblHiddenAmount.text.replace(/,/g,""));
		dataObject.addField("PaymentFlag", frmCreditCardPayment.lblPaymentFlag.text);
		dataObject.addField("p_fcdb_ref_no", frmCreditCardPayment.lblDum.text);
		var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
    //Mai 3/17/2021 DVOC

  		kony.print("Request-->CreditCardPayment ::"+JSON.stringify(serviceOptions));
		if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb("create", serviceOptions, serv_CARDPAYMENTSUCCESS, serv_CARDPAYMENTERROR);
        }
    
  }else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}

function onRowClickCC(card){
  try{
   kony.print("CARD VALUES *"+JSON.stringify(card));
  if(kony.newCARD.applyCardsOption === true){
    var storeData = isEmpty(card) ? "" : card;
    kony.store.setItem("frmAccount",storeData);
  	selected_OPTIONS_APPLYNEWCARDS([{"lblJP":card.hiddenCardNum}],"CreditCardNumber");
  }else if(gblTModule === "ccChequeBook"){
    frmCardsChequeBook.lblCardNumberTitle.skin = "sknlblanimated75";
    frmCardsChequeBook.lblCardNumberTitle.top = "12%";
	frmCardsChequeBook.lblCardNumber.text = card.hiddenCardNum;
    frmCardsChequeBook.show();
  }else{
    gblCardNum = "";
    frmCreditCardPayment.btnNext.skin="jomopaynextDisabled";
    frmCreditCardPayment.btnNext.focusSkin="jomopaynextDisabled";
    if(!isEmpty(card)){
      frmCreditCardPayment.txtBoxOtherCard.text="";
      frmCreditCardPayment.txtBoxOtherCard.setVisibility(false);
      frmCreditCardPayment.lblCardNumber.text=card.hiddenCardNum;
      frmCreditCardPayment.hiddenCardNumber.text = card.card_num;//"4787730003632182";
      gblCardNum = card.card_num;
      frmCreditCardPayment.lblTUAmount.text = formatAmountwithcomma(card.total_billed_amount,3)+" JOD";
      frmCreditCardPayment.lblPaymentValue.text = ((card.total_due_amt === "")? "0.000":formatAmountwithcomma(card.total_due_amt,3))+" JOD";
      if(frmCreditCardPayment.lblCurrency.text === "JOD")
      {
        kony.print("JOD currency "+frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.text = (card.total_due_amt === "")?null:parseFloat(card.total_due_amt).toFixed(3);
      }
      else
      {
        kony.print("Other currency "+frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.text = (card.total_due_amt === "")?null:parseFloat(card.total_due_amt).toFixed(2);
      }
      frmCreditCardPayment.lblStaticJOD.text="JOD";
      frmCreditCardPayment.lblStaticJOD1.text="JOD";
      frmCreditCardPayment.lblDate.text = formatDateDDMMYY(card.payment_due_date);
    }
    else
      {
        frmCreditCardPayment.lblStaticJOD.text="";
      frmCreditCardPayment.lblStaticJOD1.text="";
        frmCreditCardPayment.txtBoxOtherCard.text="";
      frmCreditCardPayment.txtBoxOtherCard.setVisibility(false);
      gblCardNum = "";
      frmCreditCardPayment.lblTUAmount.text = "";
      frmCreditCardPayment.lblPaymentValue.text = "";
      frmCreditCardPayment.lblDate.text = "";
         if(frmCreditCardPayment.lblCurrency.text === "JOD")
      {
        kony.print("JOD currency "+frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.placeholder="0.000";
        frmCreditCardPayment.txtFieldAmount.text="";
      }
      else
      {
        kony.print("Other currency "+frmCreditCardPayment.lblCurrency.text);
        frmCreditCardPayment.txtFieldAmount.placeholder="0.00";
        frmCreditCardPayment.txtFieldAmount.text="";
      }  
      }
    if(frmCreditCardPayment.txtFieldAmount.text !== null && frmCreditCardPayment.txtFieldAmount.text !== "" && parseFloat(frmCreditCardPayment.txtFieldAmount.text) > 0)//frmCreditCardPayment.txtBoxOtherCard.text !==""
    {
      frmCreditCardPayment.btnNext.skin="jomopaynextEnabled";
      frmCreditCardPayment.btnNext.focusSkin="jomopaynextEnabled";
    }

    frmCreditCardPayment.show();
    kony.print("On Row Click Credit Card Number");
  }
  }catch(e){
  	exceptionLogCall("::onRowClickCC::","Exception onclick segment, value assignment","UI",e);
  }
}


function serv_CARDPAYMENTSUCCESS(res){
	try{
      
      if(!isEmpty(res)){
        kony.print("Success "+res);
        //alert("Success "+JSON.stringify(res));
        kony.application.dismissLoadingScreen();
        logObj[0] = frmCreditCardPayment.hiddenLblAccountNum.text;
        logObj[1] = frmCreditCardPayment.lblBranchNum.text;
        logObj[2] = frmCreditCardPayment.lblFromCurr.text;
        logObj[3] = mask_CreditCardNumber(frmCreditCardPayment.hiddenCardNumber.text);
        logObj[12] = frmCreditCardPayment.lblHiddenAmount.text;
        logObj[13] = "CARDPAY";
        kony.print("Response - card payment :: "+JSON.stringify(res));
        is_CARDPAYMENTSUCCESS = true;
        var secondryButtonTitle = "", secondryButtonKey = "";
        var thrdButtonTitle = "", thrdButtonKey = "";//hassan go to my cards
        if(kony.boj.creditcardOption === "fromPayNow"){
          secondryButtonTitle = geti18nkey("i18n.cards.gotoCards");
          secondryButtonKey = "Cards";
        }else{
          secondryButtonTitle = geti18nkey("i18n.transfers.gotoDashPay");
          secondryButtonKey = "PaymentDashboard";
          //hassan go to my cards
          if (kony.retailBanking.globalData.prCreditCardPayList.length>0)
             {
               thrdButtonTitle=geti18nkey("i18n.cards.gotoCards");
               thrdButtonKey="CardsLanding";
             }
          //hassan go to my cards
        }
        if(!isEmpty(res.ErrorCode)){
          if(res.ErrorCode == 0 || res.ErrorCode === "00000" || res.ErrorCode === "0"){
                gblDVOC = "2";//Cards payments
            //customAlertPopup("Transaction Reference Id",res.referenceId,popupCommonAlertDimiss,"");
            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.payment.paySucc"), geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard"), secondryButtonTitle, secondryButtonKey,thrdButtonTitle, thrdButtonKey, geti18Value("i18n.common.ReferenceId") + " "  + res.referenceId);
            logObj[16] = res.referenceId; 
            logObj[17] = "SUCCESS"
            logObj[18] = "Payment Successful"  
//             frmCongratulations.show();
//             frmCreditCardPayment.dismiss(); 
          }
          else{
            var Message = getErrorMessage(res.ErrorCode);
            //alert("Message ::" + Message);
            if(isEmpty(Message)){
              Message = getServiceErrorMessage(res.ErrorCode,"serv_CARDPAYMENTSUCCESS");
            }        
            //alert("Message after ::" + Message);
//             customAlertPopup(geti18Value("i18n.Bene.Failed"), Message,popupCommonAlertDimiss,"");
            customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
            kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), Message,geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard"),secondryButtonTitle, secondryButtonKey);
            logObj[16] =""; 
            logObj[17] = "FAILURE";
            logObj[18] =  res.ErrorCode +" " +Message;
//             frmCongratulations.show();
//             frmCreditCardPayment.dismiss();
          }
        }
        else{
          logObj[16] = ""; 
          logObj[17] = "FAILURE";
          logObj[18] = "";
          customAlertPopup(geti18nkey("i18n.NUO.Error"), 
                           geti18Value("i18n.common.somethingwentwrong"),
                           popupCommonAlertDimiss, "");
          kony.application.dismissLoadingScreen();
        }

      }
      else{
        logObj[16] = ""; 
        logObj[17] = "FAILURE";
        logObj[18] = "";
        customAlertPopup(geti18nkey("i18n.NUO.Error"), 
                         geti18Value("i18n.common.somethingwentwrong"),
                         popupCommonAlertDimiss, "");
        kony.application.dismissLoadingScreen();
      }
      gblTModule = "";
      loggerCall();
    }catch(e){
      kony.print("Exception_serv_CARDPAYMENTSUCCESS ::"+e);
      exceptionLogCall("::serv_CARDPAYMENTSUCCESS::","Error in handling code","UI",e);
    }
}
  
function serv_CARDPAYMENTERROR(err){
  kony.application.dismissLoadingScreen();
  logObj[0] = frmCreditCardPayment.hiddenLblAccountNum.text;
  logObj[1] = frmCreditCardPayment.lblBranchNum.text;
  logObj[2] = frmCreditCardPayment.lblFromCurr.text;
  logObj[3] = mask_CreditCardNumber(frmCreditCardPayment.hiddenCardNumber.text);
  logObj[12] = frmCreditCardPayment.lblHiddenAmount.text;
  logObj[13] = "CARDPAY";
  logObj[16] = ""; 
  logObj[17] = "FAILURE";
  logObj[18] = err;
  gblTModule = "";
  kony.print("Response - card payment :: "+JSON.stringify(err));
  kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "",geti18Value("i18n.Bene.GotoAccDash"),geti18nkey ("i18n.common.AccountDashboard"),geti18nkey("i18n.transfers.gotoDashPay"), "PaymentDashboard");
  loggerCall();

}

function isCreditCardAvailable(data){
  try{
  	for(var i in data)
      if(data[i].cardTypeFlag === "C")
        return true;
    return false;
  }catch(e){
  	kony.print("Exception_isCreditCardAvailable ::"+e);
  }
}
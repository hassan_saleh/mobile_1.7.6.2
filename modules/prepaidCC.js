function CallServicePrePaidNewBillPayCC()
{
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var inputs = JSON.parse(kony.store.getItem("PrepaidInputs")); // hassan fix xcode
  var validationCode=frmConfirmPayBill.prValidationCode.text;
  var dueAmount=frmConfirmPayBill.prDueAmount.text;
  var fees=frmConfirmPayBill.lblFeeAmnt.text;
  var billNo="";
  var denoFlag="";
  if(inputs.billing_no_flag == "false")
    billNo="";
  else
    billNo=frmConfirmPayBill.prBillingNo.text;

  if(inputs.deno_flag == "false")
    denoFlag = "";
  else
    denoFlag=frmConfirmPayBill.prdenoFlag.text;

  var request_params ={ "custId":custid,
                       "p_fcdb_ref_no":"",
                       "lang":"eng",
                       "p_biller_code":frmConfirmPayBill.lblBillerCode.text,
                       "p_serv_type_code":frmConfirmPayBill.prServiceCode.text,
                       "p_billing_no":billNo,
                       "P_deno":denoFlag,
                       "amount":isNullorEmptyorUndefined(frmConfirmPayBill.lblPaidAmnt.text)?0:frmConfirmPayBill.lblPaidAmnt.text,
                       "p_validation_code":validationCode,
                       "p_due_amt":dueAmount,
                       "p_fees":fees,
                       "P_mobile_no":"0700000000",
                       "p_paymt_Type":"A",
                       "ccNum":frmConfirmPayBill.prAccorCCNo.text,
                       "p_AccessChannel":"MOBILE",
                       "p_PaymentMethod":"CCARD",
                       "p_SendSMSFlag":"T"
                      }
  kony.print("request_params hassan"+ JSON.stringify(request_params));
  
        var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprCcPpPayment");
      	appMFConfiguration.invokeOperation("prCcPpPayment", {},request_params,
                                           successPrePaidCC,
                                           function(error){kony.print("error"+ JSON.stringify(error));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
  

}



function successPrePaidCC(response){
  kony.print("successPass"+ JSON.stringify(response));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  if (response.ErrorCode==="00000"){
       kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"),
        geti18Value("i18n.bills.TransactionReferenceNumber")+" "+response.referenceId,
        geti18Value("i18n.Bene.GotoAccDash"), 
        geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.common.CgtBL"), 
        "ManagePayee", geti18Value("i18n.common.paynow"), "PayBill", " " + " " + "");
    }
  else
    {
        var Message = getErrorMessage(response.errorCode);
        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"),
        Message,
        geti18Value("i18n.Bene.GotoAccDash"), 
        geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.common.CgtBL"), 
        "ManagePayee", geti18Value("i18n.common.paynow"), "PayBill", " " + " " + "");
    }
}




function CallServicePrePaidRegBillPayCC()
{
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var inputs = JSON.parse(kony.store.getItem("PrepaidInputs")); // hassan fix xcode
  var validationCode=frmConfirmPayBill.prValidationCode.text;
  var dueAmount=frmConfirmPayBill.prDueAmount.text;
  var fees=frmConfirmPayBill.lblFeeAmnt.text;
  var billNo="";
  var denoFlag="";
  var amount=0;
  var billingNumber = frmPrePaidPayeeDetailsKA.txtBillingNumber.text;
  
  if(billingNumber.length > 1)  {
      billNo =billingNumber;
    }
    else{
      billNo="";
    }
  
    if(frmPrePaidPayeeDetailsKA.txtDenomination.text.length > 2){
      amount=0;
      denoFlag=frmPrePaidPayeeDetailsKA.txtDenominationCode.text;
     // kony.boj.Biller[kony.boj.selectedBillerType].selectedDenoType= {"deno_code":frmPrePaidPayeeDetailsKA.txtDenominationCode.text};
    }
    else{
      denoFlag="";
      amount=frmPrePaidPayeeDetailsKA.txtAmount.text.replace(/,/g,"");
    }
  
  var request_params ={ "custId":custid,
                       "p_fcdb_ref_no":"",
                       "lang":"eng",
                       "p_biller_code":frmConfirmPrePayBill.prBillerCode.text,
                       "p_serv_type_code":frmConfirmPrePayBill.prServiceCode.text,
                       "p_billing_no":billNo,
                       "P_deno":denoFlag,
                       "amount":isNullorEmptyorUndefined(frmConfirmPrePayBill.lblPaidAmnt.text)?0:frmConfirmPrePayBill.lblPaidAmnt.text,
                       "p_validation_code":frmConfirmPrePayBill.prValidationCode.text,
                       "p_due_amt":frmConfirmPrePayBill.prDueAmount.text,
                       "p_fees":frmConfirmPrePayBill.lblFeeAmnt.text,
                       "P_mobile_no":"0700000000",
                       "p_paymt_Type":"A",
                       "ccNum":frmConfirmPrePayBill.prAccorCCNo.text,
                       "p_AccessChannel":"MOBILE",
                       "p_PaymentMethod":"CCARD",
                       "p_SendSMSFlag":"T"
                      }
  kony.print("request_params hassan"+ JSON.stringify(request_params));
        var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprCcPpPayment");
      	appMFConfiguration.invokeOperation("prCcPpPayment", {},request_params,
                                           successPrePaidRegCC,
                                           function(error){kony.print("error"+ JSON.stringify(error));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
  


}


function successPrePaidRegCC(response){
  kony.print("success successPrePaidRegCC"+ JSON.stringify(response));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  if (response.ErrorCode==="00000"){
       kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"),
        geti18Value("i18n.bills.TransactionReferenceNumber")+" "+response.referenceId,
        geti18Value("i18n.Bene.GotoAccDash"), 
        geti18nkey("i18n.common.AccountDashboard"),geti18nkey ("i18n.common.gotoBillsScreen") , "MyBillers", "", "", " " + " " + "");
    }
  else
    {
        var Message = getErrorMessage(response.errorCode);
        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"),
        Message,
        geti18Value("i18n.Bene.GotoAccDash"), 
        geti18nkey("i18n.common.AccountDashboard"),geti18nkey ("i18n.common.gotoBillsScreen") , "MyBillers", "", "", " " + " " + "");
    }
}

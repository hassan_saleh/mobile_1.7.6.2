//Type your code here
function serv_IPS_REGISTRATION(){
  //                             	 {"key":"accNum","value":kony.store.getItem("frmAccount").accountID},
  try{    
    var requestParams = [{"key":"custId","value":custid},
                         {"key":"accNum","value":kony.store.getItem("frmAccount").accountID},
                         {"key":"branch","value":kony.store.getItem("frmAccount").branchNumber},
                         {"key":"aliasType","value": Alias_Type},
                         {"key":"aliasValue","value":frmIPSRegistration.lblAliasConfirmation.text},
                         {"key":"p_channel","value":"MOBILE"},
                         {"key":"p_user_id","value":"BOJMOB"}];
    call_CUSTOM_VERB("IPSRegistration",requestParams,
                     function(res){
      kony.print("success response ::"+JSON.stringify(res));
      if(res.statusCode === "00000"){
        //Omar ALnajjar 03/10/2021  CLIQ Register
        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.CLIQ.registeredsuccessfully"), geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard", geti18Value("i18n.CLIQ.GoToCliqDashboard"), "CliqDashboardManage", geti18Value("i18n.CLIQ.Share"), "CliqShareAlias", "");
      }else{
        var Message = getErrorMessage(res.statusCode);
        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), Message, geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard",geti18Value("i18n.CLIQ.GoToCliqDashboard"), "CliqDashboardHome", "", "", "");
      }
      frmIPSRegistration.destroy();
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }, 
                     function(err){

      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      exceptionLogCall("IPSRegistration","SERVICE FAILURE ","IPSRegistration",err);
    },"");
  }catch(e){
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("Exception_serv_IPS_REGISTRATION ::"+e);
    exceptionLogCall("IPSRegistration","SERVICE FAILURE ","IPSRegistration",e);
  }

}

//Mai 13/12/2020 Add Alias Call Mobile Fabric

function serv_IPS_AddAlias(){
  //                             	 {"key":"accNum","value":kony.store.getItem("frmAccount").accountID},
  try{    
    var requestParams = [{"key":"custId","value":custid},
                         {"key":"accNum","value":kony.store.getItem("frmAccount").accountID},
                         {"key":"branch","value":kony.store.getItem("frmAccount").branchNumber},
                         {"key":"aliasType","value": Alias_Type},
                         {"key":"aliasValue","value":frmIPSCreateAlias.lblAliasConfirmationText.text},
                         {"key":"p_channel","value":"MOBILE"},
                         {"key":"p_user_id","value":"BOJMOB"}];
    kony.print("success response ::"+JSON.stringify(requestParams));

    call_CUSTOM_VERB("IPSRegistration",requestParams,
                     function(res){
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      kony.print("success response ::"+JSON.stringify(res));
      if(res.statusCode === "00000"){
        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.CLIQ.registeredsuccessfully"), geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard", geti18Value("i18n.CLIQ.GoToCliqDashboard"), "CliqDashboardManage", geti18Value("i18n.CLIQ.Share"), "CliqShareAlias", "");
      }else{
        var Message = getErrorMessage(res.statusCode);

        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), Message, geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard",geti18Value("i18n.CLIQ.GoToCliqDashboard"), "CliqDashboardManage", "", "", "");//Omar ALnajjar Add ALais Go to cliq dashboard
      }
      frmIPSCreateAlias.destroy();
    }, 
                     function(err){
      exceptionLogCall("CreateNewAlias","SERVICE FAILURE ","CreateNewAlias",error);
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      kony.print("Exception_serv_IPSAddAlias ::"+e);
    },"");
  }catch(e){
    exceptionLogCall("CreateNewAlias","SERVICE FAILURE ","CreateNewAlias",error);
    kony.print("Exception_serv_IPSAddAlias ::"+e);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }
  //

}
//Omar ALnajjar 21/03/2021 CLIQ Manage
function serv_IPS_REGISTRATION_STATUS(){
  try{
    var segData = [];
    call_CUSTOM_VERB("checkIPSRegistrationStatus",[{"key":"custId","value":custid}], function(res){
      kony.print("IPS Registration stauts callback ::"+JSON.stringify(res));
      if(res.statusCode === "00000"){
        if(res.isRegistered === "1" && res.hasOwnProperty("aliases")){
          //                 	frmEPS.flxMain.setVisibility(true);
          //                     frmEPS.flxConfirmEPS.setVisibility(false);
          //                     frmEPS.show();
          if (gblTModule==="getAlias")
          {
            if(res.aliases!==undefined)
            {
              frmGetIPSAlias.segGetAllIPSAliases.setData([]);

              frmGetIPSAlias.segGetAllIPSAliases.widgetDataMap = { 
                lblAliasTypeSeg: "lblAliasTypeSeg",
                lblAliasSeg :"lblAliasSeg",
                lblAliasIBANSeg:"lblAliasIBANSeg",
                aliasTypeSeg:"aliasTypeSeg",
                lblAliasTypeTitleSeg:"lblAliasTypeTitleSeg",
                lblAliasTitleSeg:"lblAliasTitleSeg",
                lblALiasIBANTitleSeg:"lblALiasIBANTitleSeg"

              };
              for(var aliass in res.aliases)
              {
                res.aliases[aliass].aliasTypeTitle =kony.i18n.getLocalizedString("i18n.CLIQ.AliasTypeIPS");
                res.aliases[aliass].aliasTitle =kony.i18n.getLocalizedString("i18n.jomopay.aliastype");
                res.aliases[aliass].aliasIBANTitle =kony.i18n.getLocalizedString("i18.CLIQ.IBAN");
                res.aliases[aliass].aliasTypeSeg= res.aliases[aliass].aliasType;
                res.aliases[aliass].lblAliasIBANSeg  ="";
                
                
                if (res.aliases[aliass].aliasType==="MOBL")
                  res.aliases[aliass].aliasType=kony.i18n.getLocalizedString("i18n.jomopay.mobiletype");
                else
                  res.aliases[aliass].aliasType=kony.i18n.getLocalizedString("i18n.jomopay.aliastype");
                
                segData.push({
                  lblAliasTypeSeg: {"text":res.aliases[aliass].aliasType},
                  lblAliasSeg :{"text":res.aliases[aliass].aliasValue},
                  lblAliasIBANSeg:{"text":res.aliases[aliass].lblAliasIBANSeg},

                  aliasTypeSeg:res.aliases[aliass].aliasTypeSeg,

                  lblALiasIBANTitleSeg:{"text":res.aliases[aliass].aliasIBANTitle},
                  lblAliasTitleSeg:{"text":res.aliases[aliass].aliasTitle},
                  lblAliasTypeTitleSeg:{"text":res.aliases[aliass].aliasTypeTitle}
                });
              }
              // alert(res.aliases);lblAliasTypeSeg


              frmGetIPSAlias.segGetAllIPSAliases.setData(segData);
            }
            frmGetIPSAlias.show();
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
          }
          else
            //serv_GET_IPS_BENEFICIARIES();
            getAllIPSBenficiaries();//OMar ALnajjar
          //frmIPSManageBene.show();
        }else
          frmIPSHome.show();
      }else{
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
      }
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    },function(err){
      kony.print("IPS Registration status service failure :"+JSON.stringify(err));
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    },"");
  }catch(e){
    kony.print("Exception_serv_IPS_REGISTRATION_STATUS ::"+e);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }
}

function serv_GET_IPS_BENEFICIARIES(){
  var requestParams = [{"key":"custId","value":custid},
                       {"key":"P_BENE_TYPE","value":"IPS"}];
  call_CUSTOM_VERB("get",requestParams,
                   function(res){
    kony.print("Success ::"+JSON.stringify(res));
    //show_IPS_BENEFICIARY_LIST(res.records[0].beneList);
    getAllIPSBenficiaries();//OMar ALnajjar Add Bene
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();  
  },
                   function(err){
    kony.print("error ::"+JSON.stringify(err));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  },"getBeneficiaries");
}

function call_CUSTOM_VERB(customVerb_SERV, requestParams, successCallBack, errorCallBack,flow){
  try{
    var model = "Accounts";
    if(flow === "getBeneficiaries")
      model = "ExternalAccounts";

    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel(model, serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject(model);

    for(var i in requestParams)
      dataObject.addField(requestParams[i].key, requestParams[i].value);
    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("Service Name ::"+customVerb_SERV+"\n"+"input Parameters ::"+JSON.stringify(dataObject));
    if (kony.sdk.isNetworkAvailable()) {
      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      modelObj.customVerb(customVerb_SERV, serviceOptions, successCallBack, errorCallBack);
    } else {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
  }catch(e){
    kony.print("Exception_call_CUSTOM_VERB ::"+e);
  }
}

//Created and edited by Arpan

kony = kony || {};
kony.boj = kony.boj || {};
kony.boj.selectedDetailsList = [];
kony.boj.selectedDetailsType = "";
var RetunrReason =[{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPWRAM"),"Code":"WRAM"},{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPUPKN"),"Code":"UPKN"},{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPUPFS"),"Code":"UPFS"} ,{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPMREF"),"Code":"MREF"}];
kony.boj.getBranchList = function(){
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options = {
    "access": "online",
    "objectName": "RBObjects"
  };
  var headers = {};
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("Payee", serviceName, options);
  var dataObject = new kony.sdk.dto.DataObject("ExternalAccounts"); 

  dataObject.addField("language",kony.store.getItem("langPrefObj").toLowerCase());
  dataObject.addField("types", "1");

  var serviceOptions = {"dataObject":dataObject,"headers":headers};
  kony.print("Input params GetBranchList-->"+ JSON.stringify(serviceOptions) );
  modelObj.customVerb("getBranchList",serviceOptions, kony.boj.BranchListSuccess, kony.boj.BranchListError);
};

kony.boj.BranchListSuccess = function(res){
  if(res.branchList === undefined)
    kony.boj.selectedDetailsList = sort_ALPHABETICALLY(kony.boj.detailsForBene.Branch, "BRANCH_NAME");
  else
    kony.boj.selectedDetailsList = sort_ALPHABETICALLY(res.branchList, "BRANCH_NAME");

  for(var i in kony.boj.selectedDetailsList){
    if(kony.boj.selectedDetailsList[i].icon === undefined){
      kony.boj.selectedDetailsList[i].icon = {
        backgroundColor: kony.boj.getBackGroundColour(i)
      };
    }
    else
      break;
  }
  kony.print("Success in BranchList :: " + JSON.stringify(kony.boj.selectedDetailsList));

//   kony.boj.selectedDetailsList.sort(SortByName);
//   function SortByName(x,y) {
//       return ((x.BRANCH_NAME == y.BRANCH_NAME) ? 0 : ((x.BRANCH_NAME > y.BRANCH_NAME) ? 1 : -1 ));
//     }
  if(gblFromModule === "updateBeneficiaryBranch" || gblFromModule === "addBeneficiaryAsFav" || isAddAsTrusted === true){
    var data = kony.store.getItem("data");
    var details = get_BRANCH_DETAILS(data.benBranchNo, kony.boj.selectedDetailsList);
    frmAddExternalAccountKA.tbxBankBranch.text = data.benBranchNo;
    if(!isEmpty(details)){
      frmAddExternalAccountKA.lblBankBranch.text = details.BRANCH_NAME;
      frmAddExternalAccountKA.lblBankBranchStat.setVisibility(true);
    }else{
    	//customAlertPopup(geti18Value("i18n.maps.Info"), "Unable to get branch details", popupCommonAlertDimiss, "");
      
    	for(var i in kony.boj.detailsForBene.Branch){
        	if(kony.boj.detailsForBene.Branch[i].BRANCH_CODE === data.benBranchNo){
            	frmAddExternalAccountKA.lblBankBranch.text = kony.boj.detailsForBene.Branch[i].BRANCH_NAME;
      			frmAddExternalAccountKA.lblBankBranchStat.setVisibility(true);
            	break;
            }
        }
    }
    frmAddExternalAccountKA.lblBankBranch.skin = "sknLblBack";
    gblFromModule = "updateBeneficiary";
  }else{
    frmSelectDetailsBene.segDetails.setData(kony.boj.selectedDetailsList);
     if(gblLaunchModeOBJ.lauchMode  && gblOnceSetBranch && (!isEmpty(gblLaunchModeOBJ.accno) || !isEmpty(gblLaunchModeOBJ.iban))){
    showAddBenfiWithPreFilledData();
       gblOnceSetBranch =false;
       return;
  }else{
    frmSelectDetailsBene.show();
  }
  }
  kony.store.setItem("data","");
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen("");
};

kony.boj.BranchListError = function(err){
  kony.boj.selectedDetailsList = kony.boj.detailsForBene.Branch;

  for(var i in kony.boj.selectedDetailsList){
    if(kony.boj.selectedDetailsList[i].icon === undefined){
      kony.boj.selectedDetailsList[i].icon = {
        backgroundColor: kony.boj.getBackGroundColour(i)
      };
    }
    else
      break;
  }
  kony.print("Failure in BranchList :: " + JSON.stringify(kony.boj.selectedDetailsList));

  frmSelectDetailsBene.segDetails.setData(kony.boj.selectedDetailsList);
  if(gblLaunchModeOBJ.lauchMode  && gblOnceSetBranch &&  (!isEmpty(gblLaunchModeOBJ.accno) || !isEmpty(gblLaunchModeOBJ.iban))){
    showAddBenfiWithPreFilledData();
    gblOnceSetBranch =false;
    return;
  }else{
  frmSelectDetailsBene.show();
   }
   kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen("");
  
};

kony.boj.showSelectedDetailsFromBene = function(detailType, lblName, countryCode){
  kony.boj.detailsForBene.lblToUpdate = lblName;
  kony.boj.detailsForBene.flag = true;
  kony.boj.selectedDetailsType = detailType;
  frmSelectDetailsBene.segDetails.setData([]);

  var ctrydesc = "CTRY_S_DESC"; 
  if(kony.boj.lang === "eng")
    ctrydesc = "CTRY_S_DESC";
  else
    ctrydesc = "CTRY_B_DESC";

  if(detailType === "Country"){
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      lblData: ctrydesc,
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };
	if((kony.application.getCurrentForm().id === "frmAddExternalAccountKA") && (frmAddExternalAccountKA.btnInternational.skin === "slButtonWhiteTab") && (lblName !== "lblCountryNameKA")){
    	kony.boj.selectedDetailsList = get_COUNTRY_LIST_WITHOUT_SPECIFIC_COUNTRY([{"countryName":"JORDAN"}]);
    	kony.boj.selectedDetailsList = sort_ALPHABETICALLY(kony.boj.selectedDetailsList, ctrydesc);
    }else{
    	kony.boj.selectedDetailsList = sort_ALPHABETICALLY(kony.boj.detailsForBene.Country, ctrydesc);
    }
    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i][ctrydesc] !== undefined && kony.boj.selectedDetailsList[i][ctrydesc] !== ""){
      	kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i][ctrydesc].substring(0, 2).toUpperCase();
      }else{
      	kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CTRY_S_DESC.substring(0, 2).toUpperCase();
      }
    }
  }
  else if(detailType === "City"){
    if(kony.boj.lang === "eng")
      ctrydesc = "CITY_S_DESC";
  	else
      ctrydesc = "CITY_B_DESC";
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      lblData: ctrydesc,
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    kony.boj.selectedDetailsList = sort_ALPHABETICALLY(kony.boj.detailsForBene[detailType][countryCode], ctrydesc);
    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i][ctrydesc] !== undefined && kony.boj.selectedDetailsList[i][ctrydesc] !== ""){
      	kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i][ctrydesc].substring(0, 2).toUpperCase();
      }else{
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CITY_B_DESC.substring(0, 2).toUpperCase();
      }
    }
  }
  else if(detailType === "Branch"){
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      lblData: "BRANCH_NAME",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };
    if(gblLaunchModeOBJ.lauchMode  && (!isEmpty(gblLaunchModeOBJ.accno) || !isEmpty(gblLaunchModeOBJ.iban)) && frmAddExternalAccountKA.lblBankBranch.text != kony.i18n.getLocalizedString("i18n.Bene.BankBranch")){
      kony.print("Don nothing:");
      }
    else{
    kony.boj.getBranchList();
    }
    return;
    //kony.boj.selectedDetailsList = kony.boj.detailsForBene.Branch;
  }
  else if(detailType === "BankDetail"){
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      lblData: "BankName",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };
    
    var bankList = [];
    if(kony.application.getCurrentForm().id === "frmAddExternalAccountKA" && kony.boj.addBeneVar.selectedType === "DOM"){
  		for(var p in kony.boj.detailsForBene.BankDetail){
        	if(kony.boj.detailsForBene.BankDetail[p].SwiftCode !== "BJORJOAX")
              bankList.push(kony.boj.detailsForBene.BankDetail[p]);
        }
	}else{
    	bankList = kony.boj.detailsForBene.BankDetail;
    }
    
    kony.boj.selectedDetailsList = sort_ALPHABETICALLY(bankList, "BankName");
  }
  /*hassan OpenDeposite*/
  else if(detailType === "MaturityInstruction"){
    
        var matTypes=[];
        var matArray=[];
     if ((frmOpenTermDeposit.btnTener3Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener6Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener1Year.skin === "sknOrangeBGRNDBOJ") && 
        frmOpenTermDeposit.btnMonthly.skin === "sknOrangeBGRNDBOJ"){
           matTypes=[{"Maturity":geti18Value("i18n.termDeposit.renWithOutInterest")},{"Maturity":geti18Value("i18n.termDeposit.close")}];
    }else{
           matTypes=[{"Maturity":geti18Value("i18n.termDeposit.renWithInterest")},{"Maturity":geti18Value("i18n.termDeposit.renWithOutInterest")},{"Maturity":geti18Value("i18n.termDeposit.close")}];

    }

	frmSelectDetailsBene.lblTitle.text = geti18Value("i18n.termDeposit.renewalInstruction");    
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      lblData: "Maturity",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    kony.boj.selectedDetailsList=matTypes;
    
    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i].Maturity !== undefined && kony.boj.selectedDetailsList[i].Maturity !== ""){
      	kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].Maturity.substring(0, 2).toUpperCase();
      }else{
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CITY_B_DESC.substring(0, 2).toUpperCase();
      }
    }
  }
  /*hassan OpenDeposite*/
//Mai 11/1/2021
  
    else if(detailType === "ReasonReturn"){
  
    var Reason=[{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPWRAM"),"Code":"WRAM"},{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPUPKN"),"Code":"UPKN"},{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPUPFS"),"Code":"UPFS"} ,{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPMREF"),"Code":"MREF"}];
   // var matArray=[];
	frmSelectDetailsBene.lblTitle.text = geti18Value("i18n.Cliq.ReturnPaymentReason");    
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      //Omar Alnajjar 
      lblData: 
      //{
       "ReasonReturn",
       // skin:"sknLblWhike95"
     // },
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    kony.boj.selectedDetailsList=Reason;
    
    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i].ReasonReturn !== undefined && kony.boj.selectedDetailsList[i].ReasonReturn !== ""){
      	kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].ReasonReturn.substring(0, 2).toUpperCase();
      }else{
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CITY_B_DESC.substring(0, 2).toUpperCase();
      }
    }
  }
  
  //Omar ALnajjar 23/02/2021 R2P
   else if(detailType === "ReasonRequest"){
  
    var requestReasons=[{"ReasonRequest":geti18Value("i18n.CLIQ.RentalExpenses"),"Code":"RERE"},{"ReasonRequest":geti18Value("i18n.CLIQ.FamilyAssistance"),"Code":"FAAE"},{"ReasonRequest":geti18Value("i18n.CLIQ.EducationalServices"),"Code":"EDS"} ,{"ReasonRequest":geti18Value("i18n.CLIQ.MaintenanceTechnical"),"Code":"MTOS"},{"ReasonRequest":geti18Value("i18n.CLIQ.Transfertofriend"),"Code":"TOFF"},{"ReasonRequest":geti18Value("i18n.CLIQ.transferaccounts"),"Code":"TBAA"},{"ReasonRequest":geti18Value("i18n.CLIQ.transportationfees"),"Code":"TFTF"},{"ReasonRequest":geti18Value("i18n.CLIQ.travelfees"),"Code":"TTFF"},{"ReasonRequest":geti18Value("i18n.CLIQ.InvoicePayment"),"Code":"IPAP"},{"ReasonRequest":geti18Value("i18n.CLIQ.Governmentservices"),"Code":"GSPS"},{"ReasonRequest":geti18Value("i18n.FilterTransaction.Others"),"Code":"OSOS"}];
   // var matArray=[];
	frmSelectDetailsBene.lblTitle.text = geti18Value("i18n.CLIQ.RTPAcsRejReason");    
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      //Omar Alnajjar 
      lblData: 
      //{
       "ReasonRequest",
       // skin:"sknLblWhike95"
     // },
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };
    kony.boj.selectedDetailsList=requestReasons;
    	kony.print(JSON.stringify(kony.boj.selectedDetailsList[0]));

    for(var i in kony.boj.selectedDetailsList){

      if(kony.boj.selectedDetailsList[i].ReasonRequest !== undefined && kony.boj.selectedDetailsList[i].ReasonRequest !== ""){
      	kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].ReasonRequest.substring(0, 2).toUpperCase();
      }else{
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CITY_B_DESC.substring(0, 2).toUpperCase();
      }
    }
  }
  //omar alnajjar 21/03/2021 RTP
  else if(detailType === "RTPAcsRejReasons"){
  
    var RTPAcsRejReasons=[{"RTPAcsRejReasons":geti18Value("i18n.CLIQ.ReasonRTPWRAM"),"Code":"WRAM"},{"RTPAcsRejReasons":geti18Value("i18n.CLIQ.ReasonRTPUNRE"),"Code":"UNRE"},{"RTPAcsRejReasons":geti18Value("i18n.CLIQ.ReasonRTPAMPA"),"Code":"AMPA"}];
   // var matArray=[];
	frmSelectDetailsBene.lblTitle.text = geti18Value("i18n.CLIQ.RTPAcsRejReason");    
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      //Omar Alnajjar 
      lblData: 
      //{
       "RTPAcsRejReasons",
       // skin:"sknLblWhike95"
     // },
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    kony.boj.selectedDetailsList=RTPAcsRejReasons;
    
    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i].RTPAcsRejReasons !== undefined && kony.boj.selectedDetailsList[i].RTPAcsRejReasons !== ""){
      	kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].RTPAcsRejReasons.substring(0, 2).toUpperCase();
      }else{
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CITY_B_DESC.substring(0, 2).toUpperCase();
      }
    }
  }
  
//mai  
  for(var i in kony.boj.selectedDetailsList){
    if(kony.boj.selectedDetailsList[i].icon === undefined){
      kony.boj.selectedDetailsList[i].icon = {
        backgroundColor: kony.boj.getBackGroundColour(i)
      };
    }
    else
      break;
  }
  
  frmSelectDetailsBene.segDetails.setData(kony.boj.selectedDetailsList);
  frmSelectDetailsBene.show();
};

kony.boj.updateLabel = function(selectedItem){
  if(kony.boj.detailsForBene.lblToUpdate === "lblCountryNameKA"){
    frmAddExternalAccountKA.lblCityBank.text = geti18Value("i18n.manage_payee.cityPlh");
    frmAddExternalAccountKA.lblCityBank.skin = "sknLblNextDisabled";

    frmAddExternalAccountKA.lblCityName.text = geti18Value("i18n.manage_payee.cityPlh");
    frmAddExternalAccountKA.lblCityName.skin = "sknLblNextDisabled";

    frmAddExternalAccountKA.flxUnderlineCountryName.skin =  "sknFlxGreenLine";

    frmAddExternalAccountKA.tbxCountry.text = selectedItem.CTRY_CODE;
    var ctrydesc = "CTRY_S_DESC"; 
    if(kony.boj.lang === "eng")
      ctrydesc = "CTRY_S_DESC";
    else
      ctrydesc = "CTRY_B_DESC";
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].text = selectedItem[ctrydesc];
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].skin = "sknLblBack";
    //animateLabel("UP", "lblCountryTitleStat", frmAddExternalAccountKA.lblCountryNameKA.text, frmAddExternalAccountKA);
    frmAddExternalAccountKA.lblCountryTitleStat.setVisibility(true);
          		
        		
   }
  else if(kony.boj.detailsForBene.lblToUpdate === "lblCityName"){
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].text = selectedItem.CITY_B_DESC;
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].skin = "sknLblBack";
    frmAddExternalAccountKA.tbxCity.text = selectedItem.CITY_CODE;
    frmAddExternalAccountKA.flxUnderlineCityName.skin =  "sknFlxGreenLine";
  }
  else if(kony.boj.detailsForBene.lblToUpdate === "lblCityBank"){
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].text = selectedItem.CITY_B_DESC;
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].skin = "sknLblBack";
    frmAddExternalAccountKA.tbxCityBank.text = selectedItem.CITY_CODE;
    frmAddExternalAccountKA.flxUnderlineCityBank.skin =  "sknFlxGreenLine";
  }
  else if(kony.boj.detailsForBene.lblToUpdate === "lblBankName"){
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].text = selectedItem.BankName;
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].skin = "sknLblBack";
    frmAddExternalAccountKA.tbxBankNameInt.text = selectedItem.BankName;
    frmAddExternalAccountKA.txtSwiftCodeKA.text = selectedItem.SwiftCode;
    frmAddExternalAccountKA.flxUnderlineBankName.skin =  "sknFlxGreenLine";
    frmAddExternalAccountKA.lblBankNameStat.setVisibility(true);
    //animateLabel("UP", "lblBankNameStat", frmAddExternalAccountKA.lblBankName.text,frmAddExternalAccountKA);
  }
  else if(kony.boj.detailsForBene.lblToUpdate === "lblBankBranch"){
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].text = selectedItem.BRANCH_NAME;
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].skin = "sknLblBack";
    frmAddExternalAccountKA.tbxBankBranch.text = selectedItem.BRANCH_CODE;
    frmAddExternalAccountKA.flxUnderlineBankBranch.skin =  "sknFlxGreenLine";
    frmAddExternalAccountKA.lblBankBranchStat.setVisibility(true);
   // animateLabel("UP", "lblBankBranchStat", frmAddExternalAccountKA.lblBankBranch.text,frmAddExternalAccountKA);
  }
  else if(kony.boj.detailsForBene.lblToUpdate === "lblCountryBankDetails"){
 	frmAddExternalAccountKA.txtCountryBankDetails.text = selectedItem.CTRY_CODE;
    var ctrydesc = "CTRY_S_DESC"; 
    if(kony.boj.lang === "eng")
      ctrydesc = "CTRY_S_DESC";
    else
      ctrydesc = "CTRY_B_DESC";
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].text = selectedItem[ctrydesc];
    frmAddExternalAccountKA[kony.boj.detailsForBene.lblToUpdate].skin = "sknLblBack";
    frmAddExternalAccountKA.lblCountryBankDetailsUnderLine.skin =  "sknFlxGreenLine";
    frmAddExternalAccountKA.lblBankCountryStat.setVisibility(true);
    //animateLabel("UP", "lblBankCountryStat", frmAddExternalAccountKA.lblCountryBankDetails.text,frmAddExternalAccountKA);
  }

  kony.boj.addBeneNextStatus();
  frmAddExternalAccountKA.show();
};

kony.boj.searchDetailsBene = function(searchText){
  frmSelectDetailsBene.segDetails.setData([]);
  searchText = searchText.toLowerCase();
  var filteredData = [];

  var ctrydesc = "CTRY_S_DESC"; 
  if(kony.boj.lang === "eng")
    ctrydesc = "CTRY_S_DESC";
  else
    ctrydesc = "CTRY_B_DESC";

  if(kony.boj.selectedDetailsType === "Country"){
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      lblData: ctrydesc,
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i][ctrydesc].toLowerCase().indexOf(searchText) !== -1 || 
         kony.boj.selectedDetailsList[i].INITIAL.toLowerCase().indexOf(searchText) !== -1)
        filteredData.push(kony.boj.selectedDetailsList[i]);
    }
    frmSelectDetailsBene.segDetails.setData(filteredData);
  }
  else if(kony.boj.selectedDetailsType === "City"){
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      lblData: "CITY_B_DESC",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };
    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i].CITY_B_DESC.toLowerCase().indexOf(searchText) !== -1)
        filteredData.push(kony.boj.selectedDetailsList[i]);
    }
    frmSelectDetailsBene.segDetails.setData(filteredData);
  }
  else if(kony.boj.selectedDetailsType === "Branch"){
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      lblData: "BRANCH_NAME",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i].BRANCH_NAME.toLowerCase().indexOf(searchText) !== -1)
        filteredData.push(kony.boj.selectedDetailsList[i]);
    }
    frmSelectDetailsBene.segDetails.setData(filteredData);
  }
  else if(kony.boj.selectedDetailsType === "BankDetail"){
    frmSelectDetailsBene.segDetails.widgetDataMap = { 
      lblData: "BankName",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i].BankName.toLowerCase().indexOf(searchText) !== -1)
        filteredData.push(kony.boj.selectedDetailsList[i]);
    }
    frmSelectDetailsBene.segDetails.setData(filteredData);
  }
};

function sort_ALPHABETICALLY(object, field){
	try{
    	object.sort(SortByName);
        function SortByName(x,y) {
          return ((x[field] == y[field]) ? 0 : ((x[field] > y[field]) ? 1 : -1 ));
        };
    	return object;
    }catch(e){
    	kony.print("Exception_sort_ALPHABETICALLY ::"+e);
    }
}
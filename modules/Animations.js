//Created and edited by Arpan

function animateLabel(scale, labelWidgetName, tbxWidgetText, currentForm)
{
  try{
    var langSelected1 = kony.store.getItem("langPrefObj");
    var langSelected = langSelected1.toUpperCase();
    var form = null;
    if(currentForm !== null && currentForm !== undefined && currentForm !== "")
    form = currentForm;
    else
    form = kony.application.getCurrentForm();
    var trans100 = kony.ui.makeAffineTransform();
    var leftV = "2%";
    var rightV="2%";
    var duration = 0.5;
    var step100Config,step0Config;
if(langSelected==="AR")
      {
          if(scale == "UP"){
      top = "15%";

      trans100.scale(0.75, 0.75);

      step0Config = {
        "stepConfig": {
          "timingFunction": kony.anim.EASE
        },
        "top":"40%",
        "right": "2%",
      };
      //kony.print("ttext"+form[labelWidgetName].text);
      if(!isEmpty(form[labelWidgetName].text) && form[labelWidgetName].text.length >15){
        //kony.print("ttext11"+form[labelWidgetName].text.length);
        if(form.id === "frmEPS" && labelWidgetName === "lblIBANBeneName"){
          rightV = "-3%";

        }
       else if(form.id === "frmEPS" && labelWidgetName === "lblBeneNameIBANTitle"){
          rightV = "-3%";
        }else if(form.id === "frmRegisterUser" && labelWidgetName === "lblIDExpiryDatelblIDExpiryDate"){
          rightV = "-3%";
        }else if(form.id === "frmRegisterUser" && labelWidgetName === "lblIDCardNumberTitle"){
          rightV = "-3%";
        }else if(form.id === "frmEPS" && labelWidgetName === "lblAliasName"){
          rightV = "-3%";
        }else if(form.id === "frmAddExternalAccountKA" && labelWidgetName === "lblSwiftCode"){
          rightV = "-13%";
        }else if(form.id === "frmAddExternalAccountKA" && labelWidgetName === "lblAccountNumber"){
          if(form[labelWidgetName].text.length >32){
            rightV = "-18%";
          }else
            rightV = "-11%";
        }else rightV = "-1%";
      }else{
        
        if(form.id === "frmEPS" && labelWidgetName === "lblIBANBeneName"){
          rightV = "-4%";
        }else if(form.id === "frmNewTransferKA" && labelWidgetName === "lblDescription"){
          rightV = "-1%";
        }else rightV = "-3%";
        
      }    
      step100Config = {
        "transform": trans100,
        "anchorPoint": {
          "x": 0,
          "y": 0
        },
        "top": top,
        //#ifdef android
        "right": rightV,
        //#endif
        //#ifdef iphone
        "right": "2%",
        //#endif
        "stepConfig": {
          "timingFunction": kony.anim.EASE
        }
      };

      form[labelWidgetName].animate(
        kony.ui.createAnimation({
          //"0":step0Config,
          "100": step100Config
        }), {
          "delay": 0,
          "iterationCount": 1,
          "fillMode": kony.anim.FILL_MODE_FORWARDS,
          "duration": duration
        }, {
          "animationEnd" : null
        });
    }
    else if(scale == "DOWN"){
      trans100.scale(1, 1);

      step0Config = {
        "top": top,
        //#ifdef android
        "right": "-1%",
        //#endif
        //#ifdef iphone
        "right": "2%",
        //#endif
        "stepConfig": {
          "timingFunction": kony.anim.EASE
        }
      };

      step100Config = {
        "stepConfig": {
          "timingFunction": kony.anim.EASE
        },
        "transform": trans100,
        "top":"40%",
        "right": "2%",
      };
      if(tbxWidgetText!== null && tbxWidgetText !==""){
      }else{
        form[labelWidgetName].animate(
          kony.ui.createAnimation({
            //"0":step0Config,
            "100": step100Config
          }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": duration
          }, {
            "animationEnd": null
          });
      }}
      }
    else{
    if(scale == "UP"){
      top = "15%";

      trans100.scale(0.75, 0.75);

      step0Config = {
        "stepConfig": {
          "timingFunction": kony.anim.EASE
        },
        "top":"40%",
        "left": "2%",
      };
      //kony.print("ttext"+form[labelWidgetName].text);
      if(!isEmpty(form[labelWidgetName].text) && form[labelWidgetName].text.length >15){
        //kony.print("ttext11"+form[labelWidgetName].text.length);
        leftV = "-1%";
        if(form.id === "frmEPS" && labelWidgetName === "lblIBANBeneName")
          leftV = "2%";  
        if(form.id === "frmAddExternalAccountKA" && labelWidgetName === "lblSwiftCode")
        leftV = "-1%";
          //Nart Rawashdeh BMA-2284 07/06/2021 <<
          if((form.id === "frmEnableInternetTransactionKA" && labelWidgetName === "lblEcom") || (form.id === "frmEnableInternetTransactionKA" && labelWidgetName === "lblMailOrder"))
            leftV = "1%";
          //>>
      }else{
        if(form.id === "frmEPS" && labelWidgetName === "lblAddress")
          leftV = "2%";        
        else if(form.id === "frmEPS" && labelWidgetName === "lblIBANAlias")
          leftV = "2%";  
        else if(form.id === "frmNewTransferKA" && labelWidgetName === "lblDescription")
          leftV = "2%";  
        else leftV = "-1%";   
      }
      step100Config = {
        "transform": trans100,
        "anchorPoint": {
          "x": 0,
          "y": 0
        },
        "top": top,
        //#ifdef android
        "left": leftV,
        //#endif
        //#ifdef iphone
        "left": "2%",
        //#endif
        "stepConfig": {
          "timingFunction": kony.anim.EASE
        }
      };

      form[labelWidgetName].animate(
        kony.ui.createAnimation({
          //"0":step0Config,
          "100": step100Config
        }), {
          "delay": 0,
          "iterationCount": 1,
          "fillMode": kony.anim.FILL_MODE_FORWARDS,
          "duration": duration
        }, {
          "animationEnd" : null
        });
    }
    else if(scale == "DOWN"){
      trans100.scale(1, 1);

      step0Config = {
        "top": top,
        //#ifdef android
        "left": "-1%",
        //#endif
        //#ifdef iphone
        "left": "2%",
        //#endif
        "stepConfig": {
          "timingFunction": kony.anim.EASE
        }
      };

      step100Config = {
        "stepConfig": {
          "timingFunction": kony.anim.EASE
        },
        "transform": trans100,
        "top":"40%",
        "left": "2%",
      };
      if(tbxWidgetText!== null && tbxWidgetText !==""){
      }else{
        form[labelWidgetName].animate(
          kony.ui.createAnimation({
            //"0":step0Config,
            "100": step100Config
          }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": duration
          }, {
            "animationEnd": null
          });
      }}}
  }catch(error){
    kony.print("Error in animation: "+error);

  }    
}

function animatePopup_Sort(){
  try{

    var top = "";
    var currentform = kony.application.getCurrentForm();
    if(currentform.id === "frmManageCardsKA" || currentform.id === "frmCardsLandingKA"){
      if(currentform.flxAccountdetailsSortContainer.top == "0%")
        top = "-99%";
      else
        top = "0%";
    }else{
      if(currentform.flxAccountdetailsSortContainer.top == "100%")
        top = "8%";
      else
        top = "100%";
    }
    kony.print("animatePopup_Sort_top ::"+top,+" :: currentform.id ::"+currentform.id);
    var animationObj = kony.ui.createAnimation({"100":{"top":top,"stepConfig":{"timingFunction":kony.anim.LINEAR}}});
    currentform.flxAccountdetailsSortContainer.animate(animationObj, {
      "delay": 0,
      "iterationCount": 1,
      "fillMode": kony.anim.FILL_MODE_FORWARDS,
      "duration": 0.5
    }, null);
  }catch(e){
    kony.print("Exception_animatepopup ::"+e);
  }
}

function animate_cardsFilterOption(){
  try{
    var top = "";
    if(frmCardStatementKA.flxCardsSegmentOption.top == "100%")
      top = "8%";
    else top = "100%";
    var animationObj = kony.ui.createAnimation({"100":{"top":top,"stepConfig":{"timingFunction":kony.anim.LINEAR}}});
    frmCardStatementKA.flxCardsSegmentOption.animate(animationObj, {
      "delay": 0,
      "iterationCount": 1,
      "fillMode": kony.anim.FILL_MODE_FORWARDS,
      "duration": 0.5
    }, null);
  }catch(e){
    kony.print("Exception_animate_cardsFilterOption ::"+e);
  }
}

function animate_NEWCARDSOPTIOS(scale, labelWidgetName, tbxWidgetText, currentForm){
try{
      var form = null;
      var trans100 = kony.ui.makeAffineTransform();
      if(currentForm !== null && currentForm !== undefined && currentForm !== "")
        form = currentForm;
      else
        form = kony.application.getCurrentForm();
      var step100Config = "",left,top;
      if(scale === "UP"){
        trans100.scale(0.75, 0.75);
        if(form.id === "frmJOMOPayAddBiller"){
        left = "-5%";
        top = "28%";
        }else{
          //#ifdef android
          left = "-10%";
          //#endif
          //#ifdef iphone
          left = "-10%";
          //#endif
          top = "20%";
          if(labelWidgetName === "lblJomopayIDstatic"){
          left = "-12%";
            //#ifdef iphone
            left = "-10%";
            //#endif
          }else if(labelWidgetName === "lblstaticdescription")
            left = "-9%";
        }
      }else if(scale === "DOWN"){
        trans100.scale(1, 1);
        top = "35%";
        left = "0%";
      }
      step100Config = {
        "top": top,
      "left":left,
        "transform": trans100,
        "right":null,
        "stepConfig": {
          "timingFunction": kony.anim.EASE
        }
      };
      if(kony.store.getItem("langPrefObj") === "ar"){
      step100Config.left = null;
        step100Config.right = left;
      }
      if(tbxWidgetText !== "" && tbxWidgetText !== null && scale === "DOWN"){
      return;
      }else{
        form[labelWidgetName].animate(kony.ui.createAnimation({"100":step100Config}),
                                      {"delay": 0,
                                       "iterationCount": 1,
                                       "fillMode": kony.anim.FILL_MODE_FORWARDS,
                                       "duration": "1"
                                      }, {
          "animationEnd": null
        });
      }
    }catch(e){
    kony.print("Exception_animate_NEWCARDSOPTIOS ::"+e);
    }
}

function animate_GETPIN(index){
try{
    frmPinManagement.lblPin.animate(kony.ui.createAnimation({
          "0":{"opacity":"0%","stepConfig":{"timingFunction":kony.anim.EASE}},
          "100":{"opacity":"100%","stepConfig":{"timingFunction":kony.anim.EASE}}
        }),
                                       {"delay":0,
                                       "iterationCount":1,
                                       "fillMode":kony.anim.FILL_MODE_FORWARDS,
                                       "duration":"1"},
                                       {"animationEnd":function(){frmPinManagement.lblPin.text="*";affine_ANIMATION_GETPIN(((index+1)*20)+"%",index);}});
    }catch(e){
    kony.print("Exception_animate_GETPIN ::"+e);
    }
}

function affine_ANIMATION_GETPIN(centerx, index){
try{
        frmPinManagement.lblPin.animate(kony.ui.createAnimation({
          "100":{"centerX":centerx,"centerY":"35%","stepConfig":{"timingFunction":kony.anim.EASE}}
        }),
                                       {"delay":0,
                                       "iterationCount":1,
                                       "fillMode":kony.anim.FILL_MODE_FORWARDS,
                                       "duration":"1"},
                                       {
              "animationEnd":function(){frmPinManagement.lblPin.setVisibility(false);
                                                                  frmPinManagement["lblPin"+(index+1)].setVisibility(true);
                                                           frmPinManagement.lblPin.centerX = "50%";
                                                                  frmPinManagement.lblPin.centerY = "20%";
                                                     data_GETPIN(index+1);}});
    }catch(e){
    kony.print("Excption_affine_ANIMATION ::"+e);
    }
}
///////////


var gblLatitude,gbLlongitude,gblCurrentLocator,gblIsCurrentLocation,gblLocationData,gblLocationDataATM,gblLocationDataBranch,gblSelectionPinData,gblDirectionsData,gblFilterType,gblRoute,gblFormName,gblSearch;
var gblHqLat,gblHqLon;
var globalZoomLevel = 4;
/**
* On Click of LocateUs,gets CurrentLocation
*/

function onClickLocateUS(isLoggedIn,formName){
  kony.print("perf log for map button click -- Start");
  ShowLoadingScreen();
  gblFormName = formName;
  globalZoomLevel = 4;
  var positionoptions = {timeout:32000,fastestInterval:0,minimumTime : 0};
  kony.location.getCurrentPosition(geoLocationSuccessCallBack,geoLocationErrorCallBack,positionoptions);
  kony.retailbanking.AdvancedAtmFilter.isFilterApplied = false;
  /**
      * SuccessCallBack of  CurrentLocation
      */
  function geoLocationSuccessCallBack(response){
    try{
      gblIsCurrentLocation =true;
      frmLocatorKA.locatorSearchTextField.text = "";
      if(response && response.coords && response.coords.latitude && response.coords.longitude){
        gblLatitude =response.coords.latitude;
        gbLlongitude =response.coords.longitude; 
      } 
      if(kony.sdk.isNetworkAvailable()){
        kony.print("geoLocationSuccessCallBack gblLatitude::"+gblLatitude+", gbLlongitude::"+gbLlongitude);
        var options = {"access":"online"};
        objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
        var dataObject = new kony.sdk.dto.DataObject("Locations");
        dataObject.addField("latitude",gblLatitude);
        dataObject.addField("longitude",gbLlongitude);
        var serviceOptions = {"dataObject":dataObject};
        objectService.customVerb('get',serviceOptions, setDataMapCallBack,errorCallBack);
      }else{
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  		customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
      }
    }
    catch(e){
      customErrorCallback(e);
      kony.application.dismissLoadingScreen();
    }
  }     
  /**
      * ErrorCallBack of  CurrentLocation
      */
  function geoLocationErrorCallBack(err){
    kony.print("@@geoLocationErrorCallBack:::");
    kony.application.dismissLoadingScreen();
    gblIsCurrentLocation =false;
    gblLatitude ="";
    gbLlongitude ="";
    kony.print("geoLocationErrorCallBack::: gblLatitude: "+gblLatitude+", gbLlongitude: "+gbLlongitude);
    var options = {"access":"online"};
    objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
    var dataObject = new kony.sdk.dto.DataObject("Locations");
    dataObject.addField("latitude",gblLatitude);
    dataObject.addField("longitude",gbLlongitude);
    var serviceOptions = {"dataObject":dataObject};//,"queryParams":queryParams};
    objectService.customVerb('get',serviceOptions, setDataMapCallBack,errorCallBack);
    var errorMsg = "Error code: " + err.code;
    errorMsg = errorMsg  + " message: " + err.message;
    kony.print("errorMsg::: "+errorMsg);
    frmLocatorKA.locatorSearchTextField.text = "";

  }
}
/**
* ErrorCallBack of  ServiceCall of getLocations
*/
function errorCallBack(err){
  var msg = geti18nkey("i18n.Generic.InternalServerError");
  if( err.errmsg!= null && err.errmsg!= undefined && err.errmsg != "")
    msg = err.errmsg; // for manual service calls
  kony.application.dismissLoadingScreen();
  customAlertPopup(geti18nkey("i18n.NUO.Error"), msg, closeStopCardPopup,"", geti18Value("i18n.NUO.OKay"),"");
}

/**
* SuccesCallBack of  ServiceCall of getLocations
*/
function setDataMapCallBack(response){
  kony.print("Perf log for map setDataMapCallBack ::"+JSON.stringify(response));
  try{
    ShowLoadingScreen();
    var list = [];
    if(response){
      list = response["records"];//.Locations;var gblHqLat,gblHqLon;
      if(!isEmpty(response.records[0].currLatitude)){
        gblLatitude = response.records[0].currLatitude;
        gblHqLat = response.records[0].currLatitude;
      }
      if(!isEmpty(response.records[0].currLongitude)){
        gbLlongitude = response.records[0].currLongitude;
        gblHqLon = response.records[0].currLongitude;
      }
     kony.print("Current Lat n Lon:: "+gblLatitude+", "+gbLlongitude);
    }
    if(list === undefined || list === null || list.length === 0 || Object.keys(list[0]).length === 0){
      kony.application.dismissLoadingScreen();
      toastMsg.showToastMsg("No results found",6000);
      frmLocatorKA.locatorSearchTextField.text = "";
      return;
    }
    var locationList = [];
    if(response){
      locationList = response["records"][0];
    }
    if(gblIsCurrentLocation == true){
      //kept outside for loop because in case of random string search ,current location button should work
      gblCurrentLocator = {
        				   id : "id1",
        				   lat: response.records[0].currLatitude, 
                           lon: response.records[0].currLongitude,
                           image:"current_location.png",
                           showcallout: false
                          };
      frmLocatorKA.locatorMap.locationData = gblCurrentLocator;
      kony.print("gblIsCurrentLocation: "+gblIsCurrentLocation+"lat: "+response.records[0].currLatitude+", lon: "+response.records[0].currLatitude);
      frmLocatorKA.mapCurrentLocationWrapper.setVisibility(true);
    }else{
      gblCurrentLocator = { 
        					id : "id1",
        				    lat: gblLatitude, 
                            lon: gbLlongitude,
                            image:"branch_icon_hq.png",//"current_location.png",
                            showcallout: true
                          };
      kony.print("gblIsCurrentLocation: "+gblIsCurrentLocation+"lat: "+gblLatitude+", lon: "+gbLlongitude);
      frmLocatorKA.mapCurrentLocationWrapper.setVisibility(false);
    }
    frmLocatorKA.locatorListView.setVisibility(false);
    frmLocatorKA.locatorMapView.setVisibility(true);
    kony.print("map n list visibility");
    gblFilterType="BOTH";
    frmLocatorKA.btnBothKA.skin = "sknaccountFilterButtonEnable";
    frmLocatorKA.btnATMKA.skin = "sknaccountFilterButtonDisabled";
    frmLocatorKA.btnBranchKA.skin = "sknaccountFilterButtonDisabled";
    kony.print("@@setDataMapCallBack :::"+JSON.stringify(locationList));
    settingMapListView(locationList) ;
  } 
  catch(e){
    kony.print(e);
   customErrorCallback(e);
      kony.application.dismissLoadingScreen();
  } 
}
/**
* Iteration of Locations and Filtered
*/
function settingMapListView(locationList){
  try{
    kony.print("Inside settingMapListView locationList"+JSON.stringify(locationList));
    var tempLocaterData=[];
    var locatorResultSet=[];
    var i,j,count=0;
    var data = "";
    gblLocationData = [];gblLocationDataATM = [];gblLocationDataBranch = [];
    if(!isEmpty(locationList)){
      if(locationList["atmList"].length>0){
        gblLocationDataATM = getLocationList(locationList["atmList"]);
        for(j=0;j<gblLocationDataATM.length;j++)
        {
          if(!isEmpty(gblLocationDataATM[j])){
            kony.print("gblLocationDataATM::: "+gblLocationDataATM[j]);
            gblLocationData[j]=gblLocationDataATM[j];
            count++;
          }
        }
      }
      if(locationList["branchList"].length>0){
        gblLocationDataBranch = getLocationList(locationList["branchList"]);
        for(i=0;i<gblLocationDataBranch.length;i++)
        {
          if(!isEmpty(gblLocationDataBranch[i])){
            gblLocationData[count]=gblLocationDataBranch[i];
            kony.print("gblLocationDataBranch::: "+gblLocationDataBranch[j]);            
            count++;
          }
        }
      }
      kony.print("gblLocationData2:::"+JSON.stringify(gblLocationData));
    }
    kony.print("gblLocationData after setting gblCurrentLocator:::"+JSON.stringify(gblLocationData));
    kony.print("gblLocationDataBranch:: "+JSON.stringify(gblLocationDataBranch));
    kony.print("gblLocationDataATM:: "+JSON.stringify(gblLocationDataATM));
    kony.print("gblLocationData:: "+JSON.stringify(gblLocationData));
    kony.print("gblLocationData.length:: "+gblLocationData.length);
    frmLocatorKA.locatorMap.locationData=[];
    settingData(gblLocationData); 
  }
  catch(e){
   customErrorCallback(e);
      kony.application.dismissLoadingScreen();
  }
}
/**
* CallBack of LocateUs Button.frmLocatorKA show
*/
function locationFormNavigation(){
  kony.application.dismissLoadingScreen();
/*  frmLocatorKA.noSearchResults.isVisible = false;
  frmLocatorKA.locatorSegmentList.top = "0dp";
  frmLocatorKA.locatorSegmentList.opacity = 1;
  if(menuIconForceTouch){
    menuIconForceTouch = false;
  }
  else{*/
    frmLocatorKA.show(); 
    kony.print("perf log for map button click -- End");
//  } 

}

/**
* function to Search
*/

function searchStringmap(strin)
{
	gblSearch=[];
  if(strin === "")
  {
   kony.print("In Search String gblLocationData::"+gblLocationData);
    if(gblCurrentMapType == MAP_CONSTANTS.ALL){
      settingData(gblLocationData);
    }else if(gblCurrentMapType == MAP_CONSTANTS.ATM){
      settingData(gblLocationDataATM);
    }else{
      settingData(gblLocationDataBranch);
    }
  }
  else
    {
      var j=0;
      var sear=strin.toLowerCase();
      var searchData = "";
      kony.print("searchText:: "+sear);
      if(gblCurrentMapType == MAP_CONSTANTS.ALL){
        searchData = gblLocationData;
      }else if(gblCurrentMapType == MAP_CONSTANTS.ATM){
        searchData = gblLocationDataATM
      }else{
        searchData = gblLocationDataBranch;
      }
      kony.print("@@@SearchData:::"+JSON.stringify(searchData));
      
      for(var i in searchData)
        {
          if(searchData[i].name.toLowerCase().indexOf(sear) != -1)
            gblSearch.push(searchData[i]);
          }
      
      kony.print("gblSearch::"+JSON.stringify(gblSearch));
      settingData(gblSearch);
    }
}

/**
* Common function that sets data for Segment and Maps.Used in case of ATM and Branch Buttons
*/

function settingData(data){
  try{
    kony.print("starts funtion settingData data: "+JSON.stringify(data));
    //kony.print("starts funtion settingData data[0]: "+JSON.stringify(data[0])+", data[1]"+JSON.stringify(data[1]));
    
    frmLocatorKA.locatorMap.calloutTemplate=flxMapATMBranchWithoutLocation;
    frmLocatorKA.locatorMap.widgetDataMapForCallout={
      lblType: "rbType",
      lblName:"rbName",
      lblAddress:"rbDesc",
      btnClose:"rbClose",
      btnDirections: "rbDirections",
      lblLat: "rbLat",
      lblLong:"rbLong"
    };
    kony.print("map callout data fetched successfully");
    frmLocatorKA.locatorSegmentList.widgetDataMap={
      lblLocationName: "name",
      btnAlpha:"alphabet"
    };
    frmLocatorKA.locatorMap.calloutWidth=55;
    frmLocatorKA.locatorMap.zoomLevel=13;


    //kony.print("gblCurrentLocator---------@#@#@#@#: "+JSON.stringify(gblCurrentLocator));
    //data.push(gblCurrentLocator);
    kony.print("@#@#@#@#: "+JSON.stringify(data));
	if(!isEmpty(data)){
		data = sort_ALPHABETICALLY(data, "name");
	}
    if(!isEmpty(data) && data.length>0){
      frmLocatorKA.lblNoSearchResults.isVisible = false;
      kony.print("@#@#@#@#: "+JSON.stringify(data));
      frmLocatorKA.locatorSegmentList.removeAll();
      frmLocatorKA.locatorSegmentList.setData(data);
      kony.print("gblIsCurrentLocation:: "+gblIsCurrentLocation);
      frmLocatorKA.locatorMap.locationData=data;
      frmLocatorKA.locatorMap.addPin(gblCurrentLocator);
      if(!gblIsCurrentLocation){
        kony.print("gblIsCurrentLocation:: "+gblIsCurrentLocation);
        //frmLocatorKA.locatorMap.locationData.push(gblCurrentLocator);
      }
//     frmLocatorKA.locatorSegmentList.setVisibility(true);
//    frmLocatorKA.LabelNoRecordsKA.setVisibility(false);
    //frmLocatorKA.locatorSegmentList.onRowClick=onRowClickSegment;
//     if (userAgent=="iPhone" || userAgent=="iPad") 
//     {
//       frmLocatorKA.locatorMap.calloutTemplate["onTouchEnd"] = onClickPinCalloutiPhone;
//     }
//     else{
//       frmLocatorKA.locatorMap.onSelection=onClickPinCallout;
//     }
     frmLocatorKA.locatorMap.onPinClick=onClickPin;
     
  }
  else{
    // frmLocatorKA.locatorSegmentList.setVisibility(false);
    frmLocatorKA.locatorSegmentList.removeAll();
    //frmLocatorKA.LabelNoRecordsKA.setVisibility(true);
    //frmLocatorKA.locatorMap.locationData=[];
    kony.print("Location Data ::"+frmLocatorKA.locatorMap.locationData.length);
    kony.print("settingData>>> gblCurrentLocator:: "+JSON.stringify(gblCurrentLocator));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    if(frmLocatorKA.locatorListView.isVisible === true){
      frmLocatorKA.lblNoSearchResults.isVisible = true;
       gblmapSearchKeypad =true;
      kony.print("inside if");
    }else{
      gblmapSearchKeypad =false;
      customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.search.noSearchResults"), popupCommonAlertDimiss, "");
      kony.print("inside else");
    }
    	//customAlertPopup("", kony.i18n.getLocalizedString("i18n.search.noSearchResults"),popupCommonAlertDimiss,"");

  	frmLocatorKA.locatorMap.locationData=[];
    frmLocatorKA.locatorMap.locationData = gblCurrentLocator;
    gotoCurrentLocation();
  }
//   var lat = data[0].lat;
//   var lon = data[0].lon;
//   navigateToGivenLocation(lat,lon);
//     
 // callback();
    kony.application.dismissLoadingScreen();
    frmLocatorKA.show();
  }catch(e){
    //customErrorCallback(e);
      kony.application.dismissLoadingScreen();
    kony.print("exception in settingData"+JSON.stringify(e));
  }
}
function parseData(){
  kony.print("inside praseData...");
  try{
    
  }catch(e){
    customErrorCallback(e);
      kony.application.dismissLoadingScreen();
  }
}
/**
* On Click of ATM Filter Button.
*/
function getATMLocations(){
  ShowLoadingScreen();
  var data = [];
  ShowLoadingScreen();
 /* if(gblMapOrList == GENERIC_COSNTATNTS.MAP){
    data.push(gblCurrentLocator);
    kony.print("Inside getBranchLocations:: "+JSON.stringify(data));
    kony.print("Branch data::: "+JSON.stringify(gblLocationDataATM));
    for(var i=0; i<gblLocationDataATM.length; i++){
      data.push(gblLocationDataATM[i]);
    }
    kony.print("after setting the data getBranchLocations:: "+JSON.stringify(data));
    settingData(data,onClickFilterCallback);//gblLocationDataATM

  }else{*/
    settingData(gblLocationDataATM,onClickFilterCallback);
 /* }*/
  //settingData(gblLocationDataATM,onClickFilterCallback);
  kony.print("current global mapOrList:::"+gblMapOrList);
  gblCurrentMapType = MAP_CONSTANTS.ATM;
  onLoadListSkinChange();
  changeBtnSkins(gblCurrentMapType,gblMapOrList);
  frmLocatorKA.locatorSearchTextField.text = "";
}
/**
* On Click of Branch Filter Button.
*/
function getBranchLocations(){
  kony.print("Inside getBranchLocations...");
  var data = [];
  ShowLoadingScreen();
  /*if(gblMapOrList == GENERIC_COSNTATNTS.MAP){
    data.push(gblCurrentLocator);
    kony.print("Inside getBranchLocations:: "+JSON.stringify(data));
    kony.print("Branch data::: "+JSON.stringify(gblLocationDataBranch));
    for(var i=0; i<gblLocationDataBranch.length; i++){
      data.push(gblLocationDataBranch[i]);
    }
    kony.print("after setting the data getBranchLocations:: "+JSON.stringify(data));
    settingData(data,onClickFilterCallback);//gblLocationDataBranch

  }else{*/
    settingData(gblLocationDataBranch,onClickFilterCallback);
  /*}*/
  kony.print("getBranchLocations - current global mapOrList:::"+gblMapOrList);
  gblCurrentMapType = MAP_CONSTANTS.BRANCH;
  onLoadListSkinChange();
  changeBtnSkins(gblCurrentMapType,gblMapOrList);
  frmLocatorKA.locatorSearchTextField.text = "";
}
/**
* On Click of Both ATM & BRANCH(ALL) Filter Button.
*/
function getAllLocations(){
  ShowLoadingScreen();
  var data = [];
  /*if(gblMapOrList == GENERIC_COSNTATNTS.MAP){

    data.push(gblCurrentLocator);
    kony.print("Inside getBranchLocations:: "+JSON.stringify(data));
    kony.print("Branch data::: "+JSON.stringify(gblLocationData));
    for(var i=0; i<gblLocationData.length; i++){
      data.push(gblLocationData[i]);
    }
    kony.print("after setting the data getBranchLocations:: "+JSON.stringify(data));
    settingData(data,onClickFilterCallback);//gblLocationData

  }else{*/
    settingData(gblLocationData,onClickFilterCallback);
  /*}*/
  kony.print("getAllLocations - current global mapOrList:::"+gblMapOrList);
  gblCurrentMapType = MAP_CONSTANTS.ALL;
  onLoadListSkinChange();
  changeBtnSkins(gblCurrentMapType,gblMapOrList);
  frmLocatorKA.locatorSearchTextField.text = "";
}
/**
* On Click of Segment Row..
*/
function onRowClickSegment(){
  var listData=frmLocatorKA.locatorSegmentList.selectedRowItems[0];
  //getDetailsData(listData);
  setLocationDetails(listData);
}



function onClickOfCallBranch(phone,obj){
  if(phone !== null && phone !== undefined){
    kony.phone.dial(phone);
  }
}

/**
* If CurrentLocation is not activated.
*/
function setDataMapWithoutCurrentLoc(){
  try{
    ShowLoadingScreen();
    kony.print("setDataMapWithoutLoc");
    gblIsCurrentLocation =false;
    var options = {"access":"online"};
    objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
    var dataObject = new kony.sdk.dto.DataObject("Locations");
    var queryParams = {"city":"New York"};
    var serviceOptions = {"dataObject":dataObject,"queryParams":queryParams};
    objectService.fetch(serviceOptions, setDataMapCallBack,errorCallBack); 
  }
  catch(e){
    customErrorCallback(e);
      kony.application.dismissLoadingScreen();
  }
}
/**
* Search TextBox on Done functionality.
*/
function startSearchOnDone(){
  if(frmLocatorKA.locatorSearchTextField.text !== null && frmLocatorKA.locatorSearchTextField.text !== '' && frmLocatorKA.locatorSearchTextField.text.length > 0){
    setDataMaponSearch(frmLocatorKA.locatorSearchTextField.text.trim());
  }
}
/**
* Service Call of Search.
*/
function setDataMaponSearch(searchCriteria){
  try{
    ShowLoadingScreen();
    (new kony.retailbanking.AdvancedAtmFilter()).onSearchClearFilter();
    kony.print("setDataMaponSearch");
    var options = {"access":"online"};
    objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
    var dataObject = new kony.sdk.dto.DataObject("Locations");
    var city= searchCriteria;
    dataObject.addField("query","atms or banks in "+city.trim());
    var queryParams = {"type":"search","query":"atms or banks in "+city.trim()};
    //var queryParams = {"city":city.trim()};
    var serviceOptions = {"dataObject":dataObject};//,"queryParams":queryParams};
    objectService.customVerb('getLocationsQuery',serviceOptions, setDataMapCallBack,errorCallBack); 
  }
  catch(e){
    customErrorCallback(e);
  }
}
/**
* OnClick of Pins located on Map
*/
function onClickPin(mapid,response){
  gblSelectionPinData=[];
  //frmLocatorKA.locatorMap.navigateToLocation(response, true, true) ;
  gblSelectionPinData=response;
  kony.print("response:::"+JSON.stringify(response));
  //getDirectionsList();
}
/**
* OnClick of PinsCallout.
*/
function onClickPinCallout(mapid,response){
  getDetailsData(response);
}

function onClickPinCalloutiPhone(){
  getDetailsData(gblSelectionPinData);
}
/**
* OnClick of Image in PinsCallout.
*/
function onClickImageCallout(){
  if(gblSelectionPinData){
    settingDetailsData(gblSelectionPinData);
  }
}
/**
* OnClick of InfoButton in DetailsPage.
*/
function onClickInfoDetails(){
  try{
    var locationDataArray=[];
    locationDataArray.push(gblDirectionsData);
    if(gblRoute){
      frmDirectionsKA.locatorMap.removePolyline("route"+(gblRoute.length -1));
    } 
    frmLocatorKA.buttonInfo.skin = primaryAction;
    frmLocatorKA.getDirectionsButton.skin = sknDirectionButton;
    frmLocatorKA.locatorMap.locationData = locationDataArray;
    frmLocatorKA.locatorMap.navigateToLocation(gblDirectionsData, true, true);
    frmLocatorKA.flxLocationDetails.setVisibility(true);
    frmLocatorKA.flxLocationDirections.setVisibility(false); 
  }
  catch(e){
   customErrorCallback(e);
      kony.application.dismissLoadingScreen();
  }
}
/**
* OnClick of GetDirection in DetailsPage.
*/
function onClickgetDirections(){
  //   frmLocatorKA.buttonInfo.skin = sknDirectionButton;
  //   frmLocatorKA.btnDirection.skin = primaryAction;
  //   frmLocatorKA.flxLocationDetails.setVisibility(false);
  //   frmLocatorKA.flxLocationDirections.setVisibility(true); 
  displaySearchRoutes(gblRoute);
}
/**
* Gets distance and traveltime and routes.
*/
function getDirections(toLocationData,successCallback,ErrorCallback){
  ShowLoadingScreen();
  var searchCriteria;
  kony.print("gblPinClickDetails"+JSON.stringify(toLocationData));
  if (userAgent=="iPhone") 
  {
    searchCriteria = {
      //origin : {lat:41.695604, lon:  -73.098596},//for testing purpose USA lat & long
      origin : {lat:gblLatitude, lon: gbLlongitude},
      destination : {lat : toLocationData.lat,lon : toLocationData.lon},
      alternatives : false
    };
  }
  else{
    searchCriteria = {
      origin : {lat:gblLatitude, lon: gbLlongitude},
      destination : {lat : toLocationData.lat,lon : toLocationData.lon},
      directionServiceUrl : "https://maps.googleapis.com/maps/api/directions/json",
      alternatives : false,
      apiKey : "AIzaSyAJOkl-7hJ08jbE5sBZs9Da9qrHP_XhXro"
    };                             
  }
  kony.map.searchRoutes(searchCriteria, successCallback, ErrorCallback); 
}

function onClickFilterCallback(){
  kony.print("successfully filtered");
  kony.application.dismissLoadingScreen();
}
/**
* Returns LocationImage PIn for a type provied like ATM,BRANCH & MainBranch.
*/

function getLocationImage(type){
  kony.print("inside getLocationImage type::: "+type);
  if(type == geti18Value("i18n.locateus.btnATM")){
    kony.print("inside getLocationImage");
    return "atm_icon_line.png";
  }
  else if(type == geti18Value("i18n.locateus.btnBranch") || type == "MainBranch"){
    return "branch_icon_line.png";
  }
}
/**
* Returns Image PIn for a type provied like ATM,BRANCH & MainBranch.
*/
function getMapPinIcon(type){
  kony.print("inside getMapPinIcon type::: "+type);
  if(type == geti18Value("i18n.locateus.btnATM")){
    return "atm_icon_inactive.png";
  }
  else if(type == geti18Value("i18n.locateus.btnBranch") || type == "MainBranch"){
    return "branch_icon_inactive.png";
  }
}
/**
* Returns Image for a Status provied like CLOSED & OPEN .
*/
function getStatusImage(type){
  if(type == "CLOSED"){
    return "location_close.png";
  }
  else if(type == "OPEN"){
    return "location_open.png";
  }  

}
/**
* Returns Colour for a Status provied like CLOSED & OPEN .
*/
function getSknColorStatus(type){
  if(type == "CLOSED"){
    return "sknstatusClosed";
  }
  else if(type == "OPEN"){
    return "sknStatus";
  } 
}
/**
* Navigates to currentLocation.
*/
function gotoCurrentLocation(){
  var showDropPin = true;
  if(frmLocatorKA.searchContainer.isVisible === true && gblmapSearchKeypad){
    frmLocatorKA.locatorSearchTextField.setFocus(true);
  }else{
    frmLocatorKA.btnSearch.setFocus(true);
    frmLocatorKA.forceLayout();
  }
  gblmapSearchKeypad = true;
  kony.print("inside gotoCurrentLocation : gblLatitude::"+gblLatitude+"gbLlongitude:::"+gbLlongitude);
  kony.print("gblCurrentLocator"+JSON.stringify(gblCurrentLocator));
  if(kony.sdk.isNetworkAvailable()){ 
    if(gblLatitude && gbLlongitude){
        frmLocatorKA.locatorMap.navigateToLocation(gblCurrentLocator, false, showDropPin);
    }
  }else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
  }

}

function navigateToGivenLocation(lat,lon){
  try{
    if(lat && lon){
      var navigationInfo = {
        lat : lat,
        lon : lon,
        showcallout : false
      };
      frmLocatorKA.locatorMap.navigateTo(0,false);
    }
  }catch(e){
   customErrorCallback(e);
      kony.application.dismissLoadingScreen();
  }
}
/**
* OnClick of Back Button in frmLocatorKA.
*/
var isThisFromPushNotification = false;
function onBackforMaps(){
  var obj = new kony.retailbanking.pushNotificationsHandler();
  // kony.application.getPreviousForm().show();
  if(kony.retailbanking.pushNotificationsHandler.launchingFromNotification){

    frmLoginKA.show();
    obj.revertUIChangesOfLocatorKA();
    kony.retailbanking.pushNotificationsHandler.launchingFromNotification = false;
    return;
  }
  if(kony.retailbanking.pushNotificationsHandler.mapOpenedFromOnlineNotification){
    if(kony.retailbanking.pushNotificationsHandler.mapBeforeFormOnlineNotification){
      kony.retailbanking.pushNotificationsHandler.mapBeforeFormOnlineNotification.show();
    }
    obj.revertUIChangesOfLocatorKA();
    return;
  }

  if(gblFormName === "frmChatBots"){
    (new kony.chatbotPresentation()).backToChatBotsFromMaps();
  }

  if(gblFormName == "frmLoginKA"){ 
    //if(kony.retailBanking.globalData.deviceInfo.isIphone())
    //removeSwipe();
    frmLoginKA.show();
  }
  else if(gblFormName == "frmMoreLandingKA" && userAgent=="iPhone" ){
    frmMoreLandingKA.show();
  }
  else if(gblFormName == "frmRecentTransactionDetailsKA"){
    frmRecentTransactionDetailsKA.show();
  }
  else if(gblFormName == "frmSearchTransactionDetailsKA"){
    frmSearchTransactionDetailsKA.show();
  }
  kony.retailbanking.AdvancedAtmFilter.isFilterApplied = false;
}

/**
* OnClick of Back Button in details.
*/
function onBackBtnDetails(){
  try{
    if(gblRoute) frmLocatorKA.locatorMap.removePolyline("route"+(gblRoute.length -1));
    if(gblFilterType == "ATM"){
      getATMLocations();
    }
    else if(gblFilterType == "BRANCH"){
      getBranchLocations();
    }
    else if(gblFilterType == "BOTH"){
      getAllLocations();
    }
    else{
      getAllLocations();
    }
    frmLocatorKA.listView.setVisibility(true);
    frmLocatorKA.locatorDetailsWrapper.setVisibility(false);
  }
  catch(e){
    kony.print(e);
    customErrorCallback(e);
  }
}
function locatorListViewEnabledAnimate(){
kony.print("@@@inside locatorListViewEnabledAnimate...");
  frmLocatorKA.locatorListView.isVisible = true;
  frmLocatorKA.btnSearch.setVisibility(true);
  frmLocatorKA.locatorMapView.animate(
    kony.ui.createAnimation({100:{ "opacity": 0, "stepConfig":{"timingFunction": easeOut}}}),
    {fillMode: forwards ,duration:0.3},
    {animationEnd: function() { 
      frmLocatorKA.locatorMapView.isVisible = false;
      frmLocatorKA.locatorListView.opacity = 1;
    } 
    });
  frmLocatorKA.btnMapKA.zIndex = 1;
  frmLocatorKA.btnListKA.zIndex = 2;
  gblMapOrList = GENERIC_COSNTATNTS.LIST;
  onLoadListSkinChange();
  showOrHideSearch(false,"-15%");
  gblSearchOn = false;
  if(gblCurrentMapType!=null || gblCurrentMapType != ""){
    kony.print("locatorListViewEnabledAnimate - if: "+gblMapOrList);
    applySkin(gblCurrentMapType);    
  }else{
    kony.print("locatorListViewEnabledAnimate - if: "+gblMapOrList);
    applySkin(MAP_CONSTANTS.ALL);    
  }
  searchStringmap("");
  
  //Added Ahsan for Huawei Maps when list click
  frmLocatorKA.mainContent.setVisibility(true);
  frmLocatorKA.flxHuaweiMapContainer.setVisibility(false);
  //Added Ahsan for Huawei Maps when list click
}

function locatorMapViewEnabled(){
  kony.print("@@@inside locatorMapViewEnabled...");
  frmLocatorKA.locatorMapView.isVisible = true;	
  //#ifdef android
     checkIsHMSServiceEnabledAndShowButtonToOpenHuweiMap();
  //#endif
  frmLocatorKA.btnSearch.setVisibility(false);
  frmLocatorKA.locatorListView.animate(
    kony.ui.createAnimation({100:{ "opacity": 0,"stepConfig":{"timingFunction": easeOut}}}),
    {fillMode: forwards ,duration:0.3},
    {animationEnd: function() {
      frmLocatorKA.locatorListView.isVisible = false;
      frmLocatorKA.locatorMapView.opacity = 1;
    } 
    });  
  frmLocatorKA.btnMapKA.zIndex = 2;
  frmLocatorKA.btnListKA.zIndex = 1;
  
  frmLocatorKA.btnMapKA.skin=SKINCONSTANTS.BGWHITEBLUEFACE;
  frmLocatorKA.btnMapKA.focusSkin="sknprimaryAction";
  frmLocatorKA.btnListKA.skin=SKINCONSTANTS.BGBLUEROUNDEDWHITEFACE;

  gblMapOrList = GENERIC_COSNTATNTS.MAP;
  onLoadListSkinChange();
  showOrHideSearch(false,"-15%");
  gblSearchOn = false;
   if(gblCurrentMapType!=null || gblCurrentMapType != ""){
     kony.print("locatorMapViewEnabled - if: "+gblMapOrList);
    applySkin(gblCurrentMapType);    
  }else{
    kony.print("locatorMapViewEnabled - else: "+gblMapOrList);
    applySkin(MAP_CONSTANTS.ALL);    
  }
  searchStringmap("");
}



function openEmail()
{
  try
  {
    var to=[frmLocatorBranchDetailsKA.lblEmailAddress.text];
    var cc=[];
    var bcc = [];
    var sub = "Email to Branch";
    var msgbody = " Type your Email";
    kony.phone.openEmail(to,cc,bcc,sub,msgbody,false,[]);		
  }
  catch(err)
  {
    customErrorCallback(e);
      kony.application.dismissLoadingScreen();

  }
}

function changeBtnSkins(applyTo,mapOrList){
  kony.print("@@inside changeBtnSkins..."+applyTo+",mapOrList: "+mapOrList);
  var selectedSkin = "";
  var otherSkin = "";
  var focusSkin = "";

  try{
    if(mapOrList == GENERIC_COSNTATNTS.MAP){
      selectedSkin = SKINCONSTANTS.BGBLUEROUNDEDWHITEFACE;
      otherSkin = SKINCONSTANTS.BGWHITEBLUEFACE;
      focusSkin = SKINCONSTANTS.BGWHITEBLUEFACE
    }else if(mapOrList == GENERIC_COSNTATNTS.LIST){
      selectedSkin = SKINCONSTANTS.BTNBGLIGHTBLUE;
      otherSkin = SKINCONSTANTS.BGBLUEROUNDEDWHITEFACE;
      focusSkin = SKINCONSTANTS.BGBLUEROUNDEDWHITEFACE;
    }
    kony.print("@@@changeBtnSkins::: selectedSkin :"+selectedSkin+", otherSkin: "+otherSkin);
    if(MAP_CONSTANTS.ALL == applyTo){
      frmLocatorKA["btnBothKA"].skin = selectedSkin;
      frmLocatorKA["btnBothKA"].focusSkin = focusSkin;
      frmLocatorKA["btnBranchKA"].skin = otherSkin;
      frmLocatorKA["btnATMKA"].skin = otherSkin;
    }else if(MAP_CONSTANTS.BRANCH == applyTo){
      frmLocatorKA["btnBothKA"].skin = otherSkin;
      frmLocatorKA["btnBranchKA"].skin = selectedSkin;
      frmLocatorKA["btnBranchKA"].focusSkin = focusSkin;
      frmLocatorKA["btnATMKA"].skin = otherSkin;
    }else if(MAP_CONSTANTS.ATM == applyTo){
      frmLocatorKA["btnBothKA"].skin = otherSkin;
      frmLocatorKA["btnBranchKA"].skin = otherSkin;
      frmLocatorKA["btnATMKA"].skin = selectedSkin;
      frmLocatorKA["btnATMKA"].focusSkin = focusSkin;
    }
  }catch(exceptionObject){
    customErrorCallback(e);
      kony.application.dismissLoadingScreen();
  }
}
function getAlpha(desc){
  try{
    if(!isEmpty(desc)){
      kony.print("getAlpha::: "+desc);
      return desc.charAt(0);
    }
  }catch(e){
   customErrorCallback(e);
      kony.application.dismissLoadingScreen();
  }
}
function onLoadListSkinChange(){
  try{
    if(gblMapOrList == GENERIC_COSNTATNTS.MAP){
      frmLocatorKA.btnMapKA.skin=SKINCONSTANTS.BGWHITEBLUEFACE;
      frmLocatorKA.btnListKA.skin=SKINCONSTANTS.BGBLUEROUNDEDWHITEFACE;
      frmLocatorKA.btnListKA.focusSkin="sknprimaryAction";
      frmLocatorKA.searchAccountFilters.skin = SKINCONSTANTS.FLXBGWHITE;
    }else if(gblMapOrList == GENERIC_COSNTATNTS.LIST){
      frmLocatorKA.btnMapKA.skin=SKINCONSTANTS.BGBLUEROUNDEDWHITEFACE;
      frmLocatorKA.btnListKA.skin=SKINCONSTANTS.BGWHITEBLUEFACE;
      frmLocatorKA.btnListKA.focusSkin="sknprimaryAction";
      frmLocatorKA.searchAccountFilters.skin = SKINCONSTANTS.BGSHADEBLUE;
      frmLocatorKA.searchAccountFilters.skin = SKINCONSTANTS.FLXBGBLUE;
    }
  }catch(exceptionObject){
    kony.print("Exception found in listSkinChange:::"+JSON.stringify(exceptionObject));
  }
}
function applySkin(fetchType){
  kony.print("@@@ inside applySkin..."+fetchType+", gblMapOrList: "+gblMapOrList);
  try{
    if(gblMapOrList == GENERIC_COSNTATNTS.MAP){
      kony.print("ïf");
      changeBtnSkins(fetchType,GENERIC_COSNTATNTS.MAP);
    }else if(gblMapOrList == GENERIC_COSNTATNTS.LIST){
      kony.print("else ïf");
      changeBtnSkins(fetchType,GENERIC_COSNTATNTS.LIST);
    }
  }catch(e){
    customErrorCallback(e);
      kony.application.dismissLoadingScreen();
  }
}

function getLocationList(locationData){
  var tempLocaterData={};
  var locationDataArr = [];
  var lang = kony.store.getItem("langPrefObj");

  try{
    for(var i = 0; i < locationData.length; i++){
      if(locationData[i].latitude != "" && locationData[i].longitude != ""){
        var address1 = (lang === GENERIC_COSNTATNTS.AR)?locationData[i].addressLine1:locationData[i].addressLine1s;
        var address2 = (lang === GENERIC_COSNTATNTS.AR)?locationData[i].addressLine2s:locationData[i].addressLine2;
        var name = (lang === GENERIC_COSNTATNTS.AR)?locationData[i].name:locationData[i].name1;
        var mapPinType = (locationData[i].brch_atm_ind == "1")?geti18Value("i18n.locateus.btnBranch"):geti18Value("i18n.locateus.btnATM"); 
        var btnCloseVisible = true;
       
        
         //#ifdef android
          btnCloseVisible = false;
        //#endif
        //#ifdef iphone
        btnCloseVisible = true;
        //#endif
       
        tempLocaterData =
          {
          lat:locationData[i].latitude,
          lon:locationData[i].longitude,
          name:(lang === GENERIC_COSNTATNTS.AR)?locationData[i].name:locationData[i].name1,
          address1:address1,
          address2:address2,
          city:(lang === GENERIC_COSNTATNTS.AR)?locationData[i].city_b:locationData[i].city_s,
          phone:locationData[i].phoneNumber,
          fax:locationData[i].fax,
          extn:locationData[i].extn,
          brch_atm_ind: locationData[i].brch_atm_ind,
          brch_atm_code: locationData[i].brch_atm_code,
          placeID: locationData[i].placeID,
          showcallout: true,
          image:getMapPinIcon(mapPinType),
          calloutData:{
            rbType: mapPinType,
            rbName: name,
            rbDesc:address2 + " " + locationData[i].placeID,
            rbLat:locationData[i].latitude,
            rbLong:locationData[i].longitude,
            rbClose:{ isVisible :btnCloseVisible,"onClick":closeCallout},
           rbDirections:{"text":geti18Value("i18n.locateus.getDirections"),"onClick":getDirectionsList} 
          },
          alphabet : {"text": getAlpha(name),backgroundColor: ((1 << 24) * Math.random() | 0).toString(16)}
        };
        locationDataArr.push(tempLocaterData);          
      }

    }
    kony.print("getlocationlist function executed successfully");
  }catch(exceptionObject){
    kony.print("Exception found in getLocationList::: "+JSON.stringify(exceptionObject));
    customErrorCallback(exceptionObject);
      kony.application.dismissLoadingScreen();
  }
  gblLocationData.push(locationDataArr);
  return locationDataArr;
}

//locatorMapViewEnabled
function preshowLocatorKA(){
  
   //Added code Ahsan for checking Huawei Maps and open FFI
  
  //#ifdef android
      checkIsHMSServiceEnabledAndShowButtonToOpenHuweiMap();
  //#endif
  
  //Added code Ahsan for checking Huawei Maps and open FFI
  
  kony.print("inside preshowLocatorKA .. gblMapOrList: "+gblMapOrList+", gblCurrentMapType: "+gblCurrentMapType+", gblSearchOn: "+gblSearchOn);
  frmLocatorKA.forceLayout();
  if(!isEmpty(gblMapOrList)){
    kony.print("inside preshowLocatorKA isEmpty(gblMapOrList).."+gblMapOrList+", gblSearchOn: "+gblSearchOn);
    if(gblSearchOn){
      kony.print("inside gblSearchOn");
      showOrHideSearch(true,"0%");
     frmLocatorKA.btnSearch.setVisibility(false);
    }
  }else{
    gblInsideMap = true;
    gblCurrentMapType = MAP_CONSTANTS.ALL;
    gblMapOrList = GENERIC_COSNTATNTS.MAP;
    frmLocatorKA.btnMapKA.skin = SKINCONSTANTS.BGWHITEBLUEFACE;
    frmLocatorKA.btnListKA.skin = SKINCONSTANTS.BGBLUEROUNDEDWHITEFACE;
    frmLocatorKA.btnSearch.setVisibility(true);
    changeBtnSkins(gblCurrentMapType,GENERIC_COSNTATNTS.MAP);
  }
  changeBackNavigation();
  
}

function checkIsHMSServiceEnabledAndShowButtonToOpenHuweiMap(){
  
  kony.print("Inside checkIsHMSServiceEnabledAndShowButtonToOpenHuweiMap");
  let isHmsAvailable = HMSMap.isHmsAvailable();
  
  if(isHmsAvailable && frmLocatorKA.locatorMapView.isVisible){
    kony.print("checkIsHMSServiceEnabledAndShowButtonToOpenHuweiMap => isHmsAvailable " + isHmsAvailable);

    frmLocatorKA.mainContent.setVisibility(false);
    frmLocatorKA.flxHuaweiMapContainer.setVisibility(true);
    
    
    frmLocatorKA.btnShowHuaweiMap.onClick = function(){
       	kony.print("Ahsan gblLocationDataATM " + JSON.stringify(gblLocationDataATM));
        kony.print("Ahsan gblLocationDataBranch " + JSON.stringify(gblLocationDataBranch));

        let locationTypeList = [];
        locationTypeList.push({"atm_list" : gblLocationDataATM});
        locationTypeList.push({"branch_list" : gblLocationDataBranch});
      
        var langSelected = kony.store.getItem("langPrefObj");
        HMSMap.invokeHMSMap(langSelected, locationTypeList);
    };
    
    
  }else{
    frmLocatorKA.mainContent.setVisibility(true);
    frmLocatorKA.flxHuaweiMapContainer.setVisibility(false);
  }
}

function changeBackNavigation(){
  kony.print("##inside changeBackNavigation...");
  try{
    if(isLoggedIn){
      frmLocatorKA.btnLogin.text = kony.i18n.getLocalizedString("i18n.deposit.back");
      frmLocatorKA.flxBack.onClick = function(){frmMore.show();}
    }else{
      frmLocatorKA.btnLogin.text = kony.i18n.getLocalizedString("i18n.common.login");
      frmLocatorKA.flxBack.onClick = function(){frmLoginKA.show();}
    }
  }catch(exceptionObject){
    kony.print("Exception found in changeBackNavigation:::"+exceptionObject);
  }
}

function closeCallout(){
  kony.print("Inside closeCallout...");
  frmLocatorKA.locatorMap.dismissCallout();
}

function showOrHideSearch(flag,zIndex){
  kony.print("@@inside showOrHideSearch:::"+gblMapOrList+"flag: "+flag+", zIndex: "+zIndex);
  try{
    frmLocatorKA.searchContainer.setVisibility(flag);
    frmLocatorKA.btnSearch.setVisibility(!flag);
    frmLocatorKA.cancelButton.right = zIndex;
  }catch(ex){
    kony.print("Exception found in showOrHideSearch:::"+JSON.stringify(ex));
        customErrorCallback(ex);
      kony.application.dismissLoadingScreen();
  }
}

function showLoginForm(){
  kony.print("@@inside showLoginForm:::");
  try{
    frmLocatorKA.locatorMapView.setVisibility(true);
    frmLocatorKA.locatorMapView.zIndex = "10";
    if(isLoggedIn){
      if(gblFormName == "frmNewUserOnboardVerificationKA"){
        frmPaymentDashboard.show();
      }else
      frmMore.show();
    }
    else
      frmLoginKA.show();
    kony.print("gblMapOrList: "+gblMapOrList+", gblCurrentMapType: "+gblCurrentMapType+", gblSearchOn: "+gblSearchOn+", gblInsideMap: "+gblInsideMap);
  }catch(ex){
    kony.print("Exception found in showLoginForm:::"+JSON.stringify(ex));
  }
  
}

function releaseLocatorGlobals(){
  try{
    gblMapOrList = "";
    gblCurrentMapType = "";
    gblSearchOn = "";
    gblInsideMap = false;
    if(!gblInsideMap){
      frmLocatorKA.destroy();
    }
  }catch(ex){
    kony.print("Exception found in releaseLocatorGlobals"+ex);
    kony.application.dismissLoadingScreen();
  }
}

function predectiveFourChar(){
  kony.print("@@inside predectiveFourChar:::");
  var txtSearchVal = frmLocatorKA.locatorSearchTextField.text;
  try{
    kony.print("Length of the search String :::"+txtSearchVal.length);
    if(txtSearchVal.length>3){
      searchStringmap(frmLocatorKA.locatorSearchTextField.text);  
    }
  }catch(ex){
    kony.print("Exception found in predectiveFourChar:::"+JSON.stringify(ex));
            customErrorCallback(ex);
      kony.application.dismissLoadingScreen();
  }
}
  
function getDirectionsList(){
  kony.print("inside getDirections...");
  try{
    var src = [gblLatitude,gbLlongitude];    
    var endlon = gblSelectionPinData.lon;  
    var endlat = gblSelectionPinData.lat;
    var desc = [endlat,endlon];
    gblURL = "";
    if(kony.sdk.isNetworkAvailable()){     
      gblURL = "https://maps.google.com/maps?saddr="+src+"&daddr=" +desc;
      var locationData = frmLocatorKA.locatorMap.locationData;
      kony.print("locationData::: "+JSON.stringify(locationData));
      kony.application.dismissLoadingScreen();
      openURL();

    }else {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
    }

  }catch(ex){
    kony.print("Exception found in getDirections:::"+JSON.stringify(ex));
  }
}

function callOutSelection(){
  try{
    kony.print("eventobject callOutSelection:::");
    //#ifdef android
      getDirectionsList();
    //#endif
  }catch(ex){
    kony.print("Exception found in callOutSelection:::"+JSON.stringify(ex));
            customErrorCallback(ex);
      kony.application.dismissLoadingScreen();
  }  
}


function appLogoutATM()
{ 
  popupCommonAlertDimiss();
  //kony.sdk.mvvm.LogoutAction(); BMA-474	
  kony.application.openURL(gblURL);
}
function openURL()
{
  kony.print("inside openURL");
  if(isLoggedIn===true)
  {
    customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.contact.openURL"), appLogoutATM, popupCommonAlertDimiss, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));
  }
  else
  {
    kony.application.openURL(gblURL);
  }
}
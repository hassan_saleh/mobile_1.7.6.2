var SETTINGS_GLOBALS = {
  currentMainMenu : "",
  currentMenuActive : false,
  PROFILE: false,
  ACCOUNT:false,
  LANGUAGE:false,
  ALTERNATELOGINS:false,
  ALERT:false,
  CARD : false
};

//To check the flow is from only acccounts
var standingIns = "";
var fromAccounts = false;
var fromCards = false;
var gblTModule = "";
var gblDVOC = "";
var CARDLIST_DETAILS = {
  cardNumber : "",
  operationType : "",
  cardType: "",
  cardPin:""
};

//To check SMS opt of all accounts
var optAll = false;
var gblMapOrList = "";
var gblAccountTabModule = "";
var gblCurrentMapType = "";
var gblSearchOn = "";
var gblURL = "";
var gblInsideMap = false;
var gblHqadded = false;
var gblCardReorder = false;
var gblCardAddNickname = false;
var isFirstRun = false;//omar alnajjar

if(kony.store.getItem("SMSALERT") === null || kony.store.getItem("SMSALERT") === undefined){
	kony.store.setItem("SMSALERT",{"isSMSDisabled":false,"isALERTDisabled":false});
}


var prev_FORM = "";
var filter_OPTIONS = {
            "transactionAmount": {
                "min": null,
                "max": null
            },
            "days": null,
            "Mode": {
                "credit": null,
                "debit": null,
                "card": null,
                "all": null
            },
            "checknumber": {
                "from": null,
                "to": null
            },
            "other": {
                "from": null,
                "to": null
            },
            "year": null,
            "transactionName": null
        };


var ObjReleaseNotes = [
{"releaseNote" : "Change SMS Number"},
{"releaseNote" : "Change Mobile Number"},
{"releaseNote" : "Apply for New Card"},
{"releaseNote" : "Apply for New Sub Account"},
];


var loadBillerDetails = false;
//upgrade cards limit globals
var MIN_CARDLIMIT_CLASSIC = 0;
var MAX_CARDLIMIT_CLASSIC = 0;
var MIN_CARDLIMIT_GOLD = 0;
var MAX_CARDLIMIT_GOLD = 0;
var MIN_CARDLIMIT_PLATINUM = 0;
var MAX_CARDLIMIT_PLATINUM = 0;
var MIN_CARDLIMIT_WORLD = 0;
var MAX_CARDLIMIT_WORLD = 0;
///
var card_LINKED_ACCOUNTS = null;
var card_LINK_ACCOUNT_SERV = false;
var card_UNLINK_ACCOUNT_SERV = false;
var card_DEFAULT_ACCOUNT_SERV = false;
var card_LINKED_ACC_COUNT = 0;
var bill_BULK_PAYMENT_COUNT = 0;
var serv_DATA_CARD_LINK = {"link":null,"unlink":null,"defaultAccount":null};
var selectedIndex_BULK_PAYMENT = [];
var gblQuickFlow="";// hassan 13/07/2020
//Device Registration Global Flag
if(kony.store.getItem("isDeviceRegisteredMandatory") === undefined || kony.store.getItem("isDeviceRegisteredMandatory") === null){
	kony.store.setItem("isDeviceRegisteredMandatory", true);
}


//BOJPAY APP check variable
var ENABLEBOJPAYAPP = "N";

if(kony.store.getItem("isPushMandatory") === undefined || kony.store.getItem("isPushMandatory") === null){
	kony.store.setItem("isPushMandatory",true);
}

////
var hasCasaAccounts = false;
var transaction_LIMIT = {limit:"",
                        ownLimit:"",
                        bojLimit:"",
                        nationalLimit:"",
                        internationalLimit:""};
function callRequestOTP(){
  if(kony.sdk.isNetworkAvailable()){
    ShowLoadingScreen();
    gblMaxOTPLength = 6;
    kony.print("reOTP gblToOTPFrom="+gblToOTPFrom);
    var opName = "requestOTP";
    var countryCode = "962";
      var MobNo = "";
    var isMobileValid = true;
    // var devID = deviceid;
    //var deviceName = kony.retailBanking.globalData.deviceInfo.getDeviceInfo().name;
    var langSelected = kony.store.getItem("langPrefObj");
    var Language = langSelected.toUpperCase();
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };

    var resendFlag;
    //alert("frmNewUserOnboardVerificationKA.btnResendOTP.isVisible :: "+frmNewUserOnboardVerificationKA.btnResendOTP.isVisible );
    if(frmNewUserOnboardVerificationKA.btnResendOTP.isVisible === true){
      resendFlag = true;
      frmNewUserOnboardVerificationKA.btnResendOTP.setVisibility(false);

    }
    else{
      resendFlag = false;
    }
    var curDate = new Date();
    var month = (curDate.getMonth()+1)<10?"0"+(curDate.getMonth()+1):(curDate.getMonth()+1);
    var date = (curDate.getDate())<10?"0"+(curDate.getDate()):(curDate.getDate());
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("User", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("User"); 
    dataObject.addField("otpType","A");
    dataObject.addField("resendflag",resendFlag);
    if(Language == "EN"){
//       if(isAndroid())
//       dataObject.addField("otpMessage","<#> "+engOTPmsg+"  " +OTPHashCode);
//       else
      dataObject.addField("otpMessage",engOTPmsg);  
    }
    else{
//       if(isAndroid())
//       dataObject.addField("otpMessage","<#> "+araOTPmsg+"  " +OTPHashCode);
//       else
      dataObject.addField("otpMessage",araOTPmsg);
    }
    dataObject.addField("otplength",gblMaxOTPLength);
    dataObject.addField("language",Language);
    dataObject.addField("otpMaxLimit","180");
    dataObject.addField("custId",custid);
    dataObject.addField("otp","");
    dataObject.addField("P_SOURCE","MOB");
    dataObject.addField("P_COUNTRY_CODE","1");
    dataObject.addField("P_LANGUAGE","1");
    dataObject.addField("P_ALERT_PRIORITY","1");
    dataObject.addField("P_SILENT_FROM","0:00:00");
    dataObject.addField("P_SILENT_TO","0:00:00");
    dataObject.addField("P_IS_SILENT","0");
    dataObject.addField("P_ALERT_ID","OTP");
    dataObject.addField("P_ALERT_INS_TIME",curDate.getFullYear()+"-"+month+"-"+date);
    if(gblToOTPFrom == "UpdateSMSNumber"){
      opName = "requestOTPSMS";      
     popupCommonAlert.dismiss();
      if(gblFromModule == "UpdateSMSNumber"){     
   countryCode = isEmpty(frmRegisterUser.txtCountryCode.text) ? "962" : frmRegisterUser.txtCountryCode.text;
        MobNo = countryCode + frmRegisterUser.txtMobileNumber.text;
    }else if(gblFromModule == "UpdateMobileNumber"){ 
      countryCode = isEmpty(frmRegisterUser.txtCountryCode.text) ? "962" : frmRegisterUser.txtCountryCode.text;
      MobNo = countryCode + frmRegisterUser.txtMobileNumber.text;
    }
    
      dataObject.addField("mobileNo",MobNo);
    }
    var serviceOptions = {"dataObject":dataObject,"headers":headers};
    kony.print("Input params-->"+ serviceOptions );
    kony.print("req Input params-->"+ JSON.stringify(serviceOptions) );
    // customerAccountDetails.profileDetails = null;
    kony.print("profileDetails ::"+JSON.stringify(customerAccountDetails.profileDetails));
    if(customerAccountDetails.profileDetails === null || customerAccountDetails.profileDetails.smsno === null && customerAccountDetails.profileDetails.smsno === ""){
      var queryParams = {"custId":custid,
                         "language":kony.store.getItem("langPrefObj")=="en"?"eng":"ara"};
      var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetProfileData");
      appMFConfiguration.invokeOperation("prGetCustDetails", {},queryParams,function(response){kony.print("Success ::"+JSON.stringify(response));
                                                                                               if (response !== undefined && response.custData !== undefined && response.custData[0] !== null) {
                                                                                                 response.custData[0].maskedSMSno = mask_MobileNumber(response.custData[0].smsno);
                                                                                                 customerAccountDetails.profileDetails = response.custData[0];
                                                                                                 serv_GENERATEOTP();
                                                                                               }else{
                                                                                                 customAlertPopup(geti18Value("i18n.Bene.Failed"), 
                                                                                                                  geti18Value("i18n.common.unabletoretirvesmsno"),
                                                                                                                  popupCommonAlertDimiss, "");
                                                                                               }
                                                                                              },function(err){kony.print("Failed to get status ::"+err);
                                                                                                              customAlertPopup(geti18Value("i18n.Bene.Failed"), 
                                                                                                                               geti18Value("i18n.common.unabletoretirvesmsno"),
                                                                                                                               popupCommonAlertDimiss, "");});
    }else{
      serv_GENERATEOTP();
    }
    function serv_GENERATEOTP(){
      if (customerAccountDetails.profileDetails !== null) {
        if(customerAccountDetails.profileDetails.smsno === "" || customerAccountDetails.profileDetails.smsno === null){
          isMobileValid = false;
        }
      }
      if(isMobileValid){
        kony.timer.schedule("mytime21",timerFunc, 30, false);
        kony.print("opName=="+opName);
        modelObj.customVerb(opName,serviceOptions, reqOTPSuccessCallback, reqOTPErrorCallback);
      }else
        customAlertPopup(geti18Value("i18n.maps.Info"), 
                         geti18Value("i18n.common.numbernotregistered"),
                         popupCommonAlertDimiss, "");
      function timerFunc(){
        frmNewUserOnboardVerificationKA.btnResendOTP.setVisibility(true);
        kony.timer.cancel("mytime21");
      }

    }

    function reqOTPSuccessCallback(response){
      kony.application.dismissLoadingScreen();
            if(!isEmpty(response)){
        if(response.opstatus === 0)
        {
          kony.print("otp service -----"+JSON.stringify(response));
          if(response.otp !== undefined && response.otp !== null && response.statusCodeSMS === "00000"){
            alert("OTP :: " + response.otp);
            frmNewUserOnboardVerificationKA.show();
           
          }else if(response.statusCodeSMS === "00000" && response.statusCode === "S0036"){
            frmNewUserOnboardVerificationKA.show();
          }else if(response.statusCodeSMS !== "00000"){
            kony.timer.cancel("mytime21");
            var msg = geti18Value("i18n.common.unabletosendsmstoregisterednumber");
            if(kony.application.getCurrentForm().id === "frmRegisterUser" && frmRegisterUser.flxTxtMobileNukber.isVisible === true){
            	msg = geti18Value("i18n.otp.unabletosendsms");
            }
            customAlertPopup(geti18Value("i18n.Bene.Failed"), 
                             msg,
                             popupCommonAlertDimiss, "");
          }
        }else{
         kony.timer.cancel("mytime21");
          customAlertPopup(geti18Value("i18n.common.alert"), geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
          kony.application.dismissLoadingScreen();
        }
      }else{
        kony.timer.cancel("mytime21");
        customAlertPopup(geti18Value("i18n.common.alert"), geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
        kony.application.dismissLoadingScreen();
      }
    }

    function reqOTPErrorCallback(response){
      kony.print("An Error has occured, cant register :: otp service -----"+JSON.stringify(response));
      kony.timer.cancel("mytime21");
      customAlertPopup(geti18Value("i18n.common.alert"),  geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
      kony.application.dismissLoadingScreen();
      backFromOTP();
    }
  }
  else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}




function gotoOTPVerification()
{
  if(kony.sdk.isNetworkAvailable()){
    ShowLoadingScreen();
    var langSelected = kony.store.getItem("langPrefObj");
    var Language = langSelected.toUpperCase();
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("User", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("User"); 

    dataObject.addField("language",Language);
    dataObject.addField("otp",frmNewUserOnboardVerificationKA.txtOTP.text);
    frmNewUserOnboardVerificationKA.lblNext.skin = sknLblNextDisabled;
    var serviceOptions = {"dataObject":dataObject,"headers":headers};
    kony.print("Input params verify otp-->"+ serviceOptions );
    kony.print("Input params verify otp-->"+ JSON.stringify(serviceOptions) );
    modelObj.customVerb("verifyOTP",serviceOptions, verifyOTPSuccessCallback, verifyOTPErrorCallback);



    function verifyOTPSuccessCallback(response){
      kony.application.dismissLoadingScreen();
      if(!isEmpty(response)){
        kony.print("Success "+JSON.stringify(response));

        if(response.statusCode == "S0037"){
          nextFromOTP();
        }
        else{
          kony.print("OTP::: 1");
          if(!isAndroid()){
            frmNewUserOnboardVerificationKA.loadCustomWidgetForm.clearOTPTextField();
		    frmNewUserOnboardVerificationKA.loadCustomWidgetForm.resignKeyBoard();
          }
          frmNewUserOnboardVerificationKA.txtOTP.text = "";
          kony.print("OTP::: 2");
          frmNewUserOnboardVerificationKA.lblTxt1.text = "";
          kony.print("OTP::: 3");
          frmNewUserOnboardVerificationKA.lblTxt2.text = "";
          kony.print("OTP::: 4");
          frmNewUserOnboardVerificationKA.lblTxt3.text = "";
          kony.print("OTP::: 5");
          frmNewUserOnboardVerificationKA.lblTxt4.text = "";
          kony.print("OTP::: 6");
          frmNewUserOnboardVerificationKA.lblTxt5.text = "";
          kony.print("OTP::: 7");
          frmNewUserOnboardVerificationKA.lblTxt6.text = "";
          kony.print("OTP::: 8 Loop start");
          for(var i =1;i<7;i++){
            var lineName = "flxLine" + i;
            frmNewUserOnboardVerificationKA[lineName].skin = sknFlxGreyLine;
          }
          kony.print("OTP::: 9 Loop end");
          var Message = getServiceErrorMessage(response.statusCode,"verifyOTPSuccessCallback");
          kony.print("OTP::: 10");
          //         customAlertPopup("Authentication Failed!", Message,popupCommonAlertDimiss,"");
          frmNewUserOnboardVerificationKA.lblInvalidCredentialsKA.text = Message;
          kony.print("OTP::: 11");
          frmNewUserOnboardVerificationKA.lblInvalidCredentialsKA.setVisibility(true);
          kony.print("OTP::: 12");
          onTextChangeOTP(true);
          kony.print("OTP::: 13");
        }
      }
      else{
        customAlertPopup(geti18Value("i18n.common.alert"),geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
        kony.application.dismissLoadingScreen();
      }
    }

    function verifyOTPErrorCallback(response){
      
      customAlertPopup(geti18Value("i18n.common.alert"),  geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
      kony.application.dismissLoadingScreen();
    }
  }
  else { 
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}
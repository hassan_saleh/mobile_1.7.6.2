var CliQQRCodeAmount ="";
var CliQQRCodeMerchantName="";
var CliQQRCodeMerchantCity="";
var CliQQRCodebillingNum="";
var CliQQRCoderefId="";
var CliQQRCodeDateTime="";

function CliQQRCodeScanner_callback(crypto_TEXT){
    kony.print("crypto_TEXT ::"+JSON.stringify(crypto_TEXT));
	FrmCliQConfirmQR.lblCypherText.text = "";
	if (kony.sdk.isNetworkAvailable() && !isEmpty(crypto_TEXT)) {
      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      var inputPrams="";
      var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJEMVParse");
      //#ifdef android
      var encodedSring = encodeURIComponent(crypto_TEXT);
       inputPrams={
        "emvstring": encodedSring 
      };
      EMVString= encodedSring + ""; 
      kony.print("inputPrams for android "+EMVString);
      //#endif
      
      //#ifdef iphone
      var encodedSring1 = encodeURIComponent(crypto_TEXT.barcodestring);
       inputPrams={
        "emvstring":  encodedSring1 //encodedSring1.replace(/\//g, "\\/")
      };
      EMVString= encodedSring1 + ""; //encodedSring1.replace(/\//g, "\\/") + "";
      kony.print("inputPrams for iphone "+EMVString);
      //#endif
      kony.print("inputPrams for QRCode "+JSON.stringify(inputPrams));
      appMFConfiguration.invokeOperation("getEVMParse", {}, inputPrams, function(res){assign_CliQ_PARSED_DATA_TO_FORM(res,crypto_TEXT);}, function(err){kony.print("error ::"+JSON.stringify(err));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
  }else{
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}

function initiate_CliQQRCodeScanner(){
    try{
        QRCodeScanner.scanQRCode(CliQQRCodeScanner_callback);
    }catch(e){
        kony.print("Exception_initiate_QRCodeScanner ::"+e);
    }
}


function assign_CliQ_PARSED_DATA_TO_FORM(data, cypherText){
	try{
		kony.print("Parsed data ::"+JSON.stringify(data));
      	if(data.opstatus === 0 || data.opstatus === "0"){
          if(isEmpty(data.JomoPayData)){
             customAlertPopup("", kony.i18n.getLocalizedString("i18n.invalidJomopay"), popupCommonAlertDimiss, "");
                return;
          }
          var JomoPayParsedData=data.JomoPayData;
          if(!isEmpty(JomoPayParsedData)){
            kony.print("JomoPayParsedData "+JSON.stringify(JomoPayParsedData[0]));
            CliQQRCodeAmount=JomoPayParsedData[0].transactionAmount;
            CliQQRCodeMerchantName=JomoPayParsedData[0].merchantName;
            CliQQRCodeMerchantCity=JomoPayParsedData[0].merchantCity;
            CliQQRCodebillingNum=JomoPayParsedData[0].billingNum;
            CliQQRCoderefId=JomoPayParsedData[0].refId;
            CliQQRCodeDateTime=JomoPayParsedData[0].datetime;
            FrmCliQConfirmQR.lblCypherText.text = cypherText;
            kony.print("CliQQRCodeAmount "+CliQQRCodeAmount);
            kony.print("CliQQRCodeMerchantCity "+CliQQRCodeMerchantCity);
            kony.print("CliQQRCodeMerchantName "+CliQQRCodeMerchantName);
            kony.print("CliQQRCodebillingNum "+CliQQRCodebillingNum);
            kony.print("CliQQRCoderefId "+CliQQRCoderefId);
            kony.print("CliQQRCodeDateTime "+CliQQRCodeDateTime);
            if(!isEmpty(JomoPayParsedData[0].validation1) && !isEmpty(JomoPayParsedData[0].validation2) && !isEmpty(JomoPayParsedData[0].validation3)){
              if(kony.string.equalsIgnoreCase(JomoPayParsedData[0].validation1,"12") && kony.string.equalsIgnoreCase(JomoPayParsedData[0].validation2,"JOQR") && kony.string.equalsIgnoreCase(JomoPayParsedData[0].validation3,"JOMOP")){
                kony.print("valid:::::");
                CliQCliQRegisterDetailsService();
              }else{
                 customAlertPopup("", kony.i18n.getLocalizedString("i18n.invalidJomopay"), popupCommonAlertDimiss, "");
                return;
              }
            }else{
                customAlertPopup("", kony.i18n.getLocalizedString("i18n.invalidJomopay"), popupCommonAlertDimiss, "");
                return;
            }
            
          }else{
          	customAlertPopup("", kony.i18n.getLocalizedString("i18n.invalidJomopay"), popupCommonAlertDimiss, "");
            return;
          }
        }
    	
    }catch(e){
    	kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    	kony.print("Exception_assign_PARSED_DATA_TO_FORM ::"+e);
    }
}

//Type your code here
//jomopay using QRCode changes
function callCliQPaymentsSvc()
{
  kony.print(" In callCliQPaymentsSvc");
  var validateBalance = validate_SufficientAmount();
  if(!validateBalance){
    customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
  }
  else{
    var branchCode=FrmCliQConfirmQR.lblAccountType.text.substring(0,3);
    var fee=parseInt(0);
    kony.print("accountNumber and branchCode "+FrmCliQConfirmQR.lblAccountType.text+" and "+branchCode +" and fee "+fee);
    try{
      var inputParams = {
        "custId":custid,
        "fromAccountNumber":FrmCliQConfirmQR.lblAccountType.text,
        "SourceBranchCode":parseInt(branchCode),
        "jomopayType":"C",
        "toAccountName":JoMoPayQRCodeMerchantName,
        "amount":FrmCliQConfirmQR.lblAmount.text,
        "fees":fee,
        "transactionType":"700",
        "TransferFlag":"J",
        "p_extradata":FrmCliQConfirmQR.lblCypherText.text,
        "p_channel":"MOBILE",
        "p_user_id":"BOJMOB"
      };
      kony.print("Input Parameters ::"+JSON.stringify(inputParams));
      var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJPrConfirmJmpPayments");
      ShowLoadingScreen();
      if (kony.sdk.isNetworkAvailable()) {
        appMFConfiguration.invokeOperation("prConfirmJmpPayments", {},inputParams,success_callCliQPaymentsSvc,Error_callCliQPaymentsSvc);
      }
      else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
      }
    }catch(e){
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      kony.print("Exception_callJoMoPayPaymentsSvc ::"+e);
    }  
  }
}
function success_callCliQPaymentsSvc(res){
  //Successfully created record
  kony.print("EMV String is  "+EMVString);

  kony.sdk.mvvm.log.info("success saving record ", res);
  kony.print("Save Data FRMJOMOPAYCONFIRMATION SUCCESS ::"+JSON.stringify(res));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
   EMVString = EMVString.replace(/\\/g, "");
  
  if((res.opstatus === 0 || res.opstatus == "0")){
    if((res.referenceId !== undefined) && (res.referenceId !== null) && (res.referenceId !== "") && (res.ErrorCode === "70000" || res.ErrorCode === "00000")){
     // logObj[0] = custid; 
      logObj[0] = FrmCliQConfirmQR.lblAccountType.text;
        logObj[1] = FrmCliQConfirmQR.lblAccountType.text.substring(0,3);
        logObj[16] = res.referenceId; // reference number
      logObj[17] = "SUCCESS"; //status
      logObj[18] = "JoMoPay Transfer Successful";
      logObj[19]=EMVString;
      logObj[13]="QR_JOMOPAY";
      loggerCall();	
      if(check_JOMOPAYBENEFICIARY_EXIST()){
        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.jomopay.transfersuccessQr"),geti18Value("i18n.common.gotodashboard"),geti18nkey ("i18n.common.AccountDashboard"),geti18Value("i18n.common.gotojomopay"), "JoMo",geti18Value("i18n.jomopay.addasbene"),"addBeneJOMOPAY");
      }else{
        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.jomopay.transfersuccessQr"),geti18Value("i18n.common.gotodashboard"),geti18nkey ("i18n.common.AccountDashboard"),geti18Value("i18n.common.gotojomopay"), "JoMo");
      }
      frmCongratulations.show();
    }
    else
    {
      var errorCode = res.ErrorCode || res.code;
      logObj[16] = errorCode; // reference number
      logObj[17] = "FAILURE"; //status,
      logObj[19]=EMVString;
      logObj[13]="QR_JOMOPAY";
      kony.print("getErrorMessage(res.ErrorCode)"+getErrorMessage(errorCode));
      var message = getErrorMessage(errorCode);
      if(errorCode === "70003" || errorCode == 7003){
        message = geti18Value("i18n.jomopay.invalidmobilenumber");
      }
      if(isEmpty(message)){
        logObj[18] = message;
      }else if(message.length >200){
        logObj[18] = message.substring(0,200) ; // statuscomments
      }else{
        logObj[18] = message;
      }

      loggerCall();
      kony.boj.populateSuccessScreen("failure.png", getErrorMessage(errorCode), geti18Value("i18n.jomopay.transferfailed"),geti18Value("i18n.common.gotodashboard"),geti18nkey ("i18n.common.AccountDashboard"),geti18Value("i18n.common.gotojomopay"), "JoMo");
      //                   customAlertPopup("", message, popupCommonAlertDimiss, "");	
    }
  }else if(res.ErrorCode!==null&&res.ErrorCode!==undefined&&res.ErrorCode!==""&&(res.ErrorCode !== "70000" || res.ErrorCode !== "00000"))
  {
    kony.print("Having Error Code res.ErrorCode::"+res.ErrorCode);
    logObj[16] = res.ErrorCode; // reference number
    logObj[17] = "FAILURE"; //status,
    logObj[19]=EMVString;
    logObj[13]="QR_JOMOPAY";
    kony.print("getErrorMessage(res.ErrorCode)"+getErrorMessage(res.ErrorCode));
    var message = getErrorMessage(res.ErrorCode);
    if(res.ErrorCode === "70003" || res.ErrorCode == 7003){
      message = geti18Value("i18n.jomopay.invalidmobilenumber");
    }
    if(isEmpty(message)){
      logObj[18] = message;
    }else if(message.length >200){
      logObj[18] = message.substring(0,200) ; // statuscomments
    }else{
      logObj[18] = message;
    }
    loggerCall();
    kony.boj.populateSuccessScreen("failure.png", getErrorMessage(res.ErrorCode), geti18Value("i18n.jomopay.transferfailed"),geti18Value("i18n.common.gotodashboard"),geti18nkey ("i18n.common.AccountDashboard"),geti18Value("i18n.common.gotojomopay"), "JoMo");
    //               customAlertPopup("", message, popupCommonAlertDimiss, "");	
  }
  frmJoMoPay.destroy();
}

function Error_callJoMoPayPaymentsSvc(err) {
  //Handle error case
  kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
  kony.print("Save Data FRMJOMOPAYCONFIRMATION ERROR ::"+JSON.stringify(err));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  var errorCode = err.ErrorCode || err.code;
  logObj[16] = errorCode; // reference number
  logObj[17] = "FAILURE"; //status,
  logObj[19]=EMVString;
  logObj[13]="QR_JOMOPAY";
  kony.print("getErrorMessage(err.ErrorCode)"+getErrorMessage(errorCode));
  var message = getErrorMessage(errorCode);
  if(isEmpty(message)){
    logObj[18] = message;
  }else if(message.length >200){
    logObj[18] = message.substring(0,200) ; // statuscomments
  }else{
    logObj[18] = message;
  }
  loggerCall();
  kony.boj.populateSuccessScreen("failure.png", message, geti18Value("i18n.jomopay.transferfailed"),geti18Value("i18n.common.gotodashboard"),geti18nkey ("i18n.common.AccountDashboard"),geti18Value("i18n.common.gotojomopay"), "JoMo");
  frmJoMoPay.destroy();
  //           customAlertPopup("", message, popupCommonAlertDimiss, "");	
  var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
  kony.sdk.mvvm.log.error(exception.toString());
}
function validate_SufficientAmount(){
  try{
    var availableBal=gblJMPAccAmnt.toString().replace(/,/g,"");
    kony.print("availableBal "+availableBal);
    var bal = parseFloat(availableBal);
    var amountValue= frmJoMoPayQRConfirm.lblAmount.text;
    amountValue = amountValue.toString().replace(/,/g,"");
    kony.print("jomopay amount is "+amountValue);
    kony.print("account balance is "+bal);
    if(parseFloat(amountValue) <= bal)
      return true;
    else
      return false;
  }catch(e){
    kony.print("Exception_validate_SufficientAmount ::"+e);
  }
}
function CliQCliQRegisterDetailsService()
{
  kony.print("in CliQCliQRegisterDetailsService");
  try{
    var Language = kony.store.getItem("langPrefObj");
    Language = Language.toUpperCase();
    kony.print("Language "+Language);
    var requestObj = {
      "custId": custid,
      "lang":Language
    };
    kony.print("Input Parameters ::"+JSON.stringify(requestObj));
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprJmpGetRegisterDetails");
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    if (kony.sdk.isNetworkAvailable()) {
      appMFConfiguration.invokeOperation("prJmpGetRegisterDetails", {},requestObj,CliQRegisterDetailsServiceSuccessCallBack,CliQRegisterDetailsServiceFailureCallBack);}
    else {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
    }
  }catch(e){
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("Exception_CliQCliQRegisterDetailsService::"+e);
  }
}
function CliQRegisterDetailsServiceSuccessCallBack(res){
  try{
    kony.print("CliQRegisterDetailsServiceSuccessCallBack ::"+JSON.stringify(res));
    if(res.opstatus === 0 || res.opstatus === "0"){
      var jmpRegistrationData=res.jmpRegistrationDetails;
      if((jmpRegistrationData !== undefined) && (jmpRegistrationData !== null) && (jmpRegistrationData !== "")){
        kony.print("jmpRegistrationData "+JSON.stringify(jmpRegistrationData[0])); 
        jmpRegAccountNumber=jmpRegistrationData[0].accno;
        kony.print("jmpRegAccountNumber "+jmpRegAccountNumber);
        
        kony.print("jmpRegAccountNumber.substring(3) "+jmpRegAccountNumber.substring(3));
        
        gblJMPAccAmnt = "";
        
        if(!isEmpty(kony.retailBanking.globalData.accountsDashboardData.fromAccounts)){
        var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
            //kony.print("fromAccounts "+JSON.stringify(fromAccounts));
        //var accountNumber = fromAccounts[0].accountID;
          for(var i=0; i<fromAccounts.length; i++){
             //kony.print("fromAccounts "+fromAccounts[i].accountID+"");
             //kony.print("fromAccounts "+(jmpRegAccountNumber.substring(3)+"").indexOf(fromAccounts[i].accountID+"") != -1);
            if((jmpRegAccountNumber.substring(3)+"").indexOf(fromAccounts[i].accountID+"") != -1){
              gblJMPAccAmnt = fromAccounts[i].availableBalance;
              if(kony.store.getItem("langPrefObj") == "ar"){
               frmJoMoPayQRConfirm.lblAccBal.text =fromAccounts[i]["currencyCode"] + " " +formatamountwithCurrency(gblJMPAccAmnt, fromAccounts[i]["currencyCode"]);
               
              }else{
              FrmCliQConfirmQR.lblAccBal.text =formatamountwithCurrency(gblJMPAccAmnt, fromAccounts[i]["currencyCode"]) + " "+fromAccounts[i]["currencyCode"];
              }
                break;
            }
          }
        }

        kony.print("gblJMPAccAmnt "+gblJMPAccAmnt);
        
        FrmCliQConfirmQR.lblAccountType.text=jmpRegAccountNumber;
        kony.print("1");
        FrmCliQConfirmQR.lblMerchantName.text=CliQQRCodeMerchantName+" - "+CliQQRCodeMerchantCity;
        kony.print("2");
        FrmCliQConfirmQR.lblBillNumber.text=CliQQRCodebillingNum;
        kony.print("3");
        FrmCliQConfirmQR.lblAmount.text=CliQQRCodeAmount;
        kony.print("4");
        FrmCliQConfirmQR.lblTransactionRef.text=CliQQRCoderefId;
        kony.print("5");
        FrmCliQConfirmQR.lblDateandTime.text=CliQQRCodeDateTime;
        kony.print("6");
        if(FrmCliQConfirmQR === undefined || CliQQRCodeAmount === null || CliQQRCodeAmount === "")
          {
            FrmCliQConfirmQR.btnConfirm.setVisibility(false);
            FrmCliQConfirmQR.lblStaticJOD.setVisibility(false);
          }
        else
          {
            FrmCliQConfirmQR.btnConfirm.setVisibility(true);
            FrmCliQConfirmQR.lblStaticJOD.setVisibility(true);
          }
        FrmCliQConfirmQR.show();
      }
    }
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }catch(e){
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("Exception_JmpRegisterDetailsServiceSuccessCallBack ::"+e);
  }
}
function CliQRegisterDetailsServiceFailureCallBack(err) {
  customAlertPopup(geti18Value("i18n.Bene.Failed"),
                   geti18Value("i18n.jomopayAccNumberservFailes"),
                   popupCommonAlertDimiss, "");
  kony.application.dismissLoadingScreen();
}

//end
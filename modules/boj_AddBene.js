//Created and edited by Arpan

kony = kony || {};
kony.boj = kony.boj || {};
var isAddAsTrusted = false;
//Nart Rawashdeh 20/05/2021<<
var lengthCheckFullName;
var remainingText;
var spaceChar;
var nameSyllablesValidation;
//>>
kony.boj.addBeneVar = {
  "selectedType": "BOJ",
  "BOJ":{
    "title": "IFB",
    "flexHide":[
      "flxAddressBene",
      "flxCityName",
      "flxSwiftCode",
      "flxCountryName",
      "flxBankName",
      "flxBankNameInt",
      "flxCityBank",
      "flxCountryBankDetails"
    ],
    "flexUnHide":[
      "flxBankBranch"
    ],
    //"fieldRequired":[
    //       //[TextBoxName, Regular Expression to check, error Flex, Error to be shown, Value stored in the field, length check if any(comma seperated)]
    //       ["txtBenficiryNameKA", "^[a-zA-Z ]+$", "flxUnderlineBenficiryNameKA", geti18Value("i18n.Bene.FullNameError"), ""], 
    //       ["txtBenficiryNickName", "^[a-zA-Z ]+$", "flxUnderlineBenficiryNickName", geti18Value("i18n.Bene.FullNameError"), ""], 
    //       ["tbxAddressBene", "", "flxUnderlineAddressBene", "", ""],
    //       ["tbxCity", "", "flxUnderlineCityName", "", ""],
    //       ["externalAccountNumberTextField", "^[A-Z0-9]+$", "flxUnderlineAccountNumber", geti18Value("i18n.Bene.AccNumError"), ""], 
    //       ["txtSwiftCodeKA", "", "flxUnderlineSwiftCode", "", "", "8,11"],
    //       ["tbxCountry", "", "flxUnderlineCountryName", "", ""],
    //       ["tbxBankNameInt", "", "flxUnderlineBankName", "", ""],
    //       ["tbxBankBranch", "^[0-9]+$", "flxUnderlineBankBranch", geti18Value("i18n.Bene.BankBranchError"), ""],
    //       ["tbxCityBank", "", "flxUnderlineCityBank", "", ""],
    //       ["tbxAddressBank", "", "flxUnderlineAddressBank", "", ""],
    //       ["tbxEmail", "", "flxUnderlineEmail", "", ""]
    //     ]
  },
  "DOM":{
    "title": "DTB",
    "flexHide":[
      "flxCityName",
      "flxSwiftCode",
      "flxCountryName",
      "flxBankNameInt",
      "flxBankBranch",
      "flxCityBank",
      "flxCountryBankDetails"
    ],
    "flexUnHide":[
      "flxAddressBene",
      "flxBankName"
    ],
    //     "fieldRequired":[      
    //       ["txtBenficiryNameKA", "^[a-zA-Z ]+$", "flxUnderlineBenficiryNameKA", geti18Value("i18n.Bene.FullNameError"), ""], 
    //       ["txtBenficiryNickName", "^[a-zA-Z ]+$", "flxUnderlineBenficiryNickName", geti18Value("i18n.Bene.FullNameError"), ""], 
    //       ["tbxAddressBene", ".", "flxUnderlineAddressBene", geti18Value("i18n.Bene.AddrError"), ""],
    //       ["tbxCity", "", "flxUnderlineCityName", "", ""],
    //       ["externalAccountNumberTextField", "^[A-Z0-9]+$", "flxUnderlineAccountNumber", geti18Value("i18n.Bene.AccNumError"), ""], 
    //       ["txtSwiftCodeKA", "^[A-Z0-9]+$", "flxUnderlineSwiftCode", "", ""],
    //       ["tbxCountry", "", "flxUnderlineCountryName", "", ""],
    //       ["tbxBankNameInt", "^[A-Z/ ]+$", "flxUnderlineBankName", geti18Value("i18n.Bene.BankNameError"), ""],
    //       ["tbxBankBranch", "", "flxUnderlineBankBranch", "", ""],
    //       ["tbxCityBank", "", "flxUnderlineCityBank", "", ""],
    //       ["tbxAddressBank", "", "flxUnderlineAddressBank", "", ""],
    //       ["tbxEmail", "", "flxUnderlineEmail", "", ""]
    //     ]
  },
  "INT":{
    "title": "ITB",
    "flexHide":[
      "flxBankBranch",
      "flxBankName",
    ],
    "flexUnHide":[
      "flxAddressBene",
      "flxCityName",
      "flxSwiftCode",
      "flxCountryName",
      "flxBankNameInt",
      "flxCityBank",
      "flxCountryBankDetails",
    ],
    //     "fieldRequired":[
    //       ["txtBenficiryNameKA", "^[a-zA-Z ]+$", "flxUnderlineBenficiryNameKA", geti18Value("i18n.Bene.FullNameError"), ""], 
    //       ["txtBenficiryNickName", "^[a-zA-Z ]+$", "flxUnderlineBenficiryNickName", geti18Value("i18n.Bene.FullNameError"), ""], 
    //       ["tbxAddressBene", ".", "flxUnderlineAddressBene", geti18Value("i18n.Bene.AddrError"), ""],
    //       ["tbxCity", "^[0-9]+$", "flxUnderlineCityName", geti18Value("i18n.Bene.CityError"), ""],
    //       ["externalAccountNumberTextField", "^[A-Z0-9]+$", "flxUnderlineAccountNumber", geti18Value("i18n.Bene.AccNumError"), ""], 
    //       ["txtSwiftCodeKA", "^[A-Z0-9]+$", "flxUnderlineSwiftCode", geti18Value("i18n.Bene.BankSWIFTError"), "", "8,11"],
    //       ["tbxCountry", "^[0-9]+$", "flxUnderlineCountryName", geti18Value("i18n.Bene.CountryError"), ""],
    //       ["tbxBankNameInt", "^[a-zA-Z ]+$", "flxUnderlineBankNameInt", geti18Value("i18n.Bene.BankNameError"), ""],
    //       ["tbxBankBranch", "", "flxUnderlineBankBranch", "", ""],
    //       ["tbxCityBank", "^[0-9]+$", "flxUnderlineCityBank", geti18Value("i18n.Bene.CityError"), ""],
    //       ["tbxAddressBank", ".+", "flxUnderlineAddressBank", geti18Value("i18n.Bene.AddrError"), ""],
    //       ["tbxEmail", "", "flxUnderlineEmail", "", ""]
    //     ]
  },
};

kony.boj.addBeneficiaryAnimation = function(lblName, animationType){
  var list = [
    {
      "label":"lblBenficiryNameKA", 
      "txtbox":"txtBenficiryNameKA"
    },{
      "label":"lblBenficiryNickName",
      "txtbox":"txtBenficiryNickName"
    },{
      "label":"lblAddressBene",
      "txtbox":"tbxAddressBene"
    },{
      "label":"lblAccountNumber",
      "txtbox":"externalAccountNumberTextField"
    },{
      "label":"lblSwiftCode",
      "txtbox":"txtSwiftCodeKA"
    },{
      "label":"lblAddressBank",
      "txtbox":"tbxAddressBank"
    },{
      "label":"lblBankNameInt",
      "txtbox":"tbxBankNameInt"
    },{
      "label":"lblCityName",
      "txtbox":"tbxCity"
    },{
      "label":"lblCityBank",
      "txtbox":"tbxCityBank"
    }
  ];

  if(animationType === "reverse"){
    for(var i in list){
      if(frmAddExternalAccountKA[list[i].txtbox].text === "")
        animateLabel("DOWN", list[i].label, frmAddExternalAccountKA[list[i].txtbox].text);
      else
        animateLabel("UP", list[i].label, frmAddExternalAccountKA[list[i].txtbox].text);
    }
  }
  else{
    for(var i in list){
      kony.print(list[i].label + "---" + list[i].txtbox + "---" + lblName);
      if(list[i].label === lblName){
        //kony.print("lblName::"+lblName);
        if(frmAddExternalAccountKA[lblName].top != "15%")
          animateLabel("UP", list[i].label, frmAddExternalAccountKA[list[i].txtbox].text);
      }
      /*else
        animateLabel("DOWN", list[i].label, frmAddExternalAccountKA[list[i].txtbox].text);*/
    }
  }

};

kony.boj.displayParameter = function(selectedType){
  kony.boj.hideInline();
  kony.boj.addBeneVar.selectedType = selectedType;
  frmAddExternalAccountKA.lblAccountTypeKA.text = selectedType;//kony.boj.addBeneVar[selectedType].title;
  var list = {};
  if(selectedType === "BOJ"){
    frmAddExternalAccountKA.btnBOJ.skin = "slButtonWhiteTab";
    frmAddExternalAccountKA.btnDomestic.skin = "slButtonWhiteTabDisabled";
    frmAddExternalAccountKA.btnInternational.skin = "slButtonWhiteTabDisabled";
    list = kony.boj.addBeneVar.BOJ;
    frmAddExternalAccountKA.lblAccountNumber.text = geti18Value("i18n.Bene.Accountnumber");
    frmAddExternalAccountKA.externalAccountNumberTextField.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
  }
  else if(selectedType === "DOM"){
    gblLaunchModeOBJ.lauchMode = false;
    frmAddExternalAccountKA.btnBOJ.skin = "slButtonWhiteTabDisabled";
    frmAddExternalAccountKA.btnDomestic.skin = "slButtonWhiteTab";
    frmAddExternalAccountKA.btnInternational.skin = "slButtonWhiteTabDisabled";
    list = kony.boj.addBeneVar.DOM;
    frmAddExternalAccountKA.lblAccountNumber.text = geti18Value("i18n.common.IBAN");
    frmAddExternalAccountKA.externalAccountNumberTextField.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
    frmAddExternalAccountKA.externalAccountNumberTextField.autoCapitalize = constants.TEXTBOX_AUTO_CAPITALIZE_ALL;
  }
  else if(selectedType === "INT"){
    gblLaunchModeOBJ.lauchMode = false;
    frmAddExternalAccountKA.btnBOJ.skin = "slButtonWhiteTabDisabled";
    frmAddExternalAccountKA.btnDomestic.skin = "slButtonWhiteTabDisabled";
    frmAddExternalAccountKA.btnInternational.skin = "slButtonWhiteTab";
    list = kony.boj.addBeneVar.INT;
    frmAddExternalAccountKA.externalAccountNumberTextField.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
    frmAddExternalAccountKA.externalAccountNumberTextField.autoCapitalize = constants.TEXTBOX_AUTO_CAPITALIZE_ALL;
    frmAddExternalAccountKA.lblAccountNumber.text = geti18Value("i18n.common.IBAN")+" / "+geti18Value("i18n.common.accountNumber");
  }

  for(var i in list.flexHide){
    frmAddExternalAccountKA[list.flexHide[i]].isVisible = false;
  }

  for(i in list.flexUnHide){
    frmAddExternalAccountKA[list.flexUnHide[i]].isVisible = true;
  }

};

kony.boj.checkAddBeneValidation = function(){
  var selectedType = kony.boj.addBeneVar.selectedType;
  var checkList =  kony.boj.addBeneVar[selectedType].fieldRequired;
  var count = 0, inline = "", tempValue = "";
  kony.boj.hideInline();

  for(var i in checkList){
    inline = checkList[i][0].toString() + "Inline";
    if(checkList[i][1] !== "" && frmAddExternalAccountKA[checkList[i][0]].text !== ""){
      //     	frmAddExternalAccountKA[checkList[i][0]].text = frmAddExternalAccountKA[checkList[i][0]].text.replace(checkList[i][1],"");
      if(new RegExp(checkList[i][1]).test(frmAddExternalAccountKA[checkList[i][0]].text)){
        frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
        frmAddExternalAccountKA[inline].isVisible = true;
        frmAddExternalAccountKA[inline].text = checkList[i][3];
        kony.print("1");
        count++;
      }else{
        frmAddExternalAccountKA[inline].isVisible = false;
      }
      tempValue = frmAddExternalAccountKA[checkList[i][0]].text.replace(/\s/g, "");
      if(new RegExp(checkList[i][1]).test(tempValue) === false){
        switch(checkList[i][0]){
          case "externalAccountNumberTextField":
            if(checkList[i][5] !== "" &&  tempValue !== ""){
              if(parseInt(tempValue.length) <= parseInt(checkList[i][5])){
                if(selectedType === "DOM" && parseInt(tempValue.length) > 1){
                  if(tempValue.match(/^JO/g) === null ||
                     tempValue.match(/^JO/g) === ""){
                    frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
                    frmAddExternalAccountKA[inline].text = checkList[i][6];
                    frmAddExternalAccountKA[inline].isVisible = true;
                    kony.print("2");
                    count++; 
                  }else{
                    if(parseInt(tempValue.length) == parseInt(checkList[i][5])){
                      frmAddExternalAccountKA[inline].isVisible = false;
                      frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine"; 
                    }else{
                      frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
                      frmAddExternalAccountKA[inline].text = checkList[i][6];
                      frmAddExternalAccountKA[inline].isVisible = true;
                      kony.print("33");
                      count++;  
                    }
                  }
                }else if(selectedType === "INT" && tempValue.length > 1){ // hassan BMA-2106
                  kony.print("tempValue ::"+tempValue.match(/^[A-Z]{2}/g));
                  if(tempValue.match(/^[A-Z]{2}/g)){
                    kony.print("hassan tempValue.length "||parseInt(tempValue.length));
                    kony.print("hassan checkList[i][4].length "||parseInt(checkList[i][4].length));
                    if(((parseInt(tempValue.length) > parseInt(checkList[i][4].length)) && (parseInt(tempValue.length) > 4) && (((parseInt(tempValue.length)-1)%4)===0)))
                    {
                      frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
                      frmAddExternalAccountKA[inline].isVisible = true;
                      frmAddExternalAccountKA[inline].text = checkList[i][6];
                      kony.print("3");
                      kony.print("hassan 111112222");
                      count++;
                    }
                  }
                }else if(selectedType === "BOJ" && (parseInt(frmAddExternalAccountKA[checkList[i][0]].text.length) == parseInt(checkList[i][5]))){
                  //1941 fix
                  var benificiaryNo=frmAddExternalAccountKA[checkList[i][0]].text.substring(3,6);
                  if(benificiaryNo == parseInt(320)|| benificiaryNo == parseInt(186))
                  {
                    frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
                    frmAddExternalAccountKA[inline].isVisible = true;
                    frmAddExternalAccountKA[inline].text = checkList[i][7];
                    count++;
                  }
                  //end
                  else
                  {
                    frmAddExternalAccountKA[inline].isVisible = false;
                    frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine";
                  } 
                }else if(selectedType === "INT"){

                }else{
                  frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
                  frmAddExternalAccountKA[inline].isVisible = true;
                  frmAddExternalAccountKA[inline].text = checkList[i][6];
                  kony.print("3");
                  count++;
                }
              }else{
                frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
                frmAddExternalAccountKA[inline].isVisible = true;
                frmAddExternalAccountKA[inline].text = checkList[i][6];
                kony.print("3");
                count++;
              }
            }
            break;
          case "txtSwiftCodeKA":
            var lengthCheck = checkList[i][5].split(",");
            if((parseInt(lengthCheck[0]) <= frmAddExternalAccountKA[checkList[i][0]].text.length)){// || (parseInt(lengthCheck[1]) == frmAddExternalAccountKA[checkList[i][0]].text.length)){
              frmAddExternalAccountKA[inline].isVisible = false;
              frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine";
            }else{
              frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
              frmAddExternalAccountKA[inline].isVisible = true;
              frmAddExternalAccountKA[inline].text = checkList[i][6];
              kony.print("4");
              count++;
            }
            break;
        }
      }
      //else{
      //           frmAddExternalAccountKA[inline].isVisible = false;
      //         }
    }
    //       continue;
    //     alert(checkList[i][0] + "---" + frmAddExternalAccountKA[checkList[i][0]].text);
    if(frmAddExternalAccountKA[checkList[i][0]].text === undefined || frmAddExternalAccountKA[checkList[i][0]].text === null || frmAddExternalAccountKA[checkList[i][0]].text === ""){
      frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreyLine";
      kony.print("5");
      count++;
    }else if(checkList[i][1] === "" && frmAddExternalAccountKA[checkList[i][0]].text !== ""){
      frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine";
      checkList[i][4] = frmAddExternalAccountKA[checkList[i][0]].text.replace(/\s/g,"");
    }
    else if(!(new RegExp(checkList[i][1]).test(frmAddExternalAccountKA[checkList[i][0]].text)))
      frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine";
    checkList[i][4] = frmAddExternalAccountKA[checkList[i][0]].text.replace(/\s/g,"");
  }
  if(count > 0){
    frmAddExternalAccountKA.lblNext.skin = "sknLblNextDisabled";
    return false;
  }
  frmAddExternalAccountKA.lblNext.skin = "sknLblNextEnabled";
  return true;
};

kony.boj.restoreDetails = function(){
  var selectedType = kony.boj.addBeneVar.selectedType;
  var checkList =  kony.boj.addBeneVar[selectedType].fieldRequired;

  for(var i in checkList){
    frmAddExternalAccountKA[checkList[i][0]].text = "";//checkList[i][4];
    switch(checkList[i][0]){
      case "tbxBankBranch":
        frmAddExternalAccountKA.lblBankBranch.skin = "sknLblNextDisabled";
        frmAddExternalAccountKA.lblBankBranch.text = "";//geti18Value("i18n.Bene.BankBranch");
        break;
      case "tbxCountry":
        frmAddExternalAccountKA.lblCountryNameKA.skin = "sknLblNextDisabled";
        frmAddExternalAccountKA.lblCountryNameKA.text = "";// geti18Value("i18n.NUO.Country");
        break;
      case "tbxBankNameInt":
        frmAddExternalAccountKA.lblBankName.skin = "sknLblNextDisabled";
        frmAddExternalAccountKA.lblBankName.text = "";//geti18Value("i18n.Bene.Bankname");
        break;
      case "txtCountryBankDetails":
        frmAddExternalAccountKA.lblCountryBankDetails.skin = "sknLblNextDisabled";
        frmAddExternalAccountKA.lblCountryBankDetails.text = "";//geti18Value("i18n.myprofile.Country");
        break;
    }
  }
  frmAddExternalAccountKA.flxUnderlineAccountNumber.skin = "sknFlxGreyLine";
  frmAddExternalAccountKA.lblBankNameStat.setVisibility(false);
  frmAddExternalAccountKA.lblCountryTitleStat.setVisibility(false);
  frmAddExternalAccountKA.lblBankCountryStat.setVisibility(false);
  frmAddExternalAccountKA.lblBankBranchStat.setVisibility(false);
  frmAddExternalAccountKA.lblCountryNameKA.text = kony.i18n.getLocalizedString("i18n.NUO.Country");
  frmAddExternalAccountKA.lblBankName.text = kony.i18n.getLocalizedString("i18n.Bene.Bankname")
  frmAddExternalAccountKA.lblBankBranch.text = kony.i18n.getLocalizedString("i18n.Bene.BankBranch")
  frmAddExternalAccountKA.lblCountryBankDetails.text = kony.i18n.getLocalizedString("i18n.NUO.Country")
  kony.boj.addBeneficiaryAnimation("", "reverse");
  kony.boj.addBeneNextStatus();
};

kony.boj.clearEnteredData = function(){
  kony.boj.addBeneVar.BOJ.fieldRequired = [
    ["txtBenficiryNameKA", "", "flxUnderlineBenficiryNameKA", geti18Value("i18n.Bene.FullNameError"), "", "1", geti18Value("i18n.sendMoney.fullNameLengthHint")], //Nart Rawashdeh (JIRA BMA-2233) 
    ["txtBenficiryNickName", "", "flxUnderlineBenficiryNickName", geti18Value("i18n.Bene.FullNameError"), ""], 
    ["externalAccountNumberTextField", /[^0-9]/g, "flxUnderlineAccountNumber", geti18Value("i18n.Bene.AccNumError"), "","16", geti18Value("i18n.Bene.AccNumError"),geti18Value("i18n.AddBene.AccNumError")], 
    ["tbxBankBranch", "", "flxUnderlineBankBranch", geti18Value("i18n.Bene.BankBranchError"), ""]
  ];

  kony.boj.addBeneVar.DOM.fieldRequired = [      
    ["txtBenficiryNameKA", /[^A-Za-z0-9\s]/g, "flxUnderlineBenficiryNameKA", geti18Value("i18n.common.englishonly"), "", "1", geti18Value("i18n.sendMoney.fullNameLengthHint")], //Nart Rawashdeh (JIRA BMA-2233)
    ["txtBenficiryNickName", "", "flxUnderlineBenficiryNickName", geti18Value("i18n.Bene.FullNameError"), ""], 
    ["tbxAddressBene", /[^A-Za-z0-9,.#:\-\s]/g, "flxUnderlineAddressBene", geti18Value("i18n.common.englishonly"), ""],
    ["externalAccountNumberTextField", /[^A-Za-z0-9\s]/g, "flxUnderlineAccountNumber", geti18Value("i18n.common.englishonly"), "","30", geti18Value("i18n.beneficiary.invalidIBAN")], 
    ["tbxBankNameInt", "", "flxUnderlineBankName", geti18Value("i18n.Bene.BankNameError"), ""]
  ];

  kony.boj.addBeneVar.INT.fieldRequired = [
    ["txtBenficiryNameKA", /[^A-Za-z0-9\s]/g, "flxUnderlineBenficiryNameKA", geti18Value("i18n.common.englishonly"), "", "1", geti18Value("i18n.sendMoney.fullNameLengthHint")], //Nart Rawashdeh (JIRA BMA-2233)
    ["txtBenficiryNickName", "", "flxUnderlineBenficiryNickName", geti18Value("i18n.Bene.FullNameError"), ""], 
    ["tbxAddressBene", /[^A-Za-z0-9,.#:\-\s]/g, "flxUnderlineAddressBene", geti18Value("i18n.common.englishonly"), ""],
    ["tbxCity", /[^A-Za-z0-9,.#:\s]/g, "flxUnderlineCityName", geti18Value("i18n.common.englishonly"), ""],
    ["externalAccountNumberTextField", /[^A-Za-z0-9\s]/g, "flxUnderlineAccountNumber", geti18Value("i18n.common.englishonly"), "", "34"], 
    ["txtSwiftCodeKA", /[^A-Za-z0-9]/g, "flxUnderlineSwiftCode", geti18Value("i18n.common.englishonly"), "", "8,11",geti18Value("i18n.Bene.BankSWIFTError")],
    ["tbxCountry", "", "flxUnderlineCountryName", geti18Value("i18n.Bene.CountryError"), ""],
    ["tbxBankNameInt", /[^A-Za-z0-9,.#:\s]/g, "flxUnderlineBankNameInt", geti18Value("i18n.common.englishonly"), ""],
    ["tbxCityBank", /[^A-Za-z0-9,.#:\s]/g, "flxUnderlineCityBank", geti18Value("i18n.common.englishonly"), ""],
    ["txtCountryBankDetails", "", "lblCountryBankDetailsUnderLine", geti18Value("i18n.Bene.CountryError"), ""]
  ];
};

kony.boj.addBeneNextStatus = function(){
  var selectedType = kony.boj.addBeneVar.selectedType;
  kony.print("selectedType ::"+selectedType);
  var checkList =  kony.boj.addBeneVar[selectedType].fieldRequired;
  var count = 0, tempValue = "";
  kony.print("checkList ::"+JSON.stringify(checkList));
  var inline = "";
  for(var i in checkList){
    inline = checkList[i][0].toString() + "Inline";
    if(checkList[i][1] !== "" && frmAddExternalAccountKA[checkList[i][0]].text !== ""){
      //     	frmAddExternalAccountKA[checkList[i][0]].text = frmAddExternalAccountKA[checkList[i][0]].text.replace(checkList[i][1],"");
      kony.print("000000");
      if(new RegExp(checkList[i][1]).test(frmAddExternalAccountKA[checkList[i][0]].text)){
        frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
        frmAddExternalAccountKA[inline].isVisible = true;
        frmAddExternalAccountKA[inline].text = checkList[i][3];
        kony.print("1");
        count++;
      }else{
        kony.print("00000011");
        frmAddExternalAccountKA[inline].isVisible = false;
      }
      if(frmAddExternalAccountKA[checkList[i][0]].text === undefined || frmAddExternalAccountKA[checkList[i][0]].text === null || frmAddExternalAccountKA[checkList[i][0]].text === ""){
        frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreyLine";
        kony.print("5");
        count++;
      }else if(checkList[i][1] === "" && frmAddExternalAccountKA[checkList[i][0]].text !== ""){
        frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine";
        kony.print("2222222");
        checkList[i][4] = frmAddExternalAccountKA[checkList[i][0]].text;
      }
      else if(!(new RegExp(checkList[i][1]).test(frmAddExternalAccountKA[checkList[i][0]].text)))
        frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine";
      kony.print("4444444");
      tempValue = frmAddExternalAccountKA[checkList[i][0]].text.replace(/\s/g, "");
      kony.print("value ::"+tempValue+" --- "+(new RegExp(checkList[i][1]).test(tempValue)));
      if(new RegExp(checkList[i][1]).test(tempValue) === false){
        switch(checkList[i][0]){
          case "externalAccountNumberTextField":
            if(checkList[i][5] !== "" &&  tempValue !== ""){
              kony.print("value ::"+tempValue+" --- "+(parseInt(tempValue.length) <= parseInt(checkList[i][5])));
              if(parseInt(tempValue.length) <= parseInt(checkList[i][5])){
                if(selectedType === "DOM" && tempValue.length > 1){
                  if(tempValue.match(/^JO/g) === null ||
                     tempValue.match(/^JO/g) === ""){
                    frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
                    frmAddExternalAccountKA[inline].text = checkList[i][6];
                    frmAddExternalAccountKA[inline].isVisible = true;
                    kony.print("2");
                    count++; 
                  }else{
                    if(parseInt(tempValue.length) == parseInt(checkList[i][5])){
                      frmAddExternalAccountKA[inline].isVisible = false;
                      frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine";
                    }else{
                      frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
                      frmAddExternalAccountKA[inline].text = checkList[i][6];
                      frmAddExternalAccountKA[inline].isVisible = true;
                      kony.print("33");
                      count++;  
                    }
                  }
                  if(tempValue.match(/BJOR/g)){
                    customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.addbene.BOJBene"), popupCommonAlertDimiss, "");
                    count++;
                    break;
                  }
                  kony.print("checklist value ::"+checkList[i][4]+" :: form value ::"+tempValue+" :: calculated value ::"+((parseInt(tempValue.length)-1)%4));
                  if((parseInt(tempValue.length) > parseInt(checkList[i][4].length)) && (parseInt(tempValue.length) > 4) && (((parseInt(tempValue.length)-1)%4)==0))
                    frmAddExternalAccountKA[checkList[i][0]].text = format_IBAN_VALUE(tempValue);
                }else if(selectedType === "INT" && tempValue.length > 1){
                  kony.print("tempValue ::"+tempValue.match(/^[A-Z]{2}/g));
                  if(tempValue.match(/^JO/g)){
                    customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.addbene.JOBene"), popupCommonAlertDimiss, "");
                    count++;
                    break;
                  }else if(tempValue.match(/^[A-Z]{2}/g)){
                    if((parseInt(tempValue.length) > parseInt(checkList[i][4].length)) && (parseInt(tempValue.length) > 4) && (((parseInt(tempValue.length)-1)%4)==0))
                      frmAddExternalAccountKA[checkList[i][0]].text = format_IBAN_VALUE(tempValue);
                  }
                }else if(selectedType === "BOJ" && (parseInt(tempValue.length) == parseInt(checkList[i][5]))){
                  //1941 fix
                  var benificiaryNo=frmAddExternalAccountKA[checkList[i][0]].text.substring(3,6);
                  if(benificiaryNo == parseInt(320)|| benificiaryNo == parseInt(186))
                  {
                    frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
                    frmAddExternalAccountKA[inline].isVisible = true;
                    frmAddExternalAccountKA[inline].text = checkList[i][7];
                    count++;
                  }
                  //end
                  else
                  {
                    frmAddExternalAccountKA[inline].isVisible = false;
                    frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine";
                  }
                }else{
                  frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
                  frmAddExternalAccountKA[inline].isVisible = true;
                  frmAddExternalAccountKA[inline].text = checkList[i][6];

                  count++;
                }
              }else{
                frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
                frmAddExternalAccountKA[inline].isVisible = true;
                frmAddExternalAccountKA[inline].text = checkList[i][6];
                kony.print("3");
                count++;
              }
            }
            break;
          case "txtSwiftCodeKA":
            var lengthCheck = checkList[i][5].split(",");
            if((parseInt(lengthCheck[0]) <= frmAddExternalAccountKA[checkList[i][0]].text.length)) { //|| (parseInt(lengthCheck[1]) == frmAddExternalAccountKA[checkList[i][0]].text.length)){
              frmAddExternalAccountKA[inline].isVisible = false;
              frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine";
            }else{
              frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
              frmAddExternalAccountKA[inline].isVisible = true;
              frmAddExternalAccountKA[inline].text = checkList[i][6];
              kony.print("4");
              count++;
            }
            break;
            //Nart Rawashdeh (JIRA BMA-2233) 16/05/2021<<
          case "txtBenficiryNameKA":
            spaceChar = "";
            remainingText = "";
            nameSyllablesValidation = "";

            lengthCheckFullName = checkList[i][5];//1
            remainingText = frmAddExternalAccountKA[checkList[i][0]].text;

            kony.print("Count Before Loop: " + count);
            kony.print("Space Char Before Loop: " + spaceChar);
            kony.print("Remaining Text Before Loop: " + remainingText);
            kony.print("Name Syllables Validation Before Loop: " + nameSyllablesValidation);

            for (var x = 0; x <= remainingText.length + 1; x++){
              spaceChar = remainingText.trim();
              spaceChar = spaceChar.search(" ");
              if (spaceChar !== -1){
                remainingText = remainingText.substring((spaceChar + 1), remainingText.length);
                nameSyllablesValidation++;
              }

              kony.print("Count In Loop: " + count);
              kony.print("Space Char In Loop: " + spaceChar);
              kony.print("Remaining Text In Loop: " + remainingText);
              kony.print("Name Syllables Validation In Loop: " + nameSyllablesValidation);

            }

            kony.print("Count After Loop: " + count);
            kony.print("Space Char After Loop: " + spaceChar);
            kony.print("Remaining Text After Loop: " + remainingText);
            kony.print("Name Syllables Validation After Loop: " + nameSyllablesValidation);

            if (nameSyllablesValidation >= parseInt(lengthCheckFullName)){
              frmAddExternalAccountKA[inline].setVisibility(false);
              frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine";
            }else{
              frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxOrangeLine";
              frmAddExternalAccountKA[inline].text = checkList[i][6];
              frmAddExternalAccountKA[inline].setVisibility(true);
              count++;
            }
            break;
            //>>
        }
      }
      //else{
      //           frmAddExternalAccountKA[inline].isVisible = false;
      //         }
    }else if(frmAddExternalAccountKA[checkList[i][0]].text === undefined || frmAddExternalAccountKA[checkList[i][0]].text === null || frmAddExternalAccountKA[checkList[i][0]].text === ""){
      frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreyLine";
      count++;
    } else if(checkList[i][1] === "" && frmAddExternalAccountKA[checkList[i][0]].text !== ""){
      frmAddExternalAccountKA[checkList[i][2]].skin = "sknFlxGreenLine";
      kony.print("2222222");
      checkList[i][4] = frmAddExternalAccountKA[checkList[i][0]].text.replace(/\s/g,"");
    }
    //       continue;
    //     alert(checkList[i][0] + "---" + frmAddExternalAccountKA[checkList[i][0]].text);
    if(!isEmpty(frmAddExternalAccountKA[checkList[i][0]].text))
      checkList[i][4] = frmAddExternalAccountKA[checkList[i][0]].text.replace(/\s/g,"");
  }

  //     if (frmAddExternalAccountKA.lblRememCheckBox.text === "q"){//Omar ALnajjar add as a
  //     count++;
  //   }
  if (frmAddExternalAccountKA.btnFav.text !== "}" && frmAddExternalAccountKA.lblRememCheckBox.text !== "p"){
    count++;
  }
  kony.print("count ::"+count);
  if(count == 0)
    frmAddExternalAccountKA.lblNext.skin = "sknLblNextEnabled";
  else
    frmAddExternalAccountKA.lblNext.skin = "sknLblNextDisabled";
  kony.boj.addBeneVar[selectedType].fieldRequired = checkList;
};

kony.boj.getSelectedAccountType = function(){
  return kony.boj.addBeneVar[kony.boj.addBeneVar.selectedType].title;
};

kony.boj.hideInline = function(){
  frmAddExternalAccountKA.txtBenficiryNameKAInline.isVisible = false;
  frmAddExternalAccountKA.txtBenficiryNickNameInline.isVisible = false;
  frmAddExternalAccountKA.tbxAddressBeneInline.isVisible = false;
  frmAddExternalAccountKA.tbxCityInline.isVisible = false;
  frmAddExternalAccountKA.externalAccountNumberTextFieldInline.isVisible = false;
  frmAddExternalAccountKA.txtSwiftCodeKAInline.isVisible = false;
  frmAddExternalAccountKA.tbxCountryInline.isVisible = false;
  frmAddExternalAccountKA.tbxBankNameInline.isVisible = false;
  frmAddExternalAccountKA.tbxBankNameIntInline.isVisible = false;
  frmAddExternalAccountKA.tbxBankBranchInline.isVisible = false;
  frmAddExternalAccountKA.tbxCityBankInline.isVisible = false;
  frmAddExternalAccountKA.tbxAddressBankInline.isVisible = false;
};

kony.boj.resetAddBeneForm = function(flag){

  if(flag === true)
    return;

  if(kony.store.getItem("langPrefObj") === "en")
    kony.boj.lang = "eng";
  else
    kony.boj.lang = "ara";
  isAddAsTrusted = false;
  frmAddExternalAccountKA.lblTitle.text = geti18Value("i18n.Bene.AddanewbeneficiarySmall");
  frmAddExternalAccountKA.flxHeader.height = "18%";
  frmAddExternalAccountKA.flxTop.height = "45%";
  frmAddExternalAccountKA.flxMain.height = "82%";
  frmAddExternalAccountKA.flxTab.setVisibility(true);
  frmAddExternalAccountKA.lblCountryTitleStat.setVisibility(false);
  frmAddExternalAccountKA.lblBankCountryStat.setVisibility(false);
  frmAddExternalAccountKA.lblBankBranchStat.setVisibility(false);
  frmAddExternalAccountKA.lblBankNameStat.setVisibility(false);
  frmAddExternalAccountKA.lblCountryNameKA.text = kony.i18n.getLocalizedString("i18n.NUO.Country");
  frmAddExternalAccountKA.lblBankName.text = kony.i18n.getLocalizedString("i18n.Bene.Bankname");
  frmAddExternalAccountKA.lblBankBranch.text = kony.i18n.getLocalizedString("i18n.Bene.BankBranch");
  frmAddExternalAccountKA.lblCountryBankDetails.text = kony.i18n.getLocalizedString("i18n.NUO.Country");

  kony.boj.displayParameter(kony.boj.addBeneVar.selectedType);
  kony.boj.clearEnteredData("BOJ");
  kony.boj.clearEnteredData("DOM");
  kony.boj.clearEnteredData("INT");
  kony.boj.hideInline();
  frmAddExternalAccountKA.flxFavNote.setVisibility(false);
  frmAddExternalAccountKA.flxUnderlineBenficiryNameKA.skin =  "sknFlxGreyLine";
  frmAddExternalAccountKA.txtBenficiryNameKA.text = "";

  frmAddExternalAccountKA.flxUnderlineBenficiryNickName.skin =  "sknFlxGreyLine";
  frmAddExternalAccountKA.txtBenficiryNickName.text = "";

  frmAddExternalAccountKA.flxUnderlineAddressBene.skin =  "sknFlxGreyLine";
  frmAddExternalAccountKA.tbxAddressBene.text = "";

  //   frmAddExternalAccountKA.lblCityName.text = geti18Value("i18n.manage_payee.cityPlh");
  //   frmAddExternalAccountKA.lblCityName.skin = "sknLblNextDisabled";
  //   frmAddExternalAccountKA.flxUnderlineCityName.skin =  "sknFlxGreyLine";
  frmAddExternalAccountKA.flxUnderlineCityName.skin =  "sknFlxGreyLine";
  frmAddExternalAccountKA.tbxCity.text = "";

  frmAddExternalAccountKA.externalAccountNumberTextField.text = "";
  frmAddExternalAccountKA.flxUnderlineAccountNumber.skin =  "sknFlxGreyLine";

  frmAddExternalAccountKA.txtSwiftCodeKA.text = "";
  frmAddExternalAccountKA.flxUnderlineSwiftCode.skin =  "sknFlxGreyLine";

  //frmAddExternalAccountKA.lblCountryNameKA.text = "";//geti18Value("i18n.NUO.Country");
  frmAddExternalAccountKA.lblCountryNameKA.skin = "sknLblNextDisabled";
  frmAddExternalAccountKA.flxUnderlineCountryName.skin =  "sknFlxGreyLine";

  //frmAddExternalAccountKA.lblBankName.text = "";//geti18Value("i18n.Bene.Bankname");
  frmAddExternalAccountKA.lblBankName.skin = "sknLblNextDisabled";
  frmAddExternalAccountKA.flxUnderlineBankName.skin =  "sknFlxGreyLine";

  frmAddExternalAccountKA.tbxBankNameInt.text = "";
  frmAddExternalAccountKA.flxUnderlineBankNameInt.skin =  "sknFlxGreyLine";

  //frmAddExternalAccountKA.lblBankBranch.text = "";//geti18Value("i18n.Bene.BankBranch");
  frmAddExternalAccountKA.lblBankBranch.skin = "sknLblNextDisabled";
  frmAddExternalAccountKA.flxUnderlineBankBranch.skin =  "sknFlxGreyLine";

  //   frmAddExternalAccountKA.lblCityBank.text = geti18Value("i18n.manage_payee.cityPlh");
  //   frmAddExternalAccountKA.lblCityBank.skin = "sknLblNextDisabled";
  //   frmAddExternalAccountKA.flxUnderlineCityBank.skin =  "sknFlxGreyLine";
  frmAddExternalAccountKA.tbxCityBank.text = "";
  frmAddExternalAccountKA.flxUnderlineCityBank.skin =  "sknFlxGreyLine";

  frmAddExternalAccountKA.tbxAddressBank.text = "";
  frmAddExternalAccountKA.flxUnderlineAddressBank.skin =  "sknFlxGreyLine";

  //   frmAddExternalAccountKA.tbxEmail.text = "";
  //   frmAddExternalAccountKA.flxUnderlineEmail.skin =  "sknFlxGreyLine";

  //   frmAddExternalAccountKA.tbxBeneRelation.text = "";
  //   frmAddExternalAccountKA.flxUnderlineBeneRelation.skin =  "sknFlxGreyLine";
  //frmAddExternalAccountKA.lblCountryBankDetails.text = "";//geti18Value("i18n.NUO.Country");
  frmAddExternalAccountKA.txtCountryBankDetails.text = "";
  frmAddExternalAccountKA.lblCountryBankDetailsUnderLine.skin = "sknFlxGreyLine";
  frmAddExternalAccountKA.lblNext.skin = "sknLblNextDisabled";
  frmAddExternalAccountKA.flxConfirmPopUp.isVisible = false;
  frmAddExternalAccountKA.flxFormMain.isVisible = true;

  if(frmAddExternalAccountKA.btnBOJ.skin === "slButtonWhiteTab"){
    frmAddExternalAccountKA.lblAccountNumber.text = geti18Value("i18n.Bene.Accountnumber");
  }else if(kony.boj.addBeneVar.selectedType === "INT"){
    frmAddExternalAccountKA.lblAccountNumber.text = geti18Value("i18n.common.IBAN")+" / "+geti18Value("i18n.common.accountNumber");//i18n.transfer.RoutingNumber
    //     frmAddExternalAccountKA.lblSwiftCode.text = geti18Value("i18n.Bene.BankSWIFT")+" / "+geti18Value("i18n.transfer.RoutingNumber");
  }
  else{
    frmAddExternalAccountKA.lblAccountNumber.text = geti18Value("i18n.common.IBAN");
  }
  kony.boj.addBeneficiaryAnimation("");
  kony.print("gblLaunchModeOB::J"+JSON.stringify(gblLaunchModeOBJ));
  if(gblLaunchModeOBJ.lauchMode){
    if(!isEmpty(gblLaunchModeOBJ.accno)  && (!isEmpty(gblLaunchModeOBJ.accno) || !isEmpty(gblLaunchModeOBJ.iban))){
      frmAddExternalAccountKA.externalAccountNumberTextField.text = gblLaunchModeOBJ.accno;
      kony.boj.selectedDetailsList = isEmpty(kony.boj.selectedDetailsList) ? kony.boj.detailsForBene.Branch : kony.boj.selectedDetailsList;
      var branChCodee = gblLaunchModeOBJ.accno.substring(0,3);
      kony.print("branChCodee:"+branChCodee);
      var selectedItem = {"BRANCH_NAME": "", "BRANCH_CODE" : ""};
      for(var i=0; i<kony.boj.selectedDetailsList.length; i++){
        if(parseInt(branChCodee) == parseInt(kony.boj.selectedDetailsList[i].BRANCH_CODE)){
          selectedItem = kony.boj.selectedDetailsList[i];
          break;
        }
      }

      frmAddExternalAccountKA.lblBankBranch.text = selectedItem.BRANCH_NAME;
      frmAddExternalAccountKA.lblBankBranch.skin = "sknLblBack";
      frmAddExternalAccountKA.tbxBankBranch.text = selectedItem.BRANCH_CODE;
      frmAddExternalAccountKA.flxUnderlineBankBranch.skin =  "sknFlxGreenLine";
      frmAddExternalAccountKA.lblBankBranchStat.setVisibility(true);

      // kony.print("frmAddExternalAccountKA.externalAccountNumberTextField.text::J"+frmAddExternalAccountKA.externalAccountNumberTextField.text);
    }
    else if(!isEmpty(gblLaunchModeOBJ.iban)){
      frmAddExternalAccountKA.externalAccountNumberTextField.text = gblLaunchModeOBJ.iban; 
      // kony.print("frmAddExternalAccountKA.externalAccountNumberTextField.text::iban"+frmAddExternalAccountKA.externalAccountNumberTextField.text)
    }
    animateLabel("UP", "lblAccountNumber", "up",frmAddExternalAccountKA);
    frmAddExternalAccountKA.forceLayout();
  }



};

kony.boj.thingsToShow = function(){
  var selectedType = kony.boj.addBeneVar.selectedType;
  frmAddExternalAccountKA.lblInitial.text = frmAddExternalAccountKA.txtBenficiryNameKA.text.substring(0,2).toUpperCase();
  frmAddExternalAccountKA.flxIcon1.backgroundColor = kony.boj.getBackGroundColour(frmAddExternalAccountKA.lblInitial.text);
  //   if(gblFromModule !== "updateBeneficiary"){
  //     customAlertPopup(geti18Value("i18n.cards.Confirmation"), 
  //                      geti18Value("i18n.BeneFavourite"),
  //                      kony.boj.onClickYesFavAddBene, kony.boj.onClickNoFavAddBene, 
  //                      geti18Value("i18n.common.YES"),
  //                      geti18Value("i18n.common.NO"));
  //   }
  frmAddExternalAccountKA.flxBankBranchConf.setVisibility(false);
  frmAddExternalAccountKA.flxCityConf.setVisibility(false);
  frmAddExternalAccountKA.lblBeneName.text = frmAddExternalAccountKA.txtBenficiryNameKA.text;
  frmAddExternalAccountKA.lblBeneAccNum.text = frmAddExternalAccountKA.externalAccountNumberTextField.text;
  frmAddExternalAccountKA.lblNickName.text = frmAddExternalAccountKA.txtBenficiryNickName.text;
  frmAddExternalAccountKA.lblLanguage.text = kony.store.getItem("langPrefObj").toUpperCase();
  frmAddExternalAccountKA.lblCurrency.text = "USD";
  frmAddExternalAccountKA.lblBranchNumber.text = frmAddExternalAccountKA.tbxBankBranch.text;
  frmAddExternalAccountKA.lblAccountTypeKA.text = kony.boj.getSelectedAccountType();
  frmAddExternalAccountKA.flxConfirmPopUp.isVisible = true;
  frmAddExternalAccountKA.flxFormMain.isVisible = false;

  if(selectedType === "BOJ"){
    frmAddExternalAccountKA.flxConfirmAddress.isVisible = false;
    frmAddExternalAccountKA.flxConfirmCountry.isVisible = false;
    frmAddExternalAccountKA.flxConfirmSwift.isVisible = false;
    frmAddExternalAccountKA.flxConfirmBankName.isVisible = false;
    frmAddExternalAccountKA.flxConfirmEmail.isVisible = false;
    frmAddExternalAccountKA.flxBankBranchConf.setVisibility(true);
    frmAddExternalAccountKA.lblBankBranchConf.text = frmAddExternalAccountKA.lblBankBranch.text;
  }
  else if(selectedType === "DOM"){
    frmAddExternalAccountKA.flxConfirmAddress.isVisible = true;
    frmAddExternalAccountKA.lblConfirmAddr.text = frmAddExternalAccountKA.tbxAddressBene.text;
    frmAddExternalAccountKA.flxConfirmCountry.isVisible = false;
    frmAddExternalAccountKA.flxConfirmSwift.isVisible = false;
    frmAddExternalAccountKA.flxConfirmBankName.isVisible = true;
    frmAddExternalAccountKA.lblConfirmBankName.text = frmAddExternalAccountKA.lblBankName.text;

    frmAddExternalAccountKA.flxConfirmEmail.isVisible = false;
    //frmAddExternalAccountKA.lblConfirmEmail.text = frmAddExternalAccountKA.tbxEmail.text;
  }
  else if(selectedType === "INT"){
    frmAddExternalAccountKA.flxConfirmAddress.isVisible = true;
    frmAddExternalAccountKA.lblConfirmAddr.text = frmAddExternalAccountKA.tbxAddressBene.text+","+frmAddExternalAccountKA.tbxCity.text+","+frmAddExternalAccountKA.lblCountryNameKA.text;

    frmAddExternalAccountKA.flxConfirmCountry.isVisible = true;
    frmAddExternalAccountKA.lblConfirmCountry.text = frmAddExternalAccountKA.lblCountryBankDetails.text;

    frmAddExternalAccountKA.flxConfirmSwift.isVisible = true;
    frmAddExternalAccountKA.lblConfirmSwift.text = frmAddExternalAccountKA.txtSwiftCodeKA.text;

    frmAddExternalAccountKA.flxConfirmBankName.isVisible = true;
    frmAddExternalAccountKA.lblConfirmBankName.text = frmAddExternalAccountKA.tbxBankNameInt.text;

    frmAddExternalAccountKA.flxCityConf.setVisibility(true);
    frmAddExternalAccountKA.lblCity.text = frmAddExternalAccountKA.tbxCityBank.text;

    //frmAddExternalAccountKA.flxBankBranchConf.setVisibility(true);
    //frmAddExternalAccountKA.lblBankBranchConf.text = frmAddExternalAccountKA.lblBankBranch.text;

    frmAddExternalAccountKA.flxConfirmEmail.isVisible = false;
    //frmAddExternalAccountKA.lblConfirmEmail.text = frmAddExternalAccountKA.tbxEmail.text;
  }
};

kony.boj.onClickYesBackAddBene = function(){
  popupCommonAlertDimiss();
  if(gblFromModule === "updateBeneficiary"|| gblFromModule === "addBeneficiaryAsFav" || isAddAsTrusted === true ){
    frmShowAllBeneficiary.show();
  }else{
    if(previous_FORM !== null){
      if(previous_FORM.id === "frmManagePayeeKA"){
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var controller = INSTANCE.getFormController("frmManagePayeeKA");
        var navObject = new kony.sdk.mvvm.NavigationObject();
        navObject.setRequestOptions("managepayeesegment", {
          "headers": {},
          "queryParams": {"custId": custid,
                          "P_BENE_TYPE": "PRE"
                         }
        });
        controller.loadDataAndShowForm(navObject);
      }else{
        previous_FORM.show();
        frmAddNewPayeeKA.destroy();
      }
    }else{
      frmPaymentDashboard.show();
      frmAddNewPayeeKA.destroy();
    }
  }
  previous_FORM = null;
  gblFromModule = "";
  isAddAsTrusted = false;
  kony.boj.detailsForBene.flag = false;
  kony.boj.addBillerFromQuickPay = false;
};

kony.boj.onClickYesFavAddBene = function(){
  frmAddExternalAccountKA.lblFavourite.text = "Y";
  popupCommonAlertDimiss();
};

kony.boj.onClickNoFavAddBene = function(){
  frmAddExternalAccountKA.lblFavourite.text = "N";
  popupCommonAlertDimiss();
};

kony.boj.getNewAddedBeneDetails = function(beneId){
  var beneDetails = {
    "benType": kony.boj.getSelectedAccountType(),
    "countryName": frmAddExternalAccountKA.txtCountryBankDetails.text,//frmAddExternalAccountKA.tbxCountry.text,
    "beneficiaryName": frmAddExternalAccountKA.txtBenficiryNameKA.text,
    "bene_bank_city": frmAddExternalAccountKA.tbxCity.text,
    "bankName": frmAddExternalAccountKA.tbxBankNameInt.text,
    "bene_address1": frmAddExternalAccountKA.tbxAddressBene.text,
    "bene_address2": frmAddExternalAccountKA.tbxCountry.text,//frmAddExternalAccountKA.tbxAddressBank.text,
    "benAcctNo": frmAddExternalAccountKA.externalAccountNumberTextField.text.replace(/\s/g,""),
    "nickName": frmAddExternalAccountKA.txtBenficiryNickName.text,
    "bene_email": frmAddExternalAccountKA.tbxEmail.text,
    "swiftCode": frmAddExternalAccountKA.txtSwiftCodeKA.text,
    "benBranchNo": frmAddExternalAccountKA.tbxBankBranch.text,
    "bene_city": frmAddExternalAccountKA.tbxCityBank.text,
    "initial": frmAddExternalAccountKA.txtBenficiryNameKA.text.substring(0, 2).toUpperCase(),
    "isFav": frmAddExternalAccountKA.lblFavourite.text,
    "benef_id": beneId,
    "beneficiaryFullName":frmAddExternalAccountKA.txtBenficiryNameKA.text,
    "beneficiaryFullNickName":frmAddExternalAccountKA.txtBenficiryNickName.text
  };

  beneDetails.icon = {
    backgroundColor: kony.boj.getBackGroundColour(beneDetails.initial)
  };

  //   if(kony.boj.beneList[beneDetails.benType] === undefined || kony.boj.beneList[beneDetails.benType] === null){
  //     kony.boj.beneList[beneDetails.benType] = {
  //       "fav":[],
  //       "norm":[]
  //     };
  //   }

  if(beneDetails.isFav === "Y"){
    if (gblTModule === "send"){ //Change the fav icon when is send money Omar ALnajjar 
      beneDetails.btnSetting = {
        "text":"{"
      };
    }else{
      beneDetails.btnSetting = {
        "text":","
      };
    }
    kony.boj.beneList[beneDetails.benType].fav.push(beneDetails);
  }
  else{
    if (gblTModule === "send"){//Change the fav icon when is send money Omar ALnajjar
      beneDetails.btnSetting = {
        "text":"}"
      };
    }else{
      beneDetails.btnSetting = {
        "text":"k"
      };
    }
    kony.boj.beneList[beneDetails.benType].norm.push(beneDetails);
  }
  kony.boj.beneList.countBene++;

  return beneDetails;
};


function checkforExistingAccountAddBen(){
  kony.print("checkforExistingAccountAddBen::"+kony.boj.addBeneVar.selectedType);
  var selectedType =kony.boj.addBeneVar.selectedType;
  var accountsData = isEmpty(kony.retailBanking.globalData.accountsDashboardData.accountsData) ? kony.retailBanking.globalData.accountsDashboardData.fromAccounts : kony.retailBanking.globalData.accountsDashboardData.accountsData;
  if(!isEmpty(accountsData) && frmAddExternalAccountKA.externalAccountNumberTextField.text !== ""){
    if(selectedType === "BOJ"){
      for(var i in accountsData){
        if(kony.string.equalsIgnoreCase(accountsData[i].accountID,frmAddExternalAccountKA.externalAccountNumberTextField.text)){
          customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.benf.cantaddyouraccount"), popupCommonAlertDimiss, "");

          return false;
        }
      }

    }
    else if(selectedType === "DOM"){
      for(var j in accountsData){
        if(kony.string.equalsIgnoreCase(accountsData[j].iban,frmAddExternalAccountKA.externalAccountNumberTextField.text)){
          customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.benf.cantaddyouraccount"), popupCommonAlertDimiss, "");
          return false;
        }
      }
    }
    else if(selectedType === "INT"){
      for(var k in accountsData){
        if(kony.string.equalsIgnoreCase(accountsData[k].iban,frmAddExternalAccountKA.externalAccountNumberTextField.text)){
          customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.benf.cantaddyouraccount"), popupCommonAlertDimiss, "");
          return false;
        }
      }
    }

  }
  return true;
}

function set_EDIT_BENEFICIARY_SCREEN(data){
  try{
    frmAddExternalAccountKA.destroy();
    var details = null;
    kony.print("Edit Beneficiary data ::"+JSON.stringify(data));
    //     	gblFromModule = "updateBeneficiary";
    kony.boj.resetAddBeneForm(false);
    isAddAsTrusted = gblFromModule === "addBeneficiaryAsFav"?true:false;
    //     	frmAddExternalAccountKA.lblTitle.text = geti18Value("i18n.updatebene.title");
    frmAddExternalAccountKA.flxHeader.height = "9%";
    frmAddExternalAccountKA.flxTop.height = "90%";
    frmAddExternalAccountKA.flxMain.height = "91%";
    frmAddExternalAccountKA.txtBenficiryNameKA.text = data.beneficiaryFullName;
    frmAddExternalAccountKA.lblBeneFullName.text=data.beneficiaryFullName;// hassan update bene
    frmAddExternalAccountKA.txtBenficiryNickName.text = data.beneficiaryFullNickName;
    frmAddExternalAccountKA.externalAccountNumberTextField.text = data.benAcctNo;
    animate_UPDATE_BENEFICIARY_FIELDS(["lblBenficiryNameKA","lblBenficiryNickName","lblAccountNumber"]);
    //Omar ALnajjar Default favorite when edit trusted
    if (data.isFav === "Y"){
      frmAddExternalAccountKA.btnFav.text = "{";
      frmAddExternalAccountKA.lblFavourite.text = "Y";
      frmAddExternalAccountKA.lblRememCheckBox.text = "p";
    }else{
      frmAddExternalAccountKA.btnFav.text = "}";
      frmAddExternalAccountKA.lblFavourite.text = "N";
      frmAddExternalAccountKA.lblRememCheckBox.text = "q";
    }

    frmAddExternalAccountKA.lblBankCountryStat.setVisibility(true);
    frmAddExternalAccountKA.lblBankBranchStat.setVisibility(true);

    switch(data.benType){
      case "IFB":
        var storeData = isEmpty(data) ? "" : data;
        kony.store.setItem("data",storeData);
        gblFromModule = "updateBeneficiaryBranch";
        kony.boj.getBranchList();
        kony.boj.displayParameter("BOJ");
        break;
      case "DTB":
        frmAddExternalAccountKA.lblBankNameStat.setVisibility(true);
        frmAddExternalAccountKA.tbxAddressBene.text = data.bene_address1;
        frmAddExternalAccountKA.tbxBankNameInt.text = data.bankName;
        frmAddExternalAccountKA.lblBankName.text = data.bankName;
        if(!isEmpty(data.bankName))
          frmAddExternalAccountKA.lblBankNameStat.setVisibility(true);
        else{
          frmAddExternalAccountKA.lblBankName.text = kony.i18n.getLocalizedString("i18n.Bene.Bankname");
        } 
        frmAddExternalAccountKA.lblBankName.skin = "sknLblBack";
        animate_UPDATE_BENEFICIARY_FIELDS(["lblAddressBene"]);
        kony.boj.displayParameter("DOM");
        break;
      case "ITB":
        //           		frmAddExternalAccountKA.lblBankNameStat.setVisibility(true);
        //           		frmAddExternalAccountKA.lblCountryTitleStat.setVisibility(true);
        //           		frmAddExternalAccountKA.lblBankCountryStat.setVisibility(true);
        //         		frmAddExternalAccountKA.lblBankBranchStat.setVisibility(true);
        frmAddExternalAccountKA.tbxAddressBene.text = data.bene_address1;
        frmAddExternalAccountKA.tbxCity.text = data.bene_city;
        details = get_COUNTRY_DETAILS(data.bene_address2);
        frmAddExternalAccountKA.tbxCountry.text = data.bene_address2;
        if(!isEmpty(details)){
          if(kony.store.getItem("langPrefObj") === "en")
            frmAddExternalAccountKA.lblCountryNameKA.text = details.CTRY_S_DESC;
          else
            frmAddExternalAccountKA.lblCountryNameKA.text = details.CTRY_B_DESC;
        }
        frmAddExternalAccountKA.lblCountryNameKA.skin = "sknLblBack";
        if( frmAddExternalAccountKA.lblCountryNameKA.text !== kony.i18n.getLocalizedString("i18n.NUO.Country"))
          frmAddExternalAccountKA.lblBankCountryStat.setVisibility(true);
        else{
          frmAddExternalAccountKA.lblCountryNameKA.text = kony.i18n.getLocalizedString("i18n.NUO.Country");
        }
        frmAddExternalAccountKA.txtSwiftCodeKA.text = data.swiftCode;
        frmAddExternalAccountKA.tbxBankNameInt.text = data.bankName;
        frmAddExternalAccountKA.tbxCityBank.text = data.bene_bank_city;
        details = get_COUNTRY_DETAILS(data.countryName);
        frmAddExternalAccountKA.txtCountryBankDetails.text = data.countryName;
        if(!isEmpty(details)){
          if(kony.store.getItem("langPrefObj") === "en")
            frmAddExternalAccountKA.lblCountryBankDetails.text = details.CTRY_S_DESC;
          else
            frmAddExternalAccountKA.lblCountryBankDetails.text = details.CTRY_B_DESC;
        }
        frmAddExternalAccountKA.lblCountryBankDetails.skin = "sknLblBack";
        if(frmAddExternalAccountKA.lblCountryBankDetails.text !== kony.i18n.getLocalizedString("i18n.NUO.Country"))
          frmAddExternalAccountKA.lblCountryTitleStat.setVisibility(true);
        else{
          frmAddExternalAccountKA.lblCountryBankDetails.text = kony.i18n.getLocalizedString("i18n.NUO.Country");
        }
        animate_UPDATE_BENEFICIARY_FIELDS(["lblAddressBene","lblCityName","lblSwiftCode","lblBankNameInt","lblCityBank"]);
        kony.boj.displayParameter("INT");
        break;
    }
    frmAddExternalAccountKA.flxTab.setVisibility(false);
    kony.boj.detailsForBene.flag = true;
    kony.boj.detailsForBene.beneDataSelected = data;
    gblSelectedBene = data;
    if (isAddAsTrusted === true){
      gblFromModule = "addBeneficiaryAsFav";
      frmAddExternalAccountKA.lblTitle.text = geti18Value("18n.transfer.trustedBene");
      frmAddExternalAccountKA.lblHeaderTitle.text = geti18Value("18n.transfer.trustedBene");
      frmAddExternalAccountKA.btnConfirm.setEnabled(false);
      frmAddExternalAccountKA.flxTrustedNote.setVisibility(true);
      kony.boj.thingsToShow();
    }else{
      frmAddExternalAccountKA.btnConfirm.setEnabled(true);
      frmAddExternalAccountKA.lblTitle.text = geti18Value("i18n.updatebene.title");
      frmAddExternalAccountKA.lblHeaderTitle.text = geti18Value("i18n.Transfer.ConfirmDet");
      frmAddExternalAccountKA.flxTrustedNote.setVisibility(true);
    }
    frmAddExternalAccountKA.show();
  }catch(e){
    gblFromModule = "";
    isAddAsTrusted = false;
    kony.print("Exception_set_EDIT_BENEFICIARY_SCREEN ::"+e);
  }
}

function animate_UPDATE_BENEFICIARY_FIELDS(animateFields){
  try{
    for(var i in animateFields){
      frmAddExternalAccountKA[animateFields[i]].skin = "sknlblanimated75";
      frmAddExternalAccountKA[animateFields[i]].top = "15%";
    }
  }catch(e){
    kony.print("Excption_animate_UPDATE_BENEFICIARY_FIELDS ::"+e);
  }
}

function serv_UPDATE_BENEFICIARY_DETAILS(){
  try{
    var requestObj = {
      "custId":custid,
      "beneficiaryName":frmAddExternalAccountKA.txtBenficiryNameKA.text,
      "nickName":frmAddExternalAccountKA.txtBenficiryNickName.text,
      "bene_address1":frmAddExternalAccountKA.tbxAddressBene.text,
      "bene_city":frmAddExternalAccountKA.tbxCity.text,
      "benAcctNo":frmAddExternalAccountKA.externalAccountNumberTextField.text.replace(/\s/g,""),
      "BENE_BANK_SWIFT":frmAddExternalAccountKA.txtSwiftCodeKA.text,
      "BENE_BANK_COUNTRY":frmAddExternalAccountKA.txtCountryBankDetails.text,
      "bankName":frmAddExternalAccountKA.tbxBankNameInt.text,
      "benBranchNo":frmAddExternalAccountKA.tbxBankBranch.text,
      "bene_bank_city":frmAddExternalAccountKA.tbxCityBank.text,
      "bene_address2":frmAddExternalAccountKA.tbxCountry.text,
      "P_BENE_TYPE":frmAddExternalAccountKA.lblAccountTypeKA.text,
      "bene_email":frmAddExternalAccountKA.tbxEmail.text,
      "language":frmAddExternalAccountKA.lblLanguage.text,
      "benAcctCurrency":frmAddExternalAccountKA.lblCurrency.text,
      "Fav_flag":frmAddExternalAccountKA.lblFavourite.text,
      "P_BENEF_ID":gblSelectedBene.benef_id,
      //"P_BENEF_NAME":gblSelectedBene.beneficiaryName, // hassan update bene
      "P_BENEF_NAME":frmAddExternalAccountKA.lblBeneFullName.text, // hassan update bene
      "P_BENE_ACCOUNT":gblSelectedBene.benAcctNo
    };
    kony.print("Input Parameters ::"+JSON.stringify(requestObj));
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("updateBeneficiary");
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    if (kony.sdk.isNetworkAvailable()) {
      appMFConfiguration.invokeOperation("updateBeneficiaryTrans", {},requestObj,success_serv_UPDATE_BENEFICIARY_DETAILS,function(err){kony.print("Error ::"+JSON.stringify(err));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                                                                                                                                       gblFromModule = "";
                                                                                                                                       isAddAsTrusted = false;
                                                                                                                                       kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "",
                                     																							geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
    }
    else {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
    }
  }catch(e){
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("Exception_serv_UPDATE_BENEFICIARY_DETAILS ::"+e);
  }
}

function success_serv_UPDATE_BENEFICIARY_DETAILS(res){
  try{
    var msg = "";
    kony.print("success response ::"+JSON.stringify(res));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    if(!isEmpty(res)){
      gblFromModule = "";
      isAddAsTrusted = false;
      if(res.ErrorCode === "00000" && res.result === "00000"){
        var sucs_msg = geti18Value("i18n.updatebeneficiary.successmsg");
        if(kony.store.getItem("langPrefObj") == "ar")
          sucs_msg = sucs_msg+"\n"+frmAddExternalAccountKA.lblBeneName.text;
        else
          sucs_msg = frmAddExternalAccountKA.lblBeneName.text+"\n"+sucs_msg;
        gblSelectedBene = kony.boj.getNewAddedBeneDetails(res.ReferenceNumber);
        loadBillerDetails = true;
        if (isAddAsTrusted !== true){
          kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.Bene.Success"), sucs_msg,
                                         geti18Value("i18n.Bene.Sendmoneyto"), "SendMoney", geti18Value("i18n.Bene.Addanewbeneficiary"), "Beneficiary",geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard");
        }
      }else if (res.opstatus_prAddBeneficiary!= "0"){
        //trying to add one more time	
        serv_ADD_TRANSFER_BENEFICIARY("EditFlow");

      }else if(res.result !== "00000"){
        msg = geti18Value("i18n.updatebene.failedmsg");
      }else if(res.ErrorCode !== "00000"){
        msg = geti18Value("i18n.updatebene.failedmsg");//"Please add the biller again, for some reason biller is deleted.";
      }
      if(msg !== ""){
        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), msg,
                                       geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"));
      }
    }
  }catch(e){
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("Exception_success_serv_UPDATE_BENEFICIARY_DETAILS ::"+e);
  }

}

function serv_ADD_TRANSFER_BENEFICIARY(flow){
  try{
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("ExternalAccounts", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("ExternalAccounts");
    dataObject.addField("beneficiaryName", frmAddExternalAccountKA.txtBenficiryNameKA.text);
    dataObject.addField("nickName", frmAddExternalAccountKA.txtBenficiryNickName.text);
    dataObject.addField("bene_address1", frmAddExternalAccountKA.tbxAddressBene.text);
    dataObject.addField("bene_city", frmAddExternalAccountKA.tbxCity.text);
    dataObject.addField("benAcctNo", frmAddExternalAccountKA.externalAccountNumberTextField.text.replace(/\s/g,""));
    dataObject.addField("BENE_BANK_SWIFT", frmAddExternalAccountKA.txtSwiftCodeKA.text);
    dataObject.addField("BENE_BANK_COUNTRY", frmAddExternalAccountKA.txtCountryBankDetails.text);
    dataObject.addField("bankName", frmAddExternalAccountKA.tbxBankNameInt.text);
    dataObject.addField("benBranchNo", frmAddExternalAccountKA.tbxBankBranch.text);
    dataObject.addField("bene_bank_city", frmAddExternalAccountKA.tbxCityBank.text);
    dataObject.addField("bene_address2", frmAddExternalAccountKA.tbxCountry.text);
    dataObject.addField("P_BENE_TYPE", frmAddExternalAccountKA.lblAccountTypeKA.text);
    dataObject.addField("bene_email", frmAddExternalAccountKA.tbxEmail.text);
    dataObject.addField("language", frmAddExternalAccountKA.lblLanguage.text);
    dataObject.addField("benAcctCurrency", frmAddExternalAccountKA.lblCurrency.text);
    dataObject.addField("Fav_flag", frmAddExternalAccountKA.lblFavourite.text);
    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("data Object ::"+JSON.stringify(dataObject));
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    if (kony.sdk.isNetworkAvailable()) {
      if(flow !== undefined && flow!== "" && flow === "EditFlow"){
        modelObj.customVerb("create", serviceOptions, success_serv_ADD_TRANSFER_BENEFICIARY_VIA_EDIT, function(err){kony.print("Error ::"+JSON.stringify(err));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                                                                                                                    gblFromModule = "";
                                                                                                                    isAddAsTrusted = false;
                                                                                                                    kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "",
                                                                                                                                                   geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
      }else{
        modelObj.customVerb("create", serviceOptions, success_serv_ADD_TRANSFER_BENEFICIARY, failed_serv_ADD_TRANSFER_BENEFICIARY);
      }
    } else {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
  }catch(e){
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("Exception_serv_ADD_TRANSFER_BENEFICIARY ::"+e);
  }
}

function success_serv_ADD_TRANSFER_BENEFICIARY(res){
  try{
    kony.print("add beneficiary res ::"+JSON.stringify(res));
    gblLaunchModeOBJ.lauchMode = false;
    if(res.ErrorCode == "00000" || res.ErrorCode == "0"){
      kony.boj.detailsForBene.flag = false;
      var msg = geti18Value("i18n.common.benificaryadded").split("name");
      gblSelectedBene = kony.boj.getNewAddedBeneDetails(res.ReferenceNumber);
      //         if(kony.store.getItem("langPrefObj") === "en")
      //           kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.Bene.Success"), frmAddExternalAccountKA.lblBeneName.text + msg[0],
      //                                          geti18Value("i18n.Bene.Sendmoneyto"), "SendMoney", geti18Value("i18n.Bene.Addanewbeneficiary"), "Beneficiary");
      //         else
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.Bene.Success"), msg[0] + frmAddExternalAccountKA.lblBeneName.text +"\n"+ msg[1],
                                     geti18Value("i18n.Bene.Sendmoneyto"), "SendMoney",geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard", geti18Value("i18n.Bene.Addanewbeneficiary"), "Beneficiary");
    }
    else{
      kony.boj.detailsForBene.flag = true;
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      var Message = getServiceErrorMessage(res.ErrorCode,"frmAddExtActCExtSucSavDat");
      customAlertPopup(geti18Value("i18n.common.alert"), Message,popupCommonAlertDimiss,"", geti18nkey("i18n.settings.ok"), "");
      frmAddExternalAccountKA.flxConfirmPopUp.isVisible = false;
      frmAddExternalAccountKA.flxFormMain.isVisible = true;
      frmAddExternalAccountKA.show();
    }
  }catch(e){
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("Exception_success_serv_ADD_TRANSFER_BENEFICIARY ::"+e);
  }
}

function failed_serv_ADD_TRANSFER_BENEFICIARY(err){
  try{
    kony.print("failed ::"+JSON.stringify(err));
    gblLaunchModeOBJ.lauchMode = false;
    var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
    kony.sdk.mvvm.log.error(exception.toString());
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "",
                                   geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"));
  }catch(e){
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("Exception_failed_serv_ADD_TRANSFER_BENEFICIARY ::"+e);
  }
}

function success_serv_ADD_TRANSFER_BENEFICIARY_VIA_EDIT(res){
  try{
    kony.print("add beneficiary res ::"+JSON.stringify(res));
    gblLaunchModeOBJ.lauchMode = false;
    if(res.ErrorCode == "00000" || res.ErrorCode == "0"){
      kony.boj.detailsForBene.flag = false;
      var sucs_msg = geti18Value("i18n.updatebeneficiary.successmsg");
      if(kony.store.getItem("langPrefObj") == "ar")
        sucs_msg = sucs_msg+"\n"+frmAddExternalAccountKA.lblBeneName.text;
      else
        sucs_msg = frmAddExternalAccountKA.lblBeneName.text+"\n"+sucs_msg;
      gblSelectedBene = kony.boj.getNewAddedBeneDetails(res.ReferenceNumber);
      loadBillerDetails = true;
      if (isAddAsTrusted !== true){
        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.Bene.Success"), sucs_msg,
                                       geti18Value("i18n.Bene.Sendmoneyto"), "SendMoney", geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard",geti18Value("i18n.Bene.Addanewbeneficiary"), "Beneficiary");
      }
    }
    else{
      var msg = "Please add the biller again, for some reason biller is deleted.";
      kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), msg,
                                     geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"));
    }
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }catch(e){
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("Exception_success_serv_ADD_TRANSFER_BENEFICIARY ::"+e);
  }
}



function format_IBAN_VALUE(data){
  kony.print("IBAN ::"+data);
  var result = "";
  for(var i=0; i<data.length;i++){
    if(((i%4) == 0) && (i > 3))
      result = result + " ";
    result = result + data.charAt(i);
  }
  kony.print("formatted IBAN value ::"+result);
  return result+"";
}
var isTouchPermitted;
var isPinPermitted;
var isFacialAuthEnabled;

function showDefaultLoginScreen()
{
  if(isFirstTimeLogin())
  {
    showLoginorPinScreen("pwdLogin");
   // frmLoginKA.noThanks.setVisibility(false);
  }
  else
  {
    if(isSettingsFlagEnabled("rememberMeFlag"))
    {
      var tempSettingsData=kony.store.getItem("settingsflagsObject");
      if (tempSettingsData.defaultLoginMode == "touchid" && kony.boj.retailBanking.globals.Maintenance_Flag === "N")
         touchLoginShow();
      else if (tempSettingsData.defaultLoginMode == "faceid" && kony.boj.retailBanking.globals.Maintenance_Flag === "N")
         showLoginorPinScreen("FaceIdScreen");
      else if (tempSettingsData.defaultLoginMode == "pin")
        showLoginorPinScreen("pinLogin");
      else
        showLoginorPinScreen("pwdLogin");
     
  }
    else 
      showLoginorPinScreen("pwdLogin");
  }
  
}




function showLoginorPinScreen(whichScreen)
{
  if (whichScreen == "pwdLogin")
  {
    frmLoginKA.loginMainScreen.setVisibility(true);
    frmLoginKA.skin="sknLoginMainPage";
    frmLoginKA.loginMainPINScreen.setVisibility(false);
    //frmLoginKA.loginfacialAuthScreen.setVisibility(false);
    frmLoginKA.loginMainScreen.opacity=1;
    frmLoginKA.loginMainScreen.setEnabled(true);
    kony.boj.maintainenceCHECK();
  }
  
  else if (whichScreen == "pinLogin" && isSettingsFlagEnabled("isPinEnabledFlag") )
  {
    frmLoginKA.loginMainScreen.opacity=0.2;
    // if (DecryptValue(kony.store.getItem("userName"))!=null)
    //	frmLoginKA.PinEntryLabel.text = i18n_setPinFor+DecryptValue(kony.store.getItem("userName"));
    clearProgressFlexLogin();
    login_pass="";
    frmLoginKA.clearLink.setVisibility(true);
 	//frmLoginKA.btnCancel.setVisibility(true);
    frmLoginKA.loginMainScreen.setEnabled(false);
    frmLoginKA.loginMainPINScreen.setVisibility(true);
    //frmLoginKA.loginfacialAuthScreen.setVisibility(false);
    frmLoginKA.loginMainPINScreen.setEnabled(true);
  }
  else if (whichScreen == "FaceIdScreen" && isSettingsFlagEnabled("isFacialAuthEnabledFlag"))
   {
     ShowLoadingScreen();
     intializeFacialAuth();
     FaceAuth_initialize2();
     frmLoginKA.loginMainScreen.opacity=0.2;
     frmLoginKA.loginMainScreen.setEnabled(false);
     frmLoginKA.loginMainPINScreen.setVisibility(false);
     //frmLoginKA.loginfacialAuthScreen.setVisibility(true);
   }
  
}

function setPinforUser()
{

  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  ShowLoadingScreen();
  var options ={    "access": "online",
                "objectName": "RBObjects"
               };
  var headers = {"session_token":kony.retailBanking.globalData.session_token};
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("User",serviceName,options);
  var record = {};
  record["pin"] = firstPass;
  var dataObject = new kony.sdk.dto.DataObject("User",record);
  var requestOptions = {"dataObject":dataObject, "headers":headers};
  modelObj.update(requestOptions,setPinSuccess,customErrorCallback);
}

function setPinSuccess(response)
{
  //setUserObj(succCallBackpin); 
  function succCallBackpin()
  {
  	updateFlags("isPinEnabledFlag",true);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    authImgreplace("pinactive.png","pintickwhite.png");
    setAuthModeHeader(frmPinEntrySuccess);
	frmPinEntrySuccess.show();
  }
}

function setNavigationonPinset()
{
  if(fromSettings==true)
    frmPinEntrySuccess.btnContinueSetPin.onClick=frmFacialAuthEnable.show();
  else
     frmPinEntrySuccess.btnContinueSetPin.onClick=frmFacialAuthEnable.show();
}




function updateStatusforPin(whichFlag,featureWidget)
{ 
    if(kony.retailBanking.globalData.deviceInfo.isIphone())
     {
         if(frmDeviceregistrarionSuccessKA[featureWidget].selectedIndex == 0)
            updateFlags(whichFlag,true);
         else
           updateFlags(whichFlag,false);
     }
    else
    {
        if(frmDeviceregistrarionSuccessKA[featureWidget].src=="checkbox_unselected.png")
         {
            updateFlags(whichFlag,true);
            frmDeviceregistrarionSuccessKA[featureWidget].src="checkbox_selected.png";
         }
        else
         {
           updateFlags(whichFlag,false);
          frmDeviceregistrarionSuccessKA[featureWidget].src="checkbox_unselected.png";
         }
    }
}


function setLabibaLoginProcess(){
  frmLoginKA.flxIconMove.onTouchStart = chatbuttonTouchStart;
  frmLoginKA.flxIconMove.onTouchMove = chatbuttonTouchMove;
  frmLoginKA.flxIconMove.left = "85%";// hassan camefrom labiba
  frmLoginKA.flxIconMove.top = "57%";// hassan camefrom labiba
  if(kony.store.getItem("cameFromLabiba") === true){
    frmLoginKA.flxRegisterMobileBanking.setVisibility(false);
    frmLoginKA.flxForgotP.setVisibility(false);
    frmLoginKA.flxSwitchLanguage.setVisibility(false);
   // frmLoginKA.flxPayAppLink.setVisibility(false); 
    frmLoginKA.contactLink.setVisibility(false);
    frmLoginKA.atmLink.setVisibility(false);
    frmLoginKA.btnOnboarding.setVisibility(false);
    frmLoginKA.lblInformationSuppIcon.setVisibility(false);
    frmLoginKA.lblAtmBranchicon.setVisibility(false);
    frmLoginKA.lblNewCusticon.setVisibility(false);
    frmLoginKA.btnBOJPay.setVisibility(false);// hassan camefrom labiba
    frmLoginKA.btnPinLogin.left="25%";// hassan came from labiba
    frmLoginKA.btnTouchID.left="55%";// hassan came from labiba
    frmLoginKA.btnCancelLabibaLogin.setVisibility(true);
    frmLoginKA.flxIconMove.setVisibility(false);// hassan camefrom labiba
//     frmLoginKA.btnPinLogin.left = "55%"; Ahmad 12-8-2020
//     frmLoginKA.btnTouchID.right = "3%";  Ahmad 12-8-2020
//     frmLoginKA.btnFaceID.right = "3%";  Ahmad 12-8-2020
  }else{
    frmLoginKA.flxRegisterMobileBanking.setVisibility(true);
    frmLoginKA.flxForgotP.setVisibility(true);
    frmLoginKA.flxSwitchLanguage.setVisibility(true);
  //  frmLoginKA.flxPayAppLink.setVisibility(true);
    frmLoginKA.contactLink.setVisibility(true);
    frmLoginKA.btnOnboarding.setVisibility(true);
    frmLoginKA.atmLink.setVisibility(true);
    frmLoginKA.lblInformationSuppIcon.setVisibility(true);
    frmLoginKA.lblAtmBranchicon.setVisibility(true);
    frmLoginKA.lblNewCusticon.setVisibility(true);
    frmLoginKA.btnBOJPay.setVisibility(true);// hassan camefrom labiba
    kony.print("hassan came from labiba 22222");
    //#ifdef android
    frmLoginKA.btnPinLogin.left="40%";// hassan came from labiba
    frmLoginKA.btnTouchID.left="70%";// hassan came from labiba
    //#endif
        
    //#ifdef iphone
    frmLoginKA.btnPinLogin.left="25%";// hassan came from labiba
    frmLoginKA.btnTouchID.left="55%";// hassan came from labiba
    //#endif
    frmLoginKA.btnCancelLabibaLogin.setVisibility(false);
    frmLoginKA.flxIconMove.setVisibility(true);// hassan camefrom labiba
//     frmLoginKA.btnPinLogin.left = "55%"; Ahmad 12-8-2020
//     frmLoginKA.btnTouchID.right = "3%"; Ahmad 12-8-2020
//     frmLoginKA.btnFaceID.right = "3%"; Ahmad 12-8-2020
  }
}

function cancelLabibaLoginProcess(){
  if(kony.store.getItem("cameFromLabiba") === true){
    kony.store.setItem("cameFromLabiba", false);
    frmLoginKA.flxRegisterMobileBanking.setVisibility(true);
    frmLoginKA.flxForgotP.setVisibility(true);
    frmLoginKA.flxSwitchLanguage.setVisibility(true);
   //  frmLoginKA.flxPayAppLink.setVisibility(true);
    frmLoginKA.contactLink.setVisibility(true);
    frmLoginKA.btnOnboarding.setVisibility(true);
    frmLoginKA.atmLink.setVisibility(true);
    frmLoginKA.btnBOJPay.setVisibility(true);// hassan came from labiba
    kony.print("hassan came from labiba");
    frmLoginKA.btnPinLogin.left="40%";// hassan came from labiba
    frmLoginKA.btnTouchID.left="70%";// hassan came from labiba
    frmLoginKA.btnCancelLabibaLogin.setVisibility(false);
    frmLoginKA.flxIconMove.setVisibility(true);// hassan camefrom labiba
//     frmLoginKA.btnPinLogin.left = "55%"; Ahmad 12-8-2020
//     frmLoginKA.btnTouchID.right = "3%"; Ahmad 12-8-2020
//     frmLoginKA.btnFaceID.right = "3%";  Ahmad 12-8-2020
  }
}

function startChatbotBeforeLogin(){
  frmLoginKA.flxIconMove.left = "85%";// hassan camefrom labiba
  frmLoginKA.flxIconMove.top = "57%";// hassan camefrom labiba
  var langL = kony.store.getItem("langPrefObj");
  if(langL === "ar")
    langL = "Arabic";
  else if(langL === "en")
    langL = "English";

  kony.store.setItem("cameFromLabiba", false);
  function labibaCallBack2(){
    frmLoginKA.show();
    kony.store.setItem("cameFromLabiba", true);
  }
  kony.print("Starting Labiba");
  try{
    //#ifdef android
    returnedValue = LabibaConfig.connLabiba( langL, "0",0, labibaCallBack2);
    kony.print("Starting Labiba Returned Value "+returnedValue);
    //#endif

    //#ifdef iphone
    var Labiba2Object = new LabibaConfigIOS.Labiba2();
    Labiba2Object.startLabiba(langL,"0",labibaCallBack2);
    //#endif
  }catch(ex){
    kony.print("error calling labiba FFI "+JSON.stringify(ex)+"   "+ex);
  }
}

function startChatbotAfterLogin(custID,cameFromInside){
  
  function labibaCallBack2(){}
  if(kony.store.getItem("cameFromLabiba")===true || cameFromInside === true)
  {
    var langL = kony.store.getItem("langPrefObj");
    if(langL === "ar")
      langL = "Arabic";
    else if(langL === "en")
      langL = "English";
    kony.store.setItem("cameFromLabiba", false);
    var returnedValue;
    try{
      //#ifdef android
      returnedValue = LabibaConfig.connLabiba( langL, custID, 0,labibaCallBack2); // hassan add new param to labiba
      kony.print("Starting Labiba Returned Value "+returnedValue);
      //#endif

      //#ifdef iphone
      var Labiba2Object = new LabibaConfigIOS.Labiba2();
      Labiba2Object.startLabiba(langL,custID,labibaCallBack2);
      //#endif
    }catch(ex){
      kony.print("error calling labiba FFI "+JSON.stringify(ex));
    }
  }
}

var btnStartX=0, btnStartY=0, btnLeft = 0, btnTop = 0;
function chatbuttonTouchStart(source,x,y){
  btnStartX = x;
  btnStartY = y;
}

function chatbuttonTouchMove(source,x,y){
  btnLeft = parseInt(source.frame.x);
  btnTop = parseInt(source.frame.y);
  frmLoginKA.flxIconMove.left = btnLeft+(x-btnStartX);
  frmLoginKA.flxIconMove.top = btnTop+(y-btnStartY);
  frmLoginKA.apScrollEnable.forceLayout();
}

function chatbuttonDashboardTouchMove(source,x,y){  
  btnLeft = parseInt(source.frame.x);
  btnTop = parseInt(source.frame.y);
  frmAccountsLandingKA.flxIconMove1.left = btnLeft+(x-btnStartX);
  frmAccountsLandingKA.flxIconMove1.top = btnTop+(y-btnStartY);
  frmAccountsLandingKA.forceLayout();
}
function startOnboardingActivity(){
  var langOnboarding = kony.store.getItem("langPrefObj");
  if(langOnboarding === "ar")
    langOnboarding = true;
  else if(langOnboarding === "en")
    langOnboarding = false;

  //#ifdef android
   var PublicFaceScanEncryptionKey =
          "-----BEGIN PUBLIC KEY-----\n" +
          "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5PxZ3DLj+zP6T6HFgzzk\n" +
          "M77LdzP3fojBoLasw7EfzvLMnJNUlyRb5m8e5QyyJxI+wRjsALHvFgLzGwxM8ehz\n" +
          "DqqBZed+f4w33GgQXFZOS4AOvyPbALgCYoLehigLAbbCNTkeY5RDcmmSI/sbp+s6\n" +
          "mAiAKKvCdIqe17bltZ/rfEoL3gPKEfLXeN549LTj3XBp0hvG4loQ6eC1E1tRzSkf\n" +
          "GJD4GIVvR+j12gXAaftj3ahfYxioBH7F7HQxzmWkwDyn3bqU54eaiB7f0ftsPpWM\n" +
          "ceUaqkL2DZUvgN0efEJjnWy5y1/Gkq5GGWCROI9XG/SwXJ30BbVUehTbVcD70+ZF\n" +
          "8QIDAQAB\n" +
          "-----END PUBLIC KEY-----";
  onboardingNav.setPublicFaceMapEncryptionKey(PublicFaceScanEncryptionKey);

    var BaseURLzoom = "https://faceid.bankofjordan.com.jo";//"http://fc80888a9fb6.ngrok.io"
  onboardingNav.setZoomServerBaseURL(BaseURLzoom);
  
    var SMS_ARGS_U = "Bojiuser";
    var SMS_ARGS_P = "B0jiU$er";
  onboardingNav.setSendSMSApiHeaders(
		SMS_ARGS_U, 
		SMS_ARGS_P);
  var IMAGE_ARGS_U = "OFFTEC";
  var IMAGE_ARGS_P = "OFFTEC_wssi";
  onboardingNav.setSendImagesApiHeaders(
		 IMAGE_ARGS_U, 
		 IMAGE_ARGS_P);
  
  var DeviceKeyIdentifier = "dCIPusItiG2zzMa6dSETSaOAnHPwcMaF";
  onboardingNav.setZoomDeviceLicenseKeyIdentifier(DeviceKeyIdentifier);
  var ProductionKeyText = 
      "appId      = com.labiba.onboardingapp,labiba.bank.labiba_banks,com.bankofjordan.mobileapp,com.bankofjordan.rmobileapp\n" +
      "expiryDate = 2021-02-10\n" +
      "key        = 003045022100e10ce2397ece7d84c24763ec34debf0d7916707660218608460d8eeb004fb2f602203998ba9cba74250fe59b20401a0e2d5640a17b26462b322938e1588bacea9784\n";
 
  onboardingNav.setZoomProductionKeyText(ProductionKeyText);
  var URL = "https://bojsrvbus.bankofjordan.com.jo/BOJMOBWS-bsBojInsertSmsMobile-context-root/bsBojInsertSmsMobile";
  onboardingNav.SMS_URL(URL);
  var form_url = "https://ecmforms.bankofjordan.com.jo/Forms/MobileAccountOpening";
  //var form_url = "https://ecmwebuat.bankofjordan.com.jo/Forms/MobileAccountOpeningUAT3";
  onboardingNav.MOBILE_FORM_URL(form_url);
  //var BaseURL = "https://ecmwebuat.bankofjordan.com.jo";
  var BaseURL = "https://ecmforms.bankofjordan.com.jo";
  var BodyURL = "/SendImage/SendImage.asmx";
  onboardingNav.setSEND_IMAGES_URL(BaseURL, BodyURL);
  onboardingConfig.startActivity(langOnboarding);
  //#endif

  //#ifdef iphone
  var OnBoardingIOSObject = new IOSonboardingConfig.OnBoardingIOS();
  OnBoardingIOSObject.startOnboarding(langOnboarding);
  //#endif
}
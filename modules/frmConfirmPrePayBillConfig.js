var frmConfirmPrePayBillConfig = {
    "formid":"frmConfirmPrePayBill" ,
	"frmConfirmPrePayBill":{
	"entity":"Transactions",
	"objectServiceName":"RBObjects",
	"objectServiceOptions":{"access":"online"}
	},
  "lblBillerCode":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_BillerCode"
    }		  
  },
  "lblCustID":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"custId"
    }		  
  },
  "lblLang":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"lang"
    }		  
  },
  "lblAccountNumber":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"fromAccountNumber"
    }		  
  },
  "lblAccBr":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"SourceBranchCode"
    }		  
  },
  "lblSvcType":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_ServiceType"
    }		  
  },
  "lblBillingNumber":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_BillingNo"
    }		  
  },
  "lblPaidAmnt":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"amount"
    }		  
  },
  "lblAccessChannel":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_AccessChannel"
    }		  
  },
  "lblPaymentMethod":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_PaymentMethod"
    }		  
  },
  "lblSendSMSflag":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_SendSMSFlag"
    }		  
  },
  "lblCustInfoFlag":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_CustInfoFlag"
    }		  
  },
  "lblPaymentStatus":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_payment_Status"
    }		  
  },
  "lblDueAmnt":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_DueAmt"
    }		  
  },
  "lblFeeAmnt":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"fees"
    }		  
  },
  "lblFeeOnBiller":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_Fees_On_Biller"
    }		  
  },
  "lblBillNo":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_BillNo"
    }		  
  },
  "lblTransferFlag":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"TransferFlag"
    }		  
  },
  "lblPID":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_ID"
    }		  
  },
  "lblfcbdrefNo":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_fcdb_ref_no"
    }		  
  },
  "lblMobileNumber":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_MobileNo"
    }		  
  },
  "pidType":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_IdType"
    }		  
  },
  "pNation":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_Nation"
    }		  
  },
  "prBillerCode":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_biller_code"
    }		  
  },
  "prServiceCode":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_serv_type_code"
    }		  
  },
  "prBillingNo":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_billing_no"
    }		  
  },
  "prdenoFlag":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"P_deno"
    }		  
  },
  "prValidationCode":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_validation_code"
    }		  
  },
  "prDueAmount":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_due_amt"
    }		  
  },
  "prMobileNo":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"P_mobile_no"
    }		  
  },
  "prPaymentType":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_paymt_Type"
    }		  
  },
  "prAccorCCNo":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_acc_or_cr"
    }		  
  },
  "prAccBr":{
    "fieldprops": {
      "entity":"Transactions",
      "widgettype":"Label",
      "field":"p_account_br"
    }		  
  }

};
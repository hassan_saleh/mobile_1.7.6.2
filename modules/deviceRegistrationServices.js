kony = kony || {};
kony.boj = kony.boj || {};

kony.boj.imeiPermissionFlag = false;

var selectedRowIndex = -1;

function callRegisterDeviceBOJ() {
  kony.print("isDevRegDone-->" + isDevRegDone);
  //if isDevRegDone flag is F,Then make the service call.
  if (isDevRegDone == "F" || kony.store.getItem("isDeviceRegisteredMandatory") === true) {//isDevRegDone == "F"
    RegDevice();
  } else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    var errorMessage = kony.i18n.getLocalizedString("i18n.common.Devicealreadyredg");
    customAlertPopup(geti18Value("i18n.login.deviceRegisteration"), errorMessage, popupCommonAlertDimiss, "");
    exceptionLogCall("Device Registration","function","","callRegisterDeviceBOJ -- else "+errorMessage);
    //accountDashboardDataCall();
    return;
  }
}

function RegDevice() {
  var devID = deviceid;
  if (kony.boj.imeiPermissionFlag)
    imei = kony.os.deviceInfo().uid;
  else
    imei = "";
  var deviceName = kony.os.deviceInfo().model;
  var langSelected = kony.store.getItem("langPrefObj");
  var Language = langSelected.toUpperCase();

  var scopeObj = this;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options = {
    "access": "online",
    "objectName": "RBObjects"
  };
  var headers = {};
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
  var queryParams = {
    "Language": Language,
    "user_id": user_id,
    "custId": custid,
    "deviceId": devID,
    "devInfo": deviceName,
    "Imei": imei,
    "ksid": isEmpty(KSID) ? "" : KSID,
    "appid": konyRef.mainRef.appId
  };
  var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration", queryParams);

  var serviceOptions = {
    "dataObject": dataObject,
    "headers": headers
  }; //,"queryParams":queryParams}; 
  kony.print("Input params-->" + JSON.stringify(serviceOptions));
  modelObj.create(serviceOptions, deviceRegSuccessCallback, deviceRegErrorCallback);
}

function deviceRegSuccessCallback(response) {
  iWatchcustid = "";
  kony.print("deviceRegSuccessCallback-->" + JSON.stringify(response));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  var deviceName = kony.i18n.getLocalizedString("i18n.common.ThisDevice");
  var fid = kony.application.getCurrentForm().id;
  if (response.statusCode !== null && !isEmpty(response.statusCode) && response.statusCode === "S0009") {
    if (fid !== "frmDeviceRegistrationOptionsKA") {
      var res = devlistSelectedObj;
      deviceName = res.devInfo;
    }

    if (deviceName == kony.i18n.getLocalizedString("i18n.common.ThisDevice")) {
      if (response.soft_token != null && response.soft_token != "") {
        kony.print("response.soft_token--> " + response.soft_token);
        try{
        var resoftToken = EVfn(response.soft_token);
        var storeData = isEmpty(resoftToken) ? "" : resoftToken;
        kony.store.setItem("soft_token", storeData);
        }catch(err){
          kony.print(err);
           exceptionLogCall("EVfn","UI ERROR","UI",err);        
        }
        siriObject.softToken = response.soft_token;
        siriObject.deviceID = deviceid;
        //#ifdef iphone
        com.siriSetup.sendSoftTokenToIOS(siriObject);
        //#endif
        //#ifdef ipad
        com.siriSetup.sendSoftTokenToIOS(siriObject);
        //#endif
        kony.print("soft token : " + response.soft_token);
      }else{
        //remove the token

        siriObject.softToken = "";
        siriObject.deviceID = deviceid;
        kony.store.removeItem("soft_token");
        //#ifdef iphone
        com.siriSetup.sendSoftTokenToIOS(siriObject);
        //#endif
        //#ifdef ipad
        com.siriSetup.sendSoftTokenToIOS(siriObject);
        //#endif
      }
     } else {
       //do not save SoftT.
	   if (response.soft_token != null && response.soft_token != "") {
        kony.print("response.soft_token--> " + response.soft_token);
         try{
         var resoftToken1 = EVfn(response.soft_token);
        var storeData1 = isEmpty(resoftToken1) ? "" : resoftToken1;
        kony.store.setItem("soft_token", storeData1);
        }catch(err){
          kony.print(err);
           exceptionLogCall("EVfn","UI ERROR","UI",err);
          
        }

        siriObject.softToken = response.soft_token;
        siriObject.deviceID = deviceid;
        //#ifdef iphone
        com.siriSetup.sendSoftTokenToIOS(siriObject);
        //#endif
        //#ifdef ipad
        com.siriSetup.sendSoftTokenToIOS(siriObject);
        //#endif
        kony.print("soft token : " + response.soft_token);
      }
     } 

    isDevRegDone = "T";
    frmAuthorizationAlternatives.flxAccounts.isVisible = true;
    frmAuthorizationAlternatives.btnQuickCancel.isVisible = false;
    frmAuthorizationAlternatives.btnSaveQuickBal.isVisible = false;
    frmAuthorizationAlternatives.lblQuickAccess.text = geti18nkey("i18n.quickAccessSetting.quickAccessPrelogin");
    frmAuthorizationAlternatives.lblQuickAccessBody.text = geti18nkey("i18n.quickAccessSetting.quickAccessPreloginDesc");
    kony.print("isDeviceRegistered ::: "+isDeviceRegistered);
    if(isDeviceRegistered === "T" && kony.store.getItem("isDeviceRegisteredMandatory") === true)
    	getAccountDataforQuickPreviewAccounts();
    kony.store.setItem("isDeviceRegisteredMandatory", false);
    exceptionLogCall("DeviceRegistration",response.statusCode,"service","deviceRegSuccessCallback -> device registration succeeded");
  } else {
    isDevRegDone = "F";
    frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
    frmAccountsLandingKA.flxDeals.setVisibility(false);
    frmAccountsLandingKA.flxLoans.setVisibility(false);
    frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
    frmAccountsLandingKA.flxDeals.zIndex = 1;
    frmAccountsLandingKA.flxLoans.zIndex = 1;
    frmAccountsLandingKA.forceLayout();
    kony.print("failed :::"+isDeviceRegistered+" "+kony.store.getItem("isDeviceRegisteredMandatory"));
    if(isDeviceRegistered === "F" && kony.store.getItem("isDeviceRegisteredMandatory") === true)
    	accountDashboardDataCall();
    if (!isEmpty(response.statusCode) && response.statusCode === "S0043") {
      if(isDeviceRegistered === "T")
      	customAlertPopup(geti18Value("i18n.maps.Info"), response.statusMessage, popupCommonAlertDimiss, "");
      exceptionLogCall("DeviceRegistration",response.statusCode,"service","deviceRegSuccessCallback -> "+response.statusMessage);
    } else if (!isEmpty(response.statusCode) && response.statusCode === "S0046") {
      if(isDeviceRegistered === "T")
      	customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.deviceReg.S0046"), deRegAndRegOnCurrentUser,  popupCommonAlertDimiss, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));          
      exceptionLogCall("DeviceRegistration",response.statusCode,"service","deviceRegSuccessCallback -> "+kony.i18n.getLocalizedString("i18n.deviceReg.S0046"));
    }
    kony.store.setItem("isDeviceRegisteredMandatory", true);
  }
}

function deviceRegErrorCallback(err) {
  iWatchcustid = "";
  kony.print("deviceRegErrorCallback-->" + "An Error has occured, cant register" + err);
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  frmAuthorizationAlternatives.flxAccounts.isVisible = true;
  frmAuthorizationAlternatives.btnQuickCancel.isVisible = false;
  frmAuthorizationAlternatives.btnSaveQuickBal.isVisible = false;
  frmAuthorizationAlternatives.lblQuickAccess.text = geti18nkey("i18n.quickAccessSetting.quickAccessPrelogin");
  frmAuthorizationAlternatives.lblQuickAccessBody.text = geti18nkey("i18n.quickAccessSetting.quickAccessPreloginDesc");
  isDevRegDone = "F";
  kony.store.setItem("isDeviceRegisteredMandatory", true);
  // frmAuthorizationAlternatives.show();
  frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
  frmAccountsLandingKA.flxDeals.setVisibility(false);
  frmAccountsLandingKA.flxLoans.setVisibility(false);
  frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
  frmAccountsLandingKA.flxDeals.zIndex = 1;
  frmAccountsLandingKA.flxLoans.zIndex = 1;
  frmAccountsLandingKA.forceLayout();
  accountDashboardDataCall();
  exceptionLogCall("DeviceRegistration","","service","deviceRegErrorCallback -> "+err);
}

function getDeviceList() {
  if (kony.sdk.isNetworkAvailable()) {
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");

    dataObject.addField("custId", custid);
    dataObject.addField("lang", kony.store.getItem("langPrefObj"));

    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("Input params DeviceList-->" + JSON.stringify(serviceOptions));
    modelObj.customVerb("GetDevList", serviceOptions, deviceListFetchSuccess, deviceListFetchError);
  } else {
    kony.sdk.mvvm.KonyApplicationContext.hideLoadingScreen("");
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}

function deviceListFetchSuccess(res) {

  var devID = deviceid;
  frmDeviceRegistration.segRegisteredDevices.setData([]);
  if (res.statusCode === "S0039" && res.DeviceList !== undefined) {
    frmDeviceRegistration.segRegisteredDevices.widgetDataMap = {
      lblDeviceTitle: "devInfo"
    };
    var tempArr = [];
    for (var i in res.DeviceList) {
      kony.print(i + "-->" + JSON.stringify(res.DeviceList[i]));
      if (res.DeviceList[i].RegisterFlag === "T") {
        if (devID == res.DeviceList[i].deviceId) {
          res.DeviceList[i].devInfo = kony.i18n.getLocalizedString("i18n.common.ThisDevice");
          isDevRegDone = "T";
        }
        tempArr.push({
          "deviceId": res.DeviceList[i].deviceId,
          "devInfo": res.DeviceList[i].devInfo,
          "RegisterFlag": res.DeviceList[i].RegisterFlag,
          "KSID":  res.DeviceList[i].KSID,
          "APPID":  res.DeviceList[i].APPID,
          template: flxDeviceDetailsEnabled
        });
      } else {
        if (devID == res.DeviceList[i].deviceId) {
          res.DeviceList[i].devInfo = kony.i18n.getLocalizedString("i18n.common.ThisDevice");
          isDevRegDone = "F";
          tempArr.push({
            "deviceId": res.DeviceList[i].deviceId,
            "devInfo": res.DeviceList[i].devInfo,
            "RegisterFlag": res.DeviceList[i].RegisterFlag,
            "KSID":  res.DeviceList[i].KSID,
            "APPID":  res.DeviceList[i].APPID,
            template: flxDeviceDetailsDisabled
          });
          //remove the token

          siriObject.softToken = "";
          siriObject.deviceID = deviceid;
          kony.store.removeItem("soft_token");
          //#ifdef iphone
          com.siriSetup.sendSoftTokenToIOS(siriObject);
          //#endif   
          //#ifdef ipad
          com.siriSetup.sendSoftTokenToIOS(siriObject);
          //#endif
        }

      }
    }
    gblUnsubscribe = {
      "ksid" : "",
      "appId" : "",
      "deviceId" : ""
    };
    frmDeviceRegistration.segRegisteredDevices.setData(tempArr);
  }
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();

  var frmDeviceRegistrationModelConfigObj = INSTANCE.getFactorySharedInstance().createConfigClassObject(frmDeviceRegistrationConfig);
  var frmDeviceRegistrationControllerObj = INSTANCE.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmDeviceRegistrationController", INSTANCE, frmDeviceRegistrationModelConfigObj);
  var frmDeviceRegistrationControllerExtObj = INSTANCE.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmDeviceRegistrationControllerExtension", frmDeviceRegistrationControllerObj);
  frmDeviceRegistrationControllerObj.setControllerExtensionObject(frmDeviceRegistrationControllerExtObj);
  var frmDeviceRegistrationFormModelObj = INSTANCE.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmDeviceRegistrationFormModel", frmDeviceRegistrationControllerObj);
  var frmDeviceRegistrationFormModelExtObj = INSTANCE.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmDeviceRegistrationFormModelExtension", frmDeviceRegistrationFormModelObj);
  frmDeviceRegistrationFormModelObj.setFormModelExtensionObj(frmDeviceRegistrationFormModelExtObj);
  INSTANCE.setFormController("frmDeviceRegistration", frmDeviceRegistrationControllerObj);

  var listController = INSTANCE.getFormController("frmDeviceRegistration");
  var navObject = new kony.sdk.mvvm.NavigationObject();
  navObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
  navObject.setRequestOptions("form", {
    "headers": {}
  });
  listController.performAction("navigateTo", ["frmDeviceRegistration", navObject]);
}

function deviceListFetchError(res) {
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  var Message = geti18nkey("i18n.common.somethingwentwrong");
  customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
}

function changeRegistrationStatus(device, index) {

  selectedRowIndex = index[1];
  kony.print("device data:" + JSON.stringify(device));
  frmDeviceRegistration.lblLanguage.text = kony.store.getItem("langPrefObj");
  if (device.devInfo == kony.i18n.getLocalizedString("i18n.common.ThisDevice")) {
    var deviceName = kony.os.deviceInfo().model;
    frmDeviceRegistration.lblDeviceInfo.text = deviceName;
  } else {
    frmDeviceRegistration.lblDeviceInfo.text = device.devInfo;
  }
  frmDeviceRegistration.lblCustId.text = custid;
  frmDeviceRegistration.lblDeviceId.text = device.deviceId;
  frmDeviceRegistration.lblRegisterFlag.text = device.RegisterFlag;
  frmDeviceRegistration.lblUserId.text = user_id;
  gblUnsubscribe = {
    "ksid" : device.KSID,
    "appId" : device.APPID,
    "deviceId" : device.deviceId
  };

  if (device.RegisterFlag === "F") {
    var message = geti18Value("i18n.DeviceReg.AlertAndNotification");
    //#ifdef iphone
    message = message+" "+geti18Value("i18n.iwatch.registermessage");
    //#endif
    frmDeviceRegistration.lblRegisterFlag.text = "T";
    kony.boj.popupDeviceRegistration(message,
                                     geti18Value("i18n.DeviceReg.RegisterDeviceConfirmation"),
                                     geti18Value("i18n.common.YES"), registerPush,
                                     geti18Value("i18n.common.NO"), kony.boj.dismissPopup);
  } else {
    frmDeviceRegistration.lblRegisterFlag.text = "F";
    kony.boj.popupDeviceRegistration(geti18Value("i18n.DeviceReg.De-Register"),
                                     geti18Value("i18n.DeviceReg.De-RegisterConfirmation"),
                                     geti18Value("i18n.common.YES"), onClickYesDeviceDeRegister,
                                     geti18Value("i18n.common.NO"), kony.boj.dismissPopup);
  }
}

function onClickYesDeviceRegister() {
  frmDeviceRegistration.lblKSID.text = isEmpty(KSID) ? "" : KSID;
  frmDeviceRegistration.lblAppID.text = konyRef.mainRef.appId;
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var listController = INSTANCE.getFormController("frmDeviceRegistration");
  listController.performAction("saveData");
}

function onClickYesDeviceDeRegister() {
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var listController = INSTANCE.getFormController("frmDeviceRegistration");
  listController.performAction("deleteData");
}

function disableRegisteredDevice() {
  frmDeviceRegistration.segRegisteredDevices.widgetDataMap = {
    lblDeviceTitle: "devInfo"
  };
  frmDeviceRegistration.lblRegisterFlag.text = "F";
  var data = {
    "deviceId": frmDeviceRegistration.lblDeviceId.text,
    "devInfo": frmDeviceRegistration.lblDeviceInfo.text,
    "RegisterFlag": "F",
    "custid": custid,
    template: flxDeviceDetailsDisabled
  };

  frmDeviceRegistration.segRegisteredDevices.setDataAt(data, selectedRowIndex);

  frmDeviceRegistration.lblLanguage.text = "";
  frmDeviceRegistration.lblDeviceInfo.text = "";
  frmDeviceRegistration.lblCustId.text = "";
  frmDeviceRegistration.lblDeviceId.text = "";
  frmDeviceRegistration.lblRegisterFlag.text = "";
}

function enableRegisteredDevice() {
  frmDeviceRegistration.segRegisteredDevices.widgetDataMap = {
    lblDeviceTitle: "devInfo"
  };
  frmDeviceRegistration.lblRegisterFlag.text = "T";
  var data = {
    "deviceId": frmDeviceRegistration.lblDeviceId.text,
    "devInfo": frmDeviceRegistration.lblDeviceInfo.text,
    "RegisterFlag": "T",
    "custid": custid,
    template: flxDeviceDetailsEnabled
  };

  frmDeviceRegistration.segRegisteredDevices.setDataAt(data, selectedRowIndex);

  frmDeviceRegistration.lblLanguage.text = "";
  frmDeviceRegistration.lblDeviceInfo.text = "";
  frmDeviceRegistration.lblCustId.text = "";
  frmDeviceRegistration.lblDeviceId.text = "";
  frmDeviceRegistration.lblRegisterFlag.text = "";
}

kony.boj.readPhoneStatePermission = function() {
  try {
    //#ifdef android
    var imei = kony.os.deviceInfo().uid;
    kony.boj.imeiPermissionFlag = true;
    //#endif
  } catch (err) {
    kony.boj.imeiPermissionFlag = false;
  }
};

function permissionStatusCallback(res) {
  if (res.status == kony.application.PERMISSION_DENIED)
    customAlertPopup(geti18Value("i18n.common.alert"), geti18Value("i18n.common.phonestate"), popupCommonAlertDimiss, "");
}

function getCurrentDeviceId() {
  //#ifdef iphone
  var deviceID = com.kony.keychain.getFromKeychain("deviceID");
  if(deviceID !== null && deviceID !== undefined && deviceID !== ""){
    return deviceID;
  }else{
    deviceID = kony.os.deviceInfo().identifierForVendor;
    var status = com.kony.keychain.saveInKeychain(deviceID, "deviceID");
    return deviceID;
  }

  //#endif
   //#ifdef ipad
  var deviceID = com.kony.keychain.getFromKeychain("deviceID");
  if(deviceID !== null && deviceID !== undefined && deviceID !== ""){
    return deviceID;
  }else{
    deviceID = kony.os.deviceInfo().identifierForVendor;
    var status = com.kony.keychain.saveInKeychain(deviceID, "deviceID");
    return deviceID;
  }

  //#endif

  //#ifdef android
  return kony.os.deviceInfo().deviceid;
  //#endif
}


function deRegAndRegOnCurrentUser() {
  popupCommonAlert.dismiss();
  if (kony.sdk.isNetworkAvailable()) {
    ShowLoadingScreen();

    if (kony.boj.imeiPermissionFlag)
      imei = kony.os.deviceInfo().uid;
    else
      imei = "";
    var deviceName = kony.os.deviceInfo().model;
    var Language = kony.store.getItem("langPrefObj");
    Language = Language.toUpperCase();

    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");

    dataObject.addField("Language", Language);
    dataObject.addField("user_id", user_id);
    dataObject.addField("custId", custid);
    dataObject.addField("deviceId", deviceid);
    dataObject.addField("devInfo", deviceName);
    dataObject.addField("Imei", imei);
    dataObject.addField("ksid", isEmpty(KSID) ? "" : KSID);
    dataObject.addField("appid", konyRef.mainRef.appId);

    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("Input params verify otp-->" + JSON.stringify(serviceOptions));
    modelObj.customVerb("deAndReReg", serviceOptions, deviceRegSuccessCallback, deviceRegErrorCallback);

  } else {
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}

function showAccLanding(){
  popupCommonAlert.dismiss();
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
  frmAccountsLandingKA.flxDeals.setVisibility(false);
  frmAccountsLandingKA.flxLoans.setVisibility(false);
  frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
  frmAccountsLandingKA.flxDeals.zIndex = 1;
  frmAccountsLandingKA.flxLoans.zIndex = 1;
  frmAccountsLandingKA.forceLayout();
  gblNoThanksClickSaveDatatoQB = true;
  accountDashboardDataCall();
}
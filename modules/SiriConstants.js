// All the siri Related variables are declared in this file


/*
send environemt as 
for development  = DEV
for UAT = UAT
for production = PROD

Do not send any value for 
SiriUsername : ""
SiriPwd : ""
*/

var environment = "PROD";
var SiriUsername = "";
var SiriPwd = "";

var siriObject = {
  "softToken":"",
  "deviceID":"",
  "userName":SiriUsername,
  "pswd":SiriPwd,
  "env":environment
};
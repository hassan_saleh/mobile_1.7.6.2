//Created and edited by Arpan

kony = kony || {};
kony.boj = kony.boj || {};

kony.boj.selectedTabShowBene  ="ALL";
kony.boj.isSearchShowBene = false;

kony.boj.beneList = {
  "countBene":0
};

kony.boj.resetBeneList = function(){
  kony.boj.beneList = {
    "countBene":0,
    "IFB":{
      "fav":[],
      "norm":[]
    },
    "DTB":{
      "fav":[],
      "norm":[]
    },
    "ITB":{
      "fav":[],
      "norm":[]
	},
    "JMP":{
      "fav":[],
      "norm":[]
    }
  };
};

kony.boj.populateBeneList = function(data){
  //   kony.print("Data:" + JSON.stringify(data));

  for(var i = 0; i < data.length - 1; i++){
    var index = i;
    var tempData = data[i];
    for(var j = i + 1; j < data.length; j++){
      if(tempData.beneficiaryName.localeCompare(data[j].beneficiaryName) >= 1){
        index = j;
        tempData = data[j];
      }
    }
    data[index] = data[i];
    data[i] = tempData;
  }

  i = 0;

  for(var i in data){
    if(data[i].benType === "IFB" || data[i].benType === "DTB" || data[i].benType === "ITB"){
      data[i].initial = data[i].beneficiaryName.substring(0,2).toUpperCase();
      data[i].icon = {
        backgroundColor: kony.boj.getBackGroundColour(data[i].initial)
      };
      data[i].beneficiaryFullName = data[i].beneficiaryName;
      if(data[i].beneficiaryName.length > 30){
      	data[i].beneficiaryName = data[i].beneficiaryName.substring(0,30)+"...";
      }
      data[i].beneficiaryFullNickName = data[i].nickName;
      if(data[i].nickName.length > 30){
        data[i].nickName = data[i].nickName.substring(0,30)+"...";
      }
      if(data[i].isFav == "Y"){
        if (gblTModule === "send"){ //Change the fav icon when is send money Omar ALnajjar 
          data[i].btnSetting = {
            "text":"{"
          };
        }else{
          data[i].btnSetting = {
            "text":","
          };
        }

        kony.boj.beneList[data[i].benType].fav.push(data[i]);
      }
      else{
        if (gblTModule === "send"){//Change the fav icon when is send money Omar ALnajjar
          data[i].btnSetting = {
            "text":"}"
          };
        }else{
          data[i].btnSetting = {
            "text":"k"
          };
        }
        kony.boj.beneList[data[i].benType].norm.push(data[i]);
      }
      kony.boj.beneList.countBene++;
    }
  }
  //     alert("BOJ:\n" + JSON.stringify(kony.boj.beneList.IFB));
  //     alert("DOM:\n" + JSON.stringify(kony.boj.beneList.DTB));
  //     alert("INT:\n" + JSON.stringify(kony.boj.beneList.ITB));
};

kony.boj.setBeneData = function(selectedTab){
  kony.boj.selectedTabShowBene = selectedTab;
  if(selectedTab === "IFB"){
//   	frmShowAllBeneficiary.segAllBeneficiary.height = "79%";
    frmShowAllBeneficiary.btnTransactionStatus.setVisibility(false);
  }else{
//   	frmShowAllBeneficiary.segAllBeneficiary.height = "69%";
    frmShowAllBeneficiary.btnTransactionStatus.setVisibility(false);
  }
  if(kony.boj.beneList.countBene === 0){
    frmAddExternalAccountHome.show();
    return;
  }

  if(frmShowAllBeneficiary.tbxSearch.text !== "" && kony.boj.isSearchShowBene === true){
    kony.boj.searchBeneList(frmShowAllBeneficiary.tbxSearch.text);
    return;
  }

  frmShowAllBeneficiary.segAllBeneficiary.setData([]);
  frmShowAllBeneficiary.segAllBeneficiary.isVisible = true;
  frmShowAllBeneficiary.lblStatus.isVisible = false;
  kony.boj.deleteFlagBenf = false;
  frmShowAllBeneficiary.segAllBeneficiary.widgetDataMap = { 
    lbltmpTitle: "lbltmpTitle",
    accountNumber :"benAcctNo",
    BenificiaryName: "nickName",
    BenificiaryFullName: "beneficiaryName",
    lblInitial: "initial",
    flxIcon1: "icon",
    btnFav: "btnSetting"
  };

  var dataFav = [],dataNorm = [];

  if(selectedTab === "ALL"){
    dataFav = Array.prototype.concat.apply(dataFav, [kony.boj.beneList.IFB.fav, kony.boj.beneList.DTB.fav, kony.boj.beneList.ITB.fav]);
    dataNorm = Array.prototype.concat.apply(dataNorm, [kony.boj.beneList.IFB.norm, kony.boj.beneList.DTB.norm, kony.boj.beneList.ITB.norm]);
  }
  else{
    dataFav = kony.boj.beneList[selectedTab].fav;
    dataNorm = kony.boj.beneList[selectedTab].norm;
  }

  if(dataFav.length !== 0 && dataNorm.length !== 0){
    frmShowAllBeneficiary.segAllBeneficiary.data =[
      [
        {lbltmpTitle: geti18Value("18n.transfer.trustedBene")},
        dataFav
      ],
      [
        {lbltmpTitle: geti18Value("i18.Bene.allBene")},
        dataNorm
      ],
    ];
  }
  else if(dataFav.length !== 0){
    frmShowAllBeneficiary.segAllBeneficiary.data =[
      [
        {lbltmpTitle: geti18Value("18n.transfer.trustedBene")},
        dataFav
      ]
    ];
  }
  else if(dataNorm.length !== 0){
    frmShowAllBeneficiary.segAllBeneficiary.data =[
      [
        {lbltmpTitle: geti18Value("i18.Bene.allBene")},
        dataNorm
      ],
    ];
  }
  else if(dataFav.length === 0 && dataNorm.length === 0){
    frmShowAllBeneficiary.segAllBeneficiary.isVisible = false;
    frmShowAllBeneficiary.lblStatus.isVisible = true;
    frmShowAllBeneficiary.lblStatus.text = geti18Value("i18n.bene.noBeneError");
  }
};

kony.boj.searchBeneList = function(searchTerm){
  kony.boj.isSearchShowBene = true;
  searchTerm = searchTerm.toLowerCase();
  //   alert("----" + searchTerm);

  var dataFav = [], dataNorm = [], data = [], data2 = [], i;

  if(kony.boj.selectedTabShowBene === "ALL"){
    data = Array.prototype.concat.apply([], [kony.boj.beneList.IFB.fav, kony.boj.beneList.DTB.fav, kony.boj.beneList.ITB.fav]);
    for(i in data){
      if(data[i].beneficiaryName.toLowerCase().indexOf(searchTerm) != -1)
        dataFav.push(data[i]);
    }

    data2 = Array.prototype.concat.apply([], [kony.boj.beneList.IFB.norm, kony.boj.beneList.DTB.norm, kony.boj.beneList.ITB.norm]);
    for(i in data2){
      if(data2[i].beneficiaryName.toLowerCase().indexOf(searchTerm) != -1)
        dataNorm.push(data2[i]);
    }

    if(data.length === 0 && data2.length === 0){
      frmShowAllBeneficiary.segAllBeneficiary.isVisible = false;
      frmShowAllBeneficiary.lblStatus.isVisible = true;
      frmShowAllBeneficiary.lblStatus.text = geti18Value("i18n.bene.noBeneError");
      return;
    }
  }
  else{
    data = kony.boj.beneList[kony.boj.selectedTabShowBene].fav;
    for(i in data){
      if(data[i].beneficiaryName.toLowerCase().indexOf(searchTerm) != -1)
        dataFav.push(data[i]);
    }

    data2 = kony.boj.beneList[kony.boj.selectedTabShowBene].norm;
    for(i in data2){
      if(data2[i].beneficiaryName.toLowerCase().indexOf(searchTerm) != -1)
        dataNorm.push(data2[i]);
    }

    if(data.length === 0 && data2.length === 0){
      frmShowAllBeneficiary.segAllBeneficiary.isVisible = false;
      frmShowAllBeneficiary.lblStatus.isVisible = true;
      frmShowAllBeneficiary.lblStatus.text = geti18Value("i18n.bene.noBeneError");
      return;
    }
  }

  //   alert(JSON.stringify(dataFav) + "\n" + JSON.stringify(dataNorm));


  frmShowAllBeneficiary.segAllBeneficiary.setData([]);
  frmShowAllBeneficiary.segAllBeneficiary.isVisible = true;
  frmShowAllBeneficiary.lblStatus.isVisible = false;
  frmShowAllBeneficiary.segAllBeneficiary.widgetDataMap = { 
    lbltmpTitle: "lbltmpTitle",
    accountNumber :"benAcctNo",
    BenificiaryName: "nickName",
    BenificiaryFullName: "beneficiaryName",
    lblInitial: "initial",
    flxIcon1: "icon",
    btnFav: "btnSetting"
  };


  if(dataFav.length !== 0 && dataNorm.length !== 0){
    frmShowAllBeneficiary.segAllBeneficiary.data =[
      [
        {lbltmpTitle: geti18Value("18n.transfer.trustedBene")},
        dataFav
      ],
      [
        {lbltmpTitle: geti18Value("i18.Bene.allBene")},
        dataNorm
      ],
    ];
  }
  else if(dataFav.length !== 0){
    frmShowAllBeneficiary.segAllBeneficiary.data =[
      [
        {lbltmpTitle: geti18Value("18n.transfer.trustedBene")},
        dataFav
      ]
    ];
  }
  else if(dataNorm.length !== 0){
    frmShowAllBeneficiary.segAllBeneficiary.data =[
      [
        {lbltmpTitle: geti18Value("i18.Bene.allBene")},
        dataNorm
      ],
    ];
  }
  else if(dataFav.length === 0 && dataNorm.length === 0){
    frmShowAllBeneficiary.segAllBeneficiary.isVisible = false;
    frmShowAllBeneficiary.lblStatus.isVisible = true;
    frmShowAllBeneficiary.lblStatus.text = geti18Value("i18n.bene.noBeneSearchError");
  }
};

kony.boj.changeFavTag = function(selectedBene){
    if (gblFromModule === "addBeneficiaryAsFav" ||   isAddAsTrusted === true){
          kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen();
      onClickYesRemFav();
      return;
    }
  kony.print("Selected Bene fav ::" + JSON.stringify(selectedBene));
  if(selectedBene.isFav === "Y")
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), 
                     geti18Value("18n.transfer.Removetrusted"),
                     onClickYesRemFav, onClickNoRemFav, 
                     geti18Value("i18n.common.YES"),
                     geti18Value("i18n.common.NO"));
  else
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), 
                     geti18Value("18n.transfer.trustedBene"),
                     onClickYesRemFav, onClickNoRemFav, 
                     geti18Value("i18n.common.YES"),
                     geti18Value("i18n.common.NO"));

  function onClickYesRemFav(){
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen();
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("ExternalAccounts", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("ExternalAccounts");
//     dataObject.addField("usr", "icbsserv");
//     dataObject.addField("pass", "icbsserv_1");
    dataObject.addField("custId", custid);
    dataObject.addField("accountNumber", selectedBene.benAcctNo);
    dataObject.addField("isFav", (selectedBene.isFav === "Y")?"N":"Y");
    dataObject.addField("P_BENEF_ID", selectedBene.benef_id);
    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("serviceOptions ::"+JSON.stringify(serviceOptions));
    if (kony.sdk.isNetworkAvailable()) {
      //       kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      modelObj.customVerb("update", serviceOptions, 
                          function(res){
        kony.print("success in change fav tag ::"+JSON.stringify(res));
//         kony.print("Selected rowIndexFav ::" + JSON.stringify(frmShowAllBeneficiary.segAllBeneficiary));
//         if(userAgent === "iPhone")
          kony.boj.getRowColumnInSegment(selectedBene);
        
            if (gblFromModule === "addBeneficiaryAsFav" || isAddAsTrusted === true){ 
              frmShowAllBeneficiary.show();
    }
//         else
//           kony.boj.updateBeneSegment(selectedBene, frmShowAllBeneficiary.segAllBeneficiary.selectedRowIndex[1], frmShowAllBeneficiary.segAllBeneficiary.selectedRowIndex[0]);        
      }, 
                          function(err){
        kony.print("Failed in change fav tag ::"+JSON.stringify(err));
      });
    }
//    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    popupCommonAlertDimiss();
  }

  function onClickNoRemFav(){
    popupCommonAlertDimiss();
  }
};

kony.boj.updateBeneSegment = function(selectedBene, row, section){
  try{
    if(selectedBene.isFav === "Y"){
      for(var i in kony.boj.beneList[selectedBene.benType].fav){
        if(selectedBene.benAcctNo === kony.boj.beneList[selectedBene.benType].fav[i].benAcctNo){
          kony.boj.beneList[selectedBene.benType].fav.splice(i, 1);
//           kony.boj.beneList[selectedBene.benType].fav = kony.boj.beneList[selectedBene.benType].fav.
//           		slice(0, i).concat(kony.boj.beneList[selectedBene.benType].fav.
//                 slice(i + 1, kony.boj.beneList[selectedBene.benType].fav.length));
          kony.print("After Splice ::");
          selectedBene.isFav = (selectedBene.isFav === "Y")?"N":"Y";
          selectedBene.btnSetting.text = (selectedBene.btnSetting.text === "{")?"}":"{";
//           selectedBene.btnSetting.isVisible = !selectedBene.btnSetting.isVisible;
          kony.boj.beneList[selectedBene.benType].norm.push(selectedBene);
          break;
        }
      }
    }
    else{
      for(var i in kony.boj.beneList[selectedBene.benType].norm){
        if(selectedBene.benAcctNo === kony.boj.beneList[selectedBene.benType].norm[i].benAcctNo){
          kony.boj.beneList[selectedBene.benType].norm.splice(i, 1);
//           kony.boj.beneList[selectedBene.benType].norm = kony.boj.beneList[selectedBene.benType].norm.
//           		slice(0, i).concat(kony.boj.beneList[selectedBene.benType].norm.
//                 slice(i + 1, kony.boj.beneList[selectedBene.benType].norm.length));
          kony.print("After Splice ::");
          selectedBene.isFav = (selectedBene.isFav === "Y")?"N":"Y";
          selectedBene.btnSetting.text = (selectedBene.btnSetting.text === "{")?"}":"{";
          kony.boj.beneList[selectedBene.benType].fav.push(selectedBene);
          break;
        }
      }
    }
    
    kony.print("Changed Favourite Bene::" + selectedBene);
    
    var currForm = kony.application.getCurrentForm();
    if(currForm.id === "frmNewTransferKA"){
      frmNewTransferKA.flxIcon1.skin = sknFlxToIcon;
      frmNewTransferKA.flxIcon1.backgroundColor = kony.boj.getBackGroundColour(gblSelectedBene.initial);
      frmNewTransferKA.lblIcon1.skin = sknLblFromIcon;
      frmNewTransferKA.lblIcon1.text = gblSelectedBene.initial;
    }
  }
  catch(e){
    kony.print("Change Favoutire Bene Error ::" + JSON.stringify(e));
	exceptionLogCall("kony.boj.updateBeneSegment","UI ERROR","UI",e);
  }

  var currJForm = kony.application.getCurrentForm();
  if(currJForm.id === "frmJoMoPayLanding"){ 
    setJomoBeneDataList(selectedBene);
  }
  else{
    kony.boj.setBeneData(kony.boj.selectedTabShowBene); 
   }
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
};

kony.boj.getRowColumnInSegment = function(row){
  var data = [], columnIndex = -1;
  if(row.isFav === "Y"){
    data = frmShowAllBeneficiary.segAllBeneficiary.data[0][1];
    columnIndex = 0;
  }
  else{
    if(frmShowAllBeneficiary.segAllBeneficiary.data.length === 2){
      data = frmShowAllBeneficiary.segAllBeneficiary.data[1][1];
      columnIndex = 1;
    }
    else{
      data = frmShowAllBeneficiary.segAllBeneficiary.data[0][1];
      columnIndex = 0;
    }
  }
  
  for(var i in data){
    if(row.benef_id === data[i].benef_id){
      kony.boj.updateBeneSegment(row, i, columnIndex);
      break;
    }
  }
};
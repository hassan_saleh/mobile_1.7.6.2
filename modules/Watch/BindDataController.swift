//
//  BindDataController.swift
//  KWatchOS2 Extension
//
//  Created by BOJ on 10/09/18.
//

// import Foundation

// MARK: - List screen

func checkSession(response: [String : Any]) -> (Bool, String) {
  if let session = response[kSession] as? String {
    return (true, session)
  }
  return (false, "")
}

func bindListScreenData(form: frmWatMenuItemListController, response: [String : Any]) {

  let sessionRes = checkSession(response: response)
  if sessionRes.0 {
    form.lblLoading.setText(sessionRes.1)
    form.lblLoading.setHidden(false)
    form.lblSummery.setHidden(true)
    return
  }

  form.lblLoading.setHidden(true)
  form.lblSummery.setHidden(false)

  if let menu = Menu(rawValue: menuToken) {
    switch menu {

      case .accounts:
      if let objAccounts = response["accounts"] as? NSArray, 0 != objAccounts.count {
        accounts = objAccounts
        form.segList.setNumberOfRows(objAccounts.count, withRowType: "tempMenuItem")
        let cardNum = ""
        for i in 0..<objAccounts.count {
          if let row = form.segList.rowController(at: i) as? tempMenuItemController {
            if let accountDet = objAccounts[i] as? [String: Any] {
              row.lblCardNum.setText("\(cardNum)")
              row.lblLoanName.setText(String(describing: accountDet["accountName"] ?? ""))
              row.lblDescription.setText(String(describing: accountDet["accountID"] ?? ""))
              row.lblNumber.setText("\(String(describing: accountDet["availableBalance"] ?? "")) \(String(describing: accountDet["currencyCode"] ?? ""))")
            }
          }
        }
      }
      else {
        form.lblLoading.setText("You have no accounts.")
        form.lblLoading.setHidden(false)
        form.lblSummery.setHidden(true)
      }

      case .cards:

      if let objCards = response["cards"] as? NSArray, 0 != objCards.count {
        cards = objCards
        form.segList.setNumberOfRows(objCards.count, withRowType: "tempMenuItem")
        for i in 0..<objCards.count {
          if let row = form.segList.rowController(at: i) as? tempMenuItemController {
            if let cardsDet = objCards[i] as? [String: Any] {

              let cardSuffix = String(describing: cardsDet["card_num"] ?? "").suffix(4)
              let cardPrefix = String(describing: cardsDet["card_num"] ?? "").prefix(6)

              let cardNum = "\(cardPrefix) ****** \(cardSuffix)"
              //let cardPrefix = String(describing: cardsDet["card_num"] ?? "").prefix(4)

              row.lblLoanName.setText("\(String(describing: cardsDet["card_type"] ?? "")) ")
              row.lblCardNum.setText("\(cardNum)")
              row.lblDescription.setText("\(String(describing: cardsDet["name_on_card"] ?? ""))")
              let cardBal = String(describing: cardsDet["card_balance"] ?? "")
              let curr = "JOD"

              if cardBal.characters.count != 0 {
                let a = "\(cardBal) \(curr)"
                row.lblNumber.setText("\(a)")
              }
              else {
                let cardspl = String(describing: cardsDet["spending_limit"] ?? "")

                let Cardat = "\(cardspl) \(curr)"
                row.lblNumber.setText("\(Cardat)")
              }

            }
          }
        }
      }
      else {
        form.lblLoading.setText("No Cards to show.")
        form.lblLoading.setHidden(false)
        form.lblSummery.setHidden(true)
      }

      case .deposits:

      if let objDeposits = response["deposits"] as? NSArray, 0 != objDeposits.count {
        deposits = objDeposits
        let cardNum = ""
        form.segList.setNumberOfRows(objDeposits.count, withRowType: "tempMenuItem")
        for i in 0..<objDeposits.count {
          if let row = form.segList.rowController(at: i) as? tempMenuItemController {
            if let depositsDet = objDeposits[i] as? [String: Any] {
              row.lblLoanName.setText("\(String(describing: depositsDet["accountName"] ?? ""))")
              row.lblDescription.setText("\(String(describing: depositsDet["accountID"] ?? ""))")
              row.lblCardNum.setText("\(cardNum)")
              row.lblNumber.setText("\(String(describing: depositsDet["currentBalance"] ?? "")) \(String(describing: depositsDet["currencyCode"] ?? ""))")
            }
          }
        }
      }
      else {
        form.lblLoading.setText("You have no deposits.")
        form.lblLoading.setHidden(false)
        form.lblSummery.setHidden(true)
      }

      case .loans:

      if let objLoans = response["loans"] as? NSArray, 0 != objLoans.count {
        loans = objLoans
        let cardNum = ""
        form.segList.setNumberOfRows(objLoans.count, withRowType: "tempMenuItem")
        for i in 0..<objLoans.count {
          if let row = form.segList.rowController(at: i) as? tempMenuItemController {
            if let accountDet = objLoans[i] as? [String: Any] {
              row.lblCardNum.setText("\(cardNum)")
              row.lblLoanName.setText("\(String(describing: accountDet["accountName"] ?? ""))")
              row.lblDescription.setText(String(describing: accountDet["accountID"] ?? ""))
              row.lblNumber.setText("\(String(describing: accountDet["availableBalance"] ?? "")) \(String(describing: accountDet["currencyCode"] ?? ""))")
            }
          }
        }
      }
      else {
        form.lblLoading.setText("You have no loans.")
        form.lblLoading.setHidden(false)
        form.lblSummery.setHidden(true)
      }
    }
  }
}

// MARK: - List details

func bindListDetailsScreenData(form: frmWatMenuListItemDetailsController, response: [String : Any]) {

  let sessionRes = checkSession(response: response)
  if sessionRes.0 {
    form.lblLoading.setText(sessionRes.1)
    form.lblLoading.setHidden(false)
    form.lblSummery.setHidden(true)
    return
  }    

  form.lblLoading.setHidden(true)

  if let menu = Menu(rawValue: menuToken) {
    switch menu {

      case .accounts:

      if let transactions = response["transactions"] as? NSArray, 0 != transactions.count {
        form.segDetails.setRowTypes(["tempMenuItemDetails"])
        form.segDetails.setNumberOfRows(transactions.count, withRowType: "tempMenuItemDetails")
        let cardNum = ""
        for i in 0..<transactions.count {
          if let row = form.segDetails.rowController(at: i) as? tempMenuItemDetailsController {
            if let transactionDet = transactions[i] as? [String: Any] {

              if let tdDict = transactionDet["transactionDate"] as? [String: Any] {
                row.lblDate.setText("\(String(describing: tdDict["text"] ?? ""))")
              }

              if let descDict = transactionDet["description"] as? [String: Any] {
                row.lblDescription.setText("\(String(describing: descDict["text"] ?? ""))")
              }

              if let amountDict = transactionDet["amount"] as? [String: Any] {
                row.lblAmount.setText("\(String(describing: amountDict["text"] ?? ""))")
              }
            }
          }
        }
      }
      else {
        form.lblLoading.setText("No transactions to display.")
        form.lblLoading.setHidden(false)
      }

      case .cards:

      if let transactions = response["cardTransactions"] as? NSArray, 0 != transactions.count {
        form.segDetails.setRowTypes(["tempMenuItemDetails"])
        form.segDetails.setNumberOfRows(transactions.count, withRowType: "tempMenuItemDetails")
        for i in 0..<transactions.count {
          if let row = form.segDetails.rowController(at: i) as? tempMenuItemDetailsController {
            if let transactionDet = transactions[i] as? [String: Any] {

              if let tdDict = transactionDet["transactionDate"] as? [String: Any] {
                row.lblDate.setText("\(String(describing: tdDict["text"] ?? ""))")
              }

              if let descDict = transactionDet["description"] as? [String: Any] {
                row.lblDescription.setText("\(String(describing: descDict["text"] ?? ""))")
              }

              if let amountDict = transactionDet["amount"] as? [String: Any] {
                row.lblAmount.setText("\(String(describing: amountDict["text"] ?? ""))")
              }

            }
          }
        }
      }
      else {
        form.lblLoading.setText("No transactions to display.")
        form.lblLoading.setHidden(false)
      }

      default: break

    } // switch end
  }
}

// MARK: - Loans And deposits screen

func bindLoanAndDepositsDetailsScreenData(form: frmWatMenuLAndDDetailsController, response: [String : Any]) {

  let sessionRes = checkSession(response: response)
  if sessionRes.0 {
    form.lblLoading.setText(sessionRes.1)
    form.lblLoading.setHidden(false)
    form.lblSummery.setHidden(true)
    return
  }

  form.lblLoading.setHidden(true)

  if let menu = Menu(rawValue: menuToken) {
    switch menu {
      case .deposits:
      if let transactions = response["depositInfo"] as? NSArray, 0 != transactions.count {
        debugPrint("Transactions data \(transactions)")
        form.segDetails.setRowTypes(["tempLoansAndDeposits"])
        form.segDetails.setNumberOfRows(1, withRowType: "tempLoansAndDeposits")
        for i in 0..<1 {
          if let row = form.segDetails.rowController(at: i) as? tempLoansAndDepositsController {
            if let depositsDet = transactions[i] as? [String: Any] {

              row.lblTopTitle.setText("Next Interest Date")
              row.lblTopValue.setText("\(getDateString(dateString:String(describing: depositsDet["Next interest date"] ?? "")))")

              row.lblBottomTitle.setText("Interest Account")
              row.lblBottomValue.setText("\(String(describing: depositsDet["Interest account"] ?? ""))")
				let RoiPer = "%"

              row.lblTopTitle1.setText("Rate of Interest")
              row.lblTopValue1.setText("\(String(describing: depositsDet["interestRate"] ?? "")) \(RoiPer)") 

              row.lblBottomTitle1.setText("Maturity Date")
              row.lblBottomValue1.setText("\(getDateString(dateString:String(describing: depositsDet["Maturity Date"] ?? "")))")
				
               row.lblBottomTitle2.setText("Deposit Amount")
              row.lblBottomValue2.setText("\(selectedDepositeamount) \(selectedDepositeCT)")
              
              row.lblBottomTitle01.setText("Interest Amount")
              row.lblBottomValue01.setText("\(String(describing: depositsDet["interestAmount"] ?? "")) \(selectedDepositeCT)") 
			  
              


            }
          }
        }
      }
      else {
        form.lblLoading.setText("No deposits data.")
        form.lblLoading.setHidden(false)
      }

      case .loans:

      if let transactions = response["loanInfo"] as? NSArray, 0 != transactions.count {
        debugPrint("Loans data \(transactions)")
        form.segDetails.setRowTypes(["tempLoansAndDeposits"])
        form.segDetails.setNumberOfRows(1, withRowType: "tempLoansAndDeposits")
        for i in 0..<1 {
          if let row = form.segDetails.rowController(at: i) as? tempLoansAndDepositsController {
            if let accountDet = transactions[i] as? [String: Any] {

              row.lblTopTitle.setText("Next Payment On")
              row.lblTopValue.setText("\(getDateString(dateString: String(describing: accountDet["Next_payment_date"] ?? "")))")

              row.lblBottomTitle.setText("Payment Amount")
              let loandt = String(describing: accountDet["Payment_due"] ?? "")
              if loandt.characters.count != 0 {
                row.lblBottomValue.setText("\(String(describing: loandt)) \(selectedLoanCT)")
              }
              else {

                row.lblBottomValue.setText("\(String(describing: accountDet["Payment_due"] ?? ""))")
              }

              row.lblTopTitle1.setText("Loan Amount")
              let loandAt = String(describing: accountDet["Amount"] ?? "")
              if loandAt.characters.count != 0 {
                row.lblTopValue1.setText("\(selectedLoanamount) \(selectedLoanCT)")
              }
              else {
                row.lblTopValue1.setText("\(String(describing: accountDet["Payment_due"] ?? ""))")
              }

              row.lblBottomTitle1.setText("Loan Account Number")
              row.lblBottomValue1.setText("\(String(describing: accountDet["Deposite number"] ?? ""))") 
              
              
              row.lblBottomTitle2.setText("")
              row.lblBottomValue2.setText("") 
              
              row.lblBottomTitle01.setText("")
              row.lblBottomValue01.setText("")
              

            }
          }
        }
      }
      else {
        form.lblLoading.setText("No Loans data.")
        form.lblLoading.setHidden(false)
      }

      default: break

    }
  }

}



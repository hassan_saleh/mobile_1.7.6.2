function fetchApplicationProperties() {
  ShowLoadingScreen();
  initMetaData(success, customErrorCallback);
}

function initMetaData(success, error) {
  
  kony.sdk.mvvm.KonyApplicationContext.init();
  var configParams = {
    "ShowLoadingScreenFunction": ShowLoadingScreen
  };
  var appFactory = kony.sdk.mvvm.KonyApplicationContext.getFactorySharedInstance();
  var appPropertiesInstance = appFactory.createConfigurationServiceManagerObject();
  var appPropertyObj = appPropertiesInstance.generateAppPropsObj(configParams);
  appPropertiesInstance.apply(appPropertyObj);
  var options = "online";
  var initManager = appFactory.createAppInitManagerObject();
  initManager.registerService("MetadataServiceManager", {
    "object": appFactory.createMetadataServiceManagerObject(),
    "params": {
      "options": options
    }
  });
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(""); 
  initManager.executeRegistedServices(success, customErrorCallback);
}

function success() {
  if (getAppConfig === true) {
    if(bojcontactnumber === null || bojcontactnumber === "" || bojcontactnumber === undefined)
      appConfigManualFetch();
  }
  else
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function initialiseForms() {
  var appContext = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();

  var frmEnrolluserLandingKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmEnrolluserLandingKAConfig);
  var frmEnrolluserLandingKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmEnrolluserLandingKAController", appContext, frmEnrolluserLandingKAModelConfigObj);
  var frmEnrolluserLandingKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmEnrolluserLandingKAControllerExtension", frmEnrolluserLandingKAControllerObj);
  frmEnrolluserLandingKAControllerObj.setControllerExtensionObject(frmEnrolluserLandingKAControllerExtObj);
  var frmEnrolluserLandingKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmEnrolluserLandingKAFormModel", frmEnrolluserLandingKAControllerObj);
  var frmEnrolluserLandingKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmEnrolluserLandingKAFormModelExtension", frmEnrolluserLandingKAFormModelObj);
  frmEnrolluserLandingKAFormModelObj.setFormModelExtensionObj(frmEnrolluserLandingKAFormModelExtObj);
  appContext.setFormController("frmEnrolluserLandingKA", frmEnrolluserLandingKAControllerObj);
}

function getDeviceInfo() { //returns an object which contains name(defines the OS) and version number of the OS
  var devInfo = kony.os.deviceInfo();
  var obj = {
    "name": devInfo.name,
    "version": devInfo.version,
    "model": devInfo.model
  };
  return obj;
}

function updateFlags(whichflag, flagvalue) {
  var deviceflagdata = kony.store.getItem("settingsflagsObject");
  deviceflagdata[whichflag] = flagvalue;
  kony.retailBanking.globalData.globals.settings.whichflag = flagvalue;
  var storeData = isEmpty(deviceflagdata) ? "" : deviceflagdata;
  kony.store.setItem("settingsflagsObject", storeData);
}

function appPropSuccess(res) {
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function alertCallback(response) {
  kony.print("response" + response);
  if (response === true)
    kony.application.exit();
}

function setUserObj(succCalBack) {
  kony.print("Perf Log: User information loading Service call- start");
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var options = {
    "access": "online",
    "objectName": "RBObjects"
  };
  var headers = {
    "session_token": kony.retailBanking.globalData.session_token
  };
  var serviceName = "RBObjects";
  var modelObj = INSTANCE.getModel("User", serviceName, options);
  var dataObject = new kony.sdk.dto.DataObject("User");
  var serviceOptions = {
    "dataObject": dataObject,
    "headers": headers
  };
  modelObj.fetch(serviceOptions, dataSuccess, customErrorCallback);

  function dataSuccess(response) {
    kony.print("Perf Log: User information loading Service call- End");
    alertsFlag = true;
    if (response[0].alertsTurnedOn === "true") {
      alertsFlag = true;
    } else {
      alertsFlag = false;
    }
    kony.retailBanking.globalData.globals.userObj.userName = response[0].userName;
    kony.retailBanking.globalData.globals.userObj.dateOfBirth = response[0].dateOfBirth;
    kony.retailBanking.globalData.globals.userObj.ssn = response[0].ssn;
    kony.retailBanking.globalData.globals.userObj.email = response[0].email;
    kony.retailBanking.globalData.globals.userObj.phone = response[0].phone;
    kony.retailBanking.globalData.globals.userObj.addressLine1 = response[0].addressLine1;
    kony.retailBanking.globalData.globals.userObj.addressLine2 = response[0].addressLine2;
    kony.retailBanking.globalData.globals.userObj.city = response[0].city;
    kony.retailBanking.globalData.globals.userObj.country = response[0].country;
    //SetCurrency(response[0].country);
    kony.retailBanking.globalData.globals.userObj.state = response[0].state;
    kony.retailBanking.globalData.globals.userObj.zipcode = response[0].zipcode;
    kony.retailBanking.globalData.globals.userObj.userFirstName = response[0].userfirstname;
    kony.retailBanking.globalData.globals.userObj.depositsTCaccepted = response[0].areDepositTermsAccepted;
    kony.retailBanking.globalData.globals.settings.alerts = alertsFlag;
    kony.retailBanking.globalData.globals.userObj.acntStatementTCaccepted = response[0].areAccountStatementTermsAccepted;
    kony.retailBanking.globalData.globals.userObj.isPinSet = response[0].isPinSet;
    kony.retailBanking.globalData.globals.userObj.role = response[0].role;
    var appDate = kony.retailBanking.util.formatingDate.getApplicationFormattedDateTime(response[0].lastlogintime, kony.retailBanking.util.BACKEND_DATE_TIME_FORMAT);
    kony.retailBanking.globalData.globals.userObj.lastLoginTime = appDate;
    // 	  kony.store.setItem("lastLoginTime",EncryptValue(kony.retailBanking.globalData.globals.userObj.lastLoginTime));
    kony.retailBanking.globalData.globals.userObj.userLastName = response[0].userlastname;
    var settingsObj = "";
    if (kony.store.getItem("settingsflagsObject") !== null && kony.store.getItem("settingsflagsObject") !== undefined) {
      settingsObj = kony.store.getItem("settingsflagsObject");
    }
    settingsObj = {
      "alerts": alertsFlag,
      "DefaultTransferAcctNo": response[0].default_account_transfers,
      "DefaultDepositAcctNo": response[0].default_account_deposit,
      "DefaultPaymentAcctNo": response[0].default_account_payments,
      "DefaultCardlessAcctNo": response[0].default_account_cardless
    };
    kony.store.setItem("settingsflagsObject", settingsObj);
    if (succCalBack) {
      succCalBack.call(this);
    }
  }
}


function enrollUser(userId) {
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var controller = INSTANCE.getFormController("frmEnrolluserLandingKA");
  var viewModel = controller.getFormModel();
  var options = {
    "access": "online"
  };
  objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects", options);
  var q1, q2, q3, q4, q5;
  q1 = viewModel.getWidgetData("lstboxquestn1").getData();
  q2 = viewModel.getWidgetData("lstboxquestn2").getData();
  q3 = viewModel.getWidgetData("lstboxquestn3").getData();
  q4 = viewModel.getWidgetData("lstboxquestn4").getData();
  q5 = viewModel.getWidgetData("lstboxquestn5").getData();
  var x = [{
    "question_id": q1,
    "answer": viewModel.getViewAttributeByProperty("tbxAnswer1", "text")
  },
           {
             "question_id": q2,
             "answer": viewModel.getViewAttributeByProperty("tbxAnswer2", "text")
           },
           {
             "question_id": q3,
             "answer": viewModel.getViewAttributeByProperty("tbxAnswer3", "text")
           },
           {
             "question_id": q4,
             "answer": viewModel.getViewAttributeByProperty("tbxAnswer4", "text")
           },
           {
             "question_id": q5,
             "answer": viewModel.getViewAttributeByProperty("tbxAnswer5", "text")
           },
          ];
  x = JSON.stringify(x);
  x.replace("\"", "'");
  var record = {
    "userId": userId,
    "usersecurityli": x
  };
  var dataObject = new kony.sdk.dto.DataObject("UserSecurityQuestions");
  dataObject.setRecord(record);
  var serviceOptions = {
    "dataObject": dataObject,
    "headers": ""
  };
  objectService.create(serviceOptions, enrollSuccess, customErrorCallback);

  function enrollSuccess(res) {
    alert(res.success);
    frmLoginKA.show();
    kony.print("success question record created");
  }
}

function removeSwipe() {
  if (kony.retailBanking.globalData.deviceInfo.isIphone()) {
    frmLoginKA.accountPreviewIndicator.removeGestureRecognizer(swipeGesture);
    frmLoginKA.accountPreviewScreen.removeGestureRecognizer(swipeGesture1);
  }

}

function forgotPinNavigation() {
  showLoginorPinScreen("pwdLogin");
  frmLoginKA.lblChangeUserName.setVisibility(false);
  frmLoginKA.tbxusernameTextField.setVisibility(true);
  frmLoginKA.passwordTextField.text = "";
  frmLoginKA.passwordTextField.secureTextEntry = true;
  frmLoginKA.lblShowPass.text = "A";
  //manageUname();
  //showAuthModesflex();
}

function initFrmDeviceRegistration() {
  //frmDeviceDeRegistrationKA
  var appContext = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  var frmDeviceDeRegistrationKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmDeviceDeRegistrationKAConfig);
  var frmDeviceDeRegistrationKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAController", appContext, frmDeviceDeRegistrationKAModelConfigObj);
  var frmDeviceDeRegistrationKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAControllerExtension", frmDeviceDeRegistrationKAControllerObj);
  frmDeviceDeRegistrationKAControllerObj.setControllerExtensionObject(frmDeviceDeRegistrationKAControllerExtObj);
  var frmDeviceDeRegistrationKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAFormModel", frmDeviceDeRegistrationKAControllerObj);
  var frmDeviceDeRegistrationKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAFormModelExtension", frmDeviceDeRegistrationKAFormModelObj);
  frmDeviceDeRegistrationKAFormModelObj.setFormModelExtensionObj(frmDeviceDeRegistrationKAFormModelExtObj);
  appContext.setFormController("frmDeviceDeRegistrationKA", frmDeviceDeRegistrationKAControllerObj);
}



function fetchApplicationPropertiesWithoutLoading(){
  kony.sdk.mvvm.KonyApplicationContext.init();
  var configParams = {
    "ShowLoadingScreenFunction": ShowLoadingScreen
  };
  var appFactory = kony.sdk.mvvm.KonyApplicationContext.getFactorySharedInstance();
  var appPropertiesInstance = appFactory.createConfigurationServiceManagerObject();
  var appPropertyObj = appPropertiesInstance.generateAppPropsObj(configParams);
  appPropertiesInstance.apply(appPropertyObj);
  var options = "online";
  var initManager = appFactory.createAppInitManagerObject();
  initManager.registerService("MetadataServiceManager", {
    "object": appFactory.createMetadataServiceManagerObject(),
    "params": {
      "options": options
    }
  });
  initManager.executeRegistedServices(doNothing, doNothing);
}
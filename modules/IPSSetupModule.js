var bic_num = "";
var b_iban = "";
var ReturnPaymentIPS = [];
var IPSPaymentHistory = [];
var IPSHistorySeg = [];
//Omar alnajjar 19/03/2021 RTP
var rtpSelectedData = [];
function removeDatafrmTransfersformEPS(){
  //   frmEPS.lblAccountName1.text = "";
  //   frmEPS.lblAccountNumnextber1.text = "";
  //   frmEPS.flxIcon1.setVisibility(false);
  //   frmEPS.lblSelectanAccount1.setVisibility(true);
  //   frmEPS.btnIBAN.skin = "slButtonBlueFocus";
  //   frmEPS.btnAlias.skin = "slButtonBlueFocus";
  //   frmEPS.btnMob.skin = "slButtonBlueFocus";
  //   frmEPS.lblNext.skin = "sknLblNextDisabled";
  // Reset Alias Details //
  frmEPS.txtAliasName.text = "";
  frmEPS.txtAliasName.setEnabled(true);
  frmEPS.txtIBANAlias.text = "";
  frmEPS.txtIBANAlias.setEnabled(true);
  frmEPS.txtAliasNameMob.text = "";
  frmEPS.txtAliasNameMob.setEnabled(true);
  frmEPS.txtAmountAlias.text = "";
  frmEPS.lblIBANAliastxt.text = "";
  frmEPS.lblAliasAddresstxt.text = "";
  frmEPS.lblAliasBank.txt = "";
  // Reset Alias Details //
}
function ResetFormIPSData(){
  frmEPS.lblAccountName1.text = "";
  frmEPS.lblAccountNumber1.text = "";
  frmEPS.lblAccountbalance1.text = "";
  frmEPS.flxIcon1.setVisibility(false);
  frmEPS.lblSelectanAccount1.setVisibility(true);
  frmEPS.btnAlias.skin = "sknOrangeBGRNDBOJ";
  frmEPS.btnMob.skin = "slButtonBlueFocus";
  frmEPS.btnIBAN.skin = "slButtonBlueFocus";
  frmEPS.lblNext.skin = "sknLblNextDisabled";
  frmEPS.lblAccountName1.text = "";
  // Reset Alias Details //
  frmEPS.txtAliasName.text = "";
  frmEPS.txtAliasName.setEnabled(true);
  frmEPS.lblBeneName.text = "";
  frmEPS.txtAmountAlias.text = "";
  frmEPS.lblIBANAliastxt.text = "";
  frmEPS.lbAliasAddresstxt.text = "";
  frmEPS.lblAliasBanktxt.txt = "";
  frmEPS.flxBeneName.setVisibility(false);
  frmEPS.flxAmountAlias.setVisibility(false);
  frmEPS.flxIBANAliass.setVisibility(false);
  frmEPS.flxAliasAddress.setVisibility(false);
  frmEPS.flxAliasBank.setVisibility(false);
  // Reset Alias Details //
  // Reset Mob Details //
  frmEPS.txtAliasNameMob.text = "";
  frmEPS.txtAliasNameMob.setEnabled(true);
  frmEPS.txtAmountMob.text = "";
  frmEPS.lblIBANMobtxt.text = "";
  frmEPS.lblAddressMobtxt.text = "";
  frmEPS.lblBankMobAliastxt.text = "";
  frmEPS.lblBeneNameMob.text = "";
  frmEPS.flxBeneNameMob.setVisibility(false);
  frmEPS.flxAmountMob.setVisibility(false);
  frmEPS.flxIbanDetailsMob.setVisibility(false);
  frmEPS.flxAddressMob.setVisibility(false);
  frmEPS.flxBankMob.setVisibility(false);
  // Reset Mob Details //
  // Reset Iban Details //  
  frmEPS.txtAmountIBAN.text = "";
  frmEPS.txtIBANAlias.text = "";
  frmEPS.txtIBANAlias.setEnabled(true);
  frmEPS.txtAddressAlias.text = "";
  frmEPS.txtIBANBeneName.text="";
  frmEPS.txtBankAlias.text = "";
  frmEPS.tbxBankName.text = "";
  frmEPS.lblBeneNameIBAN.text = "";
  frmEPS.flxUnderlineIBAN.skin = "sknFlxGreyLine";
  frmEPS.flxBeneNameIBAN.setVisibility(false);
  frmEPS.lblBankNameStat.setVisibility(false);
  frmEPS.tbxBankName.setVisibility(false);
  frmEPS.lblBankName.setVisibility(true);
  // Reset Iban Details // 
  //   Default flow 
  frmEPS.flxDetailsIBAN.setVisibility(false);
  frmEPS.flxDetailsMobileScroll.setVisibility(false);
  frmEPS.flxDetailsAliasScroll.setVisibility(true); 
  frmEPS.flxConfirmEPS.setVisibility(false); 
  frmEPS.flxMain.setVisibility(true); 

  frmEPS.btnMob.setVisibility(true);
  frmEPS.btnAlias.setVisibility(true);
  frmEPS.btnIBAN.setVisibility(true);
  if (kony.store.getItem("langPrefObj")==="en"){
    frmEPS.btnMob.left = "35%";
    frmEPS.btnAlias.left = "2%";
    frmEPS.btnIBAN.left = "68%";
  }else{
    frmEPS.btnMob.right = "35%";
    frmEPS.btnAlias.right = "2%";
    frmEPS.btnIBAN.right = "68%";

  }

  //mai 11/1/2021
  frmIPSRequests.tbxReasonRP.text ="";
  frmIPSRequests.tbxReasonRP.setVisibility(true);
  frmIPSRequests.lblReasonStatRP.setVisibility(false);
  frmIPSRequests.lblReasonRP.setVisibility(true);
  frmIPSRequests.lblNextRP.skin = "sknLblNextDisabled";
  //  frmIPSRequests.lblReasonRP.setVisibility(false);

}
function SetupIPSscreen(){
  var AccDetailsSegdata = frmAccountDetailsScreen.segFre.data;
  var dataSelected =  frmAccountDetailsScreen.segFre.selectedRowItems[0];
  //  var accName =(dataSelected.anum.length>16)?(dataSelected.anum.substring(0,16)+"..."):dataSelected.anum;

  frmEPS.lblAccountName1.text = dataSelected.accountName;
  frmEPS.lblAccountNumber1.text = dataSelected.accountNumber;
  frmEPS.lblAccountbalance1.text = dataSelected.balance;
  frmEPS.flxIcon1.setVisibility(true);
  frmEPS.lblSelectanAccount1.setVisibility(false);
  //Mai
  validateLblNext();
  frmEPS.show();
}
function SetupAccountsFrom(returenedGbl){
  //var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.accountsData;
  gblTModule = returenedGbl; 
  gotoAccountDetailsScreen(1);
  //kony.boj.accounttype(fromAccounts,gblTModule);
}

function gotoAccountDetailsScreenEPS(val){

  //1960 fix
  var fromAccounts="";
  var toAccounts="";
  var accountsData="";
  kony.print("accountsData"+kony.retailBanking.globalData.accountsDashboardData.accountsData);
  if(kony.boj.siri === "fromSiri")
  {
    accountsData=kony.retailBanking.globalData.accountsDashboardData.accountsData;
  }
  else
  {
    fromAccounts = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
    toAccounts	= kony.retailBanking.globalData.accountsDashboardData.toAccounts.slice(0);
  }
  kony.print("fromAccounts :: "+fromAccounts);
  kony.print("toAccounts :: "+toAccounts);

  if(val === 1){
    if(kony.newCARD.applyCardsOption === true || kony.application.getCurrentForm().id === "frmCardLinkedAccounts" || kony.boj.selectedBillerType === "PrePaid"  || kony.boj.siri ==="fromSiri"){
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.common.accountNumber");
    }else{
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyFrom");
    }
    kony.store.setItem("toval",1);
    if((kony.application.getCurrentForm().id === "frmNewBillKA" && frmNewBillKA.btnBillsPayCards.text == "t") ||
       (kony.application.getCurrentForm().id === "frmBills" && frmBills.btnBillsPayCards.text == "t") ||
       (kony.application.getCurrentForm().id === "frmNewBillDetails" && frmNewBillDetails.btnBillsPayCards.text == "t") ){
      if(kony.retailBanking.globalData.prCreditCardPayList.length > 0){

        var tempdata = [];
        var cardData = kony.retailBanking.globalData.prCreditCardPayList;
        var cc = false;
        for(var i=0;i<cardData.length;i++){
          kony.print("cardTypeFlag ::"+cardData[i].cardTypeFlag);
          if(cardData[i].cardTypeFlag === "C"){
            cc = true;
            tempdata.push(cardData[i]);
          }
        }
        if(cc){
          accountsScreenPreshow(tempdata);
          frmAccountDetailsScreen.show();
        }
        else
          customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.nocards"),popupCommonAlertDimiss,"");
      }else
        customVerb_CARDSDETAILS();
    }else{
      if(kony.application.getCurrentForm().id === "frmCardLinkedAccounts"){
        var data = frmCardLinkedAccounts.segCardLinkedAccounts.data;
        var temp = [];
        for(var i in data){
          if(data[i].flxLinkedAccountsEnable.isVisible === true){
            for(var j in fromAccounts){
              if(fromAccounts[j].accountID === data[i].lblAccountNumber.text){
                temp.push(fromAccounts[j]);
                break;
              }
            }
          }
        }
        accountsScreenPreshow(temp);
      }
      //added for 1960
      else{
        if(kony.boj.siri ==="fromSiri")
        {
          kony.print("loding accounts for siri");
          accountsScreenPreshow(accountsData);
        }
        else
        {
          accountsScreenPreshow(fromAccounts);
        }
      }
      //       frmAccountDetailsScreen.show(); //1 july
    }
    //frmNewTransferKA.flxAcc2.setEnabled(true);

  }
  if(val==2){
    kony.store.setItem("toval",0);

    if(gblTModule!=="send")
    {
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyTo");
      var i =  kony.store.getItem("toAccount");
      for(var j in toAccounts){
        if(toAccounts[j].accountID === i.accountID)
        {
          toAccounts.splice(j,1);
          break;
        }
      }
      accountsScreenPreshow(toAccounts);      
      frmAccountDetailsScreen.show();
    }
    else{
      var data = [];
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyFrom");
      accountsScreenPreshow(fromAccounts);
      frmAccountDetailsScreen.show();
    }
    //alert(JSON.stringify("todata "+i+"index "+j+"data "+data));

  }
}

function getAllAccountsForIPS(){
  if (kony.sdk.mvvm.isNetworkAvailabile())
  {
    if (kony.sdk.mvvm.KonyApplicationContext.getAppInstance())
    {
      ShowLoadingScreen();
      var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var options ={    "access": "online",
                    "objectName": "RBObjects"
                   };
      var serviceName = "RBObjects";
      var modelObj = INSTANCE.getModel("Accounts",serviceName,options);
      var dataObject = new kony.sdk.dto.DataObject("Accounts");
      var serviceOptions = {"dataObject":dataObject,"queryParams":{"custId": custid,"lang": kony.store.getItem("langPrefObj")==="en"?"eng":"ara"}};
      modelObj.fetch(serviceOptions,refreshSucIPS,refreshFailIPS);

    }
    else{
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
    }
  }
  else{
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"),popupCommonAlertDimiss,"");
  }
}
function refreshSucIPS(response){
  kony.print("refresh accounts:::"+JSON.stringify(response));
  try{
    if(isEmpty(response)){
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup(geti18Value("i18n.common.Attention"), 
                       geti18Value("i18n.alerts.NoAccountsMsg"),
                       popupCommonAlertDimiss, "");
      exceptionLogCall("::Getallaccounts Success::","service response null","service",response);
    }else{
      var segAccountListData = response;
      var fromAccounts = [],toAccounts = [],loansData = [];
      var sectionAccData = [],segDealsData = [],segLoansData = [];
      if((!isEmpty(segAccountListData)) && segAccountListData.length>0)
      {
        for(var i in segAccountListData)
        {
          if(checkAccount(segAccountListData[i]["accountID"])){
            if(!isEmpty(segAccountListData[i]["AccNickName"]))
              segAccountListData[i]["accountName"] =  segAccountListData[i]["AccNickName"];

            if(segAccountListData[i]["accountType"] != "T" && segAccountListData[i]["accountType"] != "L" && segAccountListData[i]["dr_ind"] == "Y" && segAccountListData[i]["productCode"] != "320"){

              fromAccounts.push(segAccountListData[i]);
            }
            if(segAccountListData[i]["accountType"] != "T" && segAccountListData[i]["accountType"] != "L" && segAccountListData[i]["cr_ind"] == "Y" && segAccountListData[i]["productCode"] != "320"){
              toAccounts.push(segAccountListData[i]);
            }
          }else{
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup(geti18Value("i18n.common.Attention"),
                             geti18nkey("i18n.security.loginagain"),
                             logoutpp, "");
            return;
          }
        }
        kony.print("Refreshed fromAccounts:::"+JSON.stringify(fromAccounts));
        kony.print("Refreshed toAccounts:::"+JSON.stringify(toAccounts));
        kony.retailBanking.globalData.accountsDashboardData.fromAccounts = isEmpty(fromAccounts) ? {} : fromAccounts;
        kony.retailBanking.globalData.accountsDashboardData.toAccounts = isEmpty(toAccounts) ? {} : toAccounts;

        if(kony.newCARD.applyCardsOption === true || kony.application.getCurrentForm().id === "frmCardLinkedAccounts"){
          gotoAccountDetailsScreenEPS(1);
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }else if(gblTModule === "AccNumberStatement" || gblTModule === "AccountOrder"){
          accountsScreenPreshow(fromAccounts);
          if(gblTModule === "AccountOrder"){
            frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.my_money.accounts");
          }else{
            frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.my_money.accounts");
          }
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
          //           frmAccountDetailsScreen.show(); // edit by ahmad
        }else if(gblTModule == "IPS"){
          if(hasEnoughAccounts()){

            //           frmNewTransferKA.lblCurrency.setVisibility(false);
            //             ownAccountPreshow();
            //             removeDatafrmTransfersform();
            //             makeInlineErrorsTransfersInvisible();
            //             if(kony.store.getItem("langPrefObj") !== "ar"){
            //               frmNewTransferKA.flxInvalidAmountField.text = geti18Value("i18n.transfer.minAmount") + " 0.001" + " " + "JOD";
            //             }else{
            //               frmNewTransferKA.flxInvalidAmountField.text = "JOD" + " 0.001" + " " + geti18Value("i18n.transfer.minAmount")  ;
            //             }
            //              frmEPS.flxAcc1.onClick = gotoAccountDetailsScreenEPS(1); // 30june
            //              frmEPS.btnForward1.onClick = gotoAccountDetailsScreenEPS(1); // 30june

            //             frmNewTransferKA.flxAcc1.setEnabled(true);
          }
          else{
            customAlertPopup(geti18Value("i18n.common.Attention"), 
                             geti18Value("i18n.Transfers.InsufficientAcc"),
                             popupCommonAlertDimiss, "");

          }
          kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }
        else if(gblTModule=="send"){

          var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
          var controller = INSTANCE.getFormController("frmShowAllBeneficiary");
          var navObject = new kony.sdk.mvvm.NavigationObject();
          navObject.setRequestOptions("segAllBeneficiary",{"headers":{},"queryParams":{"P_BENE_TYPE":"","custId":custid}});
          controller.loadDataAndShowForm(navObject);
        }
        else if(gblTModule == "billspay"){
          var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
          var controller = INSTANCE.getFormController("frmManagePayeeKA");
          var navObject = new kony.sdk.mvvm.NavigationObject();
          navObject.setRequestOptions("managepayeesegment",{"headers":{},"queryParams":{"custId": custid,
                                                                                        "P_BENE_TYPE": "PRE",
                                                                                        "lang":(kony.store.getItem("langPrefObj")==="en")?"eng":"ara"}});
          controller.loadDataAndShowForm(navObject);
        }



        //frmPaymentDashboard.show();

      }else{
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup(geti18Value("i18n.common.Attention"), 
                         geti18Value("i18n.alerts.NoAccountsMsg"),
                         popupCommonAlertDimiss, "");
        exceptionLogCall("::Getallaccounts Success::","service response null","service",response);
      }
    }
  }catch(e){
    exceptionLogCall("::Getallaccounts Success::","Exception while handling success callback","UI",e);
  }
}

function refreshFailIPS(err){
  kony.application.dismissLoadingScreen();
  customAlertPopup(geti18Value("i18n.Bene.Failed"),geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
  exceptionLogCall("::Getallaccounts Failed::","service failed to respond","service",err);
}

function cliqTransferChange(){
  if(frmEPS.txtAmountAlias.text !== null && frmEPS.txtAmountAlias.text !== ""){
    var amount = frmEPS.txtAmountAlias.text;
    if(amount.indexOf(".") > -1){
      amount = amount.substring(0,amount.indexOf(".")+4);
    }
    frmEPS.txtAmountAlias.text = amount;
  }
  validateLblNext();
}

function validateLblNext(){

  kony.print("Validation for the next label is active: "+ gblTModule);
  //Mai 25/11/2020 Check if Request to Pay
  if(gblTModule=="IPSRequest")
  {
    if(frmEPS.tbxReason.isVisible === true && !isEmpty(frmEPS.lblReason.text.trim())){
      if(frmEPS.flxDetailsIBAN.isVisible === true){
        if(!isEmpty(frmEPS.txtAmountIBAN.text.trim()) && !isEmpty(frmEPS.txtIBANAlias.text.trim()) && !isEmpty(frmEPS.txtAddressAlias.text.trim()) && !isEmpty(frmEPS.txtIBANBeneName.text) && !isEmpty(frmEPS.tbxBankName.text)  && frmEPS.flxUnderlineIBAN.skin == "sknFlxGreyLine")//Omar ALnajjar cliq iban validation
          frmEPS.lblNext.skin = "sknLblNextEnabled"; 
        else{
          frmEPS.lblNext.skin = "sknLblNextDisabled";
        }
      }else if(frmEPS.flxDetailsAliasScroll.isVisible === true){
        if(frmEPS.txtAmountAlias.text !== null && frmEPS.txtAmountAlias.text.trim() !== "" && frmEPS.lblIBANAliastxt.text !== null && frmEPS.lblIBANAliastxt.text.trim() !== "" && frmEPS.lbAliasAddresstxt.text !== null && frmEPS.lbAliasAddresstxt.text.trim() !== "" && frmEPS.lblAliasBanktxt.text !== null && frmEPS.lblAliasBanktxt.text.trim() !== "" && frmEPS.lblBeneName.text !== null && frmEPS.lblBeneName.text.trim() !== "" )
          frmEPS.lblNext.skin = "sknLblNextEnabled";
        else{
          frmEPS.lblNext.skin = "sknLblNextDisabled";
        }
      }else if(frmEPS.flxDetailsMobileScroll.isVisible === true){
        if(frmEPS.txtAmountMob.text !== null && frmEPS.txtAmountMob.text.trim() !== "" && frmEPS.lblIBANMobtxt.text !== null && frmEPS.lblIBANMobtxt.text.trim() !== "" && frmEPS.lblAddressMobtxt.text !== null && frmEPS.lblAddressMobtxt.text.trim() !== "" && frmEPS.lblBeneNameMob.text !== null && frmEPS.lblBeneNameMob.text.trim() !== "")
          frmEPS.lblNext.skin = "sknLblNextEnabled";   
        else{
          frmEPS.lblNext.skin = "sknLblNextDisabled";
        }
      }
    }else{
      frmEPS.lblNext.skin = "sknLblNextDisabled";
    }

  }
  if(gblTModule=="CLIQPay")//Mai 30/11/2020 Request To Pay
  {
    if(frmIPSRequests.lblRPRSelectanAccount.isVisible === false){
      frmIPSRequests.lblRPRNext.skin = "sknLblNextEnabled";   
    }
    else{
      frmIPSRequests.lblRPRNext.skin = "sknLblNextDisabled";
    }
  }
  if(gblTModule === "IPSReturn")//Mai 29/11/2020 Return Payment
  {

    kony.print("IPS Retunr Amount: "+  parseFloat(frmIPSRequests.txtAmountRP.text.trim()));
    // Amount shouldnt be empty
    // Amount should be equal or less than Income payment amount.
    //if(frmIPSRequests.txtAmountRP.text !== null && frmIPSRequests.txtAmountRP.text.trim() !== "" && parseFloat(frmIPSRequests.txtAmountRP.text.trim()) <= parseFloat(frmIPSRequests.txtAmountRPOriginal.text))
    if(!isEmpty(frmIPSRequests.txtAmountRP.text) && parseFloat(frmIPSRequests.txtAmountRP.text.trim()) !== 0  && !isEmpty(frmIPSRequests.tbxReasonRP.text) && parseFloat(frmIPSRequests.txtAmountRP.text.trim()) <= parseFloat(frmIPSRequests.txtAmountRPOriginal.text))
    {

      //  if(frmIPSRequests.tbxReasonRP.text !== null && frmIPSRequests.tbxReasonRP.text.trim() !== "")
      frmIPSRequests.lblNextRP.skin = "sknLblNextEnabled";  
    }
    else{
      frmIPSRequests.lblNextRP.skin = "sknLblNextDisabled";
    }
  }
  else // CLIQ Transfer
  {
    if(frmEPS.lblSelectanAccount1.isVisible === false){
      if(frmEPS.flxDetailsIBAN.isVisible === true){
        if(!isEmpty(frmEPS.txtAmountIBAN.text.trim()) && parseFloat(frmEPS.txtAmountIBAN.text) > 0 && !isEmpty(frmEPS.txtIBANAlias.text.trim()) && !isEmpty(frmEPS.txtAddressAlias.text.trim()) && !isEmpty(frmEPS.txtIBANBeneName.text) && !isEmpty(frmEPS.tbxBankName.text) && frmEPS.flxUnderlineIBAN.skin == "sknFlxGreyLine")
          frmEPS.lblNext.skin = "sknLblNextEnabled"; 
        else{
          frmEPS.lblNext.skin = "sknLblNextDisabled";
        }
      }else if(frmEPS.flxDetailsAliasScroll.isVisible === true){
        if(frmEPS.txtAmountAlias.text !== null  && parseFloat(frmEPS.txtAmountAlias.text) > 0 && frmEPS.txtAmountAlias.text.trim() !== "" && frmEPS.lblIBANAliastxt.text !== null && frmEPS.lblIBANAliastxt.text.trim() !== "" && frmEPS.lbAliasAddresstxt.text !== null && frmEPS.lbAliasAddresstxt.text.trim() !== "" && frmEPS.lblAliasBanktxt.text !== null && frmEPS.lblAliasBanktxt.text.trim() !== "" && frmEPS.lblBeneName.text !== null && frmEPS.lblBeneName.text.trim() !== "" )
          frmEPS.lblNext.skin = "sknLblNextEnabled";
        else{
          frmEPS.lblNext.skin = "sknLblNextDisabled";
        }
      }else if(frmEPS.flxDetailsMobileScroll.isVisible === true){
        if(frmEPS.txtAmountMob.text !== null && parseFloat(frmEPS.txtAmountMob.text) > 0 && frmEPS.txtAmountMob.text.trim() !== "" && frmEPS.lblIBANMobtxt.text !== null && frmEPS.lblIBANMobtxt.text.trim() !== "" && frmEPS.lblAddressMobtxt.text !== null && frmEPS.lblAddressMobtxt.text.trim() !== "" && frmEPS.lblBeneNameMob.text !== null && frmEPS.lblBeneNameMob.text.trim() !== "")
          frmEPS.lblNext.skin = "sknLblNextEnabled";   
        else{
          frmEPS.lblNext.skin = "sknLblNextDisabled";
        }
      }
    }else{
      frmEPS.lblNext.skin = "sknLblNextDisabled";
    }
  }

}

function onClickYesBackIPS(){
  frmPaymentDashboard.show();
  popupCommonAlert.dismiss();
  frmEPS.destroy();
}
//Mai 12/1/2021

function onClickYesBackIPSHistory(){
  gblTModule = "CLIQHistory";

  frmIPSRequests.flxConfirmEPS.setVisibility(false);
  frmIPSRequests.flxPaymentHistory.setVisibility(true);
  //Omar ALnajjar 08/02/2021
  frmIPSRequests.skin = "CopyslFormCommon0aaa3928efb3d48";
  frmIPSRequests.flxReturnPayment.setVisibility(false);
  frmIPSRequests.flxRequestToPay.setVisibility(false);
  popupCommonAlert.dismiss();

}
function onClickYesBackRTP(){
  resetR2PForm();
  popupCommonAlertDimiss();
  frmIPSManageBene.show();
}
function onClickNoBackRTP(){
  popupCommonAlertDimiss();
}
//Omar alnajjar 21/03/2021 RTP
function onClickYesDeclineRTP(){
  popupCommonAlertDimiss();
  acceptRejectRtpRequest("RJCT");
}
//Omar Alnajjar 22/03/2021 RTP
function onClickYesDeleteALias(data){
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJModifyIPSAlias");
  appMFConfiguration.invokeOperation("prModifyIpsAlias", {},{"p_customer": custid,
                                                             "p_alias":data.lblAliasSeg.text,
                                                             "p_alias_type":data.aliasTypeSeg,
                                                             "p_status":"delete",
                                                             "p_channel":"MOBILE",
                                                             "p_user_id":"BOJMOB"},
                                     successDeleteCliqAlias,
                                     function(error){
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJModifyIPSAlias","SERVICE FAILURE","SERVICE",error);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("JSONRESPOSNE"+JSON.stringify(error));
  });
}
//Omar ALnajjar Delete CLiq ALias
function successDeleteCliqAlias(response){
  kony.print("JSONRESPOSNE"+JSON.stringify(response));
  try{
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    if (response.returnCode !== null && response.returnCode !== undefined && response.returnCode === "00000"){
      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      gblTModule="getAlias";
      serv_IPS_REGISTRATION_STATUS();      
    }else{
      var Message = getErrorMessage(response.returnCode);
      customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
      exceptionLogCall("BOJModifyIPSAlias","SERVICE FAILURE RESPONSE","SERVICE",response.returnCode);
    }
  }catch(error){
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJModifyIPSAlias","Catch FAILURE","SERVICE",error);
    kony.print("JSONRESPOSNE"+JSON.stringify(error));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }

}
function onCLickYesBackCRT(){
  resetCreateAliasForm();
  frmGetIPSAlias.show();
  frmIPSCreateAlias.destroy();
  popupCommonAlertDimiss();
}
// Mai 11/1/2021

function SetupIPSRetunrConfirmationScreen(){

  var gblFromAcc = "";
  //Mai 16/12/2020

  if(gblTModule==="IPSReturn")
  {
    frmIPSRequests.lblConfirmReason.text  = frmIPSRequests.tbxReasonRP.text; 
    frmIPSRequests.ToAccConfirmationText.text = frmIPSRequests.lblAliasRP.text;
    frmIPSRequests.lblAmountConfirmValue.text =frmIPSRequests.txtAmountRPOriginal.text;
    frmIPSRequests.lblRetunredAmountConfirmValue.text  = frmIPSRequests.txtAmountRP.text;
    frmIPSRequests.lblIBANConftext.text = frmIPSRequests.lblIBANRP.text;
    frmIPSRequests.lblRefNoConfText.text = frmIPSRequests.lblReferenceRPR.text;

    frmIPSRequests.flxConfirmReason.setVisibility(true); 
    frmIPSRequests.flxConfirmEPS.setVisibility(true);
    //Omar Alnajjar 08/02/2021
    frmIPSRequests.skin = "sknmainGradient";
    frmIPSRequests.flxReturnPayment.setVisibility(false);
  }
}
//Omar ALnajjar 17/03/2021 RTP
function SetupRTPConfirmationScreen(){
  if(frmIPSRequestToPay.btnR2PAlias.skin == "sknOrangeBGRNDBOJ" && frmIPSRequestToPay.btnR2PMob.skin == "slButtonBlueFocus" && frmIPSRequestToPay.btnR2PIBAN.skin == "slButtonBlueFocus" ){
    // if Alias Type is Alias //
    //    frmIPSRequestToPay.lblFromAccConfText.text = frmIPSRequestToPay.lblAccountName1.text;
    frmIPSRequestToPay.ToAccConfirmationText.text = frmIPSRequestToPay.lblBeneName.text;
    frmIPSRequestToPay.lblAmountConfirmValue.text = frmIPSRequestToPay.txtAmountAlias.text;
    frmIPSRequestToPay.lblIBANConftext.text = frmIPSRequestToPay.lblIBANAliastxt.text;
    frmIPSRequestToPay.lblConfirmAddr.text = frmIPSRequestToPay.lbAliasAddresstxt.text;
    frmIPSRequestToPay.lblConfirmBankName.text = frmIPSRequestToPay.lblAliasBanktxt.text;

    frmIPSRequestToPay.lblToAccConfirmationTitle.text = geti18Value("i18n.CLIQ.toAlias");
    frmIPSRequestToPay.lblToAccConfirmationText.text = frmIPSRequestToPay.txtAliasName.text;

  }else if (frmIPSRequestToPay.btnR2PAlias.skin == "slButtonBlueFocus" && frmIPSRequestToPay.btnR2PMob.skin == "sknOrangeBGRNDBOJ" && frmIPSRequestToPay.btnR2PIBAN.skin == "slButtonBlueFocus"){
    // if Alias Type is Mobile //
    //frmIPSRequestToPay.lblFromAccConfText.text = frmIPSRequestToPay.lblAccountName1.text;
    frmIPSRequestToPay.ToAccConfirmationText.text = frmIPSRequestToPay.lblBeneNameMob.text;
    frmIPSRequestToPay.lblAmountConfirmValue.text = frmIPSRequestToPay.txtAmountMob.text;
    frmIPSRequestToPay.lblIBANConftext.text = frmIPSRequestToPay.lblIBANMobtxt.text;
    frmIPSRequestToPay.lblConfirmAddr.text = frmIPSRequestToPay.lblAddressMobtxt.text;
    frmIPSRequestToPay.lblConfirmBankName.text = frmIPSRequestToPay.lblBankMobAliastxt.text;

    frmIPSRequestToPay.lblToAccConfirmationTitle.text = geti18Value("i18n.CLIQ.toMobile");
    frmIPSRequestToPay.lblToAccConfirmationText.text = frmIPSRequestToPay.txtAliasNameMob.text;

  }else if (frmIPSRequestToPay.btnR2PAlias.skin == "slButtonBlueFocus" && frmIPSRequestToPay.btnR2PMob.skin == "slButtonBlueFocus" && frmIPSRequestToPay.btnR2PIBAN.skin == "sknOrangeBGRNDBOJ"){

    // frmIPSRequestToPay.lblFromAccConfText.text = frmIPSRequestToPay.lblAccountName1.text;
    frmIPSRequestToPay.ToAccConfirmationText.text = frmIPSRequestToPay.txtIBANBeneName.text;
    frmIPSRequestToPay.lblAmountConfirmValue.text = frmIPSRequestToPay.txtAmountIBAN.text;
    frmIPSRequestToPay.lblIBANConftext.text = frmIPSRequestToPay.txtIBANAlias.text;
    frmIPSRequestToPay.lblConfirmAddr.text = frmIPSRequestToPay.txtAddressAlias.text;
    frmIPSRequestToPay.lblConfirmBankName.text = frmIPSRequestToPay.tbxBankName.text;  

    frmIPSRequestToPay.lblToAccConfirmationTitle.text = geti18Value("i18n.CLIQ.toIBAN");
    frmIPSRequestToPay.lblToAccConfirmationText.text = frmIPSRequestToPay.txtIBANAlias.text;

  }
  frmIPSRequestToPay.lblFromAccConfText.text = frmIPSRequestToPay.lblAccountNumber1.text;
}
//Omar ALnajjar 22/03/2021 Create ALias
function setupCreateAliasConfirmation(){
  frmIPSCreateAlias.flxConfirmCRT.setVisibility(true);
  frmIPSCreateAlias.flxCRTBody.setVisibility(false);
  frmIPSCreateAlias.lblFromAccConfText.text = frmIPSCreateAlias.lblAccountNumber1.text;
  if (frmIPSCreateAlias.btnCRTAlias.skin === "sknOrangeBGRNDBOJ"){
    frmIPSCreateAlias.lblAliasTypeConfirmationText.text = geti18Value("i18n.jomopay.aliastype");
    frmIPSCreateAlias.lblAliasConfirmationText.text = frmIPSCreateAlias.txtAliasName.text.trim();
    Alias_Type = "ALIAS";
  }else if (frmIPSCreateAlias.btnCRTMob.skin === "sknOrangeBGRNDBOJ"){
    Alias_Type = "MOBL";
    frmIPSCreateAlias.lblAliasTypeConfirmationText.text = geti18Value("i18n.jomopay.mobiletype");
    frmIPSCreateAlias.lblAliasConfirmationText.text = frmIPSCreateAlias.txtAliasNameMob.text.trim();
  }
  aliasValue = frmIPSCreateAlias.lblAliasConfirmationText.text.trim();
  aliasType = frmIPSCreateAlias.lblAliasTypeConfirmationText.text.trim();
}
//Get share alias information Omar ALnajjar
function GetShareAliasInfo(){

  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJAliasIPS");
  appMFConfiguration.invokeOperation("prGetIpsAliasDetails", {},{"p_customer_id": custid,
                                                                 "p_alias":aliasValue,
                                                                 "p_alias_type": Alias_Type,
                                                                 "p_channel":"MOBILE",
                                                                 "p_user_id":"BOJMOB"},
                                     successShareAliasCalling,
                                     function(error){
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJAliasIPS","SERVICE FAILURE","SERVICE",error);
  });
}
//Get share alias information Omar ALnajjar
function successShareAliasCalling(response){
  try{

    if (isEmpty(response.beniban) && isEmpty(response.benaddress) && 
        isEmpty(response.benbic) && isEmpty(response.benname))
    {
      customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.CLIQ.AliasError"), popupCommonAlertDimiss, "");
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }else
    {
      callShareAlias(response.benname,aliasValue,aliasType);
      //call share Alias
    }

  }catch(error){
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJAliasIPS","Catch FAILURE","SERVICE",error);
  }
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

}
function SetupConfirmationScreen(){

  var gblFromAcc = "";
  //Mai 16/12/2020

  if(gblTModule==="IPSReturn")
  {

    frmIPSRequests.lblConfirmReason.text  = frmIPSRequests.tbxReasonRP.text; 
    frmIPSRequests.ToAccConfirmationText.text = frmIPSRequests.lblAliasRP.text;
    frmIPSRequests.lblAmountConfirmValue.text = frmIPSRequests.txtAmountRP.text;
    frmIPSRequests.lblIBANConftext.text = frmIPSRequests.lblIBANRP.text;
    //omar ALnajjar 08/02/2021
    frmIPSRequests.skin = "sknmainGradient";
    frmIPSRequests.flxConfirmEPS.setVisibility(true);
    frmIPSRequests.flxReturnPayment.setVisibility(false);

  }
  if(gblTModule==="CLIQRequest")
  {
    frmEPS.show();
    frmEPS.flxConfirmEPS.setvisible=true;
    frmEPS.lblConfirmReason.text  = frmEPS.lblReason.text;  
  }
  else
  {
    frmEPS.show();
    frmEPS.flxConfirmEPS.visible=false;
  }
  if(frmEPS.btnAlias.skin == "sknOrangeBGRNDBOJ" && frmEPS.btnMob.skin == "slButtonBlueFocus" && frmEPS.btnIBAN.skin == "slButtonBlueFocus" ){
    // if Alias Type is Alias //
    frmEPS.flxOtherDetailScroll.setVisibility(true);
    frmEPS.lblFromAccConfText.text = frmEPS.lblAccountName1.text;
    frmEPS.ToAccConfirmationText.text = frmEPS.lblBeneName.text;
    frmEPS.lblAmountConfirmValue.text = frmEPS.txtAmountAlias.text;
    frmEPS.lblIBANConftext.text = frmEPS.lblIBANAliastxt.text;
    frmEPS.lblConfirmAddr.text = frmEPS.lbAliasAddresstxt.text;
    frmEPS.lblConfirmBankName.text = frmEPS.lblAliasBanktxt.text;
  }else if(frmEPS.flxDetailsIBAN.isVisible === true){
    gblFromAcc = frmEPS.btnIBAN.text;
    //Omar ALnajjar CLIQ TRANSFER VIA IBAN
    frmEPS.lblFromAccConfText.text = frmEPS.lblAccountName1.text;
    frmEPS.ToAccConfirmationText.text = frmEPS.txtIBANBeneName.text;
    frmEPS.lblAmountConfirmValue.text = frmEPS.txtAmountIBAN.text;
    frmEPS.lblIBANConftext.text = frmEPS.txtIBANAlias.text;
    frmEPS.lblConfirmAddr.text = frmEPS.txtAddressAlias.text;
    frmEPS.lblConfirmBankName.text = frmEPS.tbxBankName.text;
    //Omar ALnajjar CLIQ TRANSFER VIA IBAN
  }else if (frmEPS.btnAlias.skin == "slButtonBlueFocus" && frmEPS.btnMob.skin == "sknOrangeBGRNDBOJ" && frmEPS.btnIBAN.skin == "slButtonBlueFocus"){
    // if Alias Type is Mobile //
    frmEPS.lblFromAccConfText.text = frmEPS.lblAccountName1.text;
    frmEPS.ToAccConfirmationText.text = frmEPS.lblBeneNameMob.text;
    frmEPS.lblAmountConfirmValue.text = frmEPS.txtAmountMob.text;
    frmEPS.lblIBANConftext.text = frmEPS.lblIBANMobtxt.text;
    frmEPS.lblConfirmAddr.text = frmEPS.lblAddressMobtxt.text;
    frmEPS.lblConfirmBankName.text = frmEPS.lblBankMobAliastxt.text;

    //      frmEPS.flxConfirmBankName.setVisibility(false);
    //      frmEPS.lblIBANConfTitle.text = "Alias Name";
    //      frmEPS.lblIBANConftext.text = frmEPS.txtAliasName.text;
    //      frmEPS.lblConfirmAddTitle.text = frmEPS.lblAmountAlias.text;
    //      frmEPS.lblConfirmAddr.text = frmEPS.txtAmount.text;
  }else if (frmEPS.btnAlias.skin == "slButtonBlueFocus" && frmEPS.btnMob.skin == "slButtonBlueFocus" && frmEPS.btnIBAN.skin == "sknOrangeBGRNDBOJ"){

    frmEPS.lblFromAccConfText.text = frmEPS.lblAccountName1.text;
    frmEPS.ToAccConfirmationText.text = frmEPS.txtIBANBeneName.text;
    frmEPS.lblAmountConfirmValue.text = frmEPS.txtAmountIBAN.text;
    frmEPS.lblIBANConftext.text = frmEPS.txtIBANAlias.text;
    frmEPS.lblConfirmAddr.text = frmEPS.txtAddressAlias.text;
    frmEPS.lblConfirmBankName.text = frmEPS.tbxBankName.text;

  }


}
function GetAliasInfo(AliType){

  var aliasType="";

  if (frmEPS.btnAlias.skin==="sknOrangeBGRNDBOJ")
  {
    aliasType="ALIAS";
  }
  else if(frmEPS.btnMob.skin==="sknOrangeBGRNDBOJ")
  {
    aliasType="MOBL";
  }

  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJAliasIPS");
  appMFConfiguration.invokeOperation("prGetIpsAliasDetails", {},{"p_customer_id": custid,
                                                                 "p_alias":AliType,
                                                                 "p_alias_type": aliasType,
                                                                 "p_channel":"MOBILE",
                                                                 "p_user_id":"BOJMOB"},
                                     successCalling,
                                     function(error){
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJAliasIPS","SERVICE FAILURE","SERVICE",error);
  });
}
function successCalling(response){
  //   alert(response);
  try{

    if (isEmpty(response.beniban) && isEmpty(response.benaddress) && 
        isEmpty(response.benbic) && isEmpty(response.benname))
    {
      customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.CLIQ.AliasError"), popupCommonAlertDimiss, "");
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }else
    {
      b_iban = response.beniban;
      bic_num = response.benbic;
      if(frmEPS.btnAlias.skin == sknOrangeBGRNDBOJ){
        frmEPS.flxAmountAlias.setVisibility(true);
        frmEPS.flxBeneName.setVisibility(true);
        //frmEPS.flxIBANAliass.setVisibility(true);
        //frmEPS.flxAliasAddress.setVisibility(true);
        frmEPS.flxAliasBank.setVisibility(true);
        frmEPS.lblIBANAliastxt.text = response.beniban;
        frmEPS.lbAliasAddresstxt.text = response.benaddress;
        frmEPS.lblAliasBanktxt.text = response.benbic;
        frmEPS.lblBeneName.text = response.benname;
      } 
      else if(frmEPS.btnMob.skin == sknOrangeBGRNDBOJ){
        frmEPS.flxAmountMob.setVisibility(true);
        frmEPS.flxBeneNameMob.setVisibility(true);
        //frmEPS.flxIbanDetailsMob.setVisibility(true);
        // frmEPS.flxAddressMob.setVisibility(true);
        frmEPS.flxBankMob.setVisibility(true);  
        frmEPS.lblIBANMobtxt.text = response.beniban;
        frmEPS.lblAddressMobtxt.text = response.benaddress; 
        frmEPS.lblBankMobAliastxt.text = response.benbic;
        frmEPS.lblBeneNameMob.text = response.benname;
      }
      else if(frmEPS.btnIBAN.skin == sknOrangeBGRNDBOJ){
        frmEPS.lblIBANtxt.text = response.beniban;
        frmEPS.lblAddressIBANtext.text = response.benaddress; 
        frmEPS.lbIBankIBANtext.text = response.benname;
      }
    }
    validateLblNext();//Omar ALnajjar
    //   frmEPS.lblIBANConftext.text = response.beniban;
    //   frmEPS.lblConfirmBankName.text = response.benname;
    //   frmEPS.lblConfirmAddr.text = response.benaddress;
    //   frmEPS.txtAmount.text = response.benbic;
  }catch(error){
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJAliasIPS","Catch FAILURE","SERVICE",error);
  }
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

}
function  setupBanknameIPS(data){
  gblWorkflowType = null;

  kony.print("data banks ::"+JSON.stringify(data));

  frmEPS.tbxBankName.text = data.BankName;
  frmEPS.lblSwiftCode.text=data.SwiftCode;
  frmEPS.lblBankNameStat.setVisibility(true);
  frmEPS.tbxBankName.setVisibility(true);
  frmEPS.lblBankName.setVisibility(false);
  frmEPS.show();
  frmSelectDetailsBene.destroy();
  validateLblNext();//Omar ALnajjar 

}
function returnBankBranchs(detailType, lblName, countryCode){
  kony.boj.detailsForBene.lblToUpdate = lblName;
  kony.boj.detailsForBene.flag = true;
  kony.boj.selectedDetailsType = detailType;
  frmSelectBankName.segDetails.setData([]);

  var ctrydesc = "CTRY_S_DESC"; 
  if(kony.boj.lang === "eng")
    ctrydesc = "CTRY_S_DESC";
  else
    ctrydesc = "CTRY_B_DESC";

  if(detailType === "Country"){
    frmSelectBankName.segDetails.widgetDataMap = { 
      lblData: ctrydesc,
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    kony.boj.selectedDetailsList = sort_ALPHABETICALLY(kony.boj.detailsForBene.Country, ctrydesc);
    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i][ctrydesc] !== undefined && kony.boj.selectedDetailsList[i][ctrydesc] !== ""){
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i][ctrydesc].substring(0, 2).toUpperCase();
      }else{
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CTRY_S_DESC.substring(0, 2).toUpperCase();
      }
    }
  }
  else if(detailType === "City"){
    if(kony.boj.lang === "eng")
      ctrydesc = "CITY_S_DESC";
    else
      ctrydesc = "CITY_B_DESC";
    frmSelectBankName.segDetails.widgetDataMap = { 
      lblData: ctrydesc,
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    kony.boj.selectedDetailsList = sort_ALPHABETICALLY(kony.boj.detailsForBene[detailType][countryCode], ctrydesc);
    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i][ctrydesc] !== undefined && kony.boj.selectedDetailsList[i][ctrydesc] !== ""){
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i][ctrydesc].substring(0, 2).toUpperCase();
      }else{
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CITY_B_DESC.substring(0, 2).toUpperCase();
      }
    }
  }
  else if(detailType === "Branch"){
    frmSelectBankName.segDetails.widgetDataMap = { 
      lblData: "BRANCH_NAME",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };
    if(gblLaunchModeOBJ.lauchMode  && (!isEmpty(gblLaunchModeOBJ.accno) || !isEmpty(gblLaunchModeOBJ.iban)) && frmAddExternalAccountKA.lblBankBranch.text != kony.i18n.getLocalizedString("i18n.Bene.BankBranch")){
      kony.print("Don nothing:");
    }
    else{
      kony.boj.getBranchList();

    }
    return;
    //kony.boj.selectedDetailsList = kony.boj.detailsForBene.Branch;
  }
  else if(detailType === "BankDetail"){
    frmSelectBankName.segDetails.widgetDataMap = { 
      lblData: "BankName",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    var bankList = [];
    if(kony.application.getCurrentForm().id === "frmAddExternalAccountKA" && kony.boj.addBeneVar.selectedType === "DOM"){
      for(var p in kony.boj.detailsForBene.BankDetail){
        if(kony.boj.detailsForBene.BankDetail[p].SwiftCode !== "BJORJOAX")
          bankList.push(kony.boj.detailsForBene.BankDetail[p]);
      }
    }else{
      bankList = kony.boj.detailsForBene.BankDetail;
    }

    kony.boj.selectedDetailsList = sort_ALPHABETICALLY(bankList, "BankName");
  }
  /*hassan OpenDeposite*/
  else if(detailType === "MaturityInstruction"){
    //Omar ALnajjar Opend deposits
    var matArray=[];
    var matTypes=[];
    if ((frmOpenTermDeposit.btnTener3Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener6Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener1Year.skin === "sknOrangeBGRNDBOJ") && 
        frmOpenTermDeposit.btnMonthly.skin === "sknOrangeBGRNDBOJ"){
      matTypes=[{"Maturity":geti18Value("i18n.termDeposit.renWithOutInterest")},{"Maturity":geti18Value("i18n.termDeposit.close")}];
    }else{
      matTypes=[{"Maturity":geti18Value("i18n.termDeposit.renWithInterest")},{"Maturity":geti18Value("i18n.termDeposit.renWithOutInterest")},{"Maturity":geti18Value("i18n.termDeposit.close")}];

    }
    //     var matTypes=[{"Maturity":geti18Value("i18n.termDeposit.renWithInterest")},{"Maturity":geti18Value("i18n.termDeposit.renWithOutInterest")},{"Maturity":geti18Value("i18n.termDeposit.close")}];


    frmSelectBankName.segDetails.widgetDataMap = { 
      lblData: "Maturity",
      lblInitial: "INITIAL",
      flxIcon1: "icon"
    };

    kony.boj.selectedDetailsList=matTypes;

    for(var i in kony.boj.selectedDetailsList){
      if(kony.boj.selectedDetailsList[i].Maturity !== undefined && kony.boj.selectedDetailsList[i].Maturity !== ""){
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].Maturity.substring(0, 2).toUpperCase();
      }else{
        kony.boj.selectedDetailsList[i].INITIAL = kony.boj.selectedDetailsList[i].CITY_B_DESC.substring(0, 2).toUpperCase();
      }
    }
  }

  /*hassan OpenDeposite*/
  else if (detailType === "IPS"){


  }
  for(var i in kony.boj.selectedDetailsList){
    if(kony.boj.selectedDetailsList[i].icon === undefined){
      kony.boj.selectedDetailsList[i].icon = {
        backgroundColor: kony.boj.getBackGroundColour(i)
      };
    }
    else
      break;
  }

  frmSelectBankName.segDetails.setData(kony.boj.selectedDetailsList);
  frmSelectBankName.show();
}
function TransferIPS(){
  var src_acc = frmEPS.lblAccountNumber1.text;
  var alias_name = "";
  var alias_addr = "";
  var alias_amount = "";

  logObj[0] = src_acc;
  logObj[1] = kony.store.getItem("frmAccount").branchNumber;
  logObj[2] = "JOD";
//   logObj[4] = bic_num;

  if(frmEPS.btnAlias.skin == sknOrangeBGRNDBOJ){b_iban = frmEPS.lblIBANAliastxt.text;
                                                alias_name = frmEPS.lblBeneName.text;
                                                alias_addr = frmEPS.lbAliasAddresstxt.text; 
                                                alias_amount = frmEPS.txtAmountAlias.text;
                                                logObj[9] = "CLQ_Alias";
                                                logObj[10] = frmEPS.txtAliasName.text;
                                               }
  if(frmEPS.btnMob.skin == sknOrangeBGRNDBOJ){b_iban = frmEPS.lblIBANMobtxt.text;
                                              alias_name = frmEPS.lblBeneNameMob.text;
                                              alias_addr = frmEPS.lblAddressMobtxt.text;
                                              alias_amount = frmEPS.txtAmountMob.text;
                                              logObj[9] = "CLIQ_Mobile";
                                              logObj[10] = frmEPS.txtAliasNameMob.text;
                                             }
  if(frmEPS.btnIBAN.skin == sknOrangeBGRNDBOJ){b_iban = frmEPS.txtIBANAlias.text;
                                               alias_name = frmEPS.txtIBANBeneName.text;
                                               alias_addr = frmEPS.txtAddressAlias.text;
                                               alias_amount = frmEPS.txtAmountIBAN.text; 
                                               bic_num=frmEPS.lblSwiftCode.text;
                                               logObj[9] = "CLIQ_IBAN";
                                               logObj[10] = frmEPS.txtIBANAlias.text;
                                              }
  logObj[3] = b_iban;
  logObj[4] = "";
  logObj[12] = alias_amount;
  logObj[13] = "Cliq_transfer";
  logObj[15] = "";
  logObj[19] = "";
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var request_params ={
    "cust_id":custid,
    "src_acc":src_acc,
    "src_br":kony.store.getItem("frmAccount").branchNumber,
    "b_iban":b_iban,
    "b_name":alias_name,
    "b_address":alias_addr,
    "b_bic":bic_num,
    "purpose":"11110",
    "trans_amt": alias_amount,
    "trans_amt_ccy":"JOD",
    "b_channel":"MOBILE",
    "user_id":"BOJMOB"
  };
  kony.print("request_params"+JSON.stringify(request_params));
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJIPS");
  appMFConfiguration.invokeOperation("prIpsTransfer", {},request_params,
                                     successPass,
                                     function(error){kony.print("error"+ JSON.stringify(error));
                                                     customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
                                                     exceptionLogCall("BOJIPS","SERVICE FAILURE","SERVICE",error);
                                                     kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                                                     logObj[17] = "FAILURE";
                                                     logObj[16] = "";
                                                     var errmsg = error.errmsg.replace(/[',":?><}{\s\s+\n]/g, " ");
                                                     if(isEmpty(errmsg)){
                                                       logObj[18] = "errmsg is empty";
                                                     }else if(errmsg.length >200){
                                                       logObj[18] = errmsg.slice(200,400); 
                                                     }else{
                                                       logObj[18] = errmsg;
                                                     }
                                                     loggerCall();                                                
                                                    });
}
function successPass(response){
  try{
    kony.print("successPass"+ JSON.stringify(response));
    ResetFormIPSData();//Omar ALnajjar
    logObj[16] = response.errorCode;
    if (response.errorCode==="00000")
    {
      gblDVOC = "53";//Mai 3/17/2021 DVOC
      logObj[17] = "SUCCESS";
      logObj[18] = "Success cliq transfer";
      kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"),
                                     geti18Value("i18.CLIQ.SuccessMSG"),
                                     geti18Value("i18n.Bene.GotoAccDash"), 
                                     geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
                                     "CliqDashboardManage", "", "", " " + " " + "");

    }
    else
    {
      logObj[17] = "FAILURE";      
      var Message = getErrorMessage(response.errorCode);
      if(isEmpty(Message)){
        logObj[18] = Message;
      }else if(Message.length >200){
        logObj[18] = Message.substring(0,200); 
      }else{
        logObj[18] = Message;
      }
      kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"),
                                     Message,
                                     geti18Value("i18n.Bene.GotoAccDash"), 
                                     geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
                                     "CliqDashboardManage", "", "", " " + " " + "");
    }
  }catch(error){
    logObj[17] = "FAILURE";
    logObj[16] = "";
    logObj[18]= "CLIQ Transfer Catch";

    //     if(isEmpty(error)){
    //       logObj[18] = error;
    //     }else if(error.length >200){
    //       logObj[18] = error.substring(0,200); 
    //     }else{
    //       logObj[18] = error;
    //     }
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJIPS","Catch FAILURE","SERVICE",error);
  }
  loggerCall();
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

}

function creditConfirmationHandler(data){
  //Mai 16/2/2021  date format
  var selectedOutWardsData = GetSelectedCreditConfirmPayment(data.endtoendid);
  var yyyy=selectedOutWardsData.valuedate.substring(0,4);
  var mm=selectedOutWardsData.valuedate.substring(5,7);
  var dd=selectedOutWardsData.valuedate.substring(8,11);
  var fulldate=dd+"-"+mm+"-"+yyyy;

  //   kony.print("Original date::"+selectedOutWardsData.valuedate);
  //   kony.print("IPS Creditconfirm day ::"+ dd);
  //   kony.print("IPS Creditconfirm month ::"+ mm);
  //   kony.print("IPS Creditconfirm year ::"+ yyyy);
  //   kony.print("Creditconfirm full date :: "+ fulldate);
  if (selectedOutWardsData.transfertype === "IPS_OUTWARD"){
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetIPSCreditStatus");
    appMFConfiguration.invokeOperation("prGetIpsCreditStatus", {},{"p_customer_id": custid,
                                                                   "p_channel":"MOBILE",
                                                                   "p_user_id":"BOJMOB",
                                                                   "p_EndToEndId":selectedOutWardsData.endtoendid,
                                                                   "p_ValueDate":fulldate,//selectedOutWardsData.valuedate,
                                                                   "p_ReceiverParticipantCode":selectedOutWardsData.creditorinstitutioncode
                                                                  },
                                       IPSCreditConfirm,
                                       function(error){
      kony.print(JSON.stringify("Credit status error :: "+ error));
      customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.cliq.unKnown"), popupCommonAlertDimiss, "");
      exceptionLogCall("BOJGetIPSCreditStatus","Catch FAILURE","SERVICE",error);

      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    });

  }else{
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }


}

function IPSCreditConfirm(res){
  try{


    kony.print("Credit status Response :: "+ JSON.stringify(res));
    if (!isEmpty(res.returnCode) && res.returnCode === "00000"){
      if (res.payment_status === "Credited"){
        customAlertPopup(geti18Value("i18n.CLiq.TransferStatus"), geti18Value("ii18n.CLIQ.successConfirm"), popupCommonAlertDimiss, "");
      }else{
        customAlertPopup(geti18Value("i18n.CLiq.TransferStatus"), geti18Value("ii18n.CLIQ.failureConfirm"), popupCommonAlertDimiss, "");
      }
    }else{
      customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.cliq.unKnown"), popupCommonAlertDimiss, "");
    }
  }catch(error){
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJGetIPSCreditStatus","Catch FAILURE","SERVICE",error);
  }
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

}



//Mai 16/12/2020
function PopulateReturnPayment(data){
  try{
    kony.print("Retunr payment ::"+JSON.stringify(data));
    kony.print("endtoendid ::"+data.endtoendid);

    // kony.print("GetSelectedReturnPayment ::"+ JSON.stringify(GetSelectedReturnPayment(data.endtoendid)));
    ReturnPaymentIPS = [];
    ReturnPaymentIPS.push(GetSelectedReturnPayment(data.endtoendid));
    kony.print("Retunr payment Global::"+JSON.stringify(ReturnPaymentIPS));
    //  kony.print("Retunr payment ReturnPaymentIPS::"+JSON.stringify(ReturnPaymentIPS));
    //kony.boj.selectedPaymentCliq = context.widgetInfo.selectedRowItems[0];
    frmIPSRequests.flxPaymentHistory.setVisibility(false);
    frmIPSRequests.flxRequestToPay.setVisibility(false);
    frmIPSRequests.flxReturnPayment.setVisibility(true);
    //omar ALnajjar 08/02/2021
    frmIPSRequests.skin = "sknmainGradient";
    frmIPSRequests.flxConfirmEPS.setVisibility(false);
    frmIPSRequests.lblIBANRP.text =ReturnPaymentIPS[0].debtoriban.toString();//"JOARAB0987654567890";
    frmIPSRequests.lblReferenceRPR.text =ReturnPaymentIPS[0].endtoendid.toString();//"JOARAB0987654567890";
    frmIPSRequests.lblAliasRP.text =ReturnPaymentIPS[0].debtorname.toString();//"JOARAB0987654567890";
    frmIPSRequests.lblRPTransactionDate.text =ReturnPaymentIPS[0].valuedate;// "6/1/2021";
    frmIPSRequests.txtAmountRP.text=ReturnPaymentIPS[0].instructedamount.toString();//"10.25";
    frmIPSRequests.txtAmountRPOriginal.text=ReturnPaymentIPS[0].instructedamount.toString();


  }catch(e){
    kony.print("Exception_navigate_ReturnPayment ::"+e);
  }

}
//Mai 10/1/2021
function GetAcceptedPayments()
{
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJPrGetIPSAcceptedDetail");
  appMFConfiguration.invokeOperation("prGetIpsAcceptedDetails", {},{"p_customer_id": custid,
                                                                    "p_channel":"MOBILE",
                                                                    "p_user_id":"BOJMOB"},
                                     IPSTransactions,
                                     function(error){
    kony.print("failed ::" + JSON.stringify(error));
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJPrGetIPSAcceptedDetail","SERVICE FAILURE","SERVICE",error);

    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  });
}

function IPSTransactions(res){
  try{
    var temp = [];
    var IPSAccDetails =[];
    var customer="";
    IPSPaymentHistory=[];
    IPSHistorySeg = [];
    var beneficiaryAlias = "";
    var beneficiaryIBAN = "";
    var returnButton=  {"isVisible": false};
    var creditedButton=  {"isVisible": false};
    var incoming =false;
    var outgoing =false;
    var RTP =false;
    var nodata = {"isVisible": false};
    kony.print("Success IPS Accepted History ::" + JSON.stringify(res));
    frmIPSRequests.segIPSHistory.setData([]);
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    var date = new Date();


    //Omar ALnajjar 28/01/2021 Sorted IPS By Date 
    //   res.IPSAccDetails.sort((a, b) => new Date(b.valuedate) - new Date(a.valuedate));

    res.IPSAccDetails.sort(function (a, b) {
      return b.valuedate.localeCompare(a.valuedate);
    });



    frmIPSRequests.segIPSHistory.widgetDataMap = { 
      lblTransactionType: "lblTransactionType",
      transactionDatei :"transactionDatei",
      transactionAmounti: "transactionAmounti",
      lblIBANi:"lblIBANi",
      AliasNamei:"AliasNamei",
      lblTrxRefi:"lblTrxRefi",
      lblFromAccount:"lblFromAccount",
      transactionDateo :"transactionDateo",
      transactionAmounto: "transactionAmounto",
      lblIBANo:"lblIBANo",
      lblTrxRefo:"lblTrxRefo",
      btnReturnPayment:"btnReturnPayment",
      //btnCreditConfirm:"btnCreditConfirm",
      flxIncoming: "flxIncoming",
      flxOutgoing: "flxOutgoing",
      //Omar Alnajjar 16/03/2021 RTP
      flxRTP:"flxRTP",
      lblNoData: "lblNoData",
      lblCreditAccountValue:"lblCreditAccountValue",

      lblRefi:"lblRefi",
      lblAliasi:"lblAliasi",
      lblIBAnDesci:"lblIBAnDesci",
      lblTrxDatei:"lblTrxDatei",
      lblAmti:"lblAmti",
      lblCredited:"lblCredited",

      lblRefo:"lblRefo",
      lblFAcco:"lblFAcco",
      lblIBAnDescO:"lblIBAnDescO",
      lblTrxDateo:"lblTrxDateo",
      lblAmto:"lblAmto",
      lblAliaso:"lblAliaso",

      lblTransCustNameRTP:"lblTransCustNameRTP",
      lblReasonRTP:"lblReasonRTP",
      lblRefRTP:"lblRefRTP",
      lblStatusRTP:"lblStatusRTP",

      lblTransDateRTPValue:"lblTransDateRTPValue",
      lblTransAmountRTPValue:"lblTransAmountRTPValue",
      lblTransCustNameRTPValue:"lblTransCustNameRTPValue",
      lblReasonRTPValue:"lblReasonRTPValue",
      lblRefRTPValue:"lblRefRTPValue",
      lblStatusRTPValue:"lblStatusRTPValue",

      endtoendid:"endtoendid"
    };



    for (var i in res.IPSAccDetails) {
      res.IPSAccDetails[i].lblRefi = kony.i18n.getLocalizedString("i18n.common.ReferenceNumber");
      res.IPSAccDetails[i].lblAliasi = kony.i18n.getLocalizedString("i18n.CliQ.ALias");
      res.IPSAccDetails[i].lblIBAnDesci = kony.i18n.getLocalizedString("i18.CLIQ.IBAN");
      res.IPSAccDetails[i].lblTrxDatei = kony.i18n.getLocalizedString("i18n.common.transactiondate");
      res.IPSAccDetails[i].lblAmti = kony.i18n.getLocalizedString("i18n.accounts.amount");
      res.IPSAccDetails[i].lblCredited = kony.i18n.getLocalizedString("ii18n.CLIQ.creditedAccount");
      res.IPSAccDetails[i].btnReturnPayment = kony.i18n.getLocalizedString("i18n.CLIQ.ReturnPaymentBtn");//Mai 16/2/2021 i18

      res.IPSAccDetails[i].lblRefo = kony.i18n.getLocalizedString("i18n.common.ReferenceNumber");
      res.IPSAccDetails[i].lblFAcco = kony.i18n.getLocalizedString("i18n.CLIQ.FromAccount");
      res.IPSAccDetails[i].lblIBAnDescO = kony.i18n.getLocalizedString("i18n.CliQ.ToBene");
      res.IPSAccDetails[i].lblTrxDateo = kony.i18n.getLocalizedString("i18n.common.transactiondate");
      res.IPSAccDetails[i].lblAmto = kony.i18n.getLocalizedString("i18n.accounts.amount");
      res.IPSAccDetails[i].lblAliaso = kony.i18n.getLocalizedString("i18n.CliQ.ALias");

      res.IPSAccDetails[i].lblTransCustNameRTP = kony.i18n.getLocalizedString("i18.CLIQ.IBAN");
      res.IPSAccDetails[i].lblReasonRTP = kony.i18n.getLocalizedString("i18n.CLIQ.Reason");
      res.IPSAccDetails[i].lblRefRTP = kony.i18n.getLocalizedString("i18n.common.ReferenceNumber");
      res.IPSAccDetails[i].lblStatusRTP = kony.i18n.getLocalizedString("i18n.CLIQ.status");

      // res.IPSAccDetails[i].btnCreditConfirm = kony.i18n.getLocalizedString("ii18.CLIQ.creditConfirm");


      IPSPaymentHistory.push( res.IPSAccDetails[i]);

      var updatedDate = new Date(res.IPSAccDetails[i].valuedate);
      var modifedDate =res.IPSAccDetails[i].valuedate;


      //     (updatedDate.getDay() + 1) +"/"+ updatedDate.toLocaleString('default', { month: 'short' })+"/"+ updatedDate.getFullYear();

      if(res.IPSAccDetails[i].transfertype === "IPS_INWARD"){
        kony.print(" IPS_INWARD endtoendid: "+res.IPSAccDetails[i].endtoendid );
        customer = "";
        beneficiaryAlias = res.IPSAccDetails[i].debtorname;
        beneficiaryIBAN =  res.IPSAccDetails[i].debtoriban;

        incoming = {"isVisible": true};
        outgoing = {"isVisible": false};
        RTP = {"isVisible": false};
        var aaa = date_diff_indays(res.IPSAccDetails[i].valuedate);
        var qq = res.IPSAccDetails[i].transfertype === "IPS_INWARD";
        /*hassan hide return payment */
        if(res.IPSAccDetails[i].transfertype === "IPS_INWARD" && date_diff_indays(res.IPSAccDetails[i].valuedate)){
          returnButton ={"text":res.IPSAccDetails[i].btnReturnPayment,"isVisible": true};
          // creditedButton = {"text":res.IPSAccDetails[i].btnCreditConfirm,"isVisible": false};
        }else{
          returnButton ={"text":res.IPSAccDetails[i].btnReturnPayment,"isVisible": false};
          //  creditedButton = {"text":res.IPSAccDetails[i].btnCreditConfirm,"isVisible": true};

        }
        /*hassan hide return payment */
        temp.push({lblTrxRefi:{"text": res.IPSAccDetails[i].endtoendid},
                   lblTransactionType:{"text":res.IPSAccDetails[i].transfertype},
                   transactionDatei :{"text":modifedDate},
                   transactionAmounti:{"text": res.IPSAccDetails[i].instructedamount + " " + res.IPSAccDetails[i].instructedcurrency },
                   lblIBANi:{"text":beneficiaryIBAN},
                   AliasNamei:{"text":beneficiaryAlias},
                   lblCreditAccountValue:{"text":res.IPSAccDetails[i].creditoriban.substring(res.IPSAccDetails[i].creditoriban.length-16,res.IPSAccDetails[i].creditoriban.length)},

                   lblStatusRTPValue:{"text":""},
                   lblRefRTPValue:{"text":""},
                   lblReasonRTPValue:{"text":""},
                   lblTransCustNameRTPValue:{"text":""},
                   lblTransAmountRTPValue:{"text":""},
                   lblTransDateRTPValue:{"text":""},


                   lblFromAccount:{"text":""},
                   transactionDateo :{"text":""},
                   transactionAmounto:{"text":""},
                   lblIBANo:{"text":""},
                   lblTrxRefo:{"text":""},
                   btnReturnPayment: returnButton,
                   //btnCreditConfirm:creditedButton,
                   flxIncoming:incoming,
                   flxOutgoing: outgoing,
                   flxRTP:RTP,
                   lblRefi:res.IPSAccDetails[i].lblRefi,
                   lblAliasi:res.IPSAccDetails[i].lblAliasi,
                   lblIBAnDesci:res.IPSAccDetails[i].lblIBAnDesci,
                   lblTrxDatei:res.IPSAccDetails[i].lblTrxDatei,
                   lblAmti:res.IPSAccDetails[i].lblAmti,
                   lblCredited:res.IPSAccDetails[i].lblCredited,

                   lblRefo:res.IPSAccDetails[i].lblRefo,
                   lblFAcco:res.IPSAccDetails[i].lblFAcco,
                   lblIBAnDescO:res.IPSAccDetails[i].lblIBAnDescO,
                   lblTrxDateo:res.IPSAccDetails[i].lblTrxDateo,
                   lblAmto:res.IPSAccDetails[i].lblAmto,
                   lblAliaso:res.IPSAccDetails[i].lblAliaso,

                   lblTransCustNameRTP: res.IPSAccDetails[i].lblTransCustNameRTP,
                   lblReasonRTP: res.IPSAccDetails[i].lblReasonRTP,
                   lblRefRTP: res.IPSAccDetails[i].lblRefRTP,
                   lblStatusRTP: res.IPSAccDetails[i].lblStatusRTP,


                   endtoendid: res.IPSAccDetails[i].endtoendid,
                   //{lblNoData:nodata}
                  }
                 );
        IPSHistorySeg.push({lblTrxRefi:{"text": res.IPSAccDetails[i].endtoendid},
                            lblTransactionType:{"text":res.IPSAccDetails[i].transfertype},
                            transactionDatei :{"text":modifedDate},
                            transactionAmounti:{"text": res.IPSAccDetails[i].instructedamount  + " " + res.IPSAccDetails[i].instructedcurrency},
                            lblIBANi:{"text":beneficiaryIBAN},
                            AliasNamei:{"text":beneficiaryAlias},
                            lblCreditAccountValue:{"text":res.IPSAccDetails[i].creditoriban.substring(res.IPSAccDetails[i].creditoriban.length-16,res.IPSAccDetails[i].creditoriban.length)},

                            lblStatusRTPValue:{"text":""},
                            lblRefRTPValue:{"text":""},
                            lblReasonRTPValue:{"text":""},
                            lblTransCustNameRTPValue:{"text":""},
                            lblTransAmountRTPValue:{"text":""},
                            lblTransDateRTPValue:{"text":""},

                            lblFromAccount:{"text":""},
                            transactionDateo :{"text":""},
                            transactionAmounto:{"text":""},
                            lblIBANo:{"text":""},
                            lblTrxRefo:{"text":""},
                            //  btnCreditConfirm: creditedButton,
                            btnReturnPayment: returnButton,
                            flxIncoming:incoming,
                            flxOutgoing: outgoing,
                            flxRTP:RTP,

                            lblRefi:res.IPSAccDetails[i].lblRefi,
                            lblAliasi:res.IPSAccDetails[i].lblAliasi,
                            lblIBAnDesci:res.IPSAccDetails[i].lblIBAnDesci,
                            lblTrxDatei:res.IPSAccDetails[i].lblTrxDatei,
                            lblAmti:res.IPSAccDetails[i].lblAmti,
                            lblCredited:res.IPSAccDetails[i].lblCredited,

                            lblRefo:res.IPSAccDetails[i].lblRefo,
                            lblFAcco:res.IPSAccDetails[i].lblFAcco,
                            lblIBAnDescO:res.IPSAccDetails[i].lblIBAnDescO,
                            lblTrxDateo:res.IPSAccDetails[i].lblTrxDateo,
                            lblAmto:res.IPSAccDetails[i].lblAmto,
                            lblAliaso:res.IPSAccDetails[i].lblAliaso,

                            lblTransCustNameRTP: res.IPSAccDetails[i].lblTransCustNameRTP,
                            lblReasonRTP: res.IPSAccDetails[i].lblReasonRTP,
                            lblRefRTP: res.IPSAccDetails[i].lblRefRTP,
                            lblStatusRTP: res.IPSAccDetails[i].lblStatusRTP,
                            endtoendid: res.IPSAccDetails[i].endtoendid,
                           }
                          );
        kony.print(" IPS_INWARD "+ JSON.stringify(temp) );
      }
      else if(res.IPSAccDetails[i].transfertype === "IPS_OUTWARD"){

        kony.print(" IPS_OUTWARD endtoendid: "+res.IPSAccDetails[i].endtoendid );
        customer = res.IPSAccDetails[i].debtoriban.substring( res.IPSAccDetails[i].debtoriban.length-16, res.IPSAccDetails[i].debtoriban.length);//me
        beneficiaryAlias = res.IPSAccDetails[i].creditorname;
        beneficiaryIBAN =  res.IPSAccDetails[i].creditoriban;
        incoming = {"isVisible": false};
        outgoing = {"isVisible": true};
        RTP =  {"isVisible": false};
        returnButton ={"text":res.IPSAccDetails[i].btnReturnPayment,"isVisible": false};
        //creditedButton = {"text":res.IPSAccDetails[i].btnCreditConfirm,"isVisible": true};
        // temp.push(res.IPSAccDetails[i]);

        temp.push({lblTrxRefi: {"text":""},
                   lblTransactionType:{"text":res.IPSAccDetails[i].transfertype},
                   transactionDatei :{"text":""},
                   transactionAmounti:{"text":""},
                   lblIBANi:{"text":""},
                   AliasNamei:{"text":""},
                   lblCreditAccountValue:{"text":""},

                   lblStatusRTPValue:{"text":""},
                   lblRefRTPValue:{"text":""},
                   lblReasonRTPValue:{"text":""},
                   lblTransCustNameRTPValue:{"text":""},
                   lblTransAmountRTPValue:{"text":""},
                   lblTransDateRTPValue:{"text":""},

                   lblFromAccount:{"text":customer},
                   transactionDateo :{"text":modifedDate},
                   transactionAmounto:{"text":res.IPSAccDetails[i].instructedamount + " " + res.IPSAccDetails[i].instructedcurrency},
                   // btnCreditConfirm: creditedButton,
                   lblIBANo:{"text":beneficiaryIBAN},
                   lblTrxRefo:{"text":res.IPSAccDetails[i].endtoendid},
                   btnReturnPayment: returnButton,
                   flxIncoming:incoming,
                   flxOutgoing: outgoing,
                   flxRTP:RTP,
                   lblNoData:nodata,

                   lblRefi:res.IPSAccDetails[i].lblRefi,
                   lblAliasi:res.IPSAccDetails[i].lblAliasi,
                   lblIBAnDesci:res.IPSAccDetails[i].lblIBAnDesci,
                   lblTrxDatei:res.IPSAccDetails[i].lblTrxDatei,
                   lblAmti:res.IPSAccDetails[i].lblAmti,
                   lblCredited:res.IPSAccDetails[i].lblCredited,

                   lblRefo:res.IPSAccDetails[i].lblRefo,
                   lblFAcco:res.IPSAccDetails[i].lblFAcco,
                   lblIBAnDescO:res.IPSAccDetails[i].lblIBAnDescO,
                   lblTrxDateo:res.IPSAccDetails[i].lblTrxDateo,
                   lblAmto:res.IPSAccDetails[i].lblAmto,
                   lblAliaso:res.IPSAccDetails[i].lblAliaso,

                   lblTransCustNameRTP: res.IPSAccDetails[i].lblTransCustNameRTP,
                   lblReasonRTP: res.IPSAccDetails[i].lblReasonRTP,
                   lblRefRTP: res.IPSAccDetails[i].lblRefRTP,
                   lblStatusRTP: res.IPSAccDetails[i].lblStatusRTP,
                   endtoendid: res.IPSAccDetails[i].endtoendid,
                  }
                 );
        IPSHistorySeg.push({lblTrxRefi: {"text":""},
                            lblTransactionType:{"text":res.IPSAccDetails[i].transfertype},
                            transactionDatei :{"text":""},
                            transactionAmounti:{"text":""},
                            lblIBANi:{"text":""},
                            AliasNamei:{"text":""},
                            lblCreditAccountValue:{"text":""},

                            lblStatusRTPValue:{"text":""},
                            lblRefRTPValue:{"text":""},
                            lblReasonRTPValue:{"text":""},
                            lblTransCustNameRTPValue:{"text":""},
                            lblTransAmountRTPValue:{"text":""},
                            lblTransDateRTPValue:{"text":""},

                            lblFromAccount:{"text":customer},
                            transactionDateo :{"text":modifedDate},
                            transactionAmounto:{"text":res.IPSAccDetails[i].instructedamount + " " + res.IPSAccDetails[i].instructedcurrency},
                            lblIBANo:{"text":beneficiaryIBAN},
                            lblTrxRefo:{"text":res.IPSAccDetails[i].endtoendid},
                            btnReturnPayment: returnButton,
                            flxIncoming:incoming,
                            flxOutgoing: outgoing,
                            flxRTP:RTP,
                            lblNoData:nodata,
                            // btnCreditConfirm:creditedButton,
                            lblRefi:res.IPSAccDetails[i].lblRefi,
                            lblAliasi:res.IPSAccDetails[i].lblAliasi,
                            lblIBAnDesci:res.IPSAccDetails[i].lblIBAnDesci,
                            lblTrxDatei:res.IPSAccDetails[i].lblTrxDatei,
                            lblAmti:res.IPSAccDetails[i].lblAmti,
                            lblCredited:res.IPSAccDetails[i].lblCredited,

                            lblRefo:res.IPSAccDetails[i].lblRefo,
                            lblFAcco:res.IPSAccDetails[i].lblFAcco,
                            lblIBAnDescO:res.IPSAccDetails[i].lblIBAnDescO,
                            lblTrxDateo:res.IPSAccDetails[i].lblTrxDateo,
                            lblAmto:res.IPSAccDetails[i].lblAmto,
                            lblAliaso:res.IPSAccDetails[i].lblAliaso,

                            lblTransCustNameRTP: res.IPSAccDetails[i].lblTransCustNameRTP,
                            lblReasonRTP: res.IPSAccDetails[i].lblReasonRTP,
                            lblRefRTP: res.IPSAccDetails[i].lblRefRTP,
                            lblStatusRTP: res.IPSAccDetails[i].lblStatusRTP,
                            endtoendid: res.IPSAccDetails[i].endtoendid,
                            //                               lblRefi:kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
                            //     lblAliasi:kony.i18n.getLocalizedString("ii18.CLIQ.senderName"),
                            //     lblIBAnDesci:kony.i18n.getLocalizedString("ii18n.CLIQ.senderIBAN"),
                            //     lblTrxDatei:kony.i18n.getLocalizedString("i18n.CLIQ.TransferDate"),
                            //     lblAmti:kony.i18n.getLocalizedString("i18n.accounts.amount"),
                            //     lblCredited:kony.i18n.getLocalizedString("ii18n.CLIQ.creditedAccount"),

                            //     lblRefo:kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
                            //     lblFAcco:kony.i18n.getLocalizedString("i18n.CLIQ.FromAccount"),
                            //     lblIBAnDescO:kony.i18n.getLocalizedString("i18n.CliQ.ToBene"),
                            //     lblTrxDateo:kony.i18n.getLocalizedString("i18n.common.transactiondate"),
                            //     lblAmto:kony.i18n.getLocalizedString("i18n.accounts.amount"),
                            //     lblAliaso:kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
                            //                           endtoendid: res.IPSAccDetails[i].endtoendid,
                           }
                          );
        kony.print(" IPS_outward "+ JSON.stringify(temp) );
      }    else if(res.IPSAccDetails[i].transfertype === "IPS_RTP_INWARD"){

        kony.print(" IPS_RTP_INWARD endtoendid: "+res.IPSAccDetails[i].endtoendid );
        customer = res.IPSAccDetails[i].debtoriban.substring( res.IPSAccDetails[i].debtoriban.length-16, res.IPSAccDetails[i].debtoriban.length);//me
        beneficiaryAlias = res.IPSAccDetails[i].creditorname;
        beneficiaryIBAN =  res.IPSAccDetails[i].creditoriban;
        incoming = {"isVisible": false};
        outgoing = {"isVisible": false};
        RTP =  {"isVisible": true};
        returnButton ={"text":res.IPSAccDetails[i].btnReturnPayment,"isVisible": false};
        //creditedButton = {"text":res.IPSAccDetails[i].btnCreditConfirm,"isVisible": true};
        // temp.push(res.IPSAccDetails[i]);

        temp.push({lblTrxRefi: {"text":""},
                   lblTransactionType:{"text":res.IPSAccDetails[i].transfertype},
                   transactionDatei :{"text":""},
                   transactionAmounti:{"text":""},
                   lblIBANi:{"text":""},
                   AliasNamei:{"text":""},
                   lblCreditAccountValue:{"text":""},

                   lblStatusRTPValue:{"text":res.IPSAccDetails[i].transfertype},
                   lblRefRTPValue:{"text":res.IPSAccDetails[i].endtoendid},
                   lblReasonRTPValue:{"text":res.IPSAccDetails[i].transfertype},
                   lblTransCustNameRTPValue:{"text":res.IPSAccDetails[i].debtoriban},
                   lblTransAmountRTPValue:{"text":res.IPSAccDetails[i].instructedamount + " " + res.IPSAccDetails[i].instructedcurrency},
                   lblTransDateRTPValue:{"text":res.IPSAccDetails[i].valuedate},

                   lblFromAccount:{"text":""},
                   transactionDateo :{"text":""},
                   transactionAmounto:{"text":""},
                   // btnCreditConfirm: creditedButton,
                   lblIBANo:{"text":""},
                   lblTrxRefo:{"text":""},
                   btnReturnPayment: returnButton,
                   flxIncoming:incoming,
                   flxOutgoing: outgoing,
                   flxRTP:RTP,
                   lblNoData:{"text":""},

                   lblRefi:res.IPSAccDetails[i].lblRefi,
                   lblAliasi:res.IPSAccDetails[i].lblAliasi,
                   lblIBAnDesci:res.IPSAccDetails[i].lblIBAnDesci,
                   lblTrxDatei:res.IPSAccDetails[i].lblTrxDatei,
                   lblAmti:res.IPSAccDetails[i].lblAmti,
                   lblCredited:res.IPSAccDetails[i].lblCredited,

                   lblRefo:res.IPSAccDetails[i].lblRefo,
                   lblFAcco:res.IPSAccDetails[i].lblFAcco,
                   lblIBAnDescO:res.IPSAccDetails[i].lblIBAnDescO,
                   lblTrxDateo:res.IPSAccDetails[i].lblTrxDateo,
                   lblAmto:res.IPSAccDetails[i].lblAmto,
                   lblAliaso:res.IPSAccDetails[i].lblAliaso,

                   lblTransCustNameRTP: res.IPSAccDetails[i].lblTransCustNameRTP,
                   lblReasonRTP: res.IPSAccDetails[i].lblReasonRTP,
                   lblRefRTP: res.IPSAccDetails[i].lblRefRTP,
                   lblStatusRTP: res.IPSAccDetails[i].lblStatusRTP,
                   endtoendid: res.IPSAccDetails[i].endtoendid,
                  }
                 );
        IPSHistorySeg.push({lblTrxRefi: {"text":""},
                            lblTransactionType:{"text":res.IPSAccDetails[i].transfertype},
                            transactionDatei :{"text":""},
                            transactionAmounti:{"text":""},
                            lblIBANi:{"text":""},
                            AliasNamei:{"text":""},
                            lblCreditAccountValue:{"text":""},

                            lblStatusRTPValue:{"text":res.IPSAccDetails[i].transfertype},
                            lblRefRTPValue:{"text":res.IPSAccDetails[i].endtoendid},
                            lblReasonRTPValue:{"text":res.IPSAccDetails[i].transfertype},
                            lblTransCustNameRTPValue:{"text":res.IPSAccDetails[i].debtoriban},
                            lblTransAmountRTPValue:{"text":res.IPSAccDetails[i].instructedamount + " " + res.IPSAccDetails[i].instructedcurrency},
                            lblTransDateRTPValue:{"text":res.IPSAccDetails[i].valuedate},

                            lblFromAccount:{"text":""},
                            transactionDateo :{"text":""},
                            transactionAmounto:{"text":""},
                            lblIBANo:{"text":""},
                            lblTrxRefo:{"text":""},
                            btnReturnPayment: returnButton,
                            flxIncoming:incoming,
                            flxOutgoing: outgoing,
                            flxRTP:RTP,
                            lblNoData:{"text":""},
                            // btnCreditConfirm:creditedButton,
                            lblRefi:res.IPSAccDetails[i].lblRefi,
                            lblAliasi:res.IPSAccDetails[i].lblAliasi,
                            lblIBAnDesci:res.IPSAccDetails[i].lblIBAnDesci,
                            lblTrxDatei:res.IPSAccDetails[i].lblTrxDatei,
                            lblAmti:res.IPSAccDetails[i].lblAmti,
                            lblCredited:res.IPSAccDetails[i].lblCredited,

                            lblRefo:res.IPSAccDetails[i].lblRefo,
                            lblFAcco:res.IPSAccDetails[i].lblFAcco,
                            lblIBAnDescO:res.IPSAccDetails[i].lblIBAnDescO,
                            lblTrxDateo:res.IPSAccDetails[i].lblTrxDateo,
                            lblAmto:res.IPSAccDetails[i].lblAmto,
                            lblAliaso:res.IPSAccDetails[i].lblAliaso,

                            lblTransCustNameRTP: res.IPSAccDetails[i].lblTransCustNameRTP,
                            lblReasonRTP: res.IPSAccDetails[i].lblReasonRTP,
                            lblRefRTP: res.IPSAccDetails[i].lblRefRTP,
                            lblStatusRTP: res.IPSAccDetails[i].lblStatusRTP,
                            endtoendid: res.IPSAccDetails[i].endtoendid,
                            //                               lblRefi:kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
                            //     lblAliasi:kony.i18n.getLocalizedString("ii18.CLIQ.senderName"),
                            //     lblIBAnDesci:kony.i18n.getLocalizedString("ii18n.CLIQ.senderIBAN"),
                            //     lblTrxDatei:kony.i18n.getLocalizedString("i18n.CLIQ.TransferDate"),
                            //     lblAmti:kony.i18n.getLocalizedString("i18n.accounts.amount"),
                            //     lblCredited:kony.i18n.getLocalizedString("ii18n.CLIQ.creditedAccount"),

                            //     lblRefo:kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
                            //     lblFAcco:kony.i18n.getLocalizedString("i18n.CLIQ.FromAccount"),
                            //     lblIBAnDescO:kony.i18n.getLocalizedString("i18n.CliQ.ToBene"),
                            //     lblTrxDateo:kony.i18n.getLocalizedString("i18n.common.transactiondate"),
                            //     lblAmto:kony.i18n.getLocalizedString("i18n.accounts.amount"),
                            //     lblAliaso:kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
                            //                           endtoendid: res.IPSAccDetails[i].endtoendid,
                           }
                          );
        kony.print(" IPS_outward "+ JSON.stringify(temp) );

      }


    }

    kony.print("IPSHistory ::" + JSON.stringify(temp));
    frmIPSRequests.segIPSHistory.removeAll();

    //   tmmp.sort((a, b) => new Date(b.date) - new Date(a.date));
    frmIPSRequests.segIPSHistory.setData(temp);

    //  IPSPaymentHistory=temp;
    kony.print(" segment IPSHistory ::" + JSON.stringify(frmIPSRequests.segIPSHistory));
    kony.print("IPSHistory length ::" +JSON.stringify(IPSPaymentHistory));
    frmIPSRequests.btnAll.skin = "slButtonWhiteTab";
    frmIPSRequests.btnIncoming.skin = "slButtonWhiteTabDisabled";
    frmIPSRequests.btnOutgoing.skin = "slButtonWhiteTabDisabled";
    frmIPSRequests.btnRequestToPay.skin = "slButtonWhiteTabDisabled";

    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmIPSRequests.show();
    frmIPSManageBene.destroy();
  }catch(error){
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJPrGetIPSAcceptedDetail","Catch FAILURE","SERVICE",error);
  }
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

}
//Mai 10/1/2021
//Mai 13/1/ update
function date_diff_indays(transactionDate) {
  // var today = reformatDate(new Date()); 
  // (YYYY-MM-DD) 
  // var transDate = reformatDate(new Date(transactionDate));

  var today=new Date();
  var transDate = new Date(transactionDate);
  var diff=today.getTime()-transDate.getTime();
  //var mydate = new Date('2020-08-09');
  //   var numberOfDayes = (today - transDate) / (1000 * 3600 * 24) ;
  var numberOfDayes = diff / (1000 * 3600 * 24) ;

  if (numberOfDayes <=6){
    return true;
  }else{
    return false;
  }

}
function reformatDate(date){
  return date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
}
//Mai 12/1/2021
function GetSelectedReturnPayment(code) {
  var found = "";
  //   return IPSPaymentHistory.filter(
  //     function(IPSPaymentHistory){ return IPSPaymentHistory.endtoendid == code; }
  for (index = 0; index < IPSPaymentHistory.length; ++index) {
    var entry = IPSPaymentHistory[index];
    if (entry.endtoendid === code && entry.transfertype === "IPS_INWARD") {
      found = entry;
      break;
    }

  }
  return found;
}
function GetSelectedCreditConfirmPayment(code) {
  var found = "";
  //   return IPSPaymentHistory.filter(
  //     function(IPSPaymentHistory){ return IPSPaymentHistory.endtoendid == code; }
  for (index = 0; index < IPSPaymentHistory.length; ++index) {
    var entry = IPSPaymentHistory[index];
    if (entry.endtoendid === code && entry.transfertype === "IPS_OUTWARD") {
      found = entry;
      break;
    }

  }
  return found;
}

//Mai 11/1/2021
function  setupReturnPaymentReasonIPS(data){
  gblWorkflowType = null;

  frmIPSRequests.tbxReasonRP.text = data.ReasonReturn;
  frmIPSRequests.tbxReasonRP.setVisibility(true);
  frmIPSRequests.lblReasonStatRP.setVisibility(true);
  frmIPSRequests.lblReasonRP.setVisibility(false);
  var Reason=[{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPWRAM"),"Code":"WRAM"},{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPUPKN"),"Code":"UPKN"},{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPUPFS"),"Code":"UPFS"} ,{"ReasonReturn":geti18Value("i18n.CliQ.ReasonRPMREF"),"Code":"MREF"}];
  var R="";
  for(var y in  Reason)
  {
    if(Reason[y].ReasonReturn === data.ReasonReturn)
    {
      data.ReasonCode = Reason[y].Code;
    }

  }
  kony.print("Reason :: "+ JSON.stringify(data) );

  ReturnPaymentIPS[0].Reason=data.ReasonReturn;
  ReturnPaymentIPS[0].ReasonCode=data.ReasonCode;
  frmIPSRequests.show();
  gblTModule= "IPSReturn";
  validateLblNext();
  frmSelectDetailsBene.destroy();

}
//mai 12/1/2021
function RetunrPaymentIPS()
{
  kony.print("RetunrPaymentIPS call service :: "+ JSON.stringify(ReturnPaymentIPS) );
  //ReturnPaymentIPS
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var dumData = {"p_customer_id": custid,
                 "p_channel":"MOBILE",
                 "p_user_id":"BOJMOB",
                 "p_OrgEndToEndId": ReturnPaymentIPS[0].endtoendid,
                 "p_OrgValueDate": ReturnPaymentIPS[0].valuedate,
                 "p_SendingInstitutionCode":ReturnPaymentIPS[0].debtorinstitutioncode,
                 //                                                               "p_trans_amt":ReturnPaymentIPS[0].instructedamount,
                 "p_trans_amt":parseFloat(frmIPSRequests.txtAmountRP.text.trim()),
                 "p_trans_amt_ccy":ReturnPaymentIPS[0].instructedcurrency,
                 "p_src_iban":ReturnPaymentIPS[0].creditoriban,
                 "p_src_name":ReturnPaymentIPS[0].creditorname,
                 "p_src_address":ReturnPaymentIPS[0].creditoraddress,
                 "p_src_bic":ReturnPaymentIPS[0].creditorinstitutioncode,
                 "p_ben_iban":ReturnPaymentIPS[0].debtoriban,
                 "p_ben_name":ReturnPaymentIPS[0].debtorname,
                 "p_ben_address":ReturnPaymentIPS[0].debtoraddress,
                 "p_ben_bic":ReturnPaymentIPS[0].debtorinstitutioncode,
                 "p_ReturnReasonCode":ReturnPaymentIPS[0].ReasonCode,
                 "p_ReturnReasonDescription":ReturnPaymentIPS[0].Reason
                };
  kony.print("DUM_DATA::"+JSON.stringify(dumData));

  logObj[0] = ReturnPaymentIPS[0].creditoriban;//ebtor Account Number
  //   logObj[1] = ReturnPaymentIPS[0].debtorinstitutioncode;
  logObj[1] = "";
  logObj[2] = ReturnPaymentIPS[0].instructedcurrency;
  logObj[3] = ReturnPaymentIPS[0].debtoriban;
  //   logObj[4] = ReturnPaymentIPS[0].creditorinstitutioncode;
  logObj[4] = "";
  logObj[9] = "CLIQ_ref";
  logObj[10] = "";// ReturnPaymentIPS[0].endtoendid;
  logObj[12] = frmIPSRequests.txtAmountRP.text.trim();
  logObj[13] = "CLIQ_Return";
  logObj[15] = ReturnPaymentIPS[0].valuedate;
  logObj[19] = "";
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJIPSReturnTransfer");
  appMFConfiguration.invokeOperation("prIpsReturnTransfer", {},{"p_customer_id": custid,
                                                                "p_channel":"MOBILE",
                                                                "p_user_id":"BOJMOB",
                                                                "p_OrgEndToEndId": ReturnPaymentIPS[0].endtoendid,
                                                                "p_OrgValueDate": ReturnPaymentIPS[0].valuedate,
                                                                "p_SendingInstitutionCode":ReturnPaymentIPS[0].debtorinstitutioncode,
                                                                //"p_trans_amt":ReturnPaymentIPS[0].instructedamount,
                                                                "p_trans_amt":parseFloat(frmIPSRequests.txtAmountRP.text.trim()),
                                                                "p_trans_amt_ccy":ReturnPaymentIPS[0].instructedcurrency,
                                                                "p_src_iban":ReturnPaymentIPS[0].creditoriban,
                                                                "p_src_name":ReturnPaymentIPS[0].creditorname,
                                                                "p_src_address":ReturnPaymentIPS[0].creditoraddress,
                                                                "p_src_bic":ReturnPaymentIPS[0].creditorinstitutioncode,
                                                                "p_ben_iban":ReturnPaymentIPS[0].debtoriban,
                                                                "p_ben_name":ReturnPaymentIPS[0].debtorname,
                                                                "p_ben_address":ReturnPaymentIPS[0].debtoraddress,
                                                                "p_ben_bic":ReturnPaymentIPS[0].debtorinstitutioncode,
                                                                "p_ReturnReasonCode":ReturnPaymentIPS[0].ReasonCode,
                                                                "p_ReturnReasonDescription":ReturnPaymentIPS[0].Reason
                                                               },
                                     successReturn,
                                     function(error){
    kony.print("failed ::" + JSON.stringify(error));
    logObj[17] = "FAILURE";
    logObj[16] = "";
    var errmsg = error.errmsg.replace(/[',":?><}{\s\s+\n]/g, " ");
    if(isEmpty(errmsg)){
      logObj[18] = "errmsg is empty";
    }else if(errmsg.length >200){
      logObj[18] = errmsg.slice(200,400); 
    }else{
      logObj[18] = errmsg;
    }
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJIPSReturnTransfer","SERVICE FAILURE","SERVICE",error);
    loggerCall();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  });
}
function successReturn(response){
  try{
    kony.print("successReturn"+ JSON.stringify(response));
    ResetFormIPSData();//Omar ALnajjar
    logObj[16] = ReturnPaymentIPS[0].endtoendid; // response.return_code;
    if (response.return_code==="00000")
    {
      logObj[17] = "SUCCESS";
      logObj[18] = "SUCCESS Return Payment";
      kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"),
                                     geti18Value("i18.CLIQ.successReturn"),
                                     geti18Value("i18n.Bene.GotoAccDash"), 
                                     geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
                                     "CliqDashboardManage", "", "", " " + " " + "");
      // Ahmad 12-7-2020
    }
    else
    {
      logObj[19] = "Error Code " + response.return_code;
      var Message = "";
      if (response.return_code === "00013"){
        Message = getErrorMessage("00054");
      }else{
        Message = getErrorMessage(response.return_code);
      }
      logObj[17] = "FAILURE";
      if(isEmpty(Message)){
        logObj[18] = Message;
      }else if(Message.length >200){
        logObj[18] = Message.substring(0,200); 
      }else{
        logObj[18] = Message;
      }
      kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"),
                                     Message,
                                     geti18Value("i18n.Bene.GotoAccDash"), 
                                     geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
                                     "CliqDashboardManage", "", "", " " + " " + "");
    }
  }catch(error){
    logObj[17] = "FAILURE";
    logObj[16] = "";
    logObj[18]= "CLIQ Return Catch";

    //     if(isEmpty(error)){
    //       logObj[18] = error;
    //     }else if(error.length >200){
    //       logObj[18] = error.substring(0,200); 
    //     }else{
    //       logObj[18] = error;
    //     }
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.Generic.InternalServerError"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJIPSReturnTransfer","Catch FAILURE","SERVICE",error);
  }
  loggerCall();
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

}

//Omar ALnajjar R2P 23/02/2021
function IPSR2POptionButtonsHandler(button){

  switch (button){
    case frmIPSRequestToPay.btnR2PAlias:
      frmIPSRequestToPay.flxIPSR2PByMobile.setVisibility(false);
      frmIPSRequestToPay.flxIPSR2PByIBAN.setVisibility(false);
      frmIPSRequestToPay.flxIPSR2PByAlias.setVisibility(true);

      if (frmIPSRequestToPay.flxIPSR2PRequestReason.isVisible === true && frmIPSRequestToPay.btnR2PAlias.skin === "sknOrangeBGRNDBOJ"){
        //Hidden Reason in RTP
        frmIPSRequestToPay.flxIPSR2PRequestReason.setVisibility(false);
      }else{
        frmIPSRequestToPay.flxIPSR2PRequestReason.setVisibility(false);
      }

      //       if (button.skin === "sknOrangeBGRNDBOJ"){

      //           //frmIPSRequestToPay.flxIPSR2PRequestReason.setVisibility(false);
      //         }else{
      //            //frmIPSRequestToPay.flxIPSR2PRequestReason.setVisibility(true);
      //         }
      frmIPSRequestToPay.btnR2PAlias.skin = "sknOrangeBGRNDBOJ";
      frmIPSRequestToPay.btnR2PMob.skin = "slButtonBlueFocus";
      frmIPSRequestToPay.btnR2PIBAN.skin = "slButtonBlueFocus";

      // Reset Mob Details //

      frmIPSRequestToPay.txtAliasNameMob.text = "";
      frmIPSRequestToPay.txtAmountMob.text = "";
      frmIPSRequestToPay.lblIBANMobtxt.text = "";
      frmIPSRequestToPay.lblAddressMobtxt.text = "";
      frmIPSRequestToPay.lblBankMobAliastxt.text = "";
      frmIPSRequestToPay.txtIBANBeneName.text = "";
      frmIPSRequestToPay.flxBeneNameMob.setVisibility(false);
      frmIPSRequestToPay.flxAmountMob.setVisibility(false);
      frmIPSRequestToPay.flxIbanDetailsMob.setVisibility(false);
      frmIPSRequestToPay.flxAddressMob.setVisibility(false);
      frmIPSRequestToPay.flxBankMob.setVisibility(false);
      // Reset Iban Details //  
      frmIPSRequestToPay.txtAmountIBAN.text = "";
      frmIPSRequestToPay.txtIBANAlias.text = "";
      frmIPSRequestToPay.txtAddressAlias.text = "";
      frmIPSRequestToPay.txtBankAlias.text = "";
      frmIPSRequestToPay.tbxBankName.text = "";
      frmIPSRequestToPay.lblBankNameStat.setVisibility(false);
      frmIPSRequestToPay.tbxBankName.setVisibility(false);
      frmIPSRequestToPay.lblBankName.setVisibility(true);
      frmEPS.lblHintIBAN.setVisibility(false);

      frmIPSRequestToPay.btnRequest.setEnabled(false);


      break;

    case frmIPSRequestToPay.btnR2PMob:
      frmIPSRequestToPay.flxIPSR2PByAlias.setVisibility(false);
      frmIPSRequestToPay.flxIPSR2PByIBAN.setVisibility(false);
      frmIPSRequestToPay.flxIPSR2PByMobile.setVisibility(true);
      // if (button.skin !== "sknOrangeBGRNDBOJ"){
      //           frmIPSRequestToPay.flxIPSR2PRequestReason.setVisibility(false);
      //         }else{
      //            frmIPSRequestToPay.flxIPSR2PRequestReason.setVisibility(true);

      //         }

      if (frmIPSRequestToPay.flxIPSR2PRequestReason.isVisible === true && frmIPSRequestToPay.btnR2PMob.skin === "sknOrangeBGRNDBOJ"){
        //Hidden Reason in RTP

        frmIPSRequestToPay.flxIPSR2PRequestReason.setVisibility(false);
      }else{
        frmIPSRequestToPay.flxIPSR2PRequestReason.setVisibility(false);
      }
      frmIPSRequestToPay.btnR2PAlias.skin = "slButtonBlueFocus";
      frmIPSRequestToPay.btnR2PMob.skin = "sknOrangeBGRNDBOJ";
      frmIPSRequestToPay.btnR2PIBAN.skin = "slButtonBlueFocus";
      animateLabel("UP","lblAliasNameMob",frmIPSRequestToPay.txtAliasNameMob.text);
      if (isEmpty(frmIPSRequestToPay.txtAliasNameMob.text.trim())){
        frmIPSRequestToPay.txtAliasNameMob.text = "009627";
      }
      frmIPSRequestToPay.txtAmountIBAN.text = "";
      frmIPSRequestToPay.txtIBANAlias.text = "";
      frmIPSRequestToPay.txtAddressAlias.text = "";
      frmIPSRequestToPay.txtBankAlias.text = "";
      frmIPSRequestToPay.tbxBankName.text = "";
      frmIPSRequestToPay.txtIBANBeneName.text = "";
      frmIPSRequestToPay.lblBankNameStat.setVisibility(false);
      frmIPSRequestToPay.tbxBankName.setVisibility(false);
      frmIPSRequestToPay.lblBankName.setVisibility(true);
      frmEPS.lblHintIBAN.setVisibility(false);

      // Reset Iban Details // 
      // Reset Alias Details //
      frmIPSRequestToPay.txtAliasName.text = "";
      frmIPSRequestToPay.txtAmountAlias.text = "";
      frmIPSRequestToPay.lblAliasBanktxt.txt = "";
      frmIPSRequestToPay.flxBeneName.setVisibility(false);
      frmIPSRequestToPay.flxAmountAlias.setVisibility(false);
      frmIPSRequestToPay.flxAliasBank.setVisibility(false);


      frmIPSRequestToPay.btnRequest.setEnabled(false);
      break;
    case frmIPSRequestToPay.btnR2PIBAN:

      frmIPSRequestToPay.flxIPSR2PByAlias.setVisibility(false);
      frmIPSRequestToPay.flxIPSR2PByMobile.setVisibility(false);
      frmIPSRequestToPay.flxIPSR2PByIBAN.setVisibility(true);

      //Hidden Reason in RTP
      frmIPSRequestToPay.flxIPSR2PRequestReason.setVisibility(false);

      frmIPSRequestToPay.btnR2PAlias.skin = "slButtonBlueFocus";
      frmIPSRequestToPay.btnR2PMob.skin = "slButtonBlueFocus";
      frmIPSRequestToPay.btnR2PIBAN.skin = "sknOrangeBGRNDBOJ";
      // Reset Alias Details //
      frmIPSRequestToPay.txtAliasName.text = "";
      frmIPSRequestToPay.txtAmountAlias.text = "";
      frmIPSRequestToPay.lblAliasBanktxt.txt = "";
      frmIPSRequestToPay.flxBeneName.setVisibility(false);
      frmIPSRequestToPay.flxAmountAlias.setVisibility(false);
      frmIPSRequestToPay.flxAliasBank.setVisibility(false);
      // Reset Alias Details //
      // Reset Mob Details //
      frmIPSRequestToPay.txtAliasNameMob.text = "";
      frmIPSRequestToPay.txtAmountMob.text = "";
      frmIPSRequestToPay.lblIBANMobtxt.text = "";
      frmIPSRequestToPay.lblAddressMobtxt.text = "";
      frmIPSRequestToPay.lblBankMobAliastxt.text = "";
      frmIPSRequestToPay.flxBeneNameMob.setVisibility(false);
      frmIPSRequestToPay.flxAmountMob.setVisibility(false);
      frmIPSRequestToPay.flxIbanDetailsMob.setVisibility(false);
      frmIPSRequestToPay.flxAddressMob.setVisibility(false);
      frmIPSRequestToPay.flxBankMob.setVisibility(false);
      // Reset Mob Details //

      frmIPSRequestToPay.btnRequest.setEnabled(true);
      break;

  }
  //Reset Reasons 
  frmIPSRequestToPay.tbxReasonRP.text ="";
  frmIPSRequestToPay.tbxReasonRP.setVisibility(true);
  frmIPSRequestToPay.lblReasonStatRP.setVisibility(false);
  frmIPSRequestToPay.lblReasonRP.setVisibility(true);
  frmIPSRequestToPay.btnRequest.skin = "R2PbtnSKNDisable";

}

//Omar ALnajjar 22/03/2021 Create Alais
function IPSCRTOptionButtonsHandler(button){
  switch (button){
    case frmIPSCreateAlias.btnCRTAlias:
      frmIPSCreateAlias.txtAliasNameMob.text = "";
      frmIPSCreateAlias.btnCRTAlias.skin = "sknOrangeBGRNDBOJ";
      frmIPSCreateAlias.btnCRTMob.skin = "slButtonBlueFocus";
      frmIPSCreateAlias.flxIPSCRTByMobile.setVisibility(false);
      frmIPSCreateAlias.flxIPSCRTByAlias.setVisibility(true);
      break;
    case frmIPSCreateAlias.btnCRTMob:
      if (isEmpty(frmIPSCreateAlias.txtAliasNameMob.text.trim())){
        animateLabel("UP","lblAliasNameMob",frmIPSCreateAlias.txtAliasNameMob.text);
        frmIPSCreateAlias.txtAliasNameMob.text = "009627";
      }
      frmIPSCreateAlias.txtAliasName.text = "";
      frmIPSCreateAlias.btnCRTAlias.skin = "slButtonBlueFocus";
      frmIPSCreateAlias.btnCRTMob.skin = "sknOrangeBGRNDBOJ";

      frmIPSCreateAlias.flxIPSCRTByAlias.setVisibility(false);
      frmIPSCreateAlias.flxIPSCRTByMobile.setVisibility(true);


      break;
  }
  validateCreateAlias();
}
//Omar ALnajjar R2P 16/03/2021
function getPendingIPSRtpDetails(){
  resetPendingRTP();
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("prGetPendingIPSRtpDetails");
  appMFConfiguration.invokeOperation("BOJprGetPendingIpsRtpDetails", {},{"p_customer_id": custid,
                                                                         "p_channel":"MOBILE",
                                                                         "p_user_id":"BOJMOB"},
                                     successGetPendingIPSRtpDetails,
                                     function(error){
    exceptionLogCall("prGetPendingIPSRtpDetails","SERVICE FAILURE","SERVICE",error);
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");

    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

  });
}

//Omar ALnajjar R2P 16/03/2021
function successGetPendingIPSRtpDetails(res){
  try{
    kony.print("PENDING RTP" + JSON.stringify(res));
    if (res.RTP_Pen_Details !== null && res.RTP_Pen_Details !== undefined){
      var segData = [];
      frmIPSPendingRTP.segPendingRTP.setData([]);
      res.RTP_Pen_Details.sort(function (a, b) {
        return b.ValueDate.localeCompare(a.ValueDate);
      });
      frmIPSPendingRTP.segPendingRTP.widgetDataMap = { 
        lblTransDateRTPValue:"lblTransDateRTPValue",
        lblTransAmountRTPValue:"lblTransAmountRTPValue",
        lblTransCustNameRTPValue:"lblTransCustNameRTPValue",
        lblReasonRTPValue:"lblReasonRTPValue",
        lblRefRTPValue:"lblRefRTPValue",
        btnRTPAcsRej:"btnRTPAcsRej",
        lblTransCustNameRTP:"lblTransCustNameRTP",
        lblReasonRTP:"lblReasonRTP",
        lblRefRTP:"lblRefRTP",
        InstitutionCode:"InstitutionCode",

        DebtorIBAN:"DebtorIBAN",
        DebtorInstitutionCode:"DebtorInstitutionCode",
        InstructedCurrency:"InstructedCurrency",
        InstructedAmount:"InstructedAmount"

      };
      for (var i in res.RTP_Pen_Details) {
        kony.print("QWEQWEQWE" + JSON.stringify(i));

        res.RTP_Pen_Details[i].lblTransCustNameRTP = kony.i18n.getLocalizedString("i18.CLIQ.IBAN");
        res.RTP_Pen_Details[i].lblReasonRTP = kony.i18n.getLocalizedString("i18n.CLIQ.Reason");
        res.RTP_Pen_Details[i].lblRefRTP = kony.i18n.getLocalizedString("i18n.common.ReferenceNumber");
        res.RTP_Pen_Details[i].btnRTPAcsRej = kony.i18n.getLocalizedString("i18n.common.reverseback");

        segData.push({

          lblTransDateRTPValue:{"text": res.RTP_Pen_Details[i].ValueDate},      
          lblTransAmountRTPValue:{"text":res.RTP_Pen_Details[i].InstructedAmount + " "+ res.RTP_Pen_Details[i].InstructedCurrency},       
          lblTransCustNameRTPValue :{"text":res.RTP_Pen_Details[i].CreditorIBAN},        
          lblReasonRTPValue:{"text": res.RTP_Pen_Details[i].TransferType},        
          lblRefRTPValue:{"text":res.RTP_Pen_Details[i].EndToEndId},
          btnRTPAcsRej:{"text":res.RTP_Pen_Details[i].btnRTPAcsRej},
          InstitutionCode:{"text":res.RTP_Pen_Details[i].CreditorInstitutionCode},
          lblTransCustNameRTP:res.RTP_Pen_Details[i].lblTransCustNameRTP,
          lblReasonRTP:res.RTP_Pen_Details[i].lblReasonRTP,
          lblRefRTP:res.RTP_Pen_Details[i].lblRefRTP,

          DebtorIBAN:res.RTP_Pen_Details[i].DebtorIBAN,
          DebtorInstitutionCode:res.RTP_Pen_Details[i].DebtorInstitutionCode,
          InstructedCurrency:res.RTP_Pen_Details[i].InstructedCurrency,
          InstructedAmount:res.RTP_Pen_Details[i].InstructedAmount
        });
      }



      kony.print("PENDING segData" + JSON.stringify(segData));
      //Omar ALnajjar 25/03/2021 RTP
      if (segData.length === 0){
        frmIPSPendingRTP.lblNoPendingRequests.setVisibility(true);
        frmIPSPendingRTP.segPendingRTP.setVisibility(false);
      }else{
        frmIPSPendingRTP.lblNoPendingRequests.setVisibility(false);
        frmIPSPendingRTP.segPendingRTP.setVisibility(true);
        frmIPSPendingRTP.segPendingRTP.removeAll();
        frmIPSPendingRTP.segPendingRTP.setData(segData);
      }

      frmIPSPendingRTP.show();
      frmIPSManageBene.destroy();
    }
  }catch(error){
    exceptionLogCall("prGetPendingIPSRtpDetails","Catch FAILURE","SERVICE",error);
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");

  }
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

}
//OMar ALnajjar 19/03/2021 RTP
function setupAcceptRejectRTP(data){
  //Reset Reasons
  frmIPSPendingRTP.flxUnderlineReasonRP.skin = "sknFlxGreyLine";
  frmIPSPendingRTP.lblReasonCode.text = "";
  frmIPSPendingRTP.tbxReasonRP.text ="";
  frmIPSPendingRTP.tbxReasonRP.setVisibility(true);
  frmIPSPendingRTP.lblReasonStatRP.setVisibility(false);
  frmIPSPendingRTP.lblReasonRP.setVisibility(true);

  rtpSelectedData = data;
  frmIPSPendingRTP.lblRTPIBANAliasValue.text = data.lblTransCustNameRTPValue.text;
  frmIPSPendingRTP.lblRTPAmountValue.text = data.lblTransAmountRTPValue.text;
  frmIPSPendingRTP.lblRTPReasonValue.text = data.lblReasonRTPValue.text;
  frmIPSPendingRTP.flxPendingBody.setVisibility(false);
  frmIPSPendingRTP.flxRTPAcsRejec.setVisibility(true);
}
//OMar ALnajjar 19/03/2021 RTP
function resetPendingRTP(){
  frmIPSPendingRTP.flxPendingBody.setVisibility(true);
  frmIPSPendingRTP.flxRTPAcsRejec.setVisibility(false);

  frmIPSPendingRTP.lblReasonCode.text = "";
  frmIPSPendingRTP.tbxReasonRP.text ="";
  frmIPSPendingRTP.tbxReasonRP.setVisibility(true);
  frmIPSPendingRTP.lblReasonStatRP.setVisibility(false);
  frmIPSPendingRTP.lblReasonRP.setVisibility(true);
  frmIPSPendingRTP.flxUnderlineReasonRP.skin = "sknFlxGreyLine";

  //Omar ALnajjar 25/03/2021 RTP
  frmIPSPendingRTP.lblNoPendingRequests.setVisibility(false);
  frmIPSPendingRTP.segPendingRTP.setVisibility(true);
}
//Omar ALnajjar R2P 16/03/2021
function acceptRejectRtpRequest(code){
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("prIPSAcspRjctRtpRequest");

  kony.print("PARMSRejectRtpRequest" + JSON.stringify({"p_customer_id": custid,
                                                       "P_StatusCode":code,
                                                       "P_RejReasonCode":frmIPSPendingRTP.lblReasonCode.text.trim(),
                                                       "P_EndToEndId":rtpSelectedData.lblRefRTPValue.text,
                                                       "P_ValueDate":rtpSelectedData.lblTransDateRTPValue.text,
                                                       "P_SendingInstitutionCode":rtpSelectedData.InstitutionCode.text,
                                                       "p_channel":"MOBILE",
                                                       "p_user_id":"BOJMOB"}));

  logObj[3] = rtpSelectedData.lblTransCustNameRTPValue.text;
  //   logObj[1] = rtpSelectedData.InstitutionCode.text;
  logObj[1] = "";
  logObj[2] = rtpSelectedData.InstructedCurrency;

  logObj[0] = rtpSelectedData.DebtorIBAN;
  //   logObj[4] = rtpSelectedData.DebtorInstitutionCode;
  logObj[4] = "";

  logObj[9] = "CLIQ_R_ref";
  logObj[10] = "";//rtpSelectedData.lblRefRTPValue.text;
  logObj[12] = rtpSelectedData.InstructedAmount;
  if (code === "ACSP"){
    logObj[13] = "CLIQ_R_Approve";
  }else if (code === "RJCT"){
    logObj[13] = "CLIQ_R_Decline";
  }
  logObj[15] = rtpSelectedData.lblTransDateRTPValue.text;
  logObj[19] = "";
  appMFConfiguration.invokeOperation("prIpsAcspRjctRtpRequest", {},{"p_customer_id": custid,
                                                                    "P_StatusCode":code,
                                                                    "P_RejReasonCode":frmIPSPendingRTP.lblReasonCode.text.trim(),
                                                                    "P_EndToEndId":rtpSelectedData.lblRefRTPValue.text,
                                                                    "P_ValueDate":rtpSelectedData.lblTransDateRTPValue.text,
                                                                    "P_SendingInstitutionCode":rtpSelectedData.InstitutionCode.text,
                                                                    "p_channel":"MOBILE",
                                                                    "p_user_id":"BOJMOB"},
                                     successacceptRejectRtpRequest,
                                     function(error){
    kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"),
                                   geti18Value("i18n.common.somethingwentwrong"),
                                   geti18Value("i18n.Bene.GotoAccDash"), 
                                   geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
                                   "CliqDashboardManageAcsRej", "", "", " " );
    kony.print("successacceptRejectRtpRequestServ" + JSON.stringify(error));

    exceptionLogCall("prIPSAcspRjctRtpRequest","SERVICE FAILURE","SERVICE",error);
    logObj[17] = "FAILURE";
    logObj[16] = "";
    var errmsg = error.errmsg.replace(/[',":?><}{\s\s+\n]/g, " ");
    if(isEmpty(errmsg)){
      logObj[18] = "errmsg is empty";
    }else if(errmsg.length >200){
      logObj[18] = errmsg.slice(200,400); 
    }else{
      logObj[18] = errmsg;
    }
    loggerCall();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

  });
}
//Omar ALnajjar R2P 16/03/2021
function successacceptRejectRtpRequest(res){
  try{
      logObj[16] = rtpSelectedData.lblRefRTPValue.text;
    kony.print("successacceptRejectRtpRequest" + JSON.stringify(res));
    if (res.returnCode==="00000" || res.returnCode==="0"){
      logObj[17] = "SUCCESS";
      logObj[18] = "SUCCESS Accept or reject requests";
      kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"),
                                     geti18Value("i18n.CLIQ.approveRTP"),
                                     geti18Value("i18n.Bene.GotoAccDash"), 
                                     geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
                                     "CliqDashboardManageAcsRej", "", "", "");
    }else{
      logObj[19] = "Error Code " + res.returnCode;
      var Message = getErrorMessage(res.returnCode);
      logObj[17] = "FAILURE";
      if(isEmpty(Message)){
        logObj[18] = Message;
      }else if(Message.length >200){
        logObj[18] = Message.substring(0,200); 
      }else{
        logObj[18] = Message;
      }
      kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"),
                                     geti18Value("i18n.CLIQ.faliedAcsRej"),
                                     geti18Value("i18n.Bene.GotoAccDash"), 
                                     geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
                                     "CliqDashboardManageAcsRej", "", "", " " );
      //customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    }
  }catch(error){
    kony.print("successacceptCatch" + JSON.stringify(error));

    logObj[17] = "FAILURE";
    logObj[16] = "";
    logObj[18]= "CLIQ RTP accept reject";

    //     if(isEmpty(error)){
    //       logObj[18] = error;
    //     }else if(error.length >200){
    //       logObj[18] = error.substring(0,200); 
    //     }else{
    //       logObj[18] = error;
    //     }
    exceptionLogCall("prIPSAcspRjctRtpRequest","Catch FAILURE","SERVICE",error);
    kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"),
                                   geti18Value("i18n.common.somethingwentwrong"),
                                   geti18Value("i18n.Bene.GotoAccDash"), 
                                   geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
                                   "CliqDashboardManageAcsRej", "", "", " " );
  }
  loggerCall();
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();


}
//Omar ALnajjar 17/03/2021 RTP
function sendRequestPayment(){
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var alias_name = "";
  var alias_addr = "";
  var alias_amount = "";
  var d_iban = "";
  var bic_num = "";

  if(frmIPSRequestToPay.btnR2PAlias.skin == sknOrangeBGRNDBOJ){
    d_iban = frmIPSRequestToPay.lblIBANAliastxt.text;
    alias_name = frmIPSRequestToPay.lblBeneName.text;
    alias_addr = frmIPSRequestToPay.lbAliasAddresstxt.text; 
    alias_amount = frmIPSRequestToPay.txtAmountAlias.text;
    logObj[9] = "CLIQ_Alias";
    logObj[10] = frmIPSRequestToPay.txtAliasName.text;


  }

  if(frmIPSRequestToPay.btnR2PMob.skin == sknOrangeBGRNDBOJ){
    d_iban = frmIPSRequestToPay.lblIBANMobtxt.text;
    alias_name = frmIPSRequestToPay.lblBeneNameMob.text;
    alias_addr = frmIPSRequestToPay.lblAddressMobtxt.text;
    alias_amount = frmIPSRequestToPay.txtAmountMob.text;
    logObj[9] = "CLIQ_Mobile";
    logObj[10] = frmIPSRequestToPay.txtAliasNameMob.text;

  }
  if(frmIPSRequestToPay.btnR2PIBAN.skin == sknOrangeBGRNDBOJ){
    d_iban = frmIPSRequestToPay.txtIBANAlias.text;
    alias_name = frmIPSRequestToPay.txtIBANBeneName.text;
    alias_addr = frmIPSRequestToPay.txtAddressAlias.text;
    alias_amount = frmIPSRequestToPay.txtAmountIBAN.text; 
    bic_num = frmIPSRequestToPay.lblSwiftCode.text;
    logObj[9] = "CLIQ_IBAN";
    logObj[10] = frmIPSRequestToPay.txtIBANAlias.text;                          

  }

  //Change from account number 
  logObj[0] = d_iban;
  logObj[1] = frmIPSRequestToPay.lblAccountBranch.text;//Branch Number
  logObj[2] = "JOD";
  logObj[3] = frmIPSRequestToPay.lblFromAccConfText.text.trim();
  logObj[4] = "";

  logObj[12] = alias_amount;
  logObj[13] = "CLIQ_Request";
  logObj[15] = "";
  logObj[19] = "";
  kony.print("sendRequestPayment" + JSON.stringify({"p_customer_id": custid,
                                                    "p_ben_acc":frmIPSRequestToPay.lblFromAccConfText.text.trim(),
                                                    "p_ben_br":frmIPSRequestToPay.lblAccountBranch.text,

                                                    "p_src_iban":d_iban,
                                                    "p_src_name":alias_name,
                                                    "p_src_address":alias_addr,
                                                    "p_src_bic":bic_num,
                                                    //"p_purpose":frmIPSRequestToPay.lblReasonCode.text.trim(),
                                                    "p_purpose":"11110",
                                                    "p_trans_amt":alias_amount,
                                                    "p_trans_amt_ccy":"JOD",
                                                    "p_channel":"MOBILE",
                                                    "p_user_id":"BOJMOB"}));

  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprIpsCreateNewRtpRequest");
  appMFConfiguration.invokeOperation("BOJprIpsCreateNewRtpRequest", {},{"p_customer_id": custid,
                                                                        "p_ben_acc":frmIPSRequestToPay.lblAccountNumber1.text.trim(),
                                                                        "p_ben_br":frmIPSRequestToPay.lblAccountBranch.text.trim(),           
                                                                        "p_src_iban":d_iban,
                                                                        "p_src_name":alias_name,
                                                                        "p_src_address":alias_addr,
                                                                        "p_src_bic":bic_num,
                                                                        //                                                                          "p_purpose":frmIPSRequestToPay.lblReasonCode.text.trim(),
                                                                        "p_purpose":"11110",
                                                                        "p_trans_amt":alias_amount,
                                                                        "p_trans_amt_ccy":"JOD",
                                                                        "p_channel":"MOBILE",
                                                                        "p_user_id":"BOJMOB"},
                                     successSendRequestPayment,
                                     function(error){
    kony.print("failSendRequestPayment" + JSON.stringify(error));
    exceptionLogCall("BOJprIpsCreateNewRtpRequest","SERVICE FAILURE","SERVICE",error);

    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    logObj[17] = "FAILURE";
    logObj[16] = "";
    var errmsg = error.errmsg.replace(/[',":?><}{\s\s+\n]/g, " ");
    if(isEmpty(errmsg)){
      logObj[18] = "errmsg is empty";
    }else if(errmsg.length >200){
      logObj[18] = errmsg.slice(200,400); 
    }else{
      logObj[18] = errmsg;
    }
    loggerCall();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

  });
}
//Omar ALnajjar R2P 16/03/2021
function successSendRequestPayment(res){
  try{
    kony.print("successSendRequestPayment" + JSON.stringify(res));
    logObj[16] = res.returnCode;
    if (res.returnCode==="00000")
    {
      logObj[17] = "SUCCESS";
      logObj[18] = "SUCCESS CLIQ REQUEST";
      kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"),
                                     geti18Value("i18n.CLIQ.successRTP"),
                                     geti18Value("i18n.Bene.GotoAccDash"), 
                                     geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
                                     "CliqDashboardManageRTP", "", "", "");
    }
    else
    {
      logObj[17] = "FAILURE";
      var Message = getErrorMessage(res.errorCode);
      if(isEmpty(Message)){
        logObj[18] = Message;
      }else if(Message.length >200){
        logObj[18] = Message.substring(0,200); 
      }else{
        logObj[18] = Message;
      }
      kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"),
                                     geti18Value("i18n.CLIQ.failedRTP"),
                                     geti18Value("i18n.Bene.GotoAccDash"), 
                                     geti18nkey("i18n.common.AccountDashboard"),geti18Value("i18n.CLIQ.GoToCliqDashboard"),
                                     "CliqDashboardManageRTP", "", "", " " + " " + "");
    }
  }catch(error){
    logObj[16] = "";
    logObj[17] = "FAILURE";
    logObj[18] = "RTP Catch";
    //     if(isEmpty(error.errmsg)){
    //       logObj[18] = error.errmsg;
    //     }else if(error.errmsg.length >200){
    //       logObj[18] = error.errmsg.substring(0,200); 
    //     }else{
    //       logObj[18] = error.errmsg;
    //     }
    exceptionLogCall("BOJprIpsCreateNewRtpRequest","Catch FAILURE","SERVICE",error.errmsg);
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
  }
  loggerCall();
  resetR2PForm();
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}



//Omar ALnajjar R2P 23/02/2021
function getAliasR2PInfo(alias){
  if (isEmpty(alias.trim())){
    return;
  }
  var aliasType="";

  if (frmIPSRequestToPay.btnR2PAlias.skin==="sknOrangeBGRNDBOJ")
  {
    aliasType="ALIAS";
  }
  else if(frmIPSRequestToPay.btnR2PMob.skin==="sknOrangeBGRNDBOJ")
  {
    aliasType="MOBL";
  }

  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJAliasIPS");
  appMFConfiguration.invokeOperation("prGetIpsAliasDetails", {},{"p_customer_id": custid,
                                                                 "p_alias":alias,
                                                                 "p_alias_type": aliasType,
                                                                 "p_channel":"MOBILE",
                                                                 "p_user_id":"BOJMOB"},
                                     successR2PCalling,
                                     function(error){
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJAliasIPS","SERVICE FAILURE","SERVICE",error);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

  });
}
//Omar ALnajjar R2P 23/02/2021
function successR2PCalling(response){
  try{
    kony.print("successR2PCalling" + JSON.stringify(response));

    if (isEmpty(response.beniban) && isEmpty(response.benaddress) && 
        isEmpty(response.benbic) && isEmpty(response.benname))
    {
      customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.CLIQ.AliasError"), popupCommonAlertDimiss, "");
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }else
    {
      b_iban = response.beniban;
      bic_num = response.benbic;
      //       logObj[4] = response.benbic;
      if(frmIPSRequestToPay.btnR2PAlias.skin === "sknOrangeBGRNDBOJ"){
        frmIPSRequestToPay.flxAliasBank.setVisibility(true);
        frmIPSRequestToPay.flxAmountAlias.setVisibility(true);
        frmIPSRequestToPay.flxBeneName.setVisibility(true);
        frmIPSRequestToPay.lblIBANAliastxt.text = response.beniban;
        frmIPSRequestToPay.lbAliasAddresstxt.text = response.benaddress;
        frmIPSRequestToPay.lblAliasBanktxt.text = response.benbic;
        frmIPSRequestToPay.lblBeneName.text = response.benname;
      } 
      else if(frmIPSRequestToPay.btnR2PMob.skin === "sknOrangeBGRNDBOJ"){
        frmIPSRequestToPay.flxAmountMob.setVisibility(true);
        frmIPSRequestToPay.flxBankMob.setVisibility(true);  
        frmIPSRequestToPay.flxBeneNameMob.setVisibility(true);
        frmIPSRequestToPay.lblIBANMobtxt.text = response.beniban;
        frmIPSRequestToPay.lblAddressMobtxt.text = response.benaddress; 
        frmIPSRequestToPay.lblBankMobAliastxt.text = response.benbic;
        frmIPSRequestToPay.lblBeneNameMob.text = response.benname;
      }
      else if(frmIPSRequestToPay.btnR2PIBAN.skin === "sknOrangeBGRNDBOJ"){
        //         frmEPS.lblIBANtxt.text = response.beniban;
        //         frmEPS.lblAddressIBANtext.text = response.benaddress; 
        //         frmEPS.lbIBankIBANtext.text = response.benname;
        alert("test");
      }
      //Hidden Reason in RTP
      frmIPSRequestToPay.flxIPSR2PRequestReason.setVisibility(false);
      frmIPSRequestToPay.btnRequest.setEnabled(true);
    }
  }catch(error){
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("BOJAliasIPS","Catch FAILURE","SERVICE",error);
  }

  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

}
//Omar ALnajjar R2P 23/02/2021
function  setupRequestPaymentReasonIPS(data){
  gblWorkflowType = null;

  frmIPSRequestToPay.tbxReasonRP.text = data.ReasonRequest;
  frmIPSRequestToPay.tbxReasonRP.setVisibility(true);
  frmIPSRequestToPay.lblReasonStatRP.setVisibility(true);
  frmIPSRequestToPay.lblReasonRP.setVisibility(false);
  var Reason=[{"ReasonRequest":geti18Value("i18n.CLIQ.RentalExpenses"),"Code":"RERE"},{"ReasonRequest":geti18Value("i18n.CLIQ.FamilyAssistance"),"Code":"FAAE"},{"ReasonRequest":geti18Value("i18n.CLIQ.EducationalServices"),"Code":"EDS"} ,{"ReasonRequest":geti18Value("i18n.CLIQ.MaintenanceTechnical"),"Code":"MTOS"},{"ReasonRequest":geti18Value("i18n.CLIQ.Transfertofriend"),"Code":"TOFF"},{"ReasonRequest":geti18Value("i18n.CLIQ.transferaccounts"),"Code":"TBAA"},{"ReasonRequest":geti18Value("i18n.CLIQ.transportationfees"),"Code":"TFTF"},{"ReasonRequest":geti18Value("i18n.CLIQ.travelfees"),"Code":"TTFF"},{"ReasonRequest":geti18Value("i18n.CLIQ.InvoicePayment"),"Code":"IPAP"},{"ReasonRequest":geti18Value("i18n.CLIQ.Governmentservices"),"Code":"GSPS"},{"ReasonRequest":geti18Value("i18n.FilterTransaction.Others"),"Code":"OSOS"}];
  var R="";
  for(var y in  Reason)
  {
    if(Reason[y].ReasonRequest === data.ReasonRequest)
    {
      data.ReasonCode = Reason[y].Code;
    }

  }
  frmIPSRequestToPay.lblReasonCode.text = data.ReasonCode;
  kony.print("Reason :: "+ JSON.stringify(data) );
  frmIPSRequestToPay.show();
  gblTModule= "IPSR2P";
  frmSelectDetailsBene.destroy();
  validateRequestBtn();

}
//Omar ALnajjar R2P 23/02/2021
function setupAcsRejReasonRTP(data){
  gblWorkflowType = null;

  frmIPSPendingRTP.flxUnderlineReasonRP.skin = "sknFlxGreyLine";
  frmIPSPendingRTP.tbxReasonRP.text = data.RTPAcsRejReasons;
  frmIPSPendingRTP.tbxReasonRP.setVisibility(true);
  frmIPSPendingRTP.lblReasonStatRP.setVisibility(true);
  frmIPSPendingRTP.lblReasonRP.setVisibility(false);
  var Reason=[{"RTPAcsRejReasons":geti18Value("i18n.CLIQ.ReasonRTPWRAM"),"Code":"WRAM"},{"RTPAcsRejReasons":geti18Value("i18n.CLIQ.ReasonRTPUNRE"),"Code":"UNRE"},{"RTPAcsRejReasons":geti18Value("i18n.CLIQ.ReasonRTPAMPA"),"Code":"AMPA"}];

  var R="";
  for(var y in  Reason)
  {
    if(Reason[y].ReasonRequest === data.ReasonRequest)
    {
      data.ReasonCode = Reason[y].Code;
    }

  }
  frmIPSPendingRTP.lblReasonCode.text = data.ReasonCode;
  kony.print("Reason :: "+ JSON.stringify(data) );
  frmIPSPendingRTP.show();
  gblTModule= "IPSAcsRejR2P";
  frmSelectDetailsBene.destroy();
  validateRequestBtn();

}
//OMar ALnajjar 22/03/2021 RTP
function deleteALias(data){
  customAlertPopup(kony.i18n.getLocalizedString("i18n.CLIQ.deleteALiasConfirm"),
                   kony.i18n.getLocalizedString("i18n.CLIQ.deleteALias"),
                   function didTapDeleteAlias(){
    popupCommonAlertDimiss();
    onClickYesDeleteALias(data);
  }, popupCommonAlertDimiss,
                   kony.i18n.getLocalizedString("i18n.common.YES"),
                   kony.i18n.getLocalizedString("i18n.common.NO"));
}
//Omar ALnajjar 22/03/2021 Create ALias
function resetCreateAliasForm(){
  frmIPSCreateAlias.lblAccountName1.text = "";
  frmIPSCreateAlias.lblAccountNumber1.text = "";
  frmIPSCreateAlias.flxIcon1.setVisibility(false);
  frmIPSCreateAlias.lblSelectanAccount1.setVisibility(true);

  frmIPSCreateAlias.txtAliasNameMob.text = "";
  frmIPSCreateAlias.txtAliasName.text = "";

  frmIPSCreateAlias.btnCRTAlias.skin = "sknOrangeBGRNDBOJ";
  frmIPSCreateAlias.btnCRTMob.skin = "slButtonBlueFocus";

  frmIPSCreateAlias.btnNextCRT.skin = "sknLblNextDisabled";

  frmIPSCreateAlias.flxIPSCRTByAlias.setVisibility(true);
  frmIPSCreateAlias.flxIPSCRTByMobile.setVisibility(false);


  frmIPSCreateAlias.flxConfirmCRT.setVisibility(false);
  frmIPSCreateAlias.flxCRTBody.setVisibility(true);


}
//Omar ALnajjar R2P 23/02/2021
function resetR2PForm(){
  frmIPSRequestToPay.lblAccountName1.text = "";
  frmIPSRequestToPay.lblAccountNumber1.text = "";
  frmIPSRequestToPay.flxIcon1.setVisibility(false);
  frmIPSRequestToPay.lblSelectanAccount1.setVisibility(true);
  // Reset Mob Details //
  frmIPSRequestToPay.txtAliasNameMob.text = "";
  frmIPSRequestToPay.txtAmountMob.text = "";
  frmIPSRequestToPay.lblIBANMobtxt.text = "";
  frmIPSRequestToPay.lblAddressMobtxt.text = "";
  frmIPSRequestToPay.lblBankMobAliastxt.text = "";
  frmIPSRequestToPay.flxBeneNameMob.setVisibility(false);
  frmIPSRequestToPay.flxAmountMob.setVisibility(false);
  frmIPSRequestToPay.flxIbanDetailsMob.setVisibility(false);
  frmIPSRequestToPay.flxAddressMob.setVisibility(false);
  frmIPSRequestToPay.flxBankMob.setVisibility(false);

  //Reset Reasons and Requst Button
  frmIPSRequestToPay.btnRequest.setEnabled(false);
  frmIPSRequestToPay.tbxReasonRP.text ="";
  frmIPSRequestToPay.lblReasonCode.text = "";
  frmIPSRequestToPay.tbxReasonRP.setVisibility(true);
  frmIPSRequestToPay.lblReasonStatRP.setVisibility(false);
  frmIPSRequestToPay.lblReasonRP.setVisibility(true);
  frmIPSRequestToPay.flxIPSR2PRequestReason.setVisibility(false);
  // Reset Alias Details //
  frmIPSRequestToPay.txtAliasName.text = "";
  frmIPSRequestToPay.txtAmountAlias.text = "";
  frmIPSRequestToPay.lblAliasBanktxt.txt = "";
  frmIPSRequestToPay.flxBeneName.setVisibility(false);
  frmIPSRequestToPay.flxAmountAlias.setVisibility(false);
  frmIPSRequestToPay.flxAliasBank.setVisibility(false);

  // Reset IBAN  Details //
  frmIPSRequestToPay.txtAmountIBAN.text = "";
  frmIPSRequestToPay.txtIBANAlias.text = "";
  frmIPSRequestToPay.txtAddressAlias.text = "";
  frmIPSRequestToPay.txtBankAlias.text = "";
  frmIPSRequestToPay.tbxBankName.text = "";
  frmIPSRequestToPay.txtIBANBeneName.text = "";
  frmIPSRequestToPay.lblBankNameStat.setVisibility(false);
  frmIPSRequestToPay.tbxBankName.setVisibility(false);
  frmIPSRequestToPay.lblBankName.setVisibility(true);

  //Default Flow
  frmIPSRequestToPay.flxIPSR2PByAlias.setVisibility(true);
  frmIPSRequestToPay.flxIPSR2PByMobile.setVisibility(false);
  frmIPSRequestToPay.flxIPSR2PByIBAN.setVisibility(false);
  frmIPSRequestToPay.btnRequest.skin = "R2PbtnSKNDisable";
  frmIPSRequestToPay.flxR2PBody.setVisibility(true);
  frmIPSRequestToPay.flxConfirmRTP.setVisibility(false);
  frmIPSRequestToPay.flxIPSHeader.setVisibility(true);

  frmIPSRequestToPay.btnR2PAlias.skin = "sknOrangeBGRNDBOJ";
  frmIPSRequestToPay.btnR2PMob.skin = "slButtonBlueFocus";
  frmIPSRequestToPay.btnR2PIBAN.skin = "slButtonBlueFocus";

}
//Omar ALnajjar 23/02/2021 R2P
function  setupBanknameIPSR2P(data){
  gblWorkflowType = null;

  kony.print("data banks ::"+JSON.stringify(data));

  frmIPSRequestToPay.tbxBankName.text = data.BankName;
  frmIPSRequestToPay.lblSwiftCode.text=data.SwiftCode;
  frmIPSRequestToPay.lblBankNameStat.setVisibility(true);
  frmIPSRequestToPay.tbxBankName.setVisibility(true);
  frmIPSRequestToPay.lblBankName.setVisibility(false);
  frmIPSRequestToPay.show();
  frmSelectDetailsBene.destroy();
  validateRequestBtn();
}
//Omar Alnajjar 25/03/2021 R2P
function validateRequestBtn(){
  if (frmIPSRequestToPay.btnR2PAlias.skin === "sknOrangeBGRNDBOJ"){
    if (!isEmpty(frmIPSRequestToPay.txtAliasName.text) && 
        !isEmpty(frmIPSRequestToPay.txtAmountAlias.text) &&
        !isEmpty(frmIPSRequestToPay.lblAliasBanktxt.text) &&
        //Hidden Reason in RTP
        //             !isEmpty(frmIPSRequestToPay.tbxReasonRP.text ) && 
        parseFloat(frmIPSRequestToPay.txtAmountAlias.text.trim()) > 0 &&
        !isEmpty(frmIPSRequestToPay.lblAccountNumber1.text)){

      //frmIPSRequestToPay.btnRequest.setEnabled(true);
      frmIPSRequestToPay.btnRequest.skin = "R2PbtnSKN";

    }else{
      frmIPSRequestToPay.btnRequest.skin = "R2PbtnSKNDisable";
      //frmIPSRequestToPay.btnRequest.setEnabled(false);
    }

  }else if (frmIPSRequestToPay.btnR2PMob.skin === "sknOrangeBGRNDBOJ"){
    if (!isEmpty(frmIPSRequestToPay.txtAliasNameMob.text) && 
        !isEmpty(frmIPSRequestToPay.txtAmountMob.text) &&
        !isEmpty(frmIPSRequestToPay.lblBeneNameMob.text) &&
        //Hidden Reason in RTP
        //!isEmpty(frmIPSRequestToPay.tbxReasonRP.text) &&
        parseFloat(frmIPSRequestToPay.txtAmountMob.text.trim()) > 0 &&
        !isEmpty(frmIPSRequestToPay.lblBankMobAliastxt.text)&&
        !isEmpty(frmIPSRequestToPay.lblAccountNumber1.text) ){
      frmIPSRequestToPay.btnRequest.skin = "R2PbtnSKN";
      //frmIPSRequestToPay.btnRequest.setEnabled(true);
    }else{
      frmIPSRequestToPay.btnRequest.skin = "R2PbtnSKNDisable";
      //frmIPSRequestToPay.btnRequest.setEnabled(false);
    }     
  }else if (frmIPSRequestToPay.btnR2PIBAN.skin === "sknOrangeBGRNDBOJ"){
    if (!isEmpty(frmIPSRequestToPay.txtAmountIBAN.text) && 
        !isEmpty(frmIPSRequestToPay.txtIBANAlias.text) &&
        !isEmpty(frmIPSRequestToPay.txtAddressAlias.text) &&
        !isEmpty(frmIPSRequestToPay.txtIBANBeneName.text) &&

        parseFloat(frmIPSRequestToPay.txtAmountIBAN.text.trim()) > 0 &&
        //Hidden Reason in RTP
        //!isEmpty(frmIPSRequestToPay.tbxReasonRP.text) &&
        !isEmpty(frmIPSRequestToPay.tbxBankName.text) &&
        !isEmpty(frmIPSRequestToPay.lblAccountNumber1.text)){
      frmIPSRequestToPay.btnRequest.skin = "R2PbtnSKN";

      //frmIPSRequestToPay.btnRequest.setEnabled(true);
    }else{
      frmIPSRequestToPay.btnRequest.skin = "R2PbtnSKNDisable";

      //frmIPSRequestToPay.btnRequest.setEnabled(false);
    }   
  }
}
//Omar ALnajjar 24/02/2021 R2P
function cliqR2PChange(){
  if(frmIPSRequestToPay.txtAmountAlias.text !== null && frmIPSRequestToPay.txtAmountAlias.text !== ""){
    var amount = frmIPSRequestToPay.txtAmountAlias.text;
    if(amount.indexOf(".") > -1){
      amount = amount.substring(0,amount.indexOf(".")+4);
    }
    frmIPSRequestToPay.txtAmountAlias.text = amount;
    validateRequestBtn();
  }
}
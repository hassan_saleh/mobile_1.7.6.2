const timer = 1;
var tempTime = 5;

function serv_PIN_REMINDER()
{
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var requestData = {
    "custId": custid,
    "cardCode": frmManageCardsKA.lblInfoNewEPN1.text,
    "cardNum": CARDLIST_DETAILS.cardNumber,
    "p_channel": "MOBILE",
    "p_user_id": "BOJMOB",
  };
  kony.print("Cust ID: " + custid +  " Card Code: " + frmManageCardsKA.lblInfoNewEPN1.text + " Card Num: " + CARDLIST_DETAILS.cardNumber);
  headers = {};
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprCcPinReminder");
  appMFConfiguration.invokeOperation("prCcPinReminder", {},requestData,
                                     serv_PIN_REMINDER_SUCCESS,
                                     serv_PIN_REMINDER_FAILURE);
}

function serv_PIN_REMINDER_SUCCESS(res)
{
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  if (res.errorCode === "00000")
  {
    kony.print("Pin Reminder Success");
    frmPinManagement.lblFirstCharacter.text = res.cardPin.substring(0,1);
    frmPinManagement.lblSecondCharacter.text = res.cardPin.substring(1,2);
    frmPinManagement.lblThirdCharacter.text = res.cardPin.substring(2,3);
    frmPinManagement.lblFourthCharacter.text = res.cardPin.substring(3,4);
    frmPinManagement.lblPinManagementTitle.text = geti18Value("i18n.pinReminder.title");
    frmPinManagement.lblPinManagementTitle.setVisibility(true);
    frmPinManagement.lblTimer.text = "0:0" + JSON.stringify(tempTime);
    frmPinManagement.flxPinManagementMainContainer.setVisibility(false);
    frmPinManagement.flxPinReminderScreen.setVisibility(true);
    frmPinManagement.skin = "sknmainGradient";
    frmPinManagement.show();
    //startTimer();
    frmPinManagement.flxBottomHalf.setVisibility(true);
    kony.timer.schedule("pinTimer", startTimer, timer, true);
  }else{
    var Message = getErrorMessage(res.errorCode);
    customAlertPopup(geti18nkey("i18n.Bene.Error"), Message, popupCommonAlertDimiss, "");//Omar ALnajjar Pin reminder
  }
}

function serv_PIN_REMINDER_FAILURE(err)
{
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  customAlertPopup(geti18nkey("i18n.Bene.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
  kony.print(err);
}

function startTimer(){
  if(tempTime == 5){
    frmPinManagement.lblTimer.text = "0:0" + JSON.stringify(tempTime);
    frmPinManagement.lblFirstCharacter.setVisibility(false);
    frmPinManagement.lblSecondCharacter.setVisibility(false);
    frmPinManagement.lblThirdCharacter.setVisibility(false);
    frmPinManagement.lblFourthCharacter.setVisibility(false);
  }else if(tempTime === 4){
    frmPinManagement.lblTimer.text = "0:0" + JSON.stringify(tempTime);
    frmPinManagement.lblFirstCharacter.setVisibility(true);
    //frmPinManagement.lblSecondCharacter.setVisibility(false);
    //frmPinManagement.lblThirdCharacter.setVisibility(false);
    //frmPinManagement.lblFourthCharacter.setVisibility(false);
  }else if(tempTime === 3){
    frmPinManagement.lblTimer.text = "0:0" + JSON.stringify(tempTime);
    //frmPinManagement.lblFirstCharacter.setVisibility(false);
    frmPinManagement.lblSecondCharacter.setVisibility(true);
    //frmPinManagement.lblThirdCharacter.setVisibility(false);
    //frmPinManagement.lblFourthCharacter.setVisibility(false);
  }else if(tempTime === 2){
    frmPinManagement.lblTimer.text = "0:0" + JSON.stringify(tempTime);
    //frmPinManagement.lblFirstCharacter.setVisibility(false);
    //frmPinManagement.lblSecondCharacter.setVisibility(false);
    frmPinManagement.lblThirdCharacter.setVisibility(true);
    //frmPinManagement.lblFourthCharacter.setVisibility(false);
  }else if(tempTime === 1){
    frmPinManagement.lblTimer.text = "0:0" + JSON.stringify(tempTime);
    //frmPinManagement.lblFirstCharacter.setVisibility(false);
    //frmPinManagement.lblSecondCharacter.setVisibility(false);
    //frmPinManagement.lblThirdCharacter.setVisibility(false);
    frmPinManagement.lblFourthCharacter.setVisibility(true);
  }else if(tempTime === 0){
    frmPinManagement.lblTimer.text = "0:0" + JSON.stringify(tempTime);
    //frmPinManagement.lblFirstCharacter.setVisibility(false);
    //frmPinManagement.lblSecondCharacter.setVisibility(false);
    //frmPinManagement.lblThirdCharacter.setVisibility(false);
    //frmPinManagement.lblFourthCharacter.setVisibility(false);
    goToCongratulationScreen();
  }
  tempTime--;
}

function goToCongratulationScreen(){

  if(gblTModule === "PinReminder"){
    kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.Bene.Success"), geti18Value("i18n.pinReminder.successMsg"),
                                   geti18Value("i18n.cards.gotoCardsDashboard"), "Cards","","");
    kony.timer.cancel("pinTimer");
    tempTime = 5;
  }else if(gblTModule === "PinUnblock"){
    kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.Bene.Success"), geti18Value("i18n.pinUnblock.successMsg"),
                                   geti18Value("i18n.cards.gotoCardsDashboard"), "Cards","","");
  }
  frmPinManagement.skin = "CopyslFormCommon0eb853f78d72b43";
  frmPinManagement.lblPinManagementTitle.setVisibility(false);
  frmPinManagement.destroy();
}

function serv_PIN_UNBLOCK()
{
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var requestData = {
    "custId": custid,
    "p_card_no": CARDLIST_DETAILS.cardNumber,
    "p_channel": "MOBILE",
    "p_user_id": "BOJMOB",
  };
  kony.print("Cust ID: " + custid + " Card Num: " + CARDLIST_DETAILS.cardNumber);
  headers = {};
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprCcUnblockPin");
  appMFConfiguration.invokeOperation("prCcUnblockPin", {},requestData,
                                     serv_PIN_UNBLOCK_SUCCESS,
                                     serv_PIN_UNBLOCK_FAILURE);
}

function serv_PIN_UNBLOCK_SUCCESS(res)
{
  kony.print("Result: " + res.result);
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  if (res.result === "00000")
  {
    if(gblTModule === "PinUnblock"){
      goToCongratulationScreen();
    }
  }else{
    var Message = getErrorMessage(res.errorCode);
    if(gblTModule === "PinUnblock"){
      kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), "",
                                     geti18Value("i18n.cards.gotoCardsDashboard"), "Cards","","");
    }
  }
}

function serv_PIN_UNBLOCK_FAILURE(err)
{
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  customAlertPopup(geti18nkey("i18n.Bene.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
  kony.print(err);
}
//Nart PIN Management <<
function navigate_PIN_MANAGEMENT_SCREEN(){
  frmPinManagement.show();
}
//>>

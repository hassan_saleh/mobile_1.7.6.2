/*
 * Controller Extension class for frmEnrolluserLandingKA
 * Developer can edit the existing methods or can add new methods if required
 *
 */
kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};

/**
 * Creates a new Form Controller Extension.
 * @class frmEnrolluserLandingKAControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmEnrolluserLandingKAControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmEnrolluserLandingKAControllerExtension#
     */
    fetchData: function() {
        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
            kony.sdk.mvvm.log.info("success fetching data ", response);
            scopeObj.getController().processData(response);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmEnrolluserLandingKAControllerExtension#
     * @returns {Object} - processed data
     */
    processData: function(data) {
        try {
            var scopeObj = this;
            var processedData = this.$class.$superp.processData.call(this, data);
            this.getController().bindData(processedData);
            return processedData;
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        };
    },
    /** 
     * This method binds the processed data to the form. Developer can edit.
     * Default implementation binds the data to widgets in the form.
     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmEnrolluserLandingKAControllerExtension#
     */
    bindData: function(data) {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.clear();
            this.$class.$superp.bindData.call(this, data);
            this.getController().getFormModel().formatUI();
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            this.getController().showForm();
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

    },
    /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmEnrolluserLandingKAControllerExtension#
     */
    saveData: function() {
        try {
            var scopeObj = this;
            kony.print("Inside Save data of enroller user -->" + JSON.stringify(scopeObj));
            this.$class.$superp.saveData.call(this, success, error);

        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            exceptionLogCall("frmEnrolluserLandingKA","UI ERROR","UI",err);
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.print("in catch of Save data in enrollUser :: " +exception.toString());
        }

        function success(res) {
            if (!isEmpty(res)) {
                kony.print("In success of save of FrmenrolluserLanding:: " + JSON.stringify(res));

                if (gblFromModule == "RegisterUser") {
//                   alert(res);
//                   alert(res.statusType);
                    if (res.statusType == "S") {
                        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.registration.congratulations"),  geti18Value("i18n.common.mobBank"),
                            geti18Value("i18n.registration.GoToLoginScreen"), "Login");
                        kony.print("User Registered Successfully");

                        frmCongratulations.show();
                        gblFromModule = "";
                    } else {
                        kony.print("Invalid Entry");
                        kony.print("####else execution");
                        frmEnrolluserLandingKA.tbxPasswordKA.text = "";
                        frmEnrolluserLandingKA.tbxConfrmPwdKA.text = "";
                        kony.print("Invalid Entry" + res.statusMessage);
                      if(!isEmpty(res.statusMessage))
                        {
                          frmEnrolluserLandingKA.lblInvalidCredentialsKA.top=2+"%";
                           frmEnrolluserLandingKA.lblInvalidCredentialsKA.setVisibility(true);
                        frmEnrolluserLandingKA.lblInvalidCredentialsKA.text = res.statusMessage;
                        }
                      else
                      customAlertPopup(geti18Value("i18n.Bene.Failed"), kony.i18n.getLocalizedString("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
                      
                        
                        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextDisabled"; 
                        // frmEnrolluserLandingKA.flxNext.setVisibility(false);

                    }
                    kony.application.dismissLoadingScreen();
                } else if (gblFromModule == "ChangeUsername") {
                    kony.print("Username res:: " + JSON.stringify(res));

                    if (res.statusType == "S") {
                        if (frmEnrolluserLandingKA.UserFlag.text == "C") {
                            callRequestOTP();
                        } else {
                            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.registration.congratulations"), geti18Value("i18n.Change.Uname"),
                                geti18Value("i18n.registration.GoToLoginScreen"), "Login");
                            kony.print("Username changed successfully");

                            frmCongratulations.show();
                            gblFromModule = "";
                        }
                    } else {
                        kony.print("Invalid Entry");
                        frmEnrolluserLandingKA.tbxPasswordKA.text = "";
                        frmEnrolluserLandingKA.tbxConfrmPwdKA.text = "";
                        frmEnrolluserLandingKA.tbxUsernameKA.text = "";
                        kony.print("Invalid Entry - Change username" + res.statusMessage);
                        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextDisabled";
                      	frmEnrolluserLandingKA.lblInvalidCredentialsKA.top=2+"%";
                        frmEnrolluserLandingKA.lblInvalidCredentialsKA.setVisibility(true);
                 if(!isEmpty(res.statusMessage))
                        frmEnrolluserLandingKA.lblInvalidCredentialsKA.text = res.statusMessage;
                      else
                      customAlertPopup(geti18Value("i18n.Bene.Failed"), kony.i18n.getLocalizedString("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
                        frmEnrolluserLandingKA.lblInvalidCredentialsKA.setVisibility(true);

                    }
                    kony.application.dismissLoadingScreen();
                } else if (gblFromModule == "ChangePassword") {
                    kony.print("Invalid Entry ... ChangePassword@@@" + JSON.stringify(res));
                    if (res.statusType == "S") {
                        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.registration.congratulations"), geti18Value("i18n.Change.Pw"),
                            geti18Value("i18n.registration.GoToLoginScreen"), "Login");
                        kony.print("Password changed successfully");

                        frmCongratulations.show();
                        gblFromModule = "";
                    }else if (res.statusType == "F"){
                       kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed") ,geti18Value("i18n.common.samePassFailed"),
                            geti18Value("i18.common.goToChangePass"), "GoToChangePass");
                        frmCongratulations.show();
                        gblFromModule = "";
                    }
                  else {
                        kony.print("Invalid Entry");
                        frmEnrolluserLandingKA.tbxPasswordKA.text = "";
                        frmEnrolluserLandingKA.tbxConfrmPwdKA.text = "";
                        frmEnrolluserLandingKA.tbxUsernameKA.text = "";
                        kony.print("Invalid Entry - Change username" + res.statusMessage);
                      frmEnrolluserLandingKA.lblInvalidCredentialsKA.top=2+"%";
                        frmEnrolluserLandingKA.lblInvalidCredentialsKA.setVisibility(true);
                       if(!isEmpty(res.statusMessage))
                        frmEnrolluserLandingKA.lblInvalidCredentialsKA.text = res.statusMessage;
                      
                      else
                      customAlertPopup(geti18Value("i18n.Bene.Failed"), kony.i18n.getLocalizedString("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
                        frmEnrolluserLandingKA.lblInvalidCredentialsKA.setVisibility(true);

                    }
                    kony.application.dismissLoadingScreen();
                } else {
                    if (gblReqField == "Username") {
                        kony.print("Here ::");
                        if (res.statusType == "S" || (res.cust_id!==null && res.cust_id!=="") ) {
                            gblReqField = "";
                            gblUsername = frmEnrolluserLandingKA.tbxUsernameKA.text;

                            frmRegisterUser.show();

                        } else {
                            kony.print("Invalid Entry" + res.statusMessage);
                            frmEnrolluserLandingKA.tbxPasswordKA.text = "";
                            frmEnrolluserLandingKA.tbxConfrmPwdKA.text = "";
                          frmEnrolluserLandingKA.lblInvalidCredentialsKA.top=2+"%";
                          frmEnrolluserLandingKA.lblInvalidCredentialsKA.setVisibility(true);
                             if(!isEmpty(res.statusMessage))
                        frmEnrolluserLandingKA.lblInvalidCredentialsKA.text = res.statusMessage;
                      else
                      customAlertPopup(geti18Value("i18n.Bene.Failed"), kony.i18n.getLocalizedString("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
                            frmEnrolluserLandingKA.lblInvalidCredentialsKA.setVisibility(true);

                        }
                        kony.application.dismissLoadingScreen();
                    } else {
                        if (res.statusType == "S") {
                            gblReqField = "";
                            kony.print("Password Changed Successfully");
                          if(gblPayAppFlow && !isEmpty(gblLaunchModeOBJ.appFlow)){
                            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.FP.success"),
                                geti18Value("i18n.registration.GoToLoginScreen"), "Login"); //, "Call Contact Center");
                           
                          }else{
                            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.FP.success"),
                                geti18Value("i18n.registration.GoToLoginScreen"), "Login"); //, "Call Contact Center");
                          }
                            frmCongratulations.show();
                            gblFromModule = "";
                        } else {
                            kony.print("Invalid Entry" + res.statusMessage);
                            frmEnrolluserLandingKA.tbxPasswordKA.text = "";
                            frmEnrolluserLandingKA.tbxConfrmPwdKA.text = "";
                            animateLabel("DOWN", "lblPassword", frmEnrolluserLandingKA.tbxPasswordKA.text);
                            animateLabel("DOWN", "lblConfirmPassword", frmEnrolluserLandingKA.tbxConfrmPwdKA.text);
                          frmEnrolluserLandingKA.lblInvalidCredentialsKA.top=2+"%";
                          frmEnrolluserLandingKA.lblInvalidCredentialsKA.setVisibility(true);
                             if(!isEmpty(res.statusMessage))
                        frmEnrolluserLandingKA.lblInvalidCredentialsKA.text = res.statusMessage;
                      else
                      customAlertPopup(geti18Value("i18n.Bene.Failed"), kony.i18n.getLocalizedString("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
                          frmEnrolluserLandingKA.lblInvalidPass.top=0+"dp";
                            frmEnrolluserLandingKA.lblInvalidPass.setVisibility(true);

                        }
                        kony.application.dismissLoadingScreen();
                    }
                    kony.application.dismissLoadingScreen();
                }


            } else {
                frmEnrolluserLandingKA.tbxPasswordKA.text = "";
                frmEnrolluserLandingKA.tbxConfrmPwdKA.text = "";
                customAlertPopup(geti18Value("i18n.Bene.Failed"),
                    geti18Value("i18n.common.somethingwentwrong"),
                    popupCommonAlertDimiss, "");
                kony.application.dismissLoadingScreen();
            }
            kony.application.dismissLoadingScreen();
          kony.print(" test:  "+ frmEnrolluserLandingKA.lblInvalidCredentialsKA.text);
           kony.print(" test:  "+ frmEnrolluserLandingKA.lblInvalidCredentialsKA.isVisible);
        }

        function error(err) {
            kony.print("In error");
            frmEnrolluserLandingKA.tbxPasswordKA.text = "";
            frmEnrolluserLandingKA.tbxConfrmPwdKA.text = "";
            if (!isEmpty(err)) {
                kony.application.dismissLoadingScreen();
                kony.print("Error : " + err);
                kony.sdk.mvvm.log.error("-->VV In saveData errorcallback in controller extension of frmenroll", err);
                var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
                kony.sdk.mvvm.log.error("exception in save data ::" + exception.toString());
            } else {
                customAlertPopup(geti18Value("i18n.Bene.Failed"),
                    geti18Value("i18n.common.somethingwentwrong"),
                    popupCommonAlertDimiss, "");
                kony.application.dismissLoadingScreen();
            }
        }
    },
    /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmEnrolluserLandingKAControllerExtension#
     */
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method shows form.
     * @memberof frmEnrolluserLandingKAControllerExtension#
     */
    showForm: function() {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.showView();
        } catch (e) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    }
});
kony = kony || {};
kony.boj = kony.boj || {};
kony.boj.SIData = [];
kony.boj.SIsegDetailsData = []; 


// function tempSIPreshow()
// {
//   var data = [
//     {"date":"28-02-2018", "desc": "Jordan Water", amount : "123"},
//     {"date":"02-01-2018", "desc": "Jordan Gas", amount : "234"},
//     {"date":"28-02-2017", "desc": "Sayejh group", amount : "633"},
//     {"date":"28-10-2017", "desc": "Bath and Body Works", amount : "1356"},
//     {"date":"22-11-2017", "desc": "Ryan Portman", amount : "8862"},
//     {"date":"01-12-2017", "desc": "test", amount : "1677"},
//     {"date":"18-05-2017", "desc": "test $#@", amount : "2356"},
//     {"date":"12-04-2017", "desc": "test 1234", amount : "233"},
//   ];
//   for(var i in data){
//     data[i].formattedAmount =  data[i].amount + " JOD";
//     data[i].initials = data[i].desc.substring(0, 2).toUpperCase();
//   }
//   kony.boj.SIData = data;
//   kony.print("data SI "+ data);
//   kony.print("kony.boj.SIData "+ kony.boj.SIData);
//   frmStandingInstructions.tbxSearch.text = "";
//   frmStandingInstructions.segSIList.widgetDataMap = {lblTransactiondate : "date", lblTransactionDesc : "desc" , lblAmount : "formattedAmount" , lblInitial : "initials"};
//   frmStandingInstructions.segSIList.setData(data);
// }

function amountRangeDataSI(from, to){
  var data = [];
  frmStandingInstructions.segSIList.setData([]);
  if(from === -1 && to === -1)
    data = kony.boj.SIData;
  else if(from !== -1 && to === -1){
    for(var i in kony.boj.SIData){
      if(parseFloat(kony.boj.SIData[i].amount) >= parseFloat(from)){
        data.push(kony.boj.SIData[i]);
      }
    }
  }
  else if(from === -1 && to !== -1){
    for(var i in kony.boj.SIData){
      if(parseFloat(kony.boj.SIData[i].amount) <= parseFloat(to)){
        data.push(kony.boj.SIData[i]);
      }
    }
  }
  else{
    for(var i in kony.boj.SIData){
      if(parseFloat(kony.boj.SIData[i].amount) >= parseFloat(from) && 
         parseFloat(kony.boj.SIData[i].amount) <= parseFloat(to)){
        data.push(kony.boj.SIData[i]);
      }
    }
  }
  kony.print("Data pushed based on amount range"+data);
  frmStandingInstructions.segSIList.widgetDataMap = {lblTransactiondate : "formattedDate", lblTransactionDesc : "transfer_typedesc" , lblAmount : "formattedAmount", lblInitial : "initial"};

  if(data.length == 0){
    frmStandingInstructions.lblNoSI.isVisible = false;
    frmStandingInstructions.lblNoSearchResults.isVisible = true;
    frmStandingInstructions.segSIList.isVisible = false;

  }
  else{
    frmStandingInstructions.lblNoSI.isVisible = false;
    frmStandingInstructions.lblNoSearchResults.isVisible = false;
    frmStandingInstructions.segSIList.isVisible = true;
    frmStandingInstructions.segSIList.setData(data);
  }

  frmStandingInstructions.show();
}

function searchSIData(searchTerm){
  var data = [];
  searchTerm = searchTerm.toLowerCase();
  for(var i in kony.boj.SIData){
    if(kony.boj.SIData[i].formattedDate.toLowerCase().indexOf(searchTerm) != -1 ||
       kony.boj.SIData[i].beneficiary_name.toLowerCase().indexOf(searchTerm) != -1 ||
       kony.boj.SIData[i].formattedAmount.toLowerCase().indexOf(searchTerm) != -1 ){
      data.push(kony.boj.SIData[i]);
    }
  }
  kony.print("Data pushed based on search"+data);
  frmStandingInstructions.segSIList.setData([]);
  frmStandingInstructions.segSIList.widgetDataMap = {lblTransactiondate : "formattedDate", lblTransactionDesc : "beneficiary_name" , lblAmount : "formattedAmount", lblInitial : "initial"};

  if(data.length == 0){
    frmStandingInstructions.lblNoSI.isVisible = false;
    frmStandingInstructions.lblNoSearchResults.isVisible = true;
    frmStandingInstructions.segSIList.isVisible = false;

  }
  else{
    frmStandingInstructions.lblNoSI.isVisible = false;
    frmStandingInstructions.lblNoSearchResults.isVisible = false;
    frmStandingInstructions.segSIList.isVisible = true;
    frmStandingInstructions.segSIList.setData(data);
  }
}


function amountSortSI(segData, sortby, type)
{
  try{
    kony.print("amountDate_sort ::" + segData.data);
    var Data = segData.data;
    var temp = "";
    var value1 = "";
    var value2 = "";

    if (sortby.trim() == "Amount") {
      temp = "";
      for (var i = 0; i < Data.length; i++) {
        for (var j = i + 1; j < Data.length; j++) {
          value1 = Data[i].amount;
          value1 = parseFloat(value1);
          value2 = Data[j].amount;
          value2 = parseFloat(value2);
          if (((value1) > (value2)) && (type == "ascending")) {
            temp = Data[i];
            Data[i] = Data[j];
            Data[j] = temp;
          } else if (((value1) < (value2)) && (type == "descending")) {
            temp = Data[i];
            Data[i] = Data[j];
            Data[j] = temp;
          }
        }
      }
    }
    kony.print("Sorting Result " + JSON.stringify(Data));
    kony.print("length of the data ::" + Data.length);
    //         if (Data.length <= 1) {
    //             kony.application.getCurrentForm().lblExport.setEnabled(false);
    //             kony.application.getCurrentForm().CopylblFilter0f7770a92cff247.setEnabled(false);
    //         } else {
    //             kony.application.getCurrentForm().lblExport.setEnabled(true);
    //             kony.application.getCurrentForm().CopylblFilter0f7770a92cff247.setEnabled(true);
    //         }
    //     	kony.boj.SIData = Data;
    segData.removeAll();
    segData.setData(Data);
    animatePopupSortSI();
  }catch(e){
    kony.print("Exception_amount_Sort ::"+e);
    exceptionLogCall("amountSortSI","UI ERROR","UI",e);
    animatePopupSortSI();
  }
}



function animatePopupSortSI(){
  try{

    var top = "";
    var currentform = kony.application.getCurrentForm();
    if(currentform.flxAccountDetailsSort.top == "5%")
      top = "-14%";
    else top = "5%";
    var animationObj = kony.ui.createAnimation({"100":{"top":top,"stepConfig":{"timingFunction":kony.anim.LINEAR}}});
    currentform.flxAccountDetailsSort.animate(animationObj, {
      "delay": 0,
      "iterationCount": 1,
      "fillMode": kony.anim.FILL_MODE_FORWARDS,
      "duration": 0.5
    }, null);
  }catch(e){
    kony.print("Exception_animatepopup ::"+e);
  }
}



function dateSortSI(segData, sortby, type)
{
  try {
    kony.print("amountDate_sort ::" + segData.data);
    var Data = segData.data;
    var temp = "";
    var value1 = "";
    var value2 = "";

    if (sortby.trim() == "Date") {
      temp = "";
      for (var l = 0; l < Data.length; l++) {
        for (var k = l + 1; k < Data.length; k++) {
          value1 = Data[l].formattedDate.substring(3,5)+"/"+Data[l].formattedDate.substring(0,2)+"/"+Data[l].formattedDate.substring(6,Data[l].formattedDate.length);
          value2 = Data[k].formattedDate.substring(3,5)+"/"+Data[k].formattedDate.substring(0,2)+"/"+Data[k].formattedDate.substring(6,Data[k].formattedDate.length);
          kony.print("Date ::"+value1+""+value2);
          if ((new Date(value1) > new Date(value2)) && (type == "ascending")) {
            kony.print("inside asc");
            temp = Data[l];
            Data[l] = Data[k];
            Data[k] = temp;
          } else if ((new Date(value1) < new Date(value2)) && (type == "descending")) {
            kony.print("inside desc");
            temp = Data[l];
            Data[l] = Data[k];
            Data[k] = temp;
          }
        }
      }
    }
    //     	kony.boj.SIData = Data;
    kony.print("Sorting Result " + JSON.stringify(Data));
    kony.print("length of the data ::" + Data.length);
    //         if (Data.length <= 1) {
    //             kony.application.getCurrentForm().lblExport.setEnabled(false);
    //             kony.application.getCurrentForm().CopylblFilter0f7770a92cff247.setEnabled(false);
    //         } else {
    //             kony.application.getCurrentForm().lblExport.setEnabled(true);
    //             kony.application.getCurrentForm().CopylblFilter0f7770a92cff247.setEnabled(true);
    //         }
    segData.removeAll();
    segData.setData(Data);
    animatePopupSortSI();
  } catch (e) {
    kony.print("Exception_date_sort ::" + e);
    animatePopupSortSI();
  }
}




function loadAllDatatoSIDetails(data)
{
  //alert("Data details"+ JSON.stringify(data));
  frmConfirmStandingInstructions.lblID.text = data[0].debit_acc;
  frmConfirmStandingInstructions.lblReferenceID.text =  isEmpty(data[0].instruction_number) ? data[0].instruction_number : (data[0].instruction_number.replace("PI",""));
  frmConfirmStandingInstructions.lblDescription.text = data[0].transfer_typedesc;
  frmConfirmStandingInstructions.lblAmount.text = data[0].formattedAmount;
  frmConfirmStandingInstructions.lblID.text = data[0].debit_acc;
  frmConfirmStandingInstructions.initialsCategory.text = data[0].initial;
  frmConfirmStandingInstructions.lblDescription.text = data[0].transfer_typedesc;
  frmConfirmStandingInstructions.lblName.text = data[0].debit_acc_name;//data[0].beneficiary_name;
  frmConfirmStandingInstructions.lblInstructionNumber.text = data[0].instruction_number;
  frmConfirmStandingInstructions.lblScheduledDate.text = data[0].formattedDate;
  frmConfirmStandingInstructions.lblCustID.text = "";
  a = data[0].credit_acc;
  //a = a.substring(a.length -5, a.length);
  frmConfirmStandingInstructions.lblFromAcc.text = a;
  frmConfirmStandingInstructions.lblBeneficiaryName.text = data[0].beneficiary_name;
  var split_date = data[0].start_date.split("-");
  frmConfirmStandingInstructions.lblScheduledDate.text = split_date[2]+"/"+split_date[1]+"/"+split_date[0];
  split_date = null;
  split_date = data[0].end_date.split("-");
  frmConfirmStandingInstructions.lblScheduledEndDate.text = split_date[2]+"/"+split_date[1]+"/"+split_date[0];
  var frequency = "";
  if(data[0].txn_frequency === "D")
    frequency = geti18Value("i18n.transfers.Daily");
  else if(data[0].txn_frequency === "M")
    frequency = geti18Value("i18n.transfers.MonthlyOnce");
  frmConfirmStandingInstructions.lblSIFrequency.text = frequency;
  frmConfirmStandingInstructions.lblPeriodValue.text = data[0].period;
  frmConfirmStandingInstructions.flxFee.setVisibility(false);
  standingIns = data[0].instruction_number;
}

function standingInstructionDeleteSuccess(res){
  kony.print("SI delete success" + JSON.stringify(res));
  if(res.ErrorCode == 0 ||res.ErrorCode == "00000" ){
    if(res.ReferenceNum!== null && res.ReferenceNum!== ""){
    kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.bills.StoppedPayment"), geti18Value("i18n.SI.Cancelled"), 
                                   geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"), 
                                   geti18Value("i18n.SI.gotoSI"), "StandingInstructions",
                                   "", "", "");//geti18Value("i18n.common.ReferenceNumber") + " " + res.ReferenceNum
    }else{
      kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.bills.StoppedPayment"), geti18Value("i18n.SI.Cancelled"), 
                                   geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"), 
                                   geti18Value("i18n.SI.gotoSI"), "StandingInstructions",
                                   "", "", "" + "" + "");
    }
  }else{
    frmConfirmStandingInstructions.show();
    var Message = getServiceErrorMessage(res.ErrorCode,"standingInstructionDeleteSuccess");   
    customAlertPopup(geti18Value("i18n.Bene.Failed"), Message,popupCommonAlertDimiss,"");
  }
}

function standingInstructionDeleteFailure(err){
  frmConfirmStandingInstructions.show();
  var Message = geti18nkey("i18n.common.somethingwentwrong");
  customAlertPopup(geti18Value("i18n.Bene.Failed"), Message,popupCommonAlertDimiss,"");
}
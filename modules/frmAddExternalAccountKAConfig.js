//Edited by Arpan

var frmAddExternalAccountKAConfig = {
  "formid": "frmAddExternalAccountKA",
  "frmAddExternalAccountKA": {
    "entity": "ExternalAccounts",
    "objectServiceName": "RBObjects",
    "objectServiceOptions": {
      "access": "online"
    }
  },
  "txtBenficiryNameKA": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "beneficiaryName",
    }
  },
  "txtBenficiryNickName": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "nickName",
    }
  },
  "tbxAddressBene": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "bene_address1",
    }
  },
  "tbxCity": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "bene_city",
    }
  },
  "externalAccountNumberTextField": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "benAcctNo",
    }
  },
  "txtSwiftCodeKA": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "BENE_BANK_SWIFT",
    }
  },
  "txtCountryBankDetails": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "BENE_BANK_COUNTRY",
    }
  },
  "tbxBankNameInt": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "bankName",
    }
  },

  "tbxBankBranch": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "benBranchNo",
    }
  },
  "tbxCityBank": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "bene_bank_city",
    }
  },
  "tbxCountry": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "bene_address2",
    }
  },
  "lblAccountTypeKA": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "Label",
      "constrained": true,
      "field": "P_BENE_TYPE",
    }
  },

  "tbxEmail": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "TextBox",
      "constrained": true,
      "field": "bene_email",
    }
  },
  "lblLanguage": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "Label",
      "constrained": true,
      "field": "language",
    }
  },
  "lblCurrency": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "Label",
      "constrained": true,
      "field": "benAcctCurrency",
    }
  },
  "lblFavourite": {
    "fieldprops": {
      "entity": "ExternalAccounts",
      "widgettype": "Label",
      "constrained": true,
      "field": "Fav_flag",
    }
  },
};
var addIPSBeneVar=false;
var bene_name = "";
var aliasType="";
var transferFromBene = false;
var aliasOpr="";  // hassan fix delete ips alias
function GetAliasInfoAddBene(AliType){

  var aliasType="";

  if (frmAddBeneIPS.btnAlias.skin==="sknOrangeBGRNDBOJ")
  {
    aliasType="ALIAS";
  }
  else if(frmAddBeneIPS.btnMobile.skin==="sknOrangeBGRNDBOJ")
  {
    aliasType="MOBL";
  }

  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJAliasIPS");
  appMFConfiguration.invokeOperation("prGetIpsAliasDetails", {},{"p_customer_id": custid,
                                                                 "p_alias":frmAddBeneIPS.txtAliasMob.text.trim(),
                                                                 "p_alias_type": aliasType,
                                                                 "p_channel":"MOBILE",
                                                                 "p_user_id":"BOJMOB"},
                                     successCallGetAliasInfo,
                                     function(error){

  });
}

function successCallGetAliasInfo(response){
  if (!isEmpty(response.beniban) && !isEmpty(response.benaddress) && !isEmpty(response.benbic) &&!isEmpty(response.benname))
  {
    frmAddBeneIPS.txtIBAN.text = response.beniban;
    frmAddBeneIPS.txtAddress.text = response.benaddress; 
    frmAddBeneIPS.lblSwiftCode.text = response.benbic;
    frmAddBeneIPS.txtBeneName.text = response.benname;


    kony.print("bank det hassan "+JSON.stringify(kony.boj.detailsForBene.BankDetail));
    for(var i =0; i<kony.boj.detailsForBene.BankDetail.length;i++)
    {
      kony.print("bank name hassan "+kony.boj.detailsForBene.BankDetail[i].SwiftCode);
      if (kony.boj.detailsForBene.BankDetail[i].SwiftCode===response.benbic)
        frmAddBeneIPS.txtBankName.text = kony.boj.detailsForBene.BankDetail[i].BankName;
      frmAddBeneIPS.flxBankNameDiv.skin = "sknFlxGreenLine";
    }

    animateLabel("UP", "lblIBANTitle",frmAddBeneIPS.txtIBAN.text);
    animateLabel("UP", "lblBeneNameTitle",frmAddBeneIPS.txtBeneName.text);
    animateLabel("UP", "lblAddressTitle",frmAddBeneIPS.txtAddress.text);
    animateLabel("UP", "lblBankNameTitle",frmAddBeneIPS.txtBankName.text);

    frmAddBeneIPS.txtBankName.setEnabled(false);
    frmAddBeneIPS.txtBeneName.setEnabled(false);
    frmAddBeneIPS.txtIBAN.setEnabled(false);
    frmAddBeneIPS.txtAddress.setEnabled(false);

    validateNextAddIPSBene();
  }else
  {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmAddBeneIPS.txtIBAN.text = "";
    frmAddBeneIPS.txtAddress.text = ""; 
    frmAddBeneIPS.lblSwiftCode.text = "";
    frmAddBeneIPS.txtBeneName.text = "";
    frmAddBeneIPS.txtBankName.text = "";
    animateLabel("DOWN", "lblIBANTitle",frmAddBeneIPS.txtIBAN.text);
    animateLabel("DOWN", "lblBeneNameTitle",frmAddBeneIPS.txtBeneName.text);
    animateLabel("DOWN", "lblAddressTitle",frmAddBeneIPS.txtAddress.text);
    animateLabel("DOWN", "lblBankNameTitle",frmAddBeneIPS.txtBankName.text);
    frmAddBeneIPS.txtBankName.setEnabled(false);
    frmAddBeneIPS.txtBeneName.setEnabled(false);
    frmAddBeneIPS.txtIBAN.setEnabled(false);
    frmAddBeneIPS.txtAddress.setEnabled(false);
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.CLIQ.AliasError"), popupCommonAlertDimiss, "");

  }


  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

}

function onClickAliasTypeBtnBene(obj)
{

  if ((obj == "btnIBAN" || obj === "editIBAN") && frmAddBeneIPS.btnIBAN.skin==="slButtonBlueFocus")
  {
    frmAddBeneIPS.txtBankName.setEnabled(false);
    frmAddBeneIPS.txtBeneName.setEnabled(true);
    frmAddBeneIPS.txtIBAN.setEnabled(true);
    frmAddBeneIPS.txtAddress.setEnabled(true);
    frmAddBeneIPS.btnSelectBankName.setVisibility(true);
    frmAddBeneIPS.flxAliasMob.setVisibility(false);
    frmAddBeneIPS.btnIBAN.skin="sknOrangeBGRNDBOJ";
    frmAddBeneIPS.btnMobile.skin="slButtonBlueFocus";
    frmAddBeneIPS.btnAlias.skin="slButtonBlueFocus";
    frmAddBeneIPS.LblCountryCodeHint.setVisibility(false);
    frmAddBeneIPS.flxAliasDetail.setVisibility(true);
    if (obj === "btnIBAN"){
      frmAddBeneIPS.flxBankNameDiv.skin = "sknFlxGreyLine";
      frmAddBeneIPS.flxAddressDiv.skin = "sknFlxGreyLine";
      frmAddBeneIPS.flxIBANDiv.skin = "sknFlxGreyLine";
      frmAddBeneIPS.flxBeneNameDiv.skin = "sknFlxGreyLine";
      frmAddBeneIPS.flxNickNameDiv.skin = "sknFlxGreyLine";
    }else if (obj === "editIBAN"){
      if (kony.store.getItem("langPrefObj")==="en"){
        frmAddBeneIPS.btnIBAN.left = "1%";
      }else{
        frmAddBeneIPS.btnIBAN.right = "1%";
      }
      frmAddBeneIPS.btnMobile.setVisibility(false);
      frmAddBeneIPS.btnAlias.setVisibility(false);
      frmAddBeneIPS.btnIBAN.setVisibility(true);//Omar ALnajjar 
    }

  }else if(obj ==="btnAlias" || obj ==="btnMobile" || obj === "editMobile" || obj === "editAlias")
  {
    frmAddBeneIPS.txtAliasMob.text = "";
    frmAddBeneIPS.flxAliasDetail.setVisibility(false);
    frmAddBeneIPS.txtBankName.setEnabled(false);
    frmAddBeneIPS.txtBeneName.setEnabled(false);
    frmAddBeneIPS.txtIBAN.setEnabled(false);
    frmAddBeneIPS.txtAddress.setEnabled(false);
    frmAddBeneIPS.btnSelectBankName.setVisibility(false);
    frmAddBeneIPS.flxAliasMob.setVisibility(true);
    if (obj ==="btnAlias" || obj === "editAlias")
    {
      frmAddBeneIPS.btnIBAN.skin="slButtonBlueFocus";
      frmAddBeneIPS.btnMobile.skin="slButtonBlueFocus";
      frmAddBeneIPS.btnAlias.skin="sknOrangeBGRNDBOJ";
      frmAddBeneIPS.lblAliasMobTitle.text=geti18Value("i18n.jomopay.aliastype");
      frmAddBeneIPS.txtAliasMob.maxTextLength = 9;
      if (obj === "btnAlias"){

        frmAddBeneIPS.flxNickNameDiv.skin = "sknFlxGreyLine";
        frmAddBeneIPS.flxAliasMobDivider.skin = "sknFlxGreyLine";
      }else if (obj === "editAlias"){
        frmAddBeneIPS.btnIBAN.setVisibility(false);
        frmAddBeneIPS.btnMobile.setVisibility(false);	
        frmAddBeneIPS.btnAlias.setVisibility(true);
        if (kony.store.getItem("langPrefObj")==="en"){
          frmAddBeneIPS.btnAlias.left = "1%";
        }else{
          frmAddBeneIPS.btnAlias.right = "1%";
        }

      }
      frmAddBeneIPS.txtAliasMob.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
      frmAddBeneIPS.txtAliasMob.autoCapitalize = constants.TEXTBOX_AUTO_CAPITALIZE_ALL;
      frmAddBeneIPS.LblCountryCodeHint.setVisibility(false);
    }else
    {
      frmAddBeneIPS.btnIBAN.skin="slButtonBlueFocus";
      frmAddBeneIPS.btnMobile.skin="sknOrangeBGRNDBOJ";
      frmAddBeneIPS.btnAlias.skin="slButtonBlueFocus";    
      frmAddBeneIPS.lblAliasMobTitle.text=geti18Value("i18n.myprofile.MobileNumber");
      frmAddBeneIPS.txtAliasMob.maxTextLength = 15;
      if (obj === "btnMobile"){
        frmAddBeneIPS.txtAliasMob.text = "009627"; 
        frmAddBeneIPS.flxNickNameDiv.skin = "sknFlxGreyLine";
        frmAddBeneIPS.flxAliasMobDivider.skin = "sknFlxGreyLine";
      }else if (obj === "editMobile"){
        frmAddBeneIPS.btnIBAN.setVisibility(false);
        if (kony.store.getItem("langPrefObj")==="en"){
          frmAddBeneIPS.btnMobile.left = "1%";
        }else{
          frmAddBeneIPS.btnMobile.right = "1%";
        }
        frmAddBeneIPS.btnAlias.setVisibility(false);
        frmAddBeneIPS.btnMobile.setVisibility(true);
      }
      frmAddBeneIPS.txtAliasMob.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
      animateLabel("UP", "lblAliasMobTitle",frmAddBeneIPS.txtAliasMob.text);
      frmAddBeneIPS.LblCountryCodeHint.setVisibility(true);
    }   
  }
  if (obj === "editIBAN" || obj === "editMobile" || obj === "editAlias"){
    frmAddBeneIPS.show();

    frmAddBeneIPS.txtIBAN.text =     frmIPSManageBene.segIPSBene.selectedRowItems[0].BenificiaryFullName;
    frmAddBeneIPS.txtAddress.text =  frmIPSManageBene.segIPSBene.selectedRowItems[0].bene_address1;
    frmAddBeneIPS.txtBankName.text = frmIPSManageBene.segIPSBene.selectedRowItems[0].bankName;
    frmAddBeneIPS.txtBeneName.text = frmIPSManageBene.segIPSBene.selectedRowItems[0].bene_email;
    frmAddBeneIPS.txtNickName.text=  frmIPSManageBene.segIPSBene.selectedRowItems[0].nickName;
    frmAddBeneIPS.txtAliasMob.text=  frmIPSManageBene.segIPSBene.selectedRowItems[0].BenificiaryFullName;

    frmAddBeneIPS.flxNickNameDiv.skin = "sknFlxGreenLine";
    frmAddBeneIPS.flxAliasMobDivider.skin = "sknFlxGreenLine";
    frmAddBeneIPS.flxBankNameDiv.skin = "sknFlxGreenLine";
    frmAddBeneIPS.flxAddressDiv.skin = "sknFlxGreenLine";
    frmAddBeneIPS.flxIBANDiv.skin = "sknFlxGreenLine";
    frmAddBeneIPS.flxBeneNameDiv.skin = "sknFlxGreenLine";

    animateLabel("UP", "lblBankNameTitle",frmAddBeneIPS.txtBankName.text);
    animateLabel("UP", "lblAliasMobTitle",frmAddBeneIPS.txtAliasMob.text);
    animateLabel("UP", "lblNickNameTitle",frmAddBeneIPS.txtNickName.text);
    animateLabel("UP", "lblIBANTitle",frmAddBeneIPS.txtIBAN.text);
    animateLabel("UP", "lblBeneNameTitle",frmAddBeneIPS.txtBeneName.text);
    animateLabel("UP", "lblAddressTitle",frmAddBeneIPS.txtAddress.text);

  }else{
    frmAddBeneIPS.txtIBAN.text = "";
    frmAddBeneIPS.txtAddress.text = ""; 
    frmAddBeneIPS.txtBankName.text = "";
    frmAddBeneIPS.txtBeneName.text = "";
    if (obj === "btnMobile"){
      frmAddBeneIPS.txtAliasMob.text="009627";
    }else{
      frmAddBeneIPS.txtAliasMob.text="";
    }
    frmAddBeneIPS.txtNickName.text="";
    frmAddBeneIPS.lblSwiftCode.text="";
    animateLabel("DOWN", "lblAliasMobTitle",frmAddBeneIPS.txtAliasMob.text);
    animateLabel("DOWN", "lblNickNameTitle",frmAddBeneIPS.txtNickName.text);
    animateLabel("DOWN", "lblIBANTitle",frmAddBeneIPS.txtIBAN.text);
    animateLabel("DOWN", "lblBeneNameTitle",frmAddBeneIPS.txtBeneName.text);
    animateLabel("DOWN", "lblAddressTitle",frmAddBeneIPS.txtAddress.text);
    animateLabel("DOWN", "lblBankNameTitle",frmAddBeneIPS.txtBankName.text);
  }


  validateNextAddIPSBene();
}

function validateNextAddIPSBene()
{
  //   if(!isEmpty(frmAddBeneIPS.txtBeneName.text) && !isEmpty(frmAddBeneIPS.txtIBAN.text)&&
  //      !isEmpty(frmAddBeneIPS.txtAddress.text) && !isEmpty(frmAddBeneIPS.txtBankName.text) &&
  //      !isEmpty(frmAddBeneIPS.txtNickName.text))
  //     {
  if (frmAddBeneIPS.btnIBAN.skin === "sknOrangeBGRNDBOJ"){
    if(!isEmpty(frmAddBeneIPS.txtBeneName.text) && frmAddBeneIPS.txtBeneName.text.length > 3 && !isEmpty(frmAddBeneIPS.txtIBAN.text)&&
       frmAddBeneIPS.txtIBAN.text.length === 30 &&
       !isEmpty(frmAddBeneIPS.txtAddress.text) && frmAddBeneIPS.txtAddress.text.length > 3 && !isEmpty(frmAddBeneIPS.txtBankName.text) && frmAddBeneIPS.txtBankName.text.length > 3 &&
       !isEmpty(frmAddBeneIPS.txtNickName.text) && frmAddBeneIPS.txtNickName.text.length > 3)
    {
      frmAddBeneIPS.lblNext.skin="sknLblNextEnabled";
    }else{
      frmAddBeneIPS.lblNext.skin="sknLblNextDisabled";
    }
  }else{
    if(!isEmpty(frmAddBeneIPS.txtAliasMob.text) && frmAddBeneIPS.txtAliasMob.text.length > 3 && !isEmpty(frmAddBeneIPS.txtNickName.text)&& frmAddBeneIPS.txtNickName.text.length > 3 &&  frmAddBeneIPS.txtAliasMob.text !== "009627")
    {
      frmAddBeneIPS.lblNext.skin="sknLblNextEnabled";
    }
    else
    {
      frmAddBeneIPS.lblNext.skin="sknLblNextDisabled";
    }   
  }

}

function validate_IPSBene_ENTRED_VALUE(val, widget){
  try{
    var isValid = false;
    if(frmAddBeneIPS.btnMobile.skin === "sknOrangeBGRNDBOJ"){
      if(val.startsWith("009627")===false){
        if(val.startsWith("07")===true){
          widget.text="00962"+ val.substring(1);
        }else if(val.startsWith("7")===true){
          widget.text="00962"+ val;
        }else if(val.startsWith("9627")===true){
          widget.text="00"+ val;
        }else if(val.startsWith("+9627")===true){
          widget.text="00"+ val.substring(1);
        }
      }
      if(( widget.text.match(/[0-9]/g)) && (( widget.text.length >=11) && ( widget.text.length <= 15))){
        isValid = true;
      }
    }else if(frmAddBeneIPS.btnAlias.skin === "sknOrangeBGRNDBOJ"){
      if(val.match(/[^A-Z0-9]/g)){
        val = val.replace(/[^A-Z0-9]/g, "");
        widget.text = val;
        //         return;
      }
      if(val.match(/[A-Z0-9]/g) && (val.length <= 10)&& (val.length >= 3)){
        isValid = true;
        widget.text = val;
      }else if (val.length < 3){
        isValid = false;

      }else if (val.length > 10){
        isValid = true;
        widget.text = val.slice(0,10);
      }
    }
    return isValid;
  }catch(e){
    kony.print("Exception_validate_IPSBene_ENTRED_VALUE ::"+e);
  }
}

function setDataAddBeneIPS(data)
{
  frmAddBeneIPS.txtBankName.text=data.BankName;
  frmAddBeneIPS.lblSwiftCode.text=data.SwiftCode;
  frmAddBeneIPS.show();
  animateLabel("UP", "lblBankNameTitle",frmAddBeneIPS.txtBankName.text);
  frmAddBeneIPS.flxBankNameDiv.skin = "sknFlxGreenLine";
  validateNextAddIPSBene();
}

function goToConfirmPageAddBeneIPS()
{
  if (frmAddBeneIPS.lblNext.skin==="sknLblNextEnabled")
  {
    frmAddBeneIPS.flxConfirm.setVisibility(true);
    frmAddBeneIPS.flxMain.setVisibility(false);
    frmAddBeneIPS.flxConfirmDetails.setVisibility(false);

    frmAddBeneIPS.lblConfirmNickName.text=frmAddBeneIPS.txtNickName.text;
    if (frmAddBeneIPS.btnAlias.skin === "sknOrangeBGRNDBOJ"){
      frmAddBeneIPS.lblConfirmAliasValue.text = frmAddBeneIPS.txtAliasMob.text;
      frmAddBeneIPS.lblConfirmAliasTitle.text = geti18Value("i18n.CliQ.ALias");
    }else if (frmAddBeneIPS.btnMobile.skin === "sknOrangeBGRNDBOJ"){
      frmAddBeneIPS.lblConfirmAliasValue.text = frmAddBeneIPS.txtAliasMob.text;
      frmAddBeneIPS.lblConfirmAliasTitle.text = geti18Value("i18n.myprofile.MobileNumber");
    }else if (frmAddBeneIPS.btnIBAN.skin === "sknOrangeBGRNDBOJ"){
      frmAddBeneIPS.lblConfirmAliasValue.text = frmAddBeneIPS.txtIBAN.text;

      frmAddBeneIPS.lblConfirmAliasTitle.text = geti18Value("i18.CLIQ.IBAN");
      frmAddBeneIPS.lblConfirmIBAN.text=frmAddBeneIPS.txtIBAN.text;
      frmAddBeneIPS.lblConfirmAddress.text=frmAddBeneIPS.txtAddress.text;
      frmAddBeneIPS.lblConfirmBeneName.text=frmAddBeneIPS.txtBeneName.text;
      frmAddBeneIPS.lblConfirmBankName.text=frmAddBeneIPS.txtBankName.text;

      frmAddBeneIPS.flxConfirmDetails.setVisibility(true);
    }
    frmAddBeneIPS.lblNext.setVisibility(false);
  }
}


function serv_ADD_IPS_BENEFICIARY(){
  try{
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var fav="N";

    if (frmAddBeneIPS.lblCkbFav.text==="q")
      fav="N";
    else if(frmAddBeneIPS.lblCkbFav.text==="p")
      fav = "Y";


    if (frmAddBeneIPS.btnAlias.skin==="sknOrangeBGRNDBOJ")
    {
      aliasType="alias";
      bene_name = frmAddBeneIPS.txtAliasMob.text;
    }
    else if(frmAddBeneIPS.btnMobile.skin==="sknOrangeBGRNDBOJ")
    {
      aliasType="mobile";
      bene_name = frmAddBeneIPS.txtAliasMob.text;
    }
    else{
      bene_name = frmAddBeneIPS.txtIBAN.text;
      aliasType="IBAN";
    }

    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("ExternalAccounts", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("ExternalAccounts");
    dataObject.addField("beneficiaryName", bene_name);
    dataObject.addField("nickName", frmAddBeneIPS.txtNickName.text);
    dataObject.addField("bene_address1", !isEmpty(frmAddBeneIPS.txtAddress.text)?frmAddBeneIPS.txtAddress.text:"");
    dataObject.addField("bene_city", "");
    dataObject.addField("benAcctNo", "");
    dataObject.addField("BENE_BANK_SWIFT", frmAddBeneIPS.lblSwiftCode.text);
    dataObject.addField("BENE_BANK_COUNTRY", "");
    dataObject.addField("bankName",!isEmpty(frmAddBeneIPS.txtBankName.text)?frmAddBeneIPS.txtBankName.text:"");
    dataObject.addField("benBranchNo", "");
    dataObject.addField("bene_bank_city", "");
    dataObject.addField("bene_address2", aliasType);
    dataObject.addField("P_BENE_TYPE", "IPS");
    dataObject.addField("bene_email", !isEmpty(frmAddBeneIPS.txtBeneName.text)?frmAddBeneIPS.txtBeneName.text:"");
    dataObject.addField("language",kony.store.getItem("langPrefObj").toUpperCase());
    dataObject.addField("benAcctCurrency", "");
    dataObject.addField("Fav_flag", fav);
    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("data Object ::"+JSON.stringify(dataObject));
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    if (kony.sdk.isNetworkAvailable()) {
      modelObj.customVerb("create", serviceOptions, success_serv_ADD_IPS_BENEFICIARY, failed_serv_ADD_IPS_BENEFICIARY);
    } else {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
  }catch(e){
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("Exception_serv_ADD_IPS_BENEFICIARY ::"+e);
  }
}

function success_serv_ADD_IPS_BENEFICIARY(res){
  try{
    kony.print("add beneficiary res ::"+JSON.stringify(res));
    gblLaunchModeOBJ.lauchMode = false;
    if(res.ErrorCode == "00000" || res.ErrorCode == "0"){
      kony.boj.detailsForBene.flag = false;
      var msg = geti18Value("i18n.common.benificaryadded").split("name");


      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
      kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.Bene.Success"), msg[0] + frmAddBeneIPS.txtNickName.text + msg[1],
                                     geti18Value("i18n.CLIQ.GoToCliqDashboard"), "CliqDashboardManage",geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard");

    }else if(res.ErrorCode ==="00444")
    {
      kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.AddBene.BeneExist"), geti18Value("i18n.CLIQ.GoToCliqDashboard"), "CliqDashboardManage",geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard", "", "", "");
    }
    else{
      var message=getErrorMessage(res.ErrorCode);
      kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), message, geti18Value("i18n.CLIQ.GoToCliqDashboard"), "CliqDashboardManage", geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard", "", "", "");
    }
    frmAddBeneIPS.destroy();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  }catch(e){
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("Exception_success_serv_ADD_IPS_BENEFICIARY ::"+e);
  }
}

function failed_serv_ADD_IPS_BENEFICIARY(err){
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}








function getAllIPSBenficiaries()
{
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("prGetBeneficiaryDetails");
  appMFConfiguration.invokeOperation("prGetBeneficiaryDetails", {},{"p_customer_id": custid,
                                                                    "P_BENE_TYPE":"IPS",
                                                                    "p_channel":"MOBILE",
                                                                    "p_user_id":"BOJMOB"},
                                     successAllIPSBenficiaries,
                                     function(error){
      kony.print("errrrr123"+JSON.stringify(error));  
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");

  });
}


function successAllIPSBenficiaries(res)
{

  try{

    var tmpArray=[];
    var btnFav = {"isVisible":false};
    var flxButtonHolder = {"isVisible":true,"width":"66dp"};
    if (res.hasOwnProperty("beneList")){
      if (res.beneList.length === 0 ){
        frmIPSManageBene.segIPSBene.setVisibility(false);
        frmIPSManageBene.flxNoBene.setVisibility(true);
      }else{
        frmIPSManageBene.segIPSBene.setData([]);
        frmIPSManageBene.segIPSBene.setVisibility(true);
        frmIPSManageBene.flxNoBene.setVisibility(false);
        frmIPSManageBene.segIPSBene.widgetDataMap = { 
          accountNumber :"BeneIPS",
          BenificiaryName: "beneficiaryNameValue",
          BenificiaryFullName: "nickName",
          lblInitial: "initial",
          flxIcon1: "icon",
          btnFav: "btnFav",
          flxButtonHolder:"flxButtonHolder",
          swiftCode:"swiftCode"
        };
        var aliasType = "";
        for(var i in res.beneList)
        {
          if (res.beneList[i].bene_address2 === "IBAN"){
            aliasType = geti18Value("i18.CLIQ.IBAN") + " : ";
          }else if (res.beneList[i].bene_address2 === "alias"){
            aliasType = geti18Value("i18n.CliQ.ALias") + " : ";
          }else if (res.beneList[i].bene_address2 === "mobile"){
            aliasType = geti18Value("i18n.myprofile.MobileNumber") + " : ";
          }
          res.beneList[i].BeneIPS = aliasType + res.beneList[i].beneficiaryName;
          res.beneList[i].initial = res.beneList[i].nickName.substring(0, 2).toUpperCase();
          res.beneList[i].BenificiaryFullName = res.beneList[i].beneficiaryName;
          res.beneList[i].beneficiaryNameValue = "\n";

          res.beneList[i].btnFav = btnFav;
          res.beneList[i].flxButtonHolder = flxButtonHolder;
          tmpArray.push(res.beneList[i]);

        }

        frmIPSManageBene.segIPSBene.setData(tmpArray);

      }


    }else{
      frmIPSManageBene.segIPSBene.setVisibility(false);
      frmIPSManageBene.flxNoBene.setVisibility(true);
    }
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmIPSManageBene.show(); 
  }catch(error){
      kony.print("errrrrr:"+JSON.stringify(error));  
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
  }
}



function deleteIPSBene(selectedData)
{
  kony.print("deleteIPSBene:"+JSON.stringify(selectedData));  
  gblToOTPFrom = "DeleteIPSBene" ;
  customAlertPopup(geti18nkey("i18n.common.delBenConfirm"), geti18nkey("i18n.common.deleteBen"), function(){calldeleteIPSBene_serv(selectedData);}, function(){kony.boj.selectedPayee ={};popupCommonAlertDimiss();}, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));

}

function calldeleteIPSBene_serv(selectedData){
  popupCommonAlertDimiss();
  if (kony.sdk.isNetworkAvailable()) {
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    kony.print("callDeleteSelectedBenf:"+JSON.stringify(selectedData));  
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("ExternalAccounts", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("ExternalAccounts");
    dataObject.addField("custId", custid);
    dataObject.addField("P_BENEF_ID", selectedData.benef_id);
    dataObject.addField("P_BENEF_NAME", selectedData.BenificiaryFullName); 


    dataObject.addField("P_BENE_ACCOUNT", "");
    dataObject.addField("P_BENE_TYPE", "IPS");
    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("benf dataObject::" + JSON.stringify(dataObject));
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    modelObj.customVerb("deleteBenificiary", serviceOptions, calldeleteBeneSuccess, calldeleteBeneError);
  } else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}


function calldeleteBeneSuccess(res){
  kony.print("deleteBenfSuccess::"+JSON.stringify(res));
  if(res.opstatus == "0" && !isEmpty(res.result) && res.result === "00000"){
    kony.application.dismissLoadingScreen();
    //  kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.Bene.Success"), geti18Value("i18n.CLIQ.GoToCliqDashboard"),
    //                               geti18Value("i18n.CLIQ.GoToCliqDashboard"), "CliqDashboardManage",geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard");
   // if (frmAddBeneIPS.lblTitle.text === geti18Value("i18n.updatebene.title")){ // hassan fix delete ips alias
    if (aliasOpr === "Update"){  // hassan fix delete ips alias
      serv_ADD_IPS_BENEFICIARY();
    }else{
      frmIPSManageBene.show();
      getAllIPSBenficiaries();//hassan ips bene
    }

    kony.application.dismissLoadingScreen();
  }else{
    customAlertPopup(geti18nkey("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    kony.application.dismissLoadingScreen();

  }

}


function calldeleteBeneError(err){
  kony.print("deleteBenfError::"+JSON.stringify(err));
  customAlertPopup(geti18nkey("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  kony.application.dismissLoadingScreen();
}

// hassan get IPS Bene
// function show_SELECTED_BENEFICIARY_IPS_TRANSFER(data){
// 	try{

//       //  frmEPS.lblBankMobAliastxt.text=;
//         if (data.bene_address2 === "alias")
//           {
// //               frmEPS.btnMob.setEnabled(false);
// //               frmEPS.btnIBAN.setEnabled(false);
//               frmEPS.btnAlias.skin="sknOrangeBGRNDBOJ";
//               frmEPS.btnIBAN.skin="slButtonBlueFocus";
//     		  frmEPS.btnMob.skin="slButtonBlueFocus";

//               frmEPS.flxAmountAlias.setVisibility(true);
//               frmEPS.flxDetailsMobileScroll.setVisibility(false);
//               frmEPS.flxDetailsAliasScroll.setVisibility(true);
//               frmEPS.flxDetailsIBAN.setVisibility(false);
//               //frmEPS.flxIBANAliass.setVisibility(true);
//               //frmEPS.flxAliasAddress.setVisibility(true);
//               frmEPS.flxAliasBank.setVisibility(true);
//               frmEPS.flxDetailsAliasScroll.setVisibility(true);
//               frmEPS.txtAliasName.text=data.BenificiaryFullName;
//               frmEPS.lblIBANAliastxt.text = data.benAcctNo;
//               frmEPS.lbAliasAddresstxt.text = data.bene_address1;
//               frmEPS.lblAliasBanktxt.text = data.swiftCode;
//               frmEPS.lblBeneName.text = data.beneficiaryName;
//               animateLabel("UP","lblAliasName",frmEPS.txtAliasName.text,"frmEPS");
//               frmEPS.txtAliasName.setEnabled(false);
//           }else if (data.bene_address2 === "mobile")
//             {
//               frmEPS.btnIBAN.setEnabled(false);
//               frmEPS.btnAlias.setEnabled(false);
//               frmEPS.flxDetailsMobileScroll.setVisibility(true);
//               frmEPS.flxDetailsAliasScroll.setVisibility(false);
//               frmEPS.btnMob.skin="sknOrangeBGRNDBOJ";
//               frmEPS.btnAlias.skin="slButtonBlueFocus";
//               frmEPS.btnIBAN.skin="slButtonBlueFocus";

//               frmEPS.txtAliasNameMob.text=data.BenificiaryFullName;
//               frmEPS.lblBeneNameMob.text=data.beneficiaryName;
//               frmEPS.lblBankMobAliastxt.text=data.swiftCode;
//               frmEPS.lblAddressMobtxt.text = data.bene_address1;
//               frmEPS.lblIBANMobtxt.text = data.benAcctNo;
//               animateLabel("UP","lblAliasNameMob",frmEPS.txtAliasNameMob.text);
//               frmEPS.txtAliasNameMob.setEnabled(false);
//             }else if (data.bene_address2 === "IBAN")
//             {
// //               frmEPS.btnMob.setEnabled(false);
// //               frmEPS.btnAlias.setEnabled(false);
//               frmEPS.flxDetailsMobileScroll.setVisibility(false);
//               frmEPS.flxDetailsAliasScroll.setVisibility(false);
//               frmEPS.flxDetailsAliasScroll.setVisibility(false);
//               frmEPS.btnMob.skin="slButtonBlueFocus";
//               frmEPS.btnAlias.skin="slButtonBlueFocus";
//               frmEPS.btnIBAN.skin="sknOrangeBGRNDBOJ";
//               frmEPS.txtAliasNameMob.text=data.BenificiaryFullName;
//               frmEPS.lblBeneNameMob.text=data.beneficiaryName;
//               frmEPS.lblBankMobAliastxt.text=data.BENE_BANK_SWIFT;
//               frmEPS.lblAddressMobtxt.text = data.bene_address1;
//               frmEPS.lblIBANMobtxt.text = data.BenificiaryFullName;
//               animateLabel("UP","lblAliasNameMob",frmEPS.txtAliasNameMob.text);
//               frmEPS.txtAliasNameMob.setEnabled(false);
//             }
//     	frmEPS.show();

//     }catch(e){
//     	kony.print("Exception_show_SELECTED_BENEFICIARY_IPS_TRANSFER ::"+e);
//     }
// }

function show_SELECTED_BENEFICIARY_IPS_TRANSFER(data){
  try{
    transferFromBene = true;
    ResetFormIPSData();
    //  frmEPS.lblBankMobAliastxt.text=;
    if (data.bene_address2 === "alias")
    {

      frmEPS.txtAliasName.text = data.BenificiaryFullName;
      frmEPS.txtAliasName.setEnabled(false);
      frmEPS.flxDetailsIBAN.setVisibility(false);
      frmEPS.flxDetailsMobileScroll.setVisibility(false);
      frmEPS.flxDetailsAliasScroll.setVisibility(true);
      if(frmEPS.btnAlias.skin === slButtonBlueFocus){
        frmEPS.lblNext.skin = "sknLblNextDisabled";
        frmEPS.btnAlias.skin = sknOrangeBGRNDBOJ;
        frmEPS.btnIBAN.skin = slButtonBlueFocus;
        frmEPS.btnMob.skin = slButtonBlueFocus;  
      }else{
        frmEPS.btnAlias.skin = sknOrangeBGRNDBOJ;
        frmEPS.btnIBAN.skin = slButtonBlueFocus;
      }

      frmEPS.btnMob.setVisibility(false);
      if (kony.store.getItem("langPrefObj")==="en"){
        frmEPS.btnAlias.left = "1%";
      }else{
        frmEPS.btnAlias.right = "1%";
      }

      frmEPS.btnAlias.setVisibility(true);
      frmEPS.btnIBAN.setVisibility(false);


      GetAliasInfo(data.BenificiaryFullName);
      // for reset other flows 
      // Reset Mob Details //
      frmEPS.txtAliasNameMob.text = "";
      frmEPS.txtAliasNameMob.setEnabled(true);
      frmEPS.txtAmountMob.text = "";
      frmEPS.lblIBANMobtxt.text = "";
      frmEPS.lblAddressMobtxt.text = "";
      frmEPS.lblBankMobAliastxt.text = "";
      frmEPS.flxBeneNameMob.setVisibility(false);
      frmEPS.flxAmountMob.setVisibility(false);
      frmEPS.flxIbanDetailsMob.setVisibility(false);
      frmEPS.flxAddressMob.setVisibility(false);
      frmEPS.flxBankMob.setVisibility(false);
      // Reset Mob Details //
      // Reset Iban Details //  
      frmEPS.txtAmountIBAN.text = "";
      frmEPS.txtIBANAlias.text = "";
      frmEPS.txtIBANAlias.setEnabled(true);
      frmEPS.txtAddressAlias.text = "";
      frmEPS.txtBankAlias.text = "";
      frmEPS.tbxBankName.text = "";
      frmEPS.flxBeneNameIBAN.setVisibility(false);
      frmEPS.lblBankNameStat.setVisibility(false);
      frmEPS.tbxBankName.setVisibility(false);
      frmEPS.lblBankName.setVisibility(true);
      frmEPS.show();
      animateLabel("UP","lblAliasName",frmEPS.txtAliasName.text);
      // Reset Iban Details // 
      // GetAliasInfo();
      // frmEPS.flxAliasInfo.setVisibility(true);
      // frmEPS.flxMain.setVisibility(false);

    }else if (data.bene_address2 === "mobile")
    {


      frmEPS.txtAliasNameMob.text = data.BenificiaryFullName;
      frmEPS.txtAliasNameMob.setEnabled(false);
      frmEPS.flxDetailsAliasScroll.setVisibility(false);
      frmEPS.flxDetailsIBAN.setVisibility(false);
      frmEPS.flxDetailsMobileScroll.setVisibility(true);
      if(frmEPS.btnMob.skin === slButtonBlueFocus){
        frmEPS.lblNext.skin = "sknLblNextDisabled";
        frmEPS.btnMob.skin = sknOrangeBGRNDBOJ;
        frmEPS.btnAlias.skin = slButtonBlueFocus;
        frmEPS.btnIBAN.skin = slButtonBlueFocus;
      }

      if (kony.store.getItem("langPrefObj")==="en"){
        frmEPS.btnMob.left = "1%";
      }else{
        frmEPS.btnMob.right = "1%";
      }
      frmEPS.btnAlias.setVisibility(false);
      frmEPS.btnIBAN.setVisibility(false);
      frmEPS.btnMob.setVisibility(true);
      GetAliasInfo(data.BenificiaryFullName);
      //reset the other flows 
      // Reset Iban Details //  
      frmEPS.txtAmountIBAN.text = "";
      frmEPS.txtIBANAlias.text = "";
      frmEPS.txtIBANAlias.setEnabled(true);
      frmEPS.txtAddressAlias.text = "";
      frmEPS.txtBankAlias.text = "";
      frmEPS.tbxBankName.text = "";
      frmEPS.flxBeneNameIBAN.setVisibility(false);
      frmEPS.lblBankNameStat.setVisibility(false);
      frmEPS.tbxBankName.setVisibility(false);
      frmEPS.lblBankName.setVisibility(true);
      // Reset Iban Details // 
      // Reset Alias Details //
      frmEPS.txtAliasName.text = "";
      frmEPS.txtAliasName.setEnabled(true);
      frmEPS.txtAmountAlias.text = "";
      frmEPS.lblIBANAliastxt.text = "";
      frmEPS.lbAliasAddresstxt.text = "";
      frmEPS.lblAliasBanktxt.txt = "";
      frmEPS.flxBeneName.setVisibility(false);
      frmEPS.flxAmountAlias.setVisibility(false);
      frmEPS.flxIBANAliass.setVisibility(false);
      frmEPS.flxAliasAddress.setVisibility(false);
      frmEPS.flxAliasBank.setVisibility(false);
      frmEPS.show();
      animateLabel("UP","lblAliasNameMob",frmEPS.txtAliasNameMob.text);
      // Reset Alias Details //
      // animateLabel("UP", "lblIBANMob",frmEPS.lblIBANMob.text);
      // animateLabel("UP", "lblAddressMob",frmEPS.lblAddressMob.text);
      // animateLabel("UP", "lblBankMob",frmEPS.lblBankMob.text);
      // GetAliasInfo();
    }else if (data.bene_address2 === "IBAN")
    {

      //       animateLabel("UP","lblAliasNameMob",frmEPS.txtAliasNameMob.text);

      // kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      frmEPS.flxDetailsAliasScroll.setVisibility(false);
      frmEPS.flxDetailsMobileScroll.setVisibility(false);
      frmEPS.flxDetailsIBAN.setVisibility(true);
      if(frmEPS.btnIBAN.skin === slButtonBlueFocus){
        frmEPS.lblNext.skin = "sknLblNextDisabled";
        frmEPS.btnIBAN.skin = sknOrangeBGRNDBOJ;
        frmEPS.btnAlias.skin = slButtonBlueFocus;
        frmEPS.btnMob.skin = slButtonBlueFocus;
      }else{
        frmEPS.btnAlias.skin = slButtonBlueFocus;
        frmEPS.btnIBAN.skin = sknOrangeBGRNDBOJ;

      }
      frmEPS.btnMob.setVisibility(false);
      frmEPS.btnAlias.setVisibility(false);
      if (kony.store.getItem("langPrefObj")==="en"){
        frmEPS.btnIBAN.left = "1%";        
      }else{
        frmEPS.btnIBAN.right = "1%";        
      }
      frmEPS.btnIBAN.setVisibility(true);
      // reset the other flows 
      // Reset Alias Details //
      frmEPS.txtAliasName.text = "";
      frmEPS.txtAliasName.setEnabled(true);
      frmEPS.txtAmountAlias.text = "";
      frmEPS.lblIBANAliastxt.text = "";
      frmEPS.lbAliasAddresstxt.text = "";
      frmEPS.lblAliasBanktxt.txt = "";
      frmEPS.flxBeneName.setVisibility(false);
      frmEPS.flxAmountAlias.setVisibility(false);
      frmEPS.flxIBANAliass.setVisibility(false);
      frmEPS.flxAliasAddress.setVisibility(false);
      frmEPS.flxAliasBank.setVisibility(false);
      // Reset Alias Details //
      // Reset Mob Details //
      frmEPS.txtAliasNameMob.text = "";
      frmEPS.txtAliasNameMob.setEnabled(true);
      frmEPS.txtAmountMob.text = "";
      frmEPS.lblIBANMobtxt.text = "";
      frmEPS.lblAddressMobtxt.text = "";
      frmEPS.lblBankMobAliastxt.text = "";
      frmEPS.flxBeneNameMob.setVisibility(false);
      frmEPS.flxAmountMob.setVisibility(false);
      frmEPS.flxIbanDetailsMob.setVisibility(false);
      frmEPS.flxAddressMob.setVisibility(false);
      frmEPS.flxBankMob.setVisibility(false);
      // Reset Mob Details //
      frmEPS.show();
      animateLabel("UP","lblAddress",frmEPS.txtAddressAlias.text);
      animateLabel("UP","lblIBANAlias",frmEPS.txtIBANAlias.text);
      animateLabel("UP","lblIBANBeneName",frmEPS.txtIBANBeneName.text);

      frmEPS.txtIBANBeneName.text = data.bene_email;
      frmEPS.txtIBANAlias.text = data.BenificiaryFullName;
      frmEPS.txtIBANAlias.setEnabled(false);
      frmEPS.txtAddressAlias.text = data.bene_address1;
      frmEPS.tbxBankName.text = data.bankName;
      frmEPS.lblSwiftCode.text=data.swiftCode;
      frmEPS.lblBankNameStat.setVisibility(true);
      frmEPS.tbxBankName.setVisibility(true);
      frmEPS.lblBankName.setVisibility(false);
    }

  }catch(e){
    kony.print("Exception_show_SELECTED_BENEFICIARY_IPS_TRANSFER ::"+e);
  }
  validateLblNext();//OMar Alnajjar 
}
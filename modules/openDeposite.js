glbPeriod="";
glbPeriodCode="";
var interestPay="";
var matInst="";
var glbDepSelects={};
var glbCalculatorData={};
var gblopenTermDepositeMI;
var selectedBalAccount="";
var interestAmount = "";//NR!!!!
var depositDuration =  30;//NR
//Omar ALnahhar Open Deposits
var fundBranch = "";
var profBranch = "";
var renewalInd = "";

// function callRateService(isConfirmed){
//   var actpType="";
//   var effDate="";
//   if (!isEmpty(frmOpenTermDeposit.lblDepositStartDate.text) && frmOpenTermDeposit.txtDepositAmount.text !== 0 &&
//       frmOpenTermDeposit.txtDepositAmount.text!== null && !isEmpty(frmOpenTermDeposit.txtDepositAmount.text))
//   {  
//       var date=frmOpenTermDeposit.lblDepositStartDate.text.split("/");
//       //effDate = date[2]+"-"+ (parseInt(date[1])<10?"0"+date[1]:date[1]) +"-"+ (parseInt(date[0])<10?"0"+date[0]:date[0]);
//       effDate = date[2]+"-"+ date[1] +"-"+ date[0];

//       if (frmOpenTermDeposit.lblDepositCurrency.text === "JOD")
//         actpType=317;
//       else
//         actpType=318;

//       var queryParams = 
//           {
//             "currCode":frmOpenTermDeposit.lblCurrencyCode.text,
//             "valueDate": effDate,
//             "accountTYpe":actpType,
//             "amount":frmOpenTermDeposit.txtDepositAmount.text.replace(/\,/g,""),
//             "periodCode":glbDepSelects.PeriodCode,
//             "period":glbDepSelects.Period
//           };
//  kony.print("rate queryParams ::"+JSON.stringify(queryParams));
//       var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprGetFtdIntRate");

//         appMFConfiguration.invokeOperation("prGetFtdIntRate", {},queryParams,
//                                            function(res){
//           kony.print("success response ::"+JSON.stringify(res));
//           if(res.intRate !== undefined && res.intRate !== null && !isEmpty(res.intRate) && (res.errorCode === "00000")){
//           	//Omar ALnjjar Open Deposits
//             if ((frmOpenTermDeposit.btnTener6Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener1Year.skin === "sknOrangeBGRNDBOJ") && frmOpenTermDeposit.btnMonthly.skin === "sknOrangeBGRNDBOJ"){
//                             frmOpenTermDeposit.lblInterestRate.text = parseFloat(res.intRate) - 0.10;
//             }else{
//                             frmOpenTermDeposit.lblInterestRate.text = res.intRate;
//             }
//               //frmOpenTermDeposit.lblInterestRate.text = res.intRate;
//               calculateInterestAmount(frmOpenTermDeposit.txtDepositAmount.text, frmOpenTermDeposit.lblInterestRate.text, depositDuration,isConfirmed);
//           }else{
//             frmOpenTermDeposit.lblInterestRate.text =0;
//           }
//           kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();},function(err){kony.print("error in service ::"+JSON.stringify(err));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
//   }
// }


function callRateService(isConfirmed){
  var actpType="";
  var effDate="";
  var currDate= glbDepSelects.operationDate;
  if (!isEmpty(frmOpenTermDeposit.lblDepositStartDate.text) && frmOpenTermDeposit.txtDepositAmount.text !== 0 &&
      frmOpenTermDeposit.txtDepositAmount.text!== null && !isEmpty(frmOpenTermDeposit.txtDepositAmount.text))
  {  
    var date=frmOpenTermDeposit.lblDepositStartDate.text.split("/");
    //effDate = date[2]+"-"+ (parseInt(date[1])<10?"0"+date[1]:date[1]) +"-"+ (parseInt(date[0])<10?"0"+date[0]:date[0]);
    effDate = date[2]+"-"+ date[1] +"-"+ date[0];
    //     currDat1 = currDate.getFullYear() + "-" +((currDate.getMonth()+1)<10?"0"+(currDate.getMonth()+1):(currDate.getMonth()+1))+"-"+(currDate.getDate()<10?"0"+currDate.getDate():currDate.getDate());

    if (frmOpenTermDeposit.lblDepositCurrency.text === "JOD")
      actpType=317;
    else
      actpType=318;

    var queryParams = 
        {
          "P_BRCH_CODE":parseInt(fundBranch),
          "P_OPR_DATE": currDate,
          "P_VALUE_DATE":effDate,
          "P_MAIN_PERD_NUM":glbDepSelects.Period,
          "P_NUM_MAIN_PERD":glbDepSelects.PeriodCode,
          "P_CURR_CODE":frmOpenTermDeposit.lblCurrencyCode.text,
          "P_ACTP_TYPE":actpType,
          "P_CUST_ID":custid,
          "P_AMOUNT":frmOpenTermDeposit.txtDepositAmount.text.replace(/\,/g,""),
          "P_PERD_PAY_NUM":glbDepSelects.MatCode,
          "P_NUM_PAY":glbDepSelects.perd_num,
          "P_PERD_CODE":glbDepSelects.Period,// 1(days, 2 Month or 3 Year 
          "P_PERD":glbDepSelects.MatCode,// number of months 1,3,6 or 1 year
          "p_channel":"MOBILE",
          "channelUser":"BOJMOB"
        };
    kony.print("rate queryParams ::"+JSON.stringify(queryParams));
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprCreateFtdSimulation");

    appMFConfiguration.invokeOperation("prCreateFtdSimulation", {},queryParams,
                                       function(res){
      kony.print("success response ::"+JSON.stringify(res));
      //           if(res.intRate !== undefined && res.intRate !== null && !isEmpty(res.intRate) && (res.errorCode === "00000" )){
      if(res.intRate !== undefined && res.intRate !== null && !isEmpty(res.intRate) && (res.errorCode === "00000" || res.returnCode === "00000" )){

        //Omar ALnjjar Open Deposits
        //         if ((frmOpenTermDeposit.btnTener6Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener1Year.skin === "sknOrangeBGRNDBOJ") && frmOpenTermDeposit.btnMonthly.skin === "sknOrangeBGRNDBOJ"){
        //           frmOpenTermDeposit.lblInterestRate.text = parseFloat(res.intRate) - 0.10;
        //         }else{
        var matDate = res.mainMatDate.split("-");
        var interestAmount = "0.000";
        glbDepSelects.maturityDate = matDate[2] +"/" + matDate[1] + "/" + matDate[0];
        frmOpenTermDeposit.lblMatStartDate.text =  glbDepSelects.maturityDate;


        //         if (frmOpenTermDeposit.btnMonthly.skin === "sknOrangeBGRNDBOJ"){
        //           interestAmount = parseFloat(res.Interest * (depositDuration/30)).toFixed(3);
        //           frmOpenTermDeposit.lblmonthlyInterestAmountValue.text = res.Interest;
        //           frmOpenTermDeposit.flxMonthlyInterestAmount.setVisibility(true);
        //         }else{
        //           interestAmount = parseFloat(res.Interest).toFixed(3);
        //           frmOpenTermDeposit.lblmonthlyInterestAmountValue.text ="0.000";
        //           frmOpenTermDeposit.flxMonthlyInterestAmount.setVisibility(false);
        //         }
        //         frmOpenTermDeposit.lblInterestAmount.text = geti18Value("i18n.deposit.interestamount") + " " + interestAmount + " " + frmOpenTermDeposit.lblDepositCurrency.text;
        //         frmOpenTermDeposit.lblInterestAmount.setVisibility(true);        

        if (frmOpenTermDeposit.flxBorderDepositAmount.skin !== "skntextFieldDividerOrange"){
          frmOpenTermDeposit.lblInterestRate.text = res.intRate;
        }else{
          frmOpenTermDeposit.lblInterestRate.text = "0.000";
        }

        //         }
        //frmOpenTermDeposit.lblInterestRate.text = res.intRate;

        calculateInterestAmount(frmOpenTermDeposit.txtDepositAmount.text, frmOpenTermDeposit.lblInterestRate.text, depositDuration,isConfirmed);
      }else{
        glbDepSelects.maturityDate = "";
        frmOpenTermDeposit.lblInterestRate.text =0;
      }
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();},function(err){kony.print("error in service ::"+JSON.stringify(err));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();});
  }
}

function funSelectTener(tener){
  resetTenerButtons();

  var depDate = new Date();
  if (tener==="1Month")
  {
    frmOpenTermDeposit.btnTener1Month.skin="sknOrangeBGRNDBOJ";
    //glbPeriodCode=1;
    //glbPeriod=2;
    glbDepSelects.PeriodCode=1; //num-main
    glbDepSelects.Period=2;  //main-num
    glbDepSelects.tenerDesc=geti18Value("i18n.termDepodit.tener1Month");
    depositDuration = 30;

    frmOpenTermDeposit.btnMonthly.setVisibility(false);
    frmOpenTermDeposit.flxInterestPayable.setVisibility(false);
    //frmOpenTermDeposit.btnOnMaturity.centerX = "50%";

    // depDate.setMonth(parseInt(depDate.getMonth())+2);
    funInterestPayable("OnMaturity");    
  }
  else if (tener==="3Months")
  {
    //Omar ALnajjar
    //     frmOpenTermDeposit.btnMonthly.skin="slButtonBlueFocus";
    //     frmOpenTermDeposit.btnOnMaturity.skin="slButtonBlueFocus";
    frmOpenTermDeposit.btnMonthly.setVisibility(true);
    frmOpenTermDeposit.flxInterestPayable.setVisibility(true);
    //     glbDepSelects.interestPayDesc="";
    //     glbDepSelects.MatCode = "";
    //     glbDepSelects.perd_num = "";
    // frmOpenTermDeposit.btnOnMaturity.centerX = "75%";

    frmOpenTermDeposit.btnTener3Months.skin="sknOrangeBGRNDBOJ";
    //glbPeriodCode=3;
    //glbPeriod=2;
    glbDepSelects.PeriodCode=3;
    glbDepSelects.Period=2;
    glbDepSelects.tenerDesc=geti18Value("i18n.termDepodit.tener3Month");
    depositDuration = 90;
    if (frmOpenTermDeposit.btnMonthly.skin !== "sknOrangeBGRNDBOJ"){
      funInterestPayable("OnMaturity");    
    }
    //  depDate.setMonth(parseInt(depDate.getMonth())+4);
  }
  else if (tener==="6Months")
  {
    //Omar ALnajjar
    //     frmOpenTermDeposit.btnMonthly.skin="slButtonBlueFocus";
    //     frmOpenTermDeposit.btnOnMaturity.skin="slButtonBlueFocus";
    frmOpenTermDeposit.btnMonthly.setVisibility(true);
    frmOpenTermDeposit.flxInterestPayable.setVisibility(true);
    //     glbDepSelects.interestPayDesc="";
    //     glbDepSelects.MatCode = "";
    //     glbDepSelects.perd_num = "";
    // frmOpenTermDeposit.btnOnMaturity.centerX = "75%";

    frmOpenTermDeposit.btnTener6Months.skin="sknOrangeBGRNDBOJ";
    //glbPeriodCode=6;
    //glbPeriod=2;
    glbDepSelects.PeriodCode=6;
    glbDepSelects.Period=2;
    glbDepSelects.tenerDesc=geti18Value("i18n.termDepodit.tener6Month");
    depositDuration = 180;
    if (frmOpenTermDeposit.btnMonthly.skin !== "sknOrangeBGRNDBOJ"){
      funInterestPayable("OnMaturity");    
    }
    //  depDate.setMonth(parseInt(depDate.getMonth())+7);

  }
  else if (tener==="1Year")
  {
    //     //Omar ALnajjar
    //     frmOpenTermDeposit.btnMonthly.skin="slButtonBlueFocus";
    //     frmOpenTermDeposit.btnOnMaturity.skin="slButtonBlueFocus";
    //     glbDepSelects.interestPayDesc="";
    //     glbDepSelects.MatCode = "";
    //     glbDepSelects.perd_num = "";
    frmOpenTermDeposit.btnMonthly.setVisibility(true);
    frmOpenTermDeposit.flxInterestPayable.setVisibility(true);
    // frmOpenTermDeposit.btnOnMaturity.centerX = "75%";

    frmOpenTermDeposit.btnTener1Year.skin="sknOrangeBGRNDBOJ";
    //glbPeriodCode=1;
    //glbPeriod=3;
    glbDepSelects.PeriodCode=1;
    glbDepSelects.Period=3;
    glbDepSelects.tenerDesc=geti18Value("i18n.termDepodit.tener1Year");
    depositDuration = 365;
    if (frmOpenTermDeposit.btnMonthly.skin !== "sknOrangeBGRNDBOJ"){
      funInterestPayable("OnMaturity");    
    }
    //  depDate.setMonth(parseInt(depDate.getMonth())+12);
    //    depDate.setYear(parseInt(depDate.getFullYear())+1);
  }
  //Omar Alnajjar 18.02.2021 Reset selected data maturity and insterest

  //   frmOpenTermDeposit.lblMaturityInstruction.text = geti18Value("i18n.termDeposit.renewalInstruction");
  //   frmOpenTermDeposit.lblMaturityInstruction.skin = "sknLblNextDisabled";
  //   frmOpenTermDeposit.lblMaturityInstructionTitle.setVisibility(false);


  //   frmOpenTermDeposit.lblIntrestToAccount.text = geti18Value("i18.deposit.Interestaccount");
  //       frmOpenTermDeposit.lblIntrestToAccount.skin = "sknLblNextDisabled";
  //   frmOpenTermDeposit.lblIntrestToAccountTitle.setVisibility(false);

  //frmOpenTermDeposit.lblDepositStartDate.text = depDate.getDate() + "/" + (parseInt(depDate.getMonth())+1) + "/" + depDate.getFullYear();
  if (!isEmpty(frmOpenTermDeposit.txtDepositAmount.text)){
    amountFieldCheck();
  }
  if ((frmOpenTermDeposit.btnTener3Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener6Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener1Year.skin === "sknOrangeBGRNDBOJ") && 
      frmOpenTermDeposit.btnMonthly.skin === "sknOrangeBGRNDBOJ"){
    frmOpenTermDeposit.flxRenInt.setVisibility(false);
    funMatInstructions(geti18Value("i18n.termDeposit.renWithOutInterest"));
  }else{
    frmOpenTermDeposit.flxRenInt.setVisibility(true);
    funMatInstructions(geti18Value("i18n.termDeposit.renWithInterest"));
  }


  callRateService(false);
  checkNextOpenDeposite();
}

function funMatInstructions(instruction)
{
  frmOpenTermDeposit.lblMaturityInstruction.text =instruction;


  resetMatInstButtons();
  if (instruction===geti18Value("i18n.termDeposit.renWithInterest"))
  {
    glbDepSelects.matInstDesc=geti18Value("i18n.termDeposit.renWithInterest");
    glbDepSelects.renewalInd = "3";
    frmOpenTermDeposit.radioWithInter.text = "t";
    frmOpenTermDeposit.btnAccountToAddInterestTo.setVisibility(false);
    if (frmOpenTermDeposit.lblFundDeductionAccountNumber.text !==  geti18Value("i18n.termDeposit.fundDeductionAccountNumberTitle") ){
      frmOpenTermDeposit.lblIntrestToAccount.text = frmOpenTermDeposit.lblFundDeductionAccountNumber.text;
      profBranch =fundBranch;
      frmOpenTermDeposit.lblIntrestToAccountTitle.setVisibility(true);
      frmOpenTermDeposit.lblIntrestToAccount.skin = "sknLblNextDisabled";
    }


    frmOpenTermDeposit.lblMaturityInstuctionDesc.text = geti18Value("i18n.termDeposit.renWithInterest.desc");
  }
  else if (instruction===geti18Value("i18n.termDeposit.renWithOutInterest"))
  {
    glbDepSelects.matInstDesc=geti18Value("i18n.termDeposit.renWithOutInterest");
    glbDepSelects.renewalInd = "2";
    frmOpenTermDeposit.radioWithoutInter.text = "t";
    frmOpenTermDeposit.btnAccountToAddInterestTo.setVisibility(true);
    if (frmOpenTermDeposit.lblIntrestToAccount.text !== geti18Value("i18.deposit.Interestaccount"))
      frmOpenTermDeposit.lblIntrestToAccount.skin = "lblAmountCurrency";
    frmOpenTermDeposit.lblMaturityInstuctionDesc.text = geti18Value("i18n.termDeposit.renWithOutInterest.desc");

  }
  else if (instruction===geti18Value("i18n.termDeposit.close"))
  {
    glbDepSelects.matInstDesc=geti18Value("i18n.termDeposit.close");
    glbDepSelects.renewalInd = "1";
    frmOpenTermDeposit.radioMatClose.text = "t";
    frmOpenTermDeposit.btnAccountToAddInterestTo.setVisibility(true);
    frmOpenTermDeposit.lblMaturityInstuctionDesc.text = geti18Value("i18n.termDeposit.close.desc");
    if (frmOpenTermDeposit.lblIntrestToAccount.text !== geti18Value("i18.deposit.Interestaccount"))
      frmOpenTermDeposit.lblIntrestToAccount.skin = "lblAmountCurrency";
  }
  //matInst=instruction;

  checkNextOpenDeposite();
}
function getValueDate(curCode){

  var queryParams = 
      {
        "P_CURR_CODE":curCode==="JOD"?1:2
      };
  kony.print("rate queryParams ::"+JSON.stringify(queryParams));
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJFtdValueDate");

  appMFConfiguration.invokeOperation("prGetFtdValueDate", {},queryParams,
                                     function(res){
    kony.print("success response ::"+JSON.stringify(res));
    if (res.returnCode === "00000" && !isEmpty(res.valueDate) && !isEmpty(res.operationDate)){
      var valueDate = res.valueDate.split("-");
      if (gblTModule === "OpenTermDepositeCalculator"){ 

        frmDepositCalculator.lblDepositStartDate.text = valueDate[2] +"/" + valueDate[1] + "/" + valueDate[0];
        glbCalculatorData.operationDate =  res.operationDate;
        glbCalculatorData.valueDate = res.valueDate;
      }else{

        frmOpenTermDeposit.lblDepositStartDate.text = valueDate[2] +"/" + valueDate[1] + "/" + valueDate[0];
        glbDepSelects.operationDate =  res.operationDate;
        glbDepSelects.valueDate = res.valueDate;   
      }
    }else{
      customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    }
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  },function(err){

    kony.print("error in service ::"+JSON.stringify(err));
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  });

}

function funInterestPayable(type)
{
  resetInterestPayButtons();
  if (type==="Monthly")
  {
    frmOpenTermDeposit.btnMonthly.skin="sknOrangeBGRNDBOJ";
    glbDepSelects.interestPayDesc=geti18Value("i18n.termDeposit.intMonthly");
    glbDepSelects.MatCode = 1;
    glbDepSelects.perd_num = 2;
  }
  else if (type==="OnMaturity")
  {
    frmOpenTermDeposit.btnOnMaturity.skin="sknOrangeBGRNDBOJ";
    glbDepSelects.interestPayDesc=geti18Value("i18n.termDeposit.inOnMaturity");
    glbDepSelects.MatCode = glbDepSelects.PeriodCode;
    glbDepSelects.perd_num = glbDepSelects.Period;
  }
  //Omar Alnajjar Open Deposits
  //   frmOpenTermDeposit.lblMaturityInstruction.text = geti18Value("i18n.termDeposit.renewalInstruction");
  //   frmOpenTermDeposit.lblMaturityInstruction.skin = "sknLblNextDisabled";
  //   frmOpenTermDeposit.lblMaturityInstructionTitle.setVisibility(false);
  //interestPay=type;
  //glbDepSelects.interestPayDesc=type;
  if ((frmOpenTermDeposit.btnTener3Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener6Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener1Year.skin === "sknOrangeBGRNDBOJ") && 
      frmOpenTermDeposit.btnMonthly.skin === "sknOrangeBGRNDBOJ"){
    frmOpenTermDeposit.flxRenInt.setVisibility(false);
    funMatInstructions(geti18Value("i18n.termDeposit.renWithOutInterest"));
  }else{
    frmOpenTermDeposit.flxRenInt.setVisibility(true);
    funMatInstructions(geti18Value("i18n.termDeposit.renWithInterest"));
  }
  callRateService(false);
  checkNextOpenDeposite();
}


function checkNextOpenDeposite()
{var balanceAmount = 0;
 kony.print("glbDepSelects.period "+glbDepSelects.Period);

 if (!isEmpty(frmOpenTermDeposit.txtDepositAmount.text)){
   balanceAmount=parseFloat(selectedBalAccount)-parseFloat(frmOpenTermDeposit.txtDepositAmount.text.replace(/\,/g,""));

   if (frmOpenTermDeposit.txtDepositAmount.text < 10000 && frmOpenTermDeposit.lblDepositCurrency.text === "JOD")
   {
     frmOpenTermDeposit.lblMinUSD.setVisibility(false);
     frmOpenTermDeposit.lblMinJOD.setVisibility(true);
     frmOpenTermDeposit.flxBorderDepositAmount.skin="skntextFieldDividerOrange";
   }
   else if(frmOpenTermDeposit.txtDepositAmount.text < 15000 && frmOpenTermDeposit.lblDepositCurrency.text !== "JOD")    
   {
     frmOpenTermDeposit.lblMinJOD.setVisibility(false);
     frmOpenTermDeposit.lblMinUSD.setVisibility(true);
     frmOpenTermDeposit.flxBorderDepositAmount.skin="skntextFieldDividerOrange";
   }else{
     //      frmOpenTermDeposit.lblMinJOD.setVisibility(false);
     //      frmOpenTermDeposit.lblMinUSD.setVisibility(false);
     if (frmOpenTermDeposit.lblDepositCurrency.text === "JOD"){
       frmOpenTermDeposit.lblMinJOD.setVisibility(true);
       frmOpenTermDeposit.lblMinUSD.setVisibility(false);
     }
     else{
       frmOpenTermDeposit.lblMinJOD.setVisibility(false);
       frmOpenTermDeposit.lblMinUSD.setVisibility(true);
     }
     frmOpenTermDeposit.flxBorderDepositAmount.skin="skntextFieldDividerGreen";
   }

 }
 if (frmOpenTermDeposit.flxBorderDepositAmount.skin==="skntextFieldDividerGreen" &&
     frmOpenTermDeposit.lblDepositCurrency.text !==null && frmOpenTermDeposit.lblInterestRate.text !== 0.000 &&
     !isEmpty(frmOpenTermDeposit.lblInterestRate.text) &&
     !isEmpty(frmOpenTermDeposit.lblDepositStartDate.text) && //NART's CHANGES
     frmOpenTermDeposit.lblRememCheckBox.text === "p" &&
     frmOpenTermDeposit.lblMaturityInstruction.text !== geti18Value("i18n.termDeposit.renewalInstruction") &&//NART's CHANGES
     (glbDepSelects.Period!== null && glbDepSelects.Period!==undefined && !isEmpty(glbDepSelects.Period)) && 
     (glbDepSelects.interestPayDesc!== null && glbDepSelects.interestPayDesc!==undefined && !isEmpty(glbDepSelects.interestPayDesc))&&
     (glbDepSelects.matInstDesc!== null && glbDepSelects.matInstDesc!==undefined && !isEmpty(glbDepSelects.matInstDesc))&& frmOpenTermDeposit.lblIntrestToAccount.text !== geti18Value("i18.deposit.Interestaccount") &&
     balanceAmount>=0)
 {
   frmOpenTermDeposit.btnNextDeposite.setEnabled(true);
   frmOpenTermDeposit.btnNextDeposite.skin="jomopaynextEnabled";
 }
 else
 {
   frmOpenTermDeposit.btnNextDeposite.setEnabled(false);
   frmOpenTermDeposit.btnNextDeposite.skin="jomopaynextDisabled";
 }
}

function resetTenerButtons(){
  frmOpenTermDeposit.btnTener1Month.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnTener3Months.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnTener6Months.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnTener1Year.skin="slButtonBlueFocus";
}
function resetCalculatorTenerButtons(){
  frmDepositCalculator.btnTener1Month.skin="slButtonBlueFocus";
  frmDepositCalculator.btnTener3Months.skin="slButtonBlueFocus";
  frmDepositCalculator.btnTener6Months.skin="slButtonBlueFocus";
  frmDepositCalculator.btnTener1Year.skin="slButtonBlueFocus";
}

function resetMatInstButtons(){
  glbDepSelects.matInstDesc="";
  frmOpenTermDeposit.btnWithInterest.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnWithoutInterest.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnClose.skin="slButtonBlueFocus";
  frmOpenTermDeposit.radioMatClose.text = "s";
  frmOpenTermDeposit.radioWithoutInter.text = "s";
  frmOpenTermDeposit.radioWithInter.text = "s";
}

function resetInterestPayButtons()
{
  glbDepSelects.interestPayDesc="";
  glbDepSelects.MatCode = "";
  glbDepSelects.perd_num = "";
  frmOpenTermDeposit.btnMonthly.skin="slButtonBlueFocus";
  frmOpenTermDeposit.btnOnMaturity.skin="slButtonBlueFocus";
}

function goToConfirmDeposite(){
  frmOpenTermDeposit.lblConfirmationMaturityDateText.text = glbDepSelects.maturityDate;
  frmOpenTermDeposit.lblConfirmAccount.text=frmOpenTermDeposit.lblFundDeductionAccountNumber.text;
  frmOpenTermDeposit.lblConfirmDepositeAmount.text=formatAmountwithcomma(frmOpenTermDeposit.txtDepositAmount.text, 3) + " " + frmOpenTermDeposit.lblDepositCurrency.text;
  //frmOpenTermDeposit.lblConfirmDepositeAmountCurr.text=frmOpenTermDeposit.lblDepositCurrency.text;
  frmOpenTermDeposit.lblConfirmTener.text=glbDepSelects.tenerDesc;
  frmOpenTermDeposit.lblConfirmStartDepositeDate.text=frmOpenTermDeposit.lblDepositStartDate.text;
  frmOpenTermDeposit.lblConfirmProfit.text = interestAmount + " " + frmOpenTermDeposit.lblDepositCurrency.text;
  frmOpenTermDeposit.lblConfirmMonthlyAmountValue.text = frmOpenTermDeposit.lblmonthlyInterestAmountValue.text;
  frmOpenTermDeposit.lblConfirmInterestRate.text=frmOpenTermDeposit.lblInterestRate.text+"%";
  frmOpenTermDeposit.lblConfirmMaturityInstruction.text=glbDepSelects.matInstDesc;
  frmOpenTermDeposit.lblConfirmInterestPayable.text=glbDepSelects.interestPayDesc;
  frmOpenTermDeposit.lblInterestAccount.text = frmOpenTermDeposit.lblIntrestToAccount.text;
  //   frmOpenTermDeposit.lblConfirmTaxValue.text = (parseFloat(interestAmount) * 0.05 ).toFixed(3) + " " + frmOpenTermDeposit.lblDepositCurrency.text;
  frmOpenTermDeposit.lblConfirmTaxValue.text = geti18Value("i18n.deposits.TaxDesc");
  frmOpenTermDeposit.flxConfirmDeposite.setVisibility(true);
  frmOpenTermDeposit.flxDepositSelection.setVisibility(false);
  frmOpenTermDeposit.btnNextDeposite.setVisibility(false);
  //   if (frmOpenTermDeposit.btnMonthly.skin === "sknOrangeBGRNDBOJ"){
  //     frmOpenTermDeposit.flxConfirmMonthlyAmount.setVisibility(true);//Hidden Interst amount
  //   }else{
  //     frmOpenTermDeposit.flxConfirmMonthlyAmount.setVisibility(false);
  //   }

}



function serv_createDeposite()
{
  var actpType=""
  var currDate= glbDepSelects.operationDate;
  var valueDate;
  var valueDate1;
  if (frmOpenTermDeposit.lblDepositCurrency.text === "JOD")
    actpType=317;
  else
    actpType=318;

  // currDat1 = currDate.getFullYear() + "-" +((currDate.getMonth()+1)<10?"0"+(currDate.getMonth()+1):(currDate.getMonth()+1))+"-"+(currDate.getDate()<10?"0"+currDate.getDate():currDate.getDate());

  valueDate=frmOpenTermDeposit.lblDepositStartDate.text.split("/");
  valueDate1 = valueDate[2]+"-"+ valueDate[1] +"-"+ valueDate[0];

  var queryParams = 
      {
        "brchCode":parseInt(fundBranch),
        "oprDate": currDate,
        "valueDate":valueDate1,
        "currCode":frmOpenTermDeposit.lblCurrencyCode.text,
        "actpType":actpType,
        "custID":custid,
        "amount":frmOpenTermDeposit.txtDepositAmount.text.replace(/\,/g,""),
        "perdCode":glbDepSelects.Period,
        "period":glbDepSelects.PeriodCode,

        "intPeriodCode":glbDepSelects.perd_num,
        "intPeriod":glbDepSelects.MatCode,

        "intRate":frmOpenTermDeposit.lblInterestRate.text,
        "intMrg":0,
        "fundBrch":parseInt(fundBranch),//Omar ALnjjar open deposits
        "fundCacc":frmOpenTermDeposit.lblFundDeductionAccountNumber.text,
        "profBrch":parseInt(profBranch),//Omar ALnjjar open deposits
        "profCacc":frmOpenTermDeposit.lblIntrestToAccount.text,
        /* "parentBrch":1,
        "parentBrchRef":1,*/
        "renewalInd":glbDepSelects.renewalInd,
        "p_channel":"MOBILE",
        "channelUser":"BOJMOB"
      };
  kony.print("hassan queryParams-->"+ JSON.stringify(queryParams) );
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprCreateAddFtd");
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  appMFConfiguration.invokeOperation("prCreateAddFtd", {},queryParams,serv_createDeposite_success,function(err){
    kony.print("error in service ::"+JSON.stringify(err));

    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    exceptionLogCall("prCreateAddFtd","SERVICE FAILURE","SERVICE",err);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  });
}


function serv_createDeposite_success(res)
{
  try{
    kony.print("hassan success ::"+JSON.stringify(res));
    if(res.errorCode === "00000" && (res.dealBrchRef !== null && !isEmpty(res.dealBrchRef)))
    {
      gblTModule = "";
      //       frmOpenTermDeposit.flxConfirmDeposite.setVisibility(false);Omar ALnajjar Open Deposits comment
      kony.boj.populateSuccessScreen("success.png", 
                                     geti18nkey("i18n.Bene.Success"),
                                     geti18nkey("i18n.termDeposit.createSuccess"), 
                                     geti18nkey("i18n.common.CgtAD"), 
                                     geti18nkey ("i18n.common.AccountDashboard"),
                                     geti18Value("i18n.termDeposit.toDepositeScreen"),
                                     "openDeposite", "", "",geti18Value("i18n.common.ReferenceNumber") + " " + res.dealBrchRef );
    }
    else
    {
      var Message = getErrorMessage(res.returnCode);
      kony.boj.populateSuccessScreen("failure.png",
                                     geti18Value("i18n.Bene.Failed"),
                                     Message,
                                     geti18nkey("i18n.common.CgtAD"), 
                                     geti18nkey("i18n.common.AccountDashboard"),
                                     geti18Value("i18n.termDeposit.toDepositeScreen"),
                                     "openDeposite", "", "", " " );
    }
  }catch(error){
    exceptionLogCall("prCreateAddFtd","SERVICE FAILURE","SERVICE",error);
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
  }
  resetForm();//Omar ALnajjar open deposits
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

}



function setDataMaturityInstruction(data){
  frmOpenTermDeposit.lblMaturityInstruction.text = data.Maturity;
  frmOpenTermDeposit.lblMaturityInstructionTitle.setVisibility(true);
  frmOpenTermDeposit.lblMaturityInstruction.skin="lblAmountCurrency";
  funMatInstructions(data.Maturity);
  gblopenTermDepositeMI=false;
  frmOpenTermDeposit.show();
}



function callAccountsDeposite(val){

  //1960 fix
  var fromAccounts="";
  var toAccounts="";
  var accountsData="";
  kony.print("accountsData"+kony.retailBanking.globalData.accountsDashboardData.accountsData);
  if(kony.boj.siri === "fromSiri")
  {
    accountsData=kony.retailBanking.globalData.accountsDashboardData.accountsData;
  }
  else
  {
    fromAccounts = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
    toAccounts	= kony.retailBanking.globalData.accountsDashboardData.toAccounts.slice(0);
  }
  kony.print("fromAccounts :: "+fromAccounts);
  kony.print("toAccounts :: "+toAccounts);

  if(val === 1){
    if(kony.newCARD.applyCardsOption === true || kony.application.getCurrentForm().id === "frmCardLinkedAccounts" || kony.boj.selectedBillerType === "PrePaid"  || kony.boj.siri ==="fromSiri"){
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.common.accountNumber");
    }else{
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyFrom");
    }
    kony.store.setItem("toval",1);
    if((kony.application.getCurrentForm().id === "frmNewBillKA" && frmNewBillKA.btnBillsPayCards.text == "t") ||
       (kony.application.getCurrentForm().id === "frmBills" && frmBills.btnBillsPayCards.text == "t") ||
       (kony.application.getCurrentForm().id === "frmNewBillDetails" && frmNewBillDetails.btnBillsPayCards.text == "t") ){
      if(kony.retailBanking.globalData.prCreditCardPayList.length > 0){
        var tempdata = [];
        var cardData = kony.retailBanking.globalData.prCreditCardPayList;
        var cc = false;
        for(var i=0;i<cardData.length;i++){
          kony.print("cardTypeFlag ::"+cardData[i].cardTypeFlag);
          if(cardData[i].cardTypeFlag === "C"){
            cc = true;
            tempdata.push(cardData[i]);
          }
        }
        if(cc){
          accountsScreenPreshow(tempdata);
          frmAccountDetailsScreen.show();
        }
        else
          customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.nocards"),popupCommonAlertDimiss,"");
      }else
        customVerb_CARDSDETAILS();
    }else{
      if(kony.application.getCurrentForm().id === "frmCardLinkedAccounts"){
        var data = frmCardLinkedAccounts.segCardLinkedAccounts.data;
        var temp = [];
        for(var i in data){
          if(data[i].flxLinkedAccountsEnable.isVisible === true){
            for(var j in fromAccounts){
              if(fromAccounts[j].accountID === data[i].lblAccountNumber.text){
                temp.push(fromAccounts[j]);
                break;
              }
            }
          }
        }
        accountsScreenPreshow(temp);
      }
      //added for 1960
      else{ 
        if(kony.boj.siri ==="fromSiri")
        {
          kony.print("loding accounts for siri");
          accountsScreenPreshow(accountsData);
        }
        else
        {
          accountsScreenPreshow(fromAccounts);
        }
      }

      frmAccountDetailsScreen.show();
    }
    //frmNewTransferKA.flxAcc2.setEnabled(true);

  }
  if(val==2){
    kony.store.setItem("toval",0);

    if(gblTModule!=="send")
    {
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyTo");
      var i =  kony.store.getItem("toAccount");
      for(var j in toAccounts){
        if(toAccounts[j].accountID === i.accountID)
        {
          toAccounts.splice(j,1);
          break;
        }
      }
      accountsScreenPreshow(toAccounts);      
      frmAccountDetailsScreen.show();
    }
    else{
      var data = [];
      frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.Transfers.SendMoneyFrom");
      accountsScreenPreshow(fromAccounts);
      frmAccountDetailsScreen.show();
    }
    //alert(JSON.stringify("todata "+i+"index "+j+"data "+data));

  }
}


function onClickAccountDetSegDeposite(){
  try{
    var AccDetailsSegdata = frmAccountDetailsScreen.segFre.data;
    var dataSelected =  frmAccountDetailsScreen.segFre.selectedRowItems[0];
    kony.print(dataSelected.availableBalance);
    var text;
    var isNoFund = false;
    kony.print("Selected Account Data ::"+JSON.stringify(dataSelected));
    toval =  kony.store.getItem("toval");
    if(toval ===  1){
      if(dataSelected.availableBalance<=0){
        isNoFund = true;
      }
      //     }else if(parseInt(dataSelected.availableBalance).toFixed() === 0 || parseInt(dataSelected.availableBalance).toFixed() === "0"){
      //     	isNoFund = true;

      if(isNoFund && kony.newCARD.applyCardsOption === false && kony.application.getPreviousForm().id !== "frmCardLinkedAccounts"){
        customAlertPopup(geti18Value("i18n.common.Information"),geti18Value("i18n.errorCode.54"), popupCommonAlertDimiss, "", geti18Value("i18n.NUO.OKay"), "");
        isNoFund = false;
        return;
      }
    }
    kony.print("gblTModule ::"+gblTModule);

    if (gblTModule ==="OpenTermDeposite"){
      set_formOpenTermDeposite(dataSelected);
      frmOpenTermDeposit.lblFundDeductionAccountNumber.skin = "lblAmountCurrency"
      frmOpenTermDeposit.show();
      var balanceAmount=parseFloat(selectedBalAccount)-parseFloat(frmOpenTermDeposit.txtDepositAmount.text)
      if (balanceAmount<0)
      {
        kony.print("balanceeee ::"+selectedBalAccount);
        var Message=geti18Value("i18n.termDeposit.checkAmount");
        frmOpenTermDeposit.btnNextDeposite.setEnabled(false);
        frmOpenTermDeposit.btnNextDeposite.skin="jomopaynextDisabled";
        customAlertPopup(geti18Value("i18n.common.alert"), Message,popupCommonAlertDimiss,"", geti18nkey("i18n.settings.ok"), "");
      }
    }// hassan OpenTermDeposite
    else{
      set_formCreditCardPayment(dataSelected);
      frmCreditCardPayment.show();
    }
  }catch(e){
    exceptionLogCall("::onClickAccountDetailsSegment::","Exception while assigning values, on click of segment","UI",e);
  }
}
// function addBusinessDays(d,n) {
//   d = new Date(d.getTime());
//   var day = d.getDay();
//   d.setDate(d.getDate() + n + (Math.floor((n - 1 + (day % 6 || 1)) / 5) ));
//   return d;
// }
function addBusinessDays(d,n) {
  var myDate = new Date(); // Tue 22/11/2016
  myDate.setDate(myDate.getDate() + n); // Fri 25/11/2016
  return myDate;
}
/* Hassan OpenTermDeposite*/
function set_formOpenTermDeposite(dataSelected){
  try{
    kony.print("Data Selected set_formOpenTermDeposite  ::"+JSON.stringify(dataSelected));
    var name = "";
    if(dataSelected.accountName !== null && dataSelected.accountName !== undefined){
      if(dataSelected.AccNickName !== "" && dataSelected.AccNickName !== null)
        name = dataSelected.AccNickName;
      else
        name = dataSelected.accountName;
    }else{
      name = dataSelected.name_on_card;
    }

    if(gblTModule === "OpenTermDeposite"){
      //frmCardlessTransaction.lblPaymentMode.text = name;
      var todayDate = new Date();
      var numberOfDays = 1;
      selectedBalAccount=dataSelected.balance.replace(/[^0-9.]/g, '');//Omar ALnajjar Open deposits
      frmOpenTermDeposit.lblFundDeductionAccountNumber.text =dataSelected.accountID;
      frmOpenTermDeposit.lblDepositCurrency.text = dataSelected.currencyCode;
      fundBranch = dataSelected.branchNumber;
      profBranch = dataSelected.branchNumber;
      getValueDate(dataSelected.currencyCode);
      callRateService(false);
      //Omar ALnajjar OPEN Terms
      frmOpenTermDeposit.lblIntrestToAccount.text = dataSelected.accountID;
      //       frmOpenTermDeposit.lblIntrestToAccount.skin = "lblAmountCurrency";
      frmOpenTermDeposit.lblIntrestToAccountTitle.setVisibility(true);
      //       var date = "";
      //       if (dataSelected.currencyCode === "JOD"){
      //         date = addBusinessDays(todayDate,1);
      //         if (todayDate.getDay() === 4){//Thursday
      //           date = addBusinessDays(todayDate,3);
      //         }else if (todayDate.getDay() === 5){
      //           date = addBusinessDays(todayDate,2);
      //         }
      //       }else{
      //         date = addBusinessDays(todayDate,2);
      //         if (todayDate.getDay() === 3){
      //            date = addBusinessDays(todayDate,4);
      //         }
      //         else if (todayDate.getDay() === 4){//Thursday
      //           date = addBusinessDays(todayDate,4);
      //         }else if (todayDate.getDay() === 5){
      //           date = addBusinessDays(todayDate,3);
      //         }
      //       }
      //       kony.print("Data Selected addBusinessDays  ::"+JSON.stringify(date));
      //       frmOpenTermDeposit.lblDepositStartDate.text= (parseInt(date.getDate())<10?"0"+parseInt(date.getDate()):parseInt(date.getDate())) + "/" + 
      //         (parseInt(date.getMonth() + 1)<10?"0"+parseInt(date.getMonth() + 1):parseInt(date.getMonth() + 1)) +"/" + date.getFullYear();




      frmOpenTermDeposit.lblFundDeductionAccountNumberTitle.setVisibility(true);
      frmOpenTermDeposit.txtDepositAmount.setEnabled(true);
      frmOpenTermDeposit.calOpenDeposite.setEnabled(true);
      frmOpenTermDeposit.btnMaturityInstruction.setEnabled(true);
      frmOpenTermDeposit.btnAccountToAddInterestTo.setEnabled(true);
      frmOpenTermDeposit.lblCurrencyCode.text = getCurrencyCode_FROM_CURRENCY_ISO(frmOpenTermDeposit.lblDepositCurrency.text);
      //       for(var j in gblCurrList){
      //         if(frmOpenTermDeposit.lblDepositCurrency.text === gblCurrList[j].CURR_ISO){
      //           frmOpenTermDeposit.lblCurrencyCode.text=gblCurrList[j].CURR_CODE;
      //           break;
      //         }
      //       }
      //frmCardlessTransaction.lblPaymentMode.skin = "sknLblBack";
      //frmOpenTermDeposit.flxUnderlinePaymentModeBulk.skin = "sknFlxGreenLine";
      //frmCardlessTransaction.lblBranchCode.text = dataSelected.branchNumber;
      kony.store.setItem("BillPayfromAcc",dataSelected);
    }else if(gblTModule === "OpenTermDepositInterestAccount"){
      // selectedBalAccount=dataSelected.currentBalance;
      frmOpenTermDeposit.lblIntrestToAccount.text = dataSelected.accountID;
      profBranch = dataSelected.branchNumber;
      frmOpenTermDeposit.lblIntrestToAccount.skin = "lblAmountCurrency";
      frmOpenTermDeposit.lblIntrestToAccountTitle.setVisibility(true);
    }
    kony.store.setItem("BillPayfromAcc",selectedBalAccount);
    checkNextOpenDeposite();
    //checkNextCardless();
  }catch(e){
    kony.print("Exception_set_formOpenTermDeposite ::"+e);
    exceptionLogCall("set_formOpenTermDeposite","UI ERROR","UI",e);
  }
  amountFieldCheck();
}
/* Hassan OpenTermDeposite*/


//Nart
//Omar Alnajjar Terms Deposits changes
function resetForm(){
  fundBranch = 1;
  selectedBalAccount = "";
  frmOpenTermDeposit.lbldepositsTermsTXT.text = "<U>" + geti18Value("i18n.deposits.termsCheck") + "</U>";
  frmOpenTermDeposit.lblRememCheckBox.text = "q";
  frmOpenTermDeposit.lblFundDeductionAccountNumberTitle.setVisibility(false);
  frmOpenTermDeposit.lblFundDeductionAccountNumber.text = geti18Value("i18n.termDeposit.fundDeductionAccountNumberTitle");
  frmOpenTermDeposit.lblFundDeductionAccountNumber.skin = "sknLblNextDisabled";
  frmOpenTermDeposit.txtDepositAmount.text = '';
  frmOpenTermDeposit.lblMinJOD.setVisibility(false);
  frmOpenTermDeposit.lblMinUSD.setVisibility(false);
  frmOpenTermDeposit.flxBorderDepositAmount.skin = "skntextFieldDivider";
  frmOpenTermDeposit.btnTener1Month.skin = "sknOrangeBGRNDBOJ";
  glbDepSelects.PeriodCode=1;
  glbDepSelects.Period=2;
  glbDepSelects.tenerDesc=geti18Value("i18n.termDepodit.tener1Month");
  frmOpenTermDeposit.btnTener3Months.skin = "slButtonBlueFocus";
  frmOpenTermDeposit.btnTener6Months.skin = "slButtonBlueFocus";
  frmOpenTermDeposit.btnTener1Year.skin = "slButtonBlueFocus";
  frmOpenTermDeposit.lblInterestAmount.setVisibility(false);
  frmOpenTermDeposit.lblmonthlyInterestAmountValue.text = "0.000";
  frmOpenTermDeposit.flxMonthlyInterestAmount.setVisibility(false);

  depositDuration = 30;
  frmOpenTermDeposit.btnMonthly.skin = "sknOrangeBGRNDBOJ";
  //Omar Alnajjar
  glbDepSelects.interestPayDesc=geti18Value("i18n.termDeposit.inOnMaturity");
  frmOpenTermDeposit.btnOnMaturity.skin = "slButtonBlueFocus";
  frmOpenTermDeposit.lblInterestRate.text = "0.000";
  frmOpenTermDeposit.lblDepositStartDate.text = "";
  frmOpenTermDeposit.lblMatStartDate.text = "";
  frmOpenTermDeposit.lblMaturityInstruction.text = geti18Value("i18n.termDeposit.renewalInstruction");
  frmOpenTermDeposit.lblMaturityInstruction.skin = "sknLblNextDisabled";

  frmOpenTermDeposit.lblIntrestToAccountTitle.setVisibility(false);
  frmOpenTermDeposit.btnNextDeposite.skin = "jomopaynextDisabled";
  frmOpenTermDeposit.btnNextDeposite.setVisibility(true);
  frmOpenTermDeposit.flxDepositSelection.setVisibility(true);
  frmOpenTermDeposit.flxConfirmDeposite.setVisibility(false);
  frmOpenTermDeposit.lblMaturityInstructionTitle.setVisibility(false);
  //   frmOpenTermDeposit.lblIntrestToAccount.text = geti18Value("i18n.deposit.accountToAddInterestTo");
  frmOpenTermDeposit.lblIntrestToAccount.text = geti18Value("i18.deposit.Interestaccount");
  frmOpenTermDeposit.lblIntrestToAccount.skin = "sknLblNextDisabled";

  funInterestPayable("OnMaturity");
  frmOpenTermDeposit.btnMonthly.setVisibility(false);
  frmOpenTermDeposit.flxInterestPayable.setVisibility(false);
  // frmOpenTermDeposit.btnOnMaturity.centerX = "50%";

  funMatInstructions(geti18Value("i18n.termDeposit.renWithInterest"));
  frmOpenTermDeposit.lblMaturityInstuctionDesc.text = geti18Value("i18n.termDeposit.renWithInterest.desc");
  profBranch = "";
  fundBranch = "";

}
function onCLickYesBackOpenDeposits(){
  popupCommonAlertDimiss();
  frmAccountsLandingKA.show();
  resetForm();
  //   gblAccountTabModule = "GoToDepositList"; Back to landing with deals animation
  //   accountDashboardDataCall();
  frmOpenTermDeposit.destroy();
}
function amountFieldCheck(){
  kony.print("selectedBalAccount "+selectedBalAccount);
  kony.print("balance "+frmOpenTermDeposit.txtDepositAmount.text);
  var balanceAmount=parseFloat(selectedBalAccount)-parseFloat(frmOpenTermDeposit.txtDepositAmount.text);
  kony.print("balanceAmount "+balanceAmount);

  if (balanceAmount<0)
  {
    kony.print("balance55 "+frmOpenTermDeposit.txtDepositAmount.text);
    var Message=geti18Value("i18n.termDeposit.checkAmount");
    customAlertPopup(geti18Value("i18n.common.alert"), Message,popupCommonAlertDimiss,"", geti18nkey("i18n.settings.ok"), "");
    return;
  }

  if (frmOpenTermDeposit.txtDepositAmount.text < 10000 && frmOpenTermDeposit.lblDepositCurrency.text === "JOD")
  {
    frmOpenTermDeposit.lblMinUSD.setVisibility(false);
    frmOpenTermDeposit.lblMinJOD.setVisibility(true);
    frmOpenTermDeposit.flxBorderDepositAmount.skin="skntextFieldDividerOrange";
  }
  else if(frmOpenTermDeposit.txtDepositAmount.text < 15000 && frmOpenTermDeposit.lblDepositCurrency.text !== "JOD")    
  {
    frmOpenTermDeposit.lblMinJOD.setVisibility(false);
    frmOpenTermDeposit.lblMinUSD.setVisibility(true);
    frmOpenTermDeposit.flxBorderDepositAmount.skin="skntextFieldDividerOrange";
  }
  else
  {
    if (frmOpenTermDeposit.lblDepositCurrency.text === "JOD"){
      frmOpenTermDeposit.lblMinJOD.setVisibility(true);
      frmOpenTermDeposit.lblMinUSD.setVisibility(false);
    }
    else{
      frmOpenTermDeposit.lblMinJOD.setVisibility(false);
      frmOpenTermDeposit.lblMinUSD.setVisibility(true);
    }

    //Show the limitation 
    //     frmOpenTermDeposit.lblMinJOD.setVisibility(false);
    //     frmOpenTermDeposit.lblMinUSD.setVisibility(false);
    frmOpenTermDeposit.flxBorderDepositAmount.skin="skntextFieldDividerGreen";
  }
  //Omar ALnajjar Open Deposits
  if (!isEmpty(frmOpenTermDeposit.txtDepositAmount.text)){
    if (frmOpenTermDeposit.lblFundDeductionAccountNumber.text === geti18Value("i18n.termDeposit.fundDeductionAccountNumberTitle")){
      frmOpenTermDeposit.txtDepositAmount.text = fixDecimal(frmOpenTermDeposit.txtDepositAmount.text.replace(/,/g,""),3);
    }else{
      var decimal = getDecimalNumForCurr(kony.store.getItem("BillPayfromAcc").currencyCode);
      frmOpenTermDeposit.txtDepositAmount.text = fixDecimal(frmOpenTermDeposit.txtDepositAmount.text.replace(/,/g,""), decimal);
    }
  }
  if (frmOpenTermDeposit.flxBorderDepositAmount.skin !== "skntextFieldDividerOrange"){
    callRateService(false); 
  }

  checkNextOpenDeposite();
}
//Nart Interest To Account Deposit
function onClickAccountDetSegInterestToAccountDeposit(){
  try{
    var dataSelected =  frmAccountDetailsScreen.segFre.selectedRowItems[0];
    kony.print(dataSelected.availableBalance);
    kony.print("Selected Account Data ::"+JSON.stringify(dataSelected));
    kony.print("gblTModule ::"+gblTModule);
    if (gblTModule ==="OpenTermDepositInterestAccount"){
      set_formOpenTermDeposite(dataSelected);
      frmOpenTermDeposit.lblIntrestToAccount.skin = "lblAmountCurrency";
      frmOpenTermDeposit.show();
      //var balanceAmount=parseFloat(selectedBalAccount)-parseFloat(frmOpenTermDeposit.txtDepositAmount.text)
    }
  }catch(e){
    exceptionLogCall("::onClickAccountDetailsSegment::","Exception while assigning values, on click of segment","UI",e);
  }
}

//NR!!!!
function calculateInterestAmount(depositAmount, interestRate, numberOfDays,isConfirmed){
  if (!isEmpty(frmOpenTermDeposit.txtDepositAmount) && frmOpenTermDeposit.flxBorderDepositAmount.skin !== "skntextFieldDividerOrange" &&
      frmOpenTermDeposit.lblInterestRate.text !=="0.000" && depositDuration !==0 && !isEmpty(frmOpenTermDeposit.lblDepositStartDate.text)){
    depositAmount = parseFloat(depositAmount);
    interestRate = parseFloat(interestRate);
    depositDuration = parseInt(depositDuration);
    kony.print("depositAmount=" + depositAmount +"interestRate" + interestRate +"depositDuration"+ depositDuration);
    interestAmount = ((depositAmount * (interestRate/100)) * (depositDuration / 365));
    interestAmount = interestAmount.toFixed(3);
    frmOpenTermDeposit.lblInterestAmount.text = geti18Value("i18n.deposit.interestamount") + " " + parseFloat(interestAmount) + " " + frmOpenTermDeposit.lblDepositCurrency.text;
    frmOpenTermDeposit.lblInterestAmount.setVisibility(false);//Hidden Interest Amount
    if ((frmOpenTermDeposit.btnTener3Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener6Months.skin === "sknOrangeBGRNDBOJ" || frmOpenTermDeposit.btnTener1Year.skin === "sknOrangeBGRNDBOJ") && 
        frmOpenTermDeposit.btnMonthly.skin === "sknOrangeBGRNDBOJ"){
      frmOpenTermDeposit.lblmonthlyInterestAmountValue.text = (parseFloat(interestAmount) / (numberOfDays===365?12:numberOfDays/30)).toFixed(3) + " " +frmOpenTermDeposit.lblDepositCurrency.text;
      //           frmOpenTermDeposit.lblmonthlyInterestAmountValue.text = ((parseFloat(interestAmount) / 365) * 30).toFixed(3) + " " +frmOpenTermDeposit.lblDepositCurrency.text;
      frmOpenTermDeposit.flxMonthlyInterestAmount.setVisibility(false);//Hidden Interest Amount
    }else{
      frmOpenTermDeposit.lblmonthlyInterestAmountValue.text ="0.000";
      frmOpenTermDeposit.flxMonthlyInterestAmount.setVisibility(false);
    }
  }else{
    frmOpenTermDeposit.lblInterestAmount.setVisibility(false);
    frmOpenTermDeposit.flxMonthlyInterestAmount.setVisibility(false);

  }

  if (isConfirmed){
    goToConfirmDeposite();
  }
}
//!!!!
function funSelectTenerCalculator(tener){
  resetCalculatorTenerButtons();
  glbCalculatorData.tener = tener;
  var depDate = new Date();
  if (tener==="1Month")
  {
    frmDepositCalculator.btnTener1Month.skin="sknOrangeBGRNDBOJ";
    glbCalculatorData.PeriodCode=1; //num-main
    glbCalculatorData.Period=2;  //main-num
    glbCalculatorData.tenerDesc=geti18Value("i18n.termDepodit.tener1Month");
    glbCalculatorData.depositDuration = 30;
  }
  else if (tener==="3Months")
  {

    frmDepositCalculator.btnTener3Months.skin="sknOrangeBGRNDBOJ";
    glbCalculatorData.PeriodCode=3;
    glbCalculatorData.Period=2;
    glbCalculatorData.tenerDesc=geti18Value("i18n.termDepodit.tener3Month");
    glbCalculatorData.depositDuration = 90;

  }
  else if (tener==="6Months")
  {
    frmDepositCalculator.btnTener6Months.skin="sknOrangeBGRNDBOJ";
    glbCalculatorData.PeriodCode=6;
    glbCalculatorData.Period=2;
    glbCalculatorData.tenerDesc=geti18Value("i18n.termDepodit.tener6Month");
    glbCalculatorData.depositDuration = 180;
  }
  else if (tener==="1Year")
  {
    frmDepositCalculator.btnTener1Year.skin="sknOrangeBGRNDBOJ";
    glbCalculatorData.PeriodCode=1;
    glbCalculatorData.Period=3;
    glbCalculatorData.tenerDesc=geti18Value("i18n.termDepodit.tener1Year");
    glbCalculatorData.depositDuration = 365;

  }
  glbCalculatorData.interestPayDesc=geti18Value("i18n.termDeposit.inOnMaturity");
  glbCalculatorData.MatCode = glbCalculatorData.PeriodCode;
  glbCalculatorData.perd_num = glbCalculatorData.Period;
  if (!isEmpty(frmDepositCalculator.txtDepositAmount.text))
    amountFieldCheckCalculator();
}
function amountFieldCheckCalculator(){

  if (frmDepositCalculator.txtDepositAmount.text < 10000 && frmDepositCalculator.lblDepositCurrency.text === geti18Value("i18n.JODCurrency"))
  {
    frmDepositCalculator.flxBorderDepositAmount.skin="skntextFieldDividerOrange";
  }
  else if(frmDepositCalculator.txtDepositAmount.text < 15000 && frmDepositCalculator.lblDepositCurrency.text !== geti18Value("i18n.JODCurrency"))    
  {
    frmDepositCalculator.flxBorderDepositAmount.skin="skntextFieldDividerOrange";
  }
  else
  {
    frmDepositCalculator.flxBorderDepositAmount.skin="skntextFieldDividerGreen";
  }
  if (!isEmpty(frmDepositCalculator.txtDepositAmount.text)){
    frmDepositCalculator.txtDepositAmount.text = fixDecimal(frmDepositCalculator.txtDepositAmount.text.replace(/,/g,""),3);
  }
  checkCalculateDeposit();
}
function selectCalculatorCurrency(currency){
  if (currency === geti18Value("i18n.JODCurrency")){
    glbCalculatorData.currencyID = 1;
    glbCalculatorData.cuurencyKey = "JOD";
    frmDepositCalculator.lblMinNotice.text = geti18Value("18n.deposite.minmunAmountJOD");
    frmDepositCalculator.btnUSD.skin = "slButtonBlueFocus";
    frmDepositCalculator.btnJOD.skin = "sknOrangeBGRNDBOJ";
    getValueDate("JOD");
  }else{
    glbCalculatorData.currencyID = 2;
    frmDepositCalculator.lblMinNotice.text = geti18Value("18n.deposite.minmunAmountUSD");
    frmDepositCalculator.btnJOD.skin = "slButtonBlueFocus";
    frmDepositCalculator.btnUSD.skin = "sknOrangeBGRNDBOJ";
    glbCalculatorData.cuurencyKey = "USD";
    getValueDate("USD");
  }
  frmDepositCalculator.lblCurrencyCode.text = getCurrencyCode_FROM_CURRENCY_ISO(glbCalculatorData.cuurencyKey);
  frmDepositCalculator.lblDepositCurrency.text = currency;
  glbCalculatorData.currencyText = currency;
  if (!isEmpty(frmDepositCalculator.txtDepositAmount.text))
    amountFieldCheckCalculator();
}
function checkCalculateDeposit(){
  if ((glbCalculatorData.Period!== null && glbCalculatorData.Period!==undefined && !isEmpty(glbCalculatorData.Period))&&
      (glbCalculatorData.currencyID!== null && glbCalculatorData.currencyID!==undefined && !isEmpty(glbCalculatorData.currencyID))&&
      (glbCalculatorData.interestPayDesc!== null && glbCalculatorData.interestPayDesc!==undefined && !isEmpty(glbCalculatorData.interestPayDesc))&&
      frmDepositCalculator.flxBorderDepositAmount.skin==="skntextFieldDividerGreen"
     ){
    frmDepositCalculator.btnCalculateDeposits.skin = "sknbtnwhiteBGBlue";
    frmDepositCalculator.btnCalculateDeposits.setEnabled(true);
  }else{
    frmDepositCalculator.btnCalculateDeposits.skin = "sknbtnwhiteBGGray";
    frmDepositCalculator.btnCalculateDeposits.setEnabled(false);
  }
}
function resetDeposiCalculatorForm(){
  funSelectTenerCalculator("1Month");
  selectCalculatorCurrency(geti18Value("i18n.JODCurrency"));
  frmDepositCalculator.txtDepositAmount.text = "";
  frmDepositCalculator.flxBorderDepositAmount.skin = "skntextFieldDivider";
  frmDepositCalculator.btnCalculateDeposits.skin = "sknbtnwhiteBGGray";
  frmDepositCalculator.btnCalculateDeposits.setEnabled(false);
  frmDepositCalculator.flxCalculatorResults.setVisibility(false);
  frmDepositCalculator.flxBody.setVisibility(true);

}
function onCLickYesBackOpenDepositsCalculator(){
  popupCommonAlertDimiss();
  frmAccountsLandingKA.show();
  resetDeposiCalculatorForm();
  frmDepositCalculator.destroy();
}
function callDepostCalculatorService(){
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var actpType="";
  var effDate="";
  var currDate= glbCalculatorData.operationDate;
  if (!isEmpty(frmDepositCalculator.lblDepositStartDate.text) && frmDepositCalculator.txtDepositAmount.text !== 0 &&
      frmDepositCalculator.txtDepositAmount.text!== null && !isEmpty(frmDepositCalculator.txtDepositAmount.text))
  {  
    var date=frmDepositCalculator.lblDepositStartDate.text.split("/");
    effDate = date[2]+"-"+ date[1] +"-"+ date[0];

    if (frmDepositCalculator.lblDepositCurrency.text === geti18Value("i18n.JODCurrency"))
      actpType=317;
    else
      actpType=318;

    var queryParams = 
        {
          "P_BRCH_CODE":1,
          "P_OPR_DATE": currDate,
          "P_VALUE_DATE":effDate,
          "P_MAIN_PERD_NUM":glbCalculatorData.Period,
          "P_NUM_MAIN_PERD":glbCalculatorData.PeriodCode,
          "P_CURR_CODE":frmDepositCalculator.lblCurrencyCode.text,
          "P_ACTP_TYPE":actpType,
          "P_CUST_ID":custid,
          "P_AMOUNT":frmDepositCalculator.txtDepositAmount.text.replace(/\,/g,""),
          "P_PERD_PAY_NUM":glbCalculatorData.MatCode,
          "P_NUM_PAY":glbCalculatorData.perd_num,
          "P_PERD_CODE":glbCalculatorData.Period,// 1(days, 2 Month or 3 Year 
          "P_PERD":glbCalculatorData.MatCode,// number of months 1,3,6 or 1 year
          "p_channel":"MOBILE",
          "channelUser":"BOJMOB"
        };
    kony.print("rate queryParams ::"+JSON.stringify(queryParams));
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJprCreateFtdSimulation");

    appMFConfiguration.invokeOperation("prCreateFtdSimulation", {},queryParams,
                                       function(res){
      kony.print("success response ::"+JSON.stringify(res));
      //           if(res.intRate !== undefined && res.intRate !== null && !isEmpty(res.intRate) && (res.errorCode === "00000" )){
      if(res.intRate !== undefined && res.intRate !== null && !isEmpty(res.intRate) && (res.errorCode === "00000" || res.returnCode === "00000" )){
        frmDepositCalculator.lblInterestRate.text = res.intRate;
        frmDepositCalculator.lblTotalInterestAmountText.text = res.Interest; //+ " " +   glbCalculatorData.currencyText; 

        frmDepositCalculator.lblMatAmountText.text = frmDepositCalculator.txtDepositAmount.text.replace(/\,/g,"");
        frmDepositCalculator.lblMatAmountCurrency.text = glbCalculatorData.currencyText; 
        frmDepositCalculator.lblTotalInterestCurrency.text = glbCalculatorData.currencyText; 

        frmDepositCalculator.flxCalculatorResults.setVisibility(true);
        frmDepositCalculator.flxBody.setVisibility(false);
        resetForm();
        var matDate = res.mainMatDate.split("-");
        var interestAmount = "0.000";
        glbDepSelects.maturityDate = matDate[2] +"/" + matDate[1] + "/" + matDate[0];
        frmOpenTermDeposit.lblMatStartDate.text =  glbDepSelects.maturityDate;
        frmOpenTermDeposit.lblInterestRate.text = res.intRate;
      }else{
        frmDepositCalculator.lblInterestRate.text = "0.000";
        frmDepositCalculator.lblTotalInterestAmountText.text = "0.000";
      }
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();},
                                       function(err)
                                       {kony.print("error in service ::"+JSON.stringify(err));kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                                        frmDepositCalculator.lblInterestRate.text = "0.000";
                                        frmDepositCalculator.lblTotalInterestAmountText.text = "0.000";
                                       });
  }
}

function applyNowOpenDeposit(){
  getValueDate(glbCalculatorData.cuurencyKey);
  gblTModule = "OpenTermDeposite";
  frmOpenTermDeposit.txtDepositAmount.text = frmDepositCalculator.lblMatAmountText.text;
  frmOpenTermDeposit.lblDepositCurrency.text = glbCalculatorData.cuurencyKey;
  frmOpenTermDeposit.lblCurrencyCode.text = getCurrencyCode_FROM_CURRENCY_ISO(frmOpenTermDeposit.lblDepositCurrency.text);
  funSelectTener(glbCalculatorData.tener);
  funInterestPayable("OnMaturity");
  frmOpenTermDeposit.show();
  animate_NEWCARDSOPTIOS("UP", "lblDepositAmountTitle", frmOpenTermDeposit.txtDepositAmount.text);
}
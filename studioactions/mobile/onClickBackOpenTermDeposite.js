function onClickBackOpenTermDeposite(eventobject) {
    return AS_FlexContainer_b1c6acb2850a480b9f8c627242ee334d(eventobject);
}

function AS_FlexContainer_b1c6acb2850a480b9f8c627242ee334d(eventobject) {
    //Omar Alnajjar Open deposits
    if (frmOpenTermDeposit.flxConfirmDeposite.isVisible === true) {
        frmOpenTermDeposit.flxDepositSelection.setVisibility(true);
        frmOpenTermDeposit.flxConfirmDeposite.setVisibility(false);
        frmOpenTermDeposit.btnNextDeposite.setVisibility(true);
    } else {
        customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), onCLickYesBackOpenDeposits, popupCommonAlertDimiss, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
    }
}
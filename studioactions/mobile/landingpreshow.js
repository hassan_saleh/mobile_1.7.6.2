function landingpreshow(eventobject) {
    return AS_Form_cc5efc6acd8048749dd69ade1e35dc5e(eventobject);
}

function AS_Form_cc5efc6acd8048749dd69ade1e35dc5e(eventobject) {
    diffFlowFlag = "OTHERFLOW";
    frmAccountsLandingKA.flxAccntLandingNavOptKA.isVisible = false;
    accountsLandingPreShow();
    gblOtpFromforRemovingOTP = "";
    kony.store.setItem("isAppUpdated", appConfig.appVersion);
    SetupAccountslandingTalkbackFeature();
    if (frmAccountsLandingKA.flxAccountsMain.isVisible === true) {
        frmAccountsLandingKA.btnNewAccount.setVisibility(true); //hide open sub account
        //   frmAccountsLandingKA.btnNewAccount.text = "[";
        frmAccountsLandingKA.btnOpenAccout.text = geti18Value("i18n.common.openNewsubaccount");
        frmAccountsLandingKA.btnOpenAccout.setVisibility(false);
        frmAccountsLandingKA.btnDepositCalCulator.setVisibility(false);
    } else if (frmAccountsLandingKA.flxDeals.isVisible === true) {
        if (frmAccountsLandingKA.flxNoDeposits.isVisible === true) {
            frmAccountsLandingKA.btnNewAccount.setVisibility(false); //hide open sub account
            frmAccountsLandingKA.btnDepositCalCulator.right = "40dp";
        } else {
            frmAccountsLandingKA.btnNewAccount.setVisibility(true); //hide open sub account
            frmAccountsLandingKA.btnDepositCalCulator.right = "80dp";
        }
        //   frmAccountsLandingKA.btnNewAccount.setVisibility(true);//hide open sub account
        //   frmAccountsLandingKA.btnNewAccount.text = "]";
        frmAccountsLandingKA.btnOpenAccout.text = geti18Value("ii18n.termdeposite.openDeposite");
        frmAccountsLandingKA.btnOpenAccout.setVisibility(false);
        frmAccountsLandingKA.btnDepositCalCulator.setVisibility(true);
    } else {
        frmAccountsLandingKA.btnNewAccount.setVisibility(false); //hide open sub account
        //   frmAccountsLandingKA.btnNewAccount.text = "[";
        frmAccountsLandingKA.btnOpenAccout.text = geti18Value("i18n.common.openNewsubaccount");
        frmAccountsLandingKA.btnOpenAccout.setVisibility(false);
        frmAccountsLandingKA.btnDepositCalCulator.setVisibility(false);
    }
    ///Added by ahmad For set up labiba widget
    frmAccountsLandingKA.flxIconMove1.onTouchStart = chatbuttonTouchStart;
    frmAccountsLandingKA.flxIconMove1.onTouchMove = chatbuttonDashboardTouchMove;
    frmAccountsLandingKA.flxIconMove1.left = "43%";
    frmAccountsLandingKA.flxIconMove1.top = "77%";
    //GetLabibaFlag();// hassan 
    kony.print("hassan custID " + glbGetLabiba);
    if (glbGetLabiba === "T") {
        // hassan show labiba to all
        kony.print("hassan custID true");
        frmAccountsLandingKA.flxIconMove1.setVisibility(true);
    } else {
        kony.print("hassan custID false");
        frmAccountsLandingKA.flxIconMove1.setVisibility(false);
    }
}
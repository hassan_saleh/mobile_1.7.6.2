function AS_FlexContainer_OnClickBackAddBeneIPS(eventobject) {
    return AS_FlexContainer_fc5310c914d240e5be34d6e500dde36f(eventobject);
}

function AS_FlexContainer_fc5310c914d240e5be34d6e500dde36f(eventobject) {
    if (frmAddBeneIPS.flxConfirm.isVisible === true) {
        frmAddBeneIPS.flxMain.setVisibility(true);
        frmAddBeneIPS.flxConfirm.setVisibility(false);
        frmAddBeneIPS.lblNext.setVisibility(true);
    } else {
        customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseBeneIPS, onClickNoBackBeneIPS, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
    }

    function userResponseBeneIPS() {
        //   removeDatafrmTransfersformEPS();
        popupCommonAlertDimiss();
        //frmPaymentDashboard.show();
        getAllIPSBenficiaries();
        //   frmIPSManageBene.show();
        //   frmAddBeneIPS.destroy();
    }

    function onClickNoBackBeneIPS() {
        popupCommonAlertDimiss();
    }
}
function AS_FlexContainer_ea323e6592234ad0a2513ff300b2552f(eventobject) {
    if (parseFloat(frmBulkPaymentKA.lblHiddenSelBillsAmt.text) > 0) {
        frmAccountDetailsScreen.lblHead.text = "Accounts";
        gblTModule = "BulkPrePayPayment";
        gotoAccountDetailsScreen(1);
    } else {
        customAlertPopup(geti18Value("i18n.common.alert"), geti18Value("i18n.bills.bulkPaymentPay"), popupCommonAlertDimiss, "", geti18nkey("i18n.settings.ok"), "");
    }
}
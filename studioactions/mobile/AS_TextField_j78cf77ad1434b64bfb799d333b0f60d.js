function AS_TextField_j78cf77ad1434b64bfb799d333b0f60d(eventobject, changedtext) {
    //Omar Alnajjar 24/02/2021  R2P
    frmIPSRequestToPay.txtIBANAlias.text = frmIPSRequestToPay.txtIBANAlias.text.replace(/[a-z]/g, "");
    if (frmIPSRequestToPay.txtIBANAlias.text.match(/^JO/g) === null || frmIPSRequestToPay.txtIBANAlias.text.match(/^JO/g) === "") {
        frmIPSRequestToPay.flxUnderlineIBAN.skin = "sknFlxOrangeLine";
        //    frmEPS.lblNext.skin = "sknLblNextDisabled";
        frmIPSRequestToPay.lblHintIBAN.setVisibility(true);
    } else {
        if (frmIPSRequestToPay.txtIBANAlias.text.length == 30) {
            frmIPSRequestToPay.flxUnderlineIBAN.skin = "sknFlxGreyLine";
            frmIPSRequestToPay.lblHintIBAN.setVisibility(false);
        } else {
            frmIPSRequestToPay.flxUnderlineIBAN.skin = "sknFlxOrangeLine";
            frmIPSRequestToPay.btnRequest.skin = "R2PbtnSKNDisable";
            frmIPSRequestToPay.lblHintIBAN.setVisibility(true);
        }
    }
    validateRequestBtn();
}
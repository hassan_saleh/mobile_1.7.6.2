function AS_TextField_OnTextChangeTxtNickName(eventobject, changedtext) {
    return AS_TextField_bdd448379926437d9213646e0f88bb01(eventobject, changedtext);
}

function AS_TextField_bdd448379926437d9213646e0f88bb01(eventobject, changedtext) {
    if (frmAddBeneIPS.txtNickName.text.match(/^[\s]/g) !== null) {
        frmAddBeneIPS.txtNickName.text = frmAddBeneIPS.txtNickName.text.replace(/\s/g, "");
    }
    if (frmAddBeneIPS.txtNickName.text !== null && frmAddBeneIPS.txtNickName.text !== "" && frmAddBeneIPS.txtNickName.text.length > 3) {
        frmAddBeneIPS.flxNickNameDiv.skin = "sknFlxGreenLine";
    } else {
        frmAddBeneIPS.flxNickNameDiv.skin = "sknFlxOrangeLine";
        //   frmAddBeneIPS.lblNext.skin = "sknLblNextDisabled";
    }
    validateNextAddIPSBene();
}
function AS_Button_onClickNextTermDeposite(eventobject) {
    return AS_Button_g473dc801a854ad7a77ca7255ad954aa(eventobject);
}

function AS_Button_g473dc801a854ad7a77ca7255ad954aa(eventobject) {
    //Omar ALnajjar
    // goToConfirmDeposite();
    var balanceAmount = parseFloat(selectedBalAccount) - parseFloat(frmOpenTermDeposit.txtDepositAmount.text);
    if (balanceAmount < 0) {
        var Message = geti18Value("i18n.termDeposit.checkAmount");
        customAlertPopup(geti18Value("i18n.common.alert"), Message, popupCommonAlertDimiss, "", geti18nkey("i18n.settings.ok"), "");
        return;
    }
    callRateService(true);
}
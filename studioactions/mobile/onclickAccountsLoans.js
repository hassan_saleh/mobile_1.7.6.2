function onclickAccountsLoans(eventobject) {
    return AS_Button_a8b0f7792173482981b93c0dad10f950(eventobject);
}

function AS_Button_a8b0f7792173482981b93c0dad10f950(eventobject) {
    frmAccountsLandingKA.btnNewAccount.setVisibility(true); //hide open sub account
    // frmAccountsLandingKA.btnNewAccount.text = "[";
    frmAccountsLandingKA.btnDepositCalCulator.setVisibility(false);
    frmAccountsLandingKA.btnOpenAccout.text = geti18Value("i18n.common.openNewsubaccount");
    frmAccountsLandingKA.btnOpenAccout.setVisibility(false);
    if (frmAccountsLandingKA.segAccountsKA.data !== null && frmAccountsLandingKA.segAccountsKA.data.length === 1) {
        accountOnetimeCall()
    } else {
        accountsAnimattion();
    }
}
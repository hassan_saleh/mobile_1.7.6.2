function ontouchstartusernametextfieldlogin(eventobject, x, y) {
    return AS_TextField_aa9aca97808947a396553184c773c344(eventobject, x, y);
}

function AS_TextField_aa9aca97808947a396553184c773c344(eventobject, x, y) {
    try {
        var currentForm = kony.application.getCurrentForm();
        if (currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") {
            currentForm.borderBottom.skin = "skntextFieldDividerBlue";
            if (currentForm.passwordTextField.text !== null && currentForm.passwordTextField.text !== "") {
                currentForm.flxbdrpwd.skin = "skntextFieldDividerGreen";
            } else {
                currentForm.flxbdrpwd.skin = "skntextFieldDivider";
            }
            currentForm.lblClose.setVisibility(true);
            currentForm.lblClosePass.setVisibility(false);
            frmLoginKA.lblShowPass.setVisibility(false);
            currentForm.lblInvalidCredentialsKA.isVisible = false;
        }
    } catch (err) {
        kony.print("err in pwd::" + err);
    }
}
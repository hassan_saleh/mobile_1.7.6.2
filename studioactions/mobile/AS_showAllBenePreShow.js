function AS_showAllBenePreShow(eventobject) {
    return AS_Form_c3bc0f0d8a5b43569cb469fa1aecc2e9(eventobject);
}

function AS_Form_c3bc0f0d8a5b43569cb469fa1aecc2e9(eventobject) {
    kony.boj.detailsForBene.flag = false;
    kony.boj.isSearchShowBene = false;
    kony.boj.addBeneVar.selectedType = "BOJ";
    frmShowAllBeneficiary.tbxSearch.text = "";
    kony.boj.setBeneData("ALL");
    frmShowAllBeneficiary.btnAll.skin = "slButtonWhiteTab";
    frmShowAllBeneficiary.btnBOJ.skin = "slButtonWhiteTabDisabled";
    frmShowAllBeneficiary.btnDomestic.skin = "slButtonWhiteTabDisabled";
    frmShowAllBeneficiary.btnInternational.skin = "slButtonWhiteTabDisabled";
    //Omar ALnajjar trusted Bene show alert for first login after add this feature
    if (isFirstRun === true) {
        customAlertPopup(geti18Value("i18n.transfer.alertTrustTtitle"), geti18Value("i18n.transfer.alertTrustDesc"), popupCommonAlertDimiss, "", geti18Value("i18n.NUO.OKay"), "", "}");
        isFirstRun = false;
    }
}
function AS_Button_c6bbf6e8984d460ca7352f5729f4e3dd(eventobject) {
    frmIPSRequests.btnAll.skin = "slButtonWhiteTabDisabled";
    frmIPSRequests.btnIncoming.skin = "slButtonWhiteTabDisabled";
    frmIPSRequests.btnOutgoing.skin = "slButtonWhiteTab";
    frmIPSRequests.btnRequestToPay.skin = "slButtonWhiteTabDisabled";
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    loadPaymentHistory("IPS_OUTWARD");
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}
function onDoneBeneNickName(eventobject, changedtext) {
    return AS_TextField_f19f4ec953d44d89a2e1dcf53620d489(eventobject, changedtext);
}

function AS_TextField_f19f4ec953d44d89a2e1dcf53620d489(eventobject, changedtext) {
    if (!isEmpty(frmAddBeneIPS.txtNickName.text)) {
        frmAddBeneIPS.txtNickName.text = frmAddBeneIPS.txtNickName.text.trim();
        if (frmAddBeneIPS.txtNickName.text !== null && frmAddBeneIPS.txtNickName.text !== "" && frmAddBeneIPS.txtNickName.text.length > 3) {
            frmAddBeneIPS.flxNickNameDiv.skin = "sknFlxGreenLine";
        } else {
            frmAddBeneIPS.flxNickNameDiv.skin = "sknFlxOrangeLine";
        }
        validateNextAddIPSBene();
    }
}
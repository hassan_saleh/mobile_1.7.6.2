function AS_TextField_ie85758fd14148f6ba121a5d6723910d(eventobject, changedtext) {
    //Omar ALnajjar Open deposits
    // amountFieldCheck();
    //callRateService(false);
    if (frmOpenTermDeposit.txtDepositAmount.text < 10000 && frmOpenTermDeposit.lblDepositCurrency.text === "JOD") {
        frmOpenTermDeposit.lblMinUSD.setVisibility(false);
        frmOpenTermDeposit.lblMinJOD.setVisibility(true);
        frmOpenTermDeposit.flxBorderDepositAmount.skin = "skntextFieldDividerOrange";
        frmOpenTermDeposit.lblInterestRate.text = "0.000";
    } else if (frmOpenTermDeposit.txtDepositAmount.text < 15000 && frmOpenTermDeposit.lblDepositCurrency.text !== "JOD") {
        frmOpenTermDeposit.lblMinJOD.setVisibility(false);
        frmOpenTermDeposit.lblMinUSD.setVisibility(true);
        frmOpenTermDeposit.flxBorderDepositAmount.skin = "skntextFieldDividerOrange";
        frmOpenTermDeposit.lblInterestRate.text = "0.000";
    } else {
        if (frmOpenTermDeposit.lblDepositCurrency.text === "JOD") {
            frmOpenTermDeposit.lblMinJOD.setVisibility(true);
            frmOpenTermDeposit.lblMinUSD.setVisibility(false);
        } else {
            frmOpenTermDeposit.lblMinJOD.setVisibility(false);
            frmOpenTermDeposit.lblMinUSD.setVisibility(true);
        }
        //     frmOpenTermDeposit.lblMinJOD.setVisibility(false);
        //     frmOpenTermDeposit.lblMinUSD.setVisibility(false);
        frmOpenTermDeposit.flxBorderDepositAmount.skin = "skntextFieldDividerGreen";
        callRateService(false);
    }
    checkNextOpenDeposite();
}
function AS_Button_099bf61b93ef41c3bff1bf1bd00ec1f8(eventobject) {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmAddNewPayeeKA");
    var navigationObject = new kony.sdk.mvvm.NavigationObject();
    var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
    navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
    navigationObject.setRequestOptions("form", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        }
    });
    listController.performAction("navigateTo", ["frmAddNewPayeeKA", navigationObject]);
}
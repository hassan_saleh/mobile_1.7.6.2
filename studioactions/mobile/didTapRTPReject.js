function didTapRTPReject(eventobject) {
    return AS_Button_fa28a1fd29fc4eff95b3670b19477296(eventobject);
}

function AS_Button_fa28a1fd29fc4eff95b3670b19477296(eventobject) {
    //OMar ALnajjar 19/03/2021 RTP
    if (!isEmpty(frmIPSPendingRTP.tbxReasonRP.text.trim())) {
        frmIPSPendingRTP.flxUnderlineReasonRP.skin = "sknFlxGreyLine";
        customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.CLIQ.confirmDeclineRTP"), onClickYesDeclineRTP, onClickNoBackRTP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
    } else {
        frmIPSPendingRTP.flxUnderlineReasonRP.skin = "sknFlxOrangeLine";
    }
    // gblToOTPFrom = "IPSRtpOTP";
    // callRequestOTP();
}
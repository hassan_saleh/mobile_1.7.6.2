function AS_FlexContainer_a215741dde01484f90664d3097cd4588(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseESP, onClickNoBackESP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function userResponseESP() {
        removeDatafrmTransfersformEPS();
        popupCommonAlertDimiss();
        frmPaymentDashboard.show();
    }

    function onClickNoBackESP() {
        popupCommonAlertDimiss();
    }
}
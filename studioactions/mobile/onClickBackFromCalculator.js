function onClickBackFromCalculator(eventobject) {
    return AS_FlexContainer_df8ce30bdbd6424a8bea045a45df82f0(eventobject);
}

function AS_FlexContainer_df8ce30bdbd6424a8bea045a45df82f0(eventobject) {
    if (frmDepositCalculator.flxBody.isVisible === true) {
        customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), onCLickYesBackOpenDepositsCalculator, popupCommonAlertDimiss, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
    } else {
        frmDepositCalculator.flxCalculatorResults.setVisibility(false);
        frmDepositCalculator.flxBody.setVisibility(true);
        frmDepositCalculator.lblTotalInterestCurrency.text = "";
        frmDepositCalculator.lblTotalInterestAmountText.text = "0.000";
        frmDepositCalculator.lblInterestRate.text = "0.000";
        frmDepositCalculator.lblMatAmountCurrency.text = "";
        frmDepositCalculator.lblMatAmountText.text = "0.000";
    }
}
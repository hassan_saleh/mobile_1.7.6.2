function AS_TextField_passwordCardlessOnDone(eventobject, changedtext) {
    return AS_TextField_f17beaab32c2418384c9e2c50d1b592f(eventobject, changedtext);
}

function AS_TextField_f17beaab32c2418384c9e2c50d1b592f(eventobject, changedtext) {
    animateLabel("DOWN", "lblPassword", frmCardlessTransaction.tbxPassword.text);
    animateLabel("UP", "lblPassword", frmCardlessTransaction.tbxPassword.text);
    if (frmCardlessTransaction.tbxPassword.length < 6) {
        frmCardlessTransaction.flxUnderlinePassword.skin = "skntextFieldDividerOrange";
    } else {
        frmCardlessTransaction.flxUnderlinePassword.skin = "skntextFieldDivider";
    }
}
function AS_TextField_AmountFldCardLessOnDone(eventobject, changedtext) {
    return AS_TextField_a991a338c12a45ae8060a8fee3802d09(eventobject, changedtext);
}

function AS_TextField_a991a338c12a45ae8060a8fee3802d09(eventobject, changedtext) {
    frmCardlessTransaction.flxBorderAmount.skin = "skntextFieldDividerGreen";
    frmCardlessTransaction.lblHiddenAmount.text = frmCardlessTransaction.txtFieldAmount.text;
    frmCardlessTransaction.txtFieldAmount.maxTextLength = 20;
    frmCardlessTransaction.txtFieldAmount.text = formatAmountwithcomma(frmCardlessTransaction.lblHiddenAmount.text, getDecimalNumForCurr(frmCardlessTransaction.lblCurrencyCode.text));
}
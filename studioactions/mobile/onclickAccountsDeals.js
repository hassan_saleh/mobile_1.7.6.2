function onclickAccountsDeals(eventobject) {
    return AS_Button_ed3ea6423c374f07aa96c2805f82658a(eventobject);
}

function AS_Button_ed3ea6423c374f07aa96c2805f82658a(eventobject) {
    frmAccountsLandingKA.flxLoans.setVisibility(false)
    frmAccountsLandingKA.btnNewAccount.setVisibility(true); //hide open sub account
    frmAccountsLandingKA.btnDepositCalCulator.setVisibility(false);
    frmAccountsLandingKA.btnOpenAccout.text = geti18Value("i18n.common.openNewsubaccount");
    frmAccountsLandingKA.btnOpenAccout.setVisibility(false);
    // frmAccountsLandingKA.btnNewAccount.text = "[";
    if (frmAccountsLandingKA.segAccountsKA.data !== null && frmAccountsLandingKA.segAccountsKA.data.length === 1) {
        accountOnetimeCall()
    } else {
        accountsAnimattion();
    }
}
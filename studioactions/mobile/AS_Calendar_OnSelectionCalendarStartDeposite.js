function AS_Calendar_OnSelectionCalendarStartDeposite(eventobject, isValidDateSelected) {
    return AS_Calendar_adef66f9976745a4afed6cf3c8daffd4(eventobject, isValidDateSelected);
}

function AS_Calendar_adef66f9976745a4afed6cf3c8daffd4(eventobject, isValidDateSelected) {
    var field = frmOpenTermDeposit.calOpenDeposite;
    frmOpenTermDeposit.lblDepositStartDate.text = (parseInt(field.dateComponents[0]) < 10 ? "0" + field.dateComponents[0] : field.dateComponents[0]) + "/" + (parseInt(field.dateComponents[1]) < 10 ? "0" + field.dateComponents[1] : field.dateComponents[1]) + "/" + field.dateComponents[2];
    checkNextOpenDeposite();
    callRateService(false);
}
function AS_Calendar_onselectionfrommReqStmnt(eventobject, isValidDateSelected) {
    return AS_Calendar_h74d563d409144b382240b5fef7ffd4e(eventobject, isValidDateSelected);
}

function AS_Calendar_h74d563d409144b382240b5fef7ffd4e(eventobject, isValidDateSelected) {
    var field = frmRequestStatementAccounts.CalFrom;
    var date = field.dateComponents[0] < 10 ? "0" + field.dateComponents[0] : field.dateComponents[0];
    var month = field.dateComponents[1] < 10 ? "0" + field.dateComponents[1] : field.dateComponents[1];
    frmRequestStatementAccounts.lblFromDate.text = month + "/" + field.dateComponents[2];
    frmRequestStatementAccounts.lblLine2.skin = "sknFlxGreenLine";
    frmRequestStatementAccounts.lblFromService.text = month + "/" + field.dateComponents[2];
    frmRequestStatementAccounts.lblFrom.setVisibility(true);
    //frmRequestStatementAccounts.lblFromDate.text = ""; 
    //Set valid startDate for ToDate
    //var field = frmRequestStatementAccounts.CalFrom;
    //var date = field.dateComponents[0]<10?"0"+field.dateComponents[0]:field.dateComponents[0];
    //var month = field.dateComponents[1]<10?"0"+field.dateComponents[1]:field.dateComponents[1];
    //frmRequestStatementAccounts.CalTo.date = ([date+1, month, field.dateComponents[2]]);
    frmRequestStatementAccounts.lblFromDateHidee.text = month + "/" + date + "/" + field.dateComponents[2];
    frmRequestStatementAccounts.CalTo.validStartDate = ([date, month, field.dateComponents[2]]);
    var currDate = new Date();
    frmRequestStatementAccounts.CalTo.validEndDate = [currDate.getDate(), currDate.getMonth() + 1, currDate.getFullYear()];
    nextSkinCheckForRequestStaement();
}
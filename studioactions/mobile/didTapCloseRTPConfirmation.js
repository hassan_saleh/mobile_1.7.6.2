function didTapCloseRTPConfirmation(eventobject) {
    return AS_Button_e0435da60eba443092a36d2daab4aeb9(eventobject);
}

function AS_Button_e0435da60eba443092a36d2daab4aeb9(eventobject) {
    //Omar ALnajjar R2P 23/02/2021
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), onClickYesBackRTP, onClickNoBackRTP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
}
function AS_TextField_OnTextChangeBeneNameAddBene(eventobject, changedtext) {
    return AS_TextField_b8556a74cb6740fbb56835b97d4d99b1(eventobject, changedtext);
}

function AS_TextField_b8556a74cb6740fbb56835b97d4d99b1(eventobject, changedtext) {
    if (frmAddBeneIPS.txtBeneName.text.length < 3) {
        frmAddBeneIPS.flxBeneNameDiv.skin = "sknFlxOrangeLine";
        frmAddBeneIPS.lblNext.skin = "sknLblNextDisabled";
    } else {
        frmAddBeneIPS.flxBeneNameDiv.skin = "sknFlxGreenLine";
    }
    validateNextAddIPSBene();
}
function IPSReturnPayment_BAck_onClick(eventobject) {
    return AS_FlexContainer_a5e271f14bcc439e9a72cde792ebd091(eventobject);
}

function AS_FlexContainer_a5e271f14bcc439e9a72cde792ebd091(eventobject) {
    //Mai 11/12/2020
    //   removeDatafrmTransfersformEPS();
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseIPSReturn, onClickNoBackIPSReturn, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function userResponseIPSReturn() {
        frmIPSRequests.lblNextRP.skin = "CopysknLblNextDisabled";
        ResetFormIPSData();
        popupCommonAlertDimiss();
        //frmPaymentDashboard.show();
        frmIPSRequests.show();
        gblTModule = "";
        frmIPSRequests.flxReturnPayment.setVisibility(false);
        frmIPSRequests.flxRequestToPay.setVisibility(false);
        frmIPSRequests.flxConfirmEPS.setVisibility(false);
        frmIPSRequests.flxPaymentHistory.setVisibility(true);
        frmIPSRequests.skin = "CopyslFormCommon0aaa3928efb3d48";
    }

    function onClickNoBackIPSReturn() {
        popupCommonAlertDimiss();
    }
}
function txtemail1onDone(eventobject, changedtext) {
    return AS_TextField_d898582466cd4e7192a1901693dc903a(eventobject, changedtext);
}

function AS_TextField_d898582466cd4e7192a1901693dc903a(eventobject, changedtext) {
    try {
        frmEstatementLandingKA.txtEmail1.text = frmEstatementLandingKA.txtEmail1.text.trim();
        if (frmEstatementLandingKA.txtEmail1.text !== null && frmEstatementLandingKA.txtEmail1.text !== "" && isValidEmaill(frmEstatementLandingKA.txtEmail1.text)) {
            frmEstatementLandingKA.txtEmail2.text = frmEstatementLandingKA.txtEmail2.text.trim();
            if (frmEstatementLandingKA.txtEmail2.text == null || frmEstatementLandingKA.txtEmail2.text == "") {
                frmEstatementLandingKA.borderBottom1.skin = "skntextFieldDividerGreen";
                frmEstatementLandingKA.lblInvalidEmail1.setVisibility(false);
                frmEstatementLandingKA.borderBottom2.skin = "skntextFieldDividerGreen";
                frmEstatementLandingKA.lblInvalidEmail2.setVisibility(false);
                if (frmEstatementLandingKA.flxSwitchOnTouchLogin.isVisible && frmEstatementLandingKA.lblTermsandConditionsCheckBox.text === "p") frmEstatementLandingKA.lblNext.skin = "sknLblNextEnabled";
                else frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
            } else {
                if (isValidEmaill(frmEstatementLandingKA.txtEmail2.text)) {
                    frmEstatementLandingKA.borderBottom1.skin = "skntextFieldDividerGreen";
                    frmEstatementLandingKA.lblInvalidEmail1.setVisibility(false);
                    frmEstatementLandingKA.borderBottom2.skin = "skntextFieldDividerGreen";
                    frmEstatementLandingKA.lblInvalidEmail2.setVisibility(false);
                    if (frmEstatementLandingKA.flxSwitchOnTouchLogin.isVisible && frmEstatementLandingKA.lblTermsandConditionsCheckBox.text === "p") frmEstatementLandingKA.lblNext.skin = "sknLblNextEnabled";
                    else frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
                } else {
                    frmEstatementLandingKA.borderBottom2.skin = "skntextFieldDivider";
                    frmEstatementLandingKA.lblInvalidEmail2.text = geti18Value("i18n.Estmt.invalidEmail");
                    frmEstatementLandingKA.lblInvalidEmail2.setVisibility(true);
                    frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
                }
            }
        } else {
            frmEstatementLandingKA.borderBottom1.skin = "skntextFieldDivider";
            frmEstatementLandingKA.lblInvalidEmail1.text = geti18Value("i18n.Estmt.invalidEmail");
            frmEstatementLandingKA.lblInvalidEmail1.setVisibility(true);
            frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
        }
    } catch (err) {
        kony.print("err in pwd::" + err);
    }
}
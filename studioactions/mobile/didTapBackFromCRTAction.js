function didTapBackFromCRTAction(eventobject) {
    return AS_FlexContainer_f30bebb864754a6499f71b857d21aa6e(eventobject);
}

function AS_FlexContainer_f30bebb864754a6499f71b857d21aa6e(eventobject) {
    //Omar ALnajjar 22/03/2021 Create ALias
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), onCLickYesBackCRT, popupCommonAlertDimiss, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
}
function IPS_PaymentHistory_RequestToPayOnCLick(eventobject) {
    return AS_Button_c7c5c2d47d1c4ef78db8777e0b657b56(eventobject);
}

function AS_Button_c7c5c2d47d1c4ef78db8777e0b657b56(eventobject) {
    frmIPSRequests.btnAll.skin = "slButtonWhiteTabDisabled";
    frmIPSRequests.btnIncoming.skin = "slButtonWhiteTabDisabled";
    frmIPSRequests.btnOutgoing.skin = "slButtonWhiteTabDisabled";
    frmIPSRequests.btnRequestToPay.skin = "slButtonWhiteTab";
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    loadPaymentHistory("IPS_RTP_INWARD");
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}
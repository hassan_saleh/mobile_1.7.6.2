function AS_Label_OnTouchEndBackAddBeneIPS(eventobject, x, y) {
    return AS_Label_a2109694f62643c0a7a7d36ee6f96d18(eventobject, x, y);
}

function AS_Label_a2109694f62643c0a7a7d36ee6f96d18(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseBeneIPS, onClickNoBackBeneIPS, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function userResponseBeneIPS() {
        //   removeDatafrmTransfersformEPS();
        frmAddBeneIPS.destroy();
        popupCommonAlertDimiss();
        //frmPaymentDashboard.show();
        frmIPSManageBene.show();
    }

    function onClickNoBackBeneIPS() {
        popupCommonAlertDimiss();
    }
}
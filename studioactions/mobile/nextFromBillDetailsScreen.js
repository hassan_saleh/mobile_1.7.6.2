function nextFromBillDetailsScreen(eventobject, x, y) {
    return AS_Label_dbff6c18e44d4e3fb2ffe3aaa62c1623(eventobject, x, y);
}

function AS_Label_dbff6c18e44d4e3fb2ffe3aaa62c1623(eventobject, x, y) {
    if (validate_SufficientBalance()) {
        if (gblQuickFlow === "post") {
            if (validate_MINMAXAMOUNTBILLPAYMENT() === "min") {
                var minAmount = "";
                minAmount = geti18Value("i18n.billPaymentminAmount") + " (" + frmNewBillDetails.minAmount.text + ")";
                customAlertPopup(geti18Value("i18n.maps.Info"), minAmount, popupCommonAlertDimiss, "");
            } else if (validate_MINMAXAMOUNTBILLPAYMENT() === "max") {
                var maxAmount = "";
                maxAmount = geti18Value("i18n.billPaymentmaxAmount") + " (" + frmNewBillDetails.maxAmount.text + ")";
                customAlertPopup(geti18Value("i18n.maps.Info"), maxAmount, popupCommonAlertDimiss, "");
            } else {
                var x = nextFromNewDetailsScreen();
                if (x == 1) {
                    assignDatatoConfirmPostpaid();
                    if (frmNewBillDetails.btnBillsPayCards.text === "s") {
                        serv_BILLSCOMISSIONCHARGE(frmNewBillDetails.tbxAmount.text.replace(/,/g, ""));
                    } else if (frmNewBillDetails.btnBillsPayCards.text === "t") {
                        serv_BILLSCOMISSIONCHARGE(frmNewBillDetails.tbxAmount.text.replace(/,/g, "")); // hassan 14/02/2021
                        //frmConfirmPayBill.show();
                    }
                    //     nextfrmPostPaidQuickPay();
                } else {}
            }
        } else {
            if (frmNewBillDetails.btnBillsPayCards.text === "s") serv_BILLSCOMISSIONCHARGE(frmNewBillKA.tbxAmount.text.replace(/,/g, ""));
            //   nextfrmPrePaidQuickPay();
            assignDatatoConfirmPrepaid();
        }
        QuickFlowprev = "now";
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
    }
}
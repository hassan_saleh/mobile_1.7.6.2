function onCLickCloseCRTConfirm(eventobject) {
    return AS_Button_je4a6401c98c43e1ae15ef8eb1ddfe32(eventobject);
}

function AS_Button_je4a6401c98c43e1ae15ef8eb1ddfe32(eventobject) {
    //Omar ALnajjar 22/03/2021 Create ALias
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), onCLickYesBackCRT, popupCommonAlertDimiss, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
}
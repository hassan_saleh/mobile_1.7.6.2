function AS_TextField_onTextChangeTxtAddressAddBene(eventobject, changedtext) {
    return AS_TextField_b359980e87ae41908b7bd53802a0b9f0(eventobject, changedtext);
}

function AS_TextField_b359980e87ae41908b7bd53802a0b9f0(eventobject, changedtext) {
    if (frmAddBeneIPS.txtAddress.text.length < 3) {
        frmAddBeneIPS.flxAddressDiv.skin = "sknFlxOrangeLine";
        frmAddBeneIPS.lblNext.skin = "sknLblNextDisabled";
    } else {
        frmAddBeneIPS.flxAddressDiv.skin = "sknFlxGreenLine";
    }
    validateNextAddIPSBene();
}
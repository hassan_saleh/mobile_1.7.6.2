function mobileEndEditingIOSCRT(eventobject, changedtext) {
    return AS_TextField_c11bed79175a41debdff0ffe9627143c(eventobject, changedtext);
}

function AS_TextField_c11bed79175a41debdff0ffe9627143c(eventobject, changedtext) {
    //Omar ALnajjar 22/03/2021 Create ALias
    animateLabel("DOWN", "lblAliasNameMob", frmIPSCreateAlias.txtAliasNameMob.text);
    frmIPSCreateAlias.txtAliasNameMob.text = formatAsInternationalNumber(frmIPSCreateAlias.txtAliasNameMob.text);
    validateCreateAlias();
}
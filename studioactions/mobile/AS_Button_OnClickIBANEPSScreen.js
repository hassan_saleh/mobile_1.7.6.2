function AS_Button_OnClickIBANEPSScreen(eventobject) {
    return AS_Button_h05db8b9899c48469a3e8409ea660b75(eventobject);
}

function AS_Button_h05db8b9899c48469a3e8409ea660b75(eventobject) {
    if (!transferFromBene) {
        // kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        frmEPS.flxDetailsAliasScroll.setVisibility(false);
        frmEPS.flxDetailsMobileScroll.setVisibility(false);
        frmEPS.flxDetailsIBAN.setVisibility(true);
        if (frmEPS.btnIBAN.skin === slButtonBlueFocus) {
            frmEPS.lblNext.skin = "sknLblNextDisabled";
            frmEPS.btnIBAN.skin = sknOrangeBGRNDBOJ;
            frmEPS.btnAlias.skin = slButtonBlueFocus;
            frmEPS.btnMob.skin = slButtonBlueFocus;
        } else {
            frmEPS.btnAlias.skin = slButtonBlueFocus;
            frmEPS.btnIBAN.skin = sknOrangeBGRNDBOJ;
        }
        // reset the other flows 
        // Reset Alias Details //
        frmEPS.txtAliasName.text = "";
        frmEPS.txtAmountAlias.text = "";
        frmEPS.lblIBANAliastxt.text = "";
        frmEPS.lbAliasAddresstxt.text = "";
        frmEPS.lblAliasBanktxt.txt = "";
        frmEPS.flxBeneName.setVisibility(false);
        frmEPS.flxAmountAlias.setVisibility(false);
        frmEPS.flxIBANAliass.setVisibility(false);
        frmEPS.flxAliasAddress.setVisibility(false);
        frmEPS.flxAliasBank.setVisibility(false);
        // Reset Alias Details //
        // Reset Mob Details //
        frmEPS.txtAliasNameMob.text = "";
        frmEPS.txtAmountMob.text = "";
        frmEPS.lblIBANMobtxt.text = "";
        frmEPS.lblAddressMobtxt.text = "";
        frmEPS.lblBankMobAliastxt.text = "";
        frmEPS.flxBeneNameMob.setVisibility(false);
        frmEPS.flxAmountMob.setVisibility(false);
        frmEPS.flxIbanDetailsMob.setVisibility(false);
        frmEPS.flxAddressMob.setVisibility(false);
        frmEPS.flxBankMob.setVisibility(false);
        // Reset Mob Details //
    }
}
function frmLanguagePostShow(eventobject) {
    return AS_Form_f5aec9db43a34c5fb6d58064f8999ebb(eventobject);
}

function AS_Form_f5aec9db43a34c5fb6d58064f8999ebb(eventobject) {
    if (kony.sdk.isNetworkAvailable()) {
        //Do not do anything and let the app work as is.
        deviceid = getCurrentDeviceId();
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.common.appalaunchnointernet"), popupCommonAlertDimissclose, "");
    }
    var langFlag = kony.store.getItem("langFlagSettingObj");
    if (langFlag) {
        if (gblPayAppFlow && !isEmpty(gblLaunchModeOBJ.appFlow)) {
            kony.print("languagetologin::");
            gblFromModule = "ForgotPassword";
            gblReqField = "Username";
            initialiseAndCallEnroll();
            frmLoginKA.passwordTextField.text = "";
        }
    }
}
function onclickLoansAcc(eventobject) {
    return AS_Button_d9d4d61dae1a423aa29b8e3fff52f073(eventobject);
}

function AS_Button_d9d4d61dae1a423aa29b8e3fff52f073(eventobject) {
    frmAccountsLandingKA.flxDeals.setVisibility(false);
    if (frmAccountsLandingKA.segAccountsKALoans.data !== null && frmAccountsLandingKA.segAccountsKALoans.data.length === 1) {
        LoanOnetimeCall();
    } else {
        loansAnimation();
    }
    frmAccountsLandingKA.btnNewAccount.setVisibility(false); //hide open sub account
    //       frmAccountsLandingKA.btnNewAccount.text = "[";
    frmAccountsLandingKA.btnOpenAccout.text = geti18Value("i18n.common.openNewsubaccount");
    frmAccountsLandingKA.btnOpenAccout.setVisibility(false);
    frmAccountsLandingKA.btnDepositCalCulator.setVisibility(false);
}
function AS_FlexContainer_db6c48b0e1c5490cbbb20d6661b9c872(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseESP, onClickNoBackESP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function userResponseESP() {
        //   removeDatafrmTransfersformEPS();
        ResetFormIPSData();
        popupCommonAlertDimiss();
        frmPaymentDashboard.show();
    }

    function onClickNoBackESP() {
        popupCommonAlertDimiss();
    }
}
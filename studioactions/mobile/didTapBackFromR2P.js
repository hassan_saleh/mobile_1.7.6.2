function didTapBackFromR2P(eventobject) {
    return AS_FlexContainer_hf1c4bc952db4c94ba83f457a747858e(eventobject);
}

function AS_FlexContainer_hf1c4bc952db4c94ba83f457a747858e(eventobject) {
    //Omar ALnajjar R2P 23/02/2021
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseESP, onClickNoBackESP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function userResponseESP() {
        resetR2PForm();
        popupCommonAlertDimiss();
        frmIPSManageBene.show();
    }

    function onClickNoBackESP() {
        popupCommonAlertDimiss();
    }
}
function didTapBackFromCRT(eventobject) {
    return AS_FlexContainer_ged558a7d7c1480bb21e575d50c742b5(eventobject);
}

function AS_FlexContainer_ged558a7d7c1480bb21e575d50c742b5(eventobject) {
    //Omar ALnajjar 22/03/2021 Create ALias
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), onCLickYesBackCRT, popupCommonAlertDimiss, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
}
function EPS_ReqUestToPay_PreShow(eventobject) {
    return AS_Form_c32dc7a986c940f69bc278f83ef6232e(eventobject);
}

function AS_Form_c32dc7a986c940f69bc278f83ef6232e(eventobject) {
    // frmEPS.txtAmountIBAN.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE; // Edit by ahmad 12-17-2020
    // frmEPS.txtIBANAlias.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE; // Edit by ahmad 12-17-2020
    // frmEPS.txtAddressAlias.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE; // Edit by ahmad 12-17-2020
    // frmEPS.txtAmountMob.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE; // Edit by ahmad 12-17-2020
    // frmEPS.txtAliasName.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE; // Edit by ahmad 12-17-2020
    // frmEPS.txtAliasNameMob.keyboardActionLabel=constants.TEXTBOX_KEYBOARD_LABEL_DONE; // Edit by ahmad 12-17-2020
    //Mai 5/1/2021
    /*frmEPS.flxAccounFrom.setVisibility(true);
    frmEPS.lblSplit1.setVisibility(true);
    frmEPS.flxRPRequestReason.setVisibility(false);
    */
    if (gblTModule === "CLIQRequest" || gblTModule === "CLIQPay") {
        frmEPS.flxAccounFrom.setVisibility(false);
        frmEPS.lblSplit1.setVisibility(false);
        frmEPS.flxRPRequestReason.setVisibility(true);
    }
}
function AS_Image_h9f117c2109f4e76a16eeb3f647cd29f(eventobject, x, y) {
    isScanCard = true;
    if (isAndroid()) {
        askPermission();
    } else {
        var iPhoneMicroBlinkObject = new BojScan.iPhoneMicroBlink(SCANCARDLICENSEIOS);
        //Invokes method 'scanCardWithCallback' on the object
        iPhoneMicroBlinkObject.scanCardWithCallback(
            /**Function*/
            microBlinkCallBack);
    }
    //kony.timer.schedule("scanCardTimer",scanCardTimerFn, 15, false);
}
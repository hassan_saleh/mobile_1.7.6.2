function onclickLoansDeals(eventobject) {
    return AS_Button_fc9a052b4171486e92240db2f2cbe8d6(eventobject);
}

function AS_Button_fc9a052b4171486e92240db2f2cbe8d6(eventobject) {
    frmAccountsLandingKA.btnNewAccount.setVisibility(false); //hide open sub account
    //       frmAccountsLandingKA.btnNewAccount.text = "[";
    frmAccountsLandingKA.btnOpenAccout.text = geti18Value("i18n.common.openNewsubaccount");
    frmAccountsLandingKA.btnOpenAccout.setVisibility(false);
    frmAccountsLandingKA.btnDepositCalCulator.setVisibility(false);
    if (frmAccountsLandingKA.segAccountsKALoans.data !== null && frmAccountsLandingKA.segAccountsKALoans.data.length === 1) {
        LoanOnetimeCall();
    } else {
        loansAnimation();
    }
}
function AS_Button_OnClickAddCliQBene(eventobject) {
    return AS_Button_je3617faa99e41968fbb8f228362a47c(eventobject);
}

function AS_Button_je3617faa99e41968fbb8f228362a47c(eventobject) {
    //onClickLocateUS(isLoggedIn,"frmMore");
    frmAddBeneIPS.destroy();
    frmAddBeneIPS.show();
    frmAddBeneIPS.btnAlias.skin = "sknOrangeBGRNDBOJ";
    frmAddBeneIPS.txtBankName.setEnabled(false);
    frmAddBeneIPS.txtBeneName.setEnabled(false);
    frmAddBeneIPS.txtIBAN.setEnabled(false);
    frmAddBeneIPS.txtAddress.setEnabled(false);
    animateLabel("DOWN", "lblIBANTitle", frmAddBeneIPS.txtIBAN.text);
    animateLabel("DOWN", "lblBeneNameTitle", frmAddBeneIPS.txtBeneName.text);
    animateLabel("DOWN", "lblAddressTitle", frmAddBeneIPS.txtAddress.text);
    animateLabel("DOWN", "lblBankNameTitle", frmAddBeneIPS.txtBankName.text);
}
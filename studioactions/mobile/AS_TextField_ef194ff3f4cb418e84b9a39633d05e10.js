function AS_TextField_ef194ff3f4cb418e84b9a39633d05e10(eventobject, changedtext) {
    frmCardlessTransaction.flxBorderAmount.skin = "skntextFieldDividerGreen";
    frmCardlessTransaction.lblHiddenAmount.text = frmCardlessTransaction.txtFieldAmount.text;
    frmCardlessTransaction.txtFieldAmount.maxTextLength = 20;
    frmCardlessTransaction.txtFieldAmount.text = formatAmountwithcomma(frmCardlessTransaction.lblHiddenAmount.text, getDecimalNumForCurr(frmCardlessTransaction.lblCurrencyCode.text));
}
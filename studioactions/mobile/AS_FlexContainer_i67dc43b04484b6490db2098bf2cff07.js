function AS_FlexContainer_i67dc43b04484b6490db2098bf2cff07(eventobject) {
    if (frmEPS.lblNext.skin === "sknLblNextEnabled") {
        if (gblModule == "CLIQ") {
            if (validate_SufficientBalance()) {
                kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
                SetupConfirmationScreen();
                //   GetAliasInfo();
                frmEPS.flxMain.setVisibility(false);
                frmEPS.flxConfirmEPS.setVisibility(true);
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            } else {
                customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
            }
        } else { // Mai 26/11/2020 if IPSRequest
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            SetupConfirmationScreen();
            //   GetAliasInfo();
            frmEPS.flxMain.setVisibility(false);
            frmEPS.flxConfirmEPS.setVisibility(true);
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.jomopay.fillDetails"), popupCommonAlertDimiss, "");
    }
}
function AS_onRowClickSelectDetail(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_ab0e4baf7fc742c0b6906a3adcaf8ba4(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_ab0e4baf7fc742c0b6906a3adcaf8ba4(eventobject, sectionNumber, rowNumber) {
    //alert(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
    if (kony.newCARD.applyCardsOption === true) {
        selected_OPTIONS_APPLYNEWCARDS(frmSelectDetailsBene.segDetails.selectedItems[0], kony.boj.selectedDetailsType);
    } else if (gblWorkflowType === "IPS") {
        setupBanknameIPS(frmSelectDetailsBene.segDetails.selectedItems[0]);
    }
    //Omar ALnajjar 23/02/2021 R2P
    else if (gblWorkflowType === "IPSR2PBanks") {
        setupBanknameIPSR2P(frmSelectDetailsBene.segDetails.selectedItems[0]);
    }
    //Omar ALnajjar Add Bene
    else if (gblWorkflowType === "IPSAddBeneBank") {
        setDataAddBeneIPS(frmSelectDetailsBene.segDetails.selectedItems[0]);
    } else if (gblWorkflowType === "IPSReturn") { //Mai 11/1/2021
        kony.print("Return reason : " + JSON.stringify(frmSelectDetailsBene.segDetails.selectedItems[0]));
        setupReturnPaymentReasonIPS(frmSelectDetailsBene.segDetails.selectedItems[0]);
    }
    //Omar ALnajjar 23/02/2021 R2P
    else if (gblWorkflowType === "IPSR2P") {
        kony.print("Request reason : " + JSON.stringify(frmSelectDetailsBene.segDetails.selectedItems[0]));
        setupRequestPaymentReasonIPS(frmSelectDetailsBene.segDetails.selectedItems[0]);
    }
    //Omar ALnajjar 21/03/2021 R2P
    else if (gblWorkflowType === "IPSAcsRejR2P") {
        kony.print("Request reason : " + JSON.stringify(frmSelectDetailsBene.segDetails.selectedItems[0]));
        setupAcsRejReasonRTP(frmSelectDetailsBene.segDetails.selectedItems[0]);
    } else {
        if (gblsubaccBranchList) {
            setDatatoCheckOrderBranchList(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        } else if (gblrequestStatementBranch) {
            setDatatoRequestStatementBranchList(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        } else if (gblRequestNewPinBranchList) {
            setDatatoRequestNewPinBranchList(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        } else if (gblRequestStatementCrediCards) {
            setDatatoRequestStatementBranchList(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        } else if (gblopenTermDepositeMI) { // hassan open deposite
            setDataMaturityInstruction(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        } else {
            kony.boj.updateLabel(frmSelectDetailsBene.segDetails.selectedRowItems[0]);
        }
    }
}
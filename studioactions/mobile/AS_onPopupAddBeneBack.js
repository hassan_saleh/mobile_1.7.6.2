function AS_onPopupAddBeneBack(eventobject) {
    return AS_FlexContainer_e39ab003e52d4eb1a6335efa1d42442a(eventobject);
}

function AS_FlexContainer_e39ab003e52d4eb1a6335efa1d42442a(eventobject) {
    // frmAddExternalAccountKA.flxConfirmPopUp.isVisible = false;
    // frmAddExternalAccountKA.flxFormMain.isVisible = true;
    if (gblFromModule === "addBeneficiaryAsFav" || isAddAsTrusted === true) {
        // removeDatafrmTransfersform();
        makeInlineErrorsTransfersInvisible();
        gblTModule = "send";
        getAllAccounts();
        //Omar ALnajjar Available Limits
        frmNewTransferKA.btnHelp.setVisibility(true);
        //Omar 3/25/2021 Clear form
        removeDatafrmTransfersform();
    } else {
        frmAddExternalAccountKA.flxConfirmPopUp.isVisible = false;
        frmAddExternalAccountKA.flxFormMain.isVisible = true;
    }
}
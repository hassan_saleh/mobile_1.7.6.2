function AS_TextField_OnTextChangAliasMobAddBene(eventobject, changedtext) {
    return AS_TextField_fcdbb5d043f645d0ba6867c384b92efb(eventobject, changedtext);
}

function AS_TextField_fcdbb5d043f645d0ba6867c384b92efb(eventobject, changedtext) {
    var isValid = validate_IPSBene_ENTRED_VALUE(frmAddBeneIPS.txtAliasMob.text, frmAddBeneIPS.txtAliasMob);
    if (isValid) {
        frmAddBeneIPS.flxAliasMobDivider.skin = "sknFlxGreenLine";
    } else {
        frmAddBeneIPS.flxAliasMobDivider.skin = "sknFlxOrangeLine";
        frmAddBeneIPS.lblNext.skin = "sknLblNextDisabled";
    }
    validateNextAddIPSBene(); //OMar ALnajjar
}
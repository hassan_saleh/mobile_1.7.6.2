function didTapDeleteAlias(eventobject, context) {
    return AS_Button_a0bdf5c0ceef4c1a88711c3f868a0f7f(eventobject, context);
}

function AS_Button_a0bdf5c0ceef4c1a88711c3f868a0f7f(eventobject, context) {
    //Oةar ALnajjar 22/03/2021
    if (context.widgetInfo.data.length === 1) {
        customAlertPopup(geti18Value("i18n.common.Information"), geti18Value("i18n.CLIQ.deleteNote"), popupCommonAlertDimiss, "", geti18Value("i18n.NUO.OKay"), "");
    } else {
        deleteALias(context.widgetInfo.selectedRowItems[0]);
    }
}
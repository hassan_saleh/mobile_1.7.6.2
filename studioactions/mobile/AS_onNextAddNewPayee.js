function AS_onNextAddNewPayee(eventobject, x, y) {
    return AS_Label_ef9b03e39df748ff8610232ad085c6e1(eventobject, x, y);
}

function AS_Label_ef9b03e39df748ff8610232ad085c6e1(eventobject, x, y) {
    if (frmAddNewPayeeKA.lblNext.skin === "sknLblNextEnabled") {
        if (kony.boj.isNextEnabledBiller()) {
            if (kony.boj.isBillerRegistered()) customAlertPopup(geti18Value("i18n.common.AlreadyRegistered"), geti18Value("i18n.bills.BillerAlreadyRegistered"), popupCommonAlertDimiss, "");
            else kony.boj.onClickNextAddBiller();
        }
    }
}
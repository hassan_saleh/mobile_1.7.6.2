function AS_Segment_aae2b1aa35dc464082b4c2955a66212f(eventobject, sectionNumber, rowNumber) {
    var phNum = frmContactUsKA.contactSegmentList.selectedItems[0].phoneNum;

    function onClickCall(response) {
        if (response) {
            try {
                kony.phone.dial(phNum);
            } catch (err) {
                alert(i18n_dailError + " " + err);
            }
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": i18n_call,
        "yesLabel": i18n_call,
        "noLabel": i18n_cancel,
        "alertIcon": "phone_icon_inactive.png",
        "message": phNum,
        "alertHandler": onClickCall
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}
function AS_Button_d4425b3e3ba642e3bb88c950c5cab78f(eventobject) {
    kony.store.setItem("denoFlag", {});
    frmNewBillKA.btnPrePaid.skin = "slButtonWhiteTabDisabled";
    frmNewBillKA.btnPostPaid.skin = "slButtonWhiteTab";
    gblQuickFlow = "post";
    onClickPostPaidQuickPay();
    kony.boj.BillerResetFlag = true;
    kony.boj.selectedBillerType = "PostPaid";
    clearNewBillScreen();
    frmNewBillKA.show();
}
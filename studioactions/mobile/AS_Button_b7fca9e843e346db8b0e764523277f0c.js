function AS_Button_b7fca9e843e346db8b0e764523277f0c(eventobject) {
    if (frmAccountsLandingKA.flxDeals.isVisible === true) {
        gblTModule = "OpenTermDeposite";
        // getAllAccounts();
        resetForm();
        //kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        frmOpenTermDeposit.show();
    } else {
        frmNewSubAccountLandingNew.lblNext.skin = "sknLblNextDisabled";
        frmNewSubAccountLandingNew.lblFre.text = geti18Value("i18n.common.accountType");
        frmNewSubAccountLandingNew.lblCurr.text = geti18Value("i18n.common.currencyType");
        frmNewSubAccountConfirm.lblCheck1.text = "q";
        frmNewSubAccountLandingNew.lblFrequency.setVisibility(false);
        frmNewSubAccountLandingNew.lblCurrHead.setVisibility(false);
        frmNewSubAccountLandingNew.lblLine4.skin = "sknFlxGreyLine"
        frmNewSubAccountLandingNew.CopylblLine0ib1721fa6b7140.skin = "sknFlxGreyLine";
        frmNewSubAccountLandingNew.show();
    }
}
function AS_Button_jd571d5134834b98b279455faff60766(eventobject) {
    //Omar ALnajjar
    // goToConfirmDeposite();
    var balanceAmount = parseFloat(selectedBalAccount) - parseFloat(frmOpenTermDeposit.txtDepositAmount.text);
    if (balanceAmount < 0) {
        var Message = geti18Value("i18n.termDeposit.checkAmount");
        customAlertPopup(geti18Value("i18n.common.alert"), Message, popupCommonAlertDimiss, "", geti18nkey("i18n.settings.ok"), "");
        return;
    }
    callRateService(true);
}
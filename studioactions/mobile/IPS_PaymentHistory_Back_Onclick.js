function IPS_PaymentHistory_Back_Onclick(eventobject) {
    return AS_FlexContainer_b07c48a28eeb4c1e89635dc3eaa69155(eventobject);
}

function AS_FlexContainer_b07c48a28eeb4c1e89635dc3eaa69155(eventobject) {
    //Mai 5/1/2021
    if (gblTModule === "IPSReturn") {
        frmIPSRequests.flxPaymentHistory.isVisible = true;
        frmIPSRequests.flxReturnPayment.isVisible = false;
        frmIPSRequests.flxRequestToPay.isVisible = false;
        frmIPSRequests.flxConfirmEPS.setVisibility(false);
    } else {
        getAllIPSBenficiaries();
        // gblTModule = "CLIQ";
        // ResetFormIPSData();
        // getAllAccounts();
        //   frmIPSManageBene.show();
        //   frmIPSRequests.destroy();
    }
}
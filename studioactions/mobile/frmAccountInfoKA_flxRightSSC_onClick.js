function frmAccountInfoKA_flxRightSSC_onClick(eventobject) {
    return AS_FlexContainer_b27fa417155f45d3b153b0d941c198d1(eventobject);
}

function AS_FlexContainer_b27fa417155f45d3b153b0d941c198d1(eventobject) {
    frmAccountInfoKA.flxTabsWrapper.setVisibility(false);
    frmAccountInfoKA.imgDivSett.setVisibility(false);
    frmAccountInfoKA.flxAccountOptions.setVisibility(false);
    frmAccountInfoKA.flxSSCInformation.setVisibility(true);
    frmAccountInfoKA.lblLoanAccountInfo.text = geti18Value("i18n.filtertransaction.Back");
}
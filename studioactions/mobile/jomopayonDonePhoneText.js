function jomopayonDonePhoneText(eventobject, changedtext) {
    return AS_TextField_d7ae597713984b5eb36c253f1b8d4104(eventobject, changedtext);
}

function AS_TextField_d7ae597713984b5eb36c253f1b8d4104(eventobject, changedtext) {
    // animateLabel("DOWN","lblBeneficiaryStaticText",frmJoMoPay.txtPhoneNo.text);
    animate_NEWCARDSOPTIOS("DOWN", "lblBeneficiaryStaticText", frmJoMoPay.txtPhoneNo.text);
    frmJoMoPay.flxBorderBenificiary.skin = "skntextFieldDividerGreen";
    var amount = frmJoMoPay.txtFieldAmount.text.trim();
    if (!isEmpty(amount)) {
        var balance = parseFloat(frmJoMoPay.lblAccountBalance.text.replace(/[^0-9]/g, "")) - parseFloat(amount);
        if (frmJoMoPay.txtFieldAmount.text !== null && frmJoMoPay.txtFieldAmount.text !== "" && frmJoMoPay.txtFieldAmount.text !== "." && frmJoMoPay.txtFieldAmount.text !== "0" && frmJoMoPay.txtFieldAmount.text !== 0) {
            frmJoMoPay.txtFieldAmount.text = Number.parseFloat(amount).toFixed(3);
        }
        if (parseFloat(balance) < 0) {
            frmJoMoPay.flxBorderAmount.skin = "sknFlxOrangeLine";
            customAlertPopup(geti18Value("i18n.common.Information"), geti18Value("i18n.errorCode.54"), popupCommonAlertDimiss, "", geti18Value("i18n.NUO.OKay"), "");
            return;
        } else {
            frmJoMoPay.flxBorderAmount.skin = "skntextFieldDividerGreen";
        }
    }
}
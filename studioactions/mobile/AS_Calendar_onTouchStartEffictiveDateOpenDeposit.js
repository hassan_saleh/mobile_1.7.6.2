function AS_Calendar_onTouchStartEffictiveDateOpenDeposit(eventobject, x, y) {
    return AS_Calendar_b875b06ed8034ec7bf6f78f1bb545817(eventobject, x, y);
}

function AS_Calendar_b875b06ed8034ec7bf6f78f1bb545817(eventobject, x, y) {
    amountFieldCheck();
    var caldate = new Date();
    if (frmOpenTermDeposit.lblDepositCurrency.text === "JOD") {
        frmOpenTermDeposit.calOpenDeposite.validStartDate = [caldate.getDate() + 1, caldate.getMonth() + 1, caldate.getFullYear()];
    } else if (frmOpenTermDeposit.lblDepositCurrency.text === "USD") {
        frmOpenTermDeposit.calOpenDeposite.validStartDate = [caldate.getDate() + 2, caldate.getMonth() + 1, caldate.getFullYear()];
    } else {
        frmOpenTermDeposit.calOpenDeposite.validStartDate = [caldate.getDate() + 1, caldate.getMonth() + 1, caldate.getFullYear()];
    }
    frmOpenTermDeposit.calOpenDeposite.validEndDate = [caldate.getDate() + 10, caldate.getMonth() + 1, caldate.getFullYear()];
}
function AS_TextField_AmountFldCardLessOnEndEditing(eventobject, changedtext) {
    return AS_TextField_a34f756eb3464d5ab8d671a25d8c9076(eventobject, changedtext);
}

function AS_TextField_a34f756eb3464d5ab8d671a25d8c9076(eventobject, changedtext) {
    onendeditingCaredless();
    frmCardlessTransaction.txtFieldAmount.maxTextLength = 20;
    frmCardlessTransaction.txtFieldAmount.text = formatAmountwithcomma(frmCardlessTransaction.lblHiddenAmount.text, getDecimalNumForCurr(frmCardlessTransaction.lblCurrencyCode.text));
}
function onClickNextQuickPay(eventobject, x, y) {
    return AS_Label_cc8ef073b8dd4ef691820b676f4d6388(eventobject, x, y);
}

function AS_Label_cc8ef073b8dd4ef691820b676f4d6388(eventobject, x, y) {
    //Mai 3/21/2021 DVOC
    gblDVOC = "9"; // "Bills payment";
    frmNewBillDetails.tbxAmount.text = "";
    frmNewBillDetails.lblPaymentMode.text = "Accounts";
    frmNewBillDetails.flxConversionAmt.setVisibility(false);
    if (gblQuickFlow == "post") {
        getBillDetailsPostpaid();
    } else {
        getBillDetailsPrepaid();
    }
    clearNewDetailsScreen();
}
function AS_Button_c0fa828040584c70bcc619aeeab9ce81(eventobject) {
    frmEPS.flxDetailsAlias.setVisibility(false);
    frmEPS.flxDetailsIBAN.setVisibility(true);
    if (frmEPS.btnIBAN.skin === slButtonBlueFocus) {
        frmEPS.btnIBAN.skin = sknOrangeBGRNDBOJ;
        frmEPS.btnAlias.skin = slButtonBlueFocus;
    } else {
        frmEPS.btnAlias.skin = sknOrangeBGRNDBOJ;
        frmEPS.btnIBAN.skin = slButtonBlueFocus;
    }
}
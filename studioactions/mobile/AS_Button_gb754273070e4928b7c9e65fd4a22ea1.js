function AS_Button_gb754273070e4928b7c9e65fd4a22ea1(eventobject) {
    //Omar Alnajjar 24/02/2021 R2P
    if (frmIPSRequestToPay.btnRequest.skin === "R2PbtnSKN") {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        SetupRTPConfirmationScreen();
        frmIPSRequestToPay.flxR2PBody.setVisibility(false);
        frmIPSRequestToPay.flxConfirmRTP.setVisibility(true);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.jomopay.fillDetails"), popupCommonAlertDimiss, "");
    }
    // sendRequestPayment();
}
function IBANTextChange(eventobject, changedtext) {
    return AS_TextField_f61b57f0e1934b288a3856f7eec70c58(eventobject, changedtext);
}

function AS_TextField_f61b57f0e1934b288a3856f7eec70c58(eventobject, changedtext) {
    //Omar Alnajjar 24/02/2021  R2P
    frmIPSRequestToPay.txtIBANAlias.text = frmIPSRequestToPay.txtIBANAlias.text.replace(/[a-z]/g, "");
    if (frmIPSRequestToPay.txtIBANAlias.text.match(/^JO/g) === null || frmIPSRequestToPay.txtIBANAlias.text.match(/^JO/g) === "") {
        frmIPSRequestToPay.flxUnderlineIBAN.skin = "sknFlxOrangeLine";
        //    frmEPS.lblNext.skin = "sknLblNextDisabled";
        frmIPSRequestToPay.lblHintIBAN.setVisibility(true);
    } else {
        if (frmIPSRequestToPay.txtIBANAlias.text.length == 30) {
            frmIPSRequestToPay.flxUnderlineIBAN.skin = "sknFlxGreyLine";
            frmIPSRequestToPay.lblHintIBAN.setVisibility(false);
        } else {
            frmIPSRequestToPay.flxUnderlineIBAN.skin = "sknFlxOrangeLine";
            frmIPSRequestToPay.btnRequest.skin = "R2PbtnSKNDisable";
            frmIPSRequestToPay.lblHintIBAN.setVisibility(true);
        }
    }
    validateRequestBtn();
}
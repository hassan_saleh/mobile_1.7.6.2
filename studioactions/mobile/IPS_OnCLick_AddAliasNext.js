function IPS_OnCLick_AddAliasNext(eventobject) {
    return AS_FlexContainer_cbd2fb7723054e2e976b78ef9c5fc349(eventobject);
}

function AS_FlexContainer_cbd2fb7723054e2e976b78ef9c5fc349(eventobject) {
    if (frmEPS.lblNext.skin === "sknLblNextEnabled") {
        if (validate_SufficientBalance()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            SetupConfirmationScreen();
            //   GetAliasInfo();
            frmEPS.flxMain.setVisibility(false);
            frmEPS.flxConfirmEPS.setVisibility(true);
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        }
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.jomopay.fillDetails"), popupCommonAlertDimiss, "");
    }
}
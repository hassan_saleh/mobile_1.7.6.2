function AS_FlexContainer_j590b5ee0c8d4dc4942cc6429f52acf9(eventobject) {
    if (frmPinManagement.flxPinReminderScreen.isVisible === true) {
        frmPinManagement.lblPinManagementTitle.text = geti18Value("i18n.pinManagement.title");
        frmPinManagement.flxPinManagementMainContainer.setVisibility(true);
        frmPinManagement.flxPinReminderScreen.setVisibility(false);
    } else {
        frmManageCardsKA.show();
        frmPinManagement.destroy();
    }
}
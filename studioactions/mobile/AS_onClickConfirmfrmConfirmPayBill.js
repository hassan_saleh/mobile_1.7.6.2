function AS_onClickConfirmfrmConfirmPayBill(eventobject) {
    return AS_Button_b0857bb1a33a4eeb804ae4b23919b7a9(eventobject);
}

function AS_Button_b0857bb1a33a4eeb804ae4b23919b7a9(eventobject) {
    gblToOTPFrom = "ConfirmPayBill";
    gblToOTPFrom = "ConfirmPayBillPayNow"; // hassan OTP POST and PRE
    //Mai 21/3/2021 DVOC
    gblDVOC = "9"; //"Bills payment";
    //callRequestOTP();
    if (frmConfirmPayBill.flxTotalAmount.isVisible === true) {
        var amnt = frmConfirmPayBill.lblTotalAmount.text;
        amnt = amnt.substring(0, amnt.length - 4);
        if (parseFloat(kony.store.getItem("BillPayfromAcc").availableBalance) >= parseFloat(amnt) && kony.store.getItem("BillPayfromAcc").ccydesc === "JOD") { // hassan OTP POST and PRE
            /*  if(kony.boj.selectedBillerType === "PostPaid" && parseFloat(amnt) > 5)// hassan OTP POST and PRE
                callRequestOTP();*/
            if (kony.boj.selectedBillerType === "PrePaid" && parseFloat(amnt) > 20) { // hassan OTP POST and PRE
                callRequestOTP();
            } else if (frmNewBillKA.tbxBillerCategory.text === "MWPS") callRequestOTP();
            else { // hassan OTP POST and PRE
                ShowLoadingScreen();
                performBillPayPostpaid();
            }
        } else if (kony.store.getItem("BillPayfromAcc").ccydesc !== "JOD") {
            ShowLoadingScreen();
            performBillPayPostpaid();
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        }
    } else {
        /*  if(kony.boj.selectedBillerType === "PostPaid" && frmConfirmPayBill.lblPaidAmnt.text > 5)// hassan OTP POST and PRE
            callRequestOTP();*/
        if (kony.boj.selectedBillerType === "PrePaid" && frmConfirmPayBill.prDueAmount.text > 20) { // hassan OTP POST and PRE
            callRequestOTP();
        } else if (frmNewBillKA.tbxBillerCategory.text === "MWPS") callRequestOTP();
        else { // hassan OTP POST and PRE
            ShowLoadingScreen();
            performBillPayPostpaid();
        }
    }
}
function onTouchEndEcommerceText(eventobject, x, y) {
    return AS_TextField_a176250a93a04056ab52cf917386327e(eventobject, x, y);
}

function AS_TextField_a176250a93a04056ab52cf917386327e(eventobject, x, y) {
    if (eventobject.id === "txtEcomLimit") {
        animateLabel("UP", "lblEcom", frmEnableInternetTransactionKA.txtEcomLimit.text);
        animateLabel("DOWN", "lblMailOrder", frmEnableInternetTransactionKA.txtMailLimit.text);
        onTextChange_ECOMMERCELIMIT(frmEnableInternetTransactionKA.txtEcomLimit, frmEnableInternetTransactionKA.sliderECommerceLmt, "onTextChange", "internetlimit");
    } else if (eventobject.id === "txtMailLimit") {
        animateLabel("DOWN", "lblEcom", frmEnableInternetTransactionKA.txtEcomLimit.text);
        animateLabel("UP", "lblMailOrder", frmEnableInternetTransactionKA.txtMailLimit.text);
        onTextChange_ECOMMERCELIMIT(frmEnableInternetTransactionKA.txtMailLimit, frmEnableInternetTransactionKA.sliderMailOrderLimit, "onTextChange", "mailorderlimit");
    }
}
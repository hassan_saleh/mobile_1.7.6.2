function btnConfirmOnclickfrmConfirmTransfer(eventobject) {
    return AS_Button_6b9fcdd57f9145a9b4e38255741be7fb(eventobject);
}

function AS_Button_6b9fcdd57f9145a9b4e38255741be7fb(eventobject) {
    kony.print("TYPE: " + gblSelectedBene.benType);
    var fullNameFlagConfirm = false;
    if (kony.boj.addBeneVar.selectedType === "BOJ" || gblTModule === "own" || gblTModule === "web") {
        kony.print("Do nothing:");
        popupCommonAlertDimiss();
        gblToOTPFrom = "OwnAccounts";
        ShowLoadingScreen();
        if (gblTModule == "send") {
            //Nart Rawashdeh 01/06/2021<<
            if (gblSelectedBene.benType === "DTB" || gblSelectedBene.benType === "ITB") {
                checkFullName();
                if (fullNameFlagConfirm === true) {
                    if (gblSelectedBene.isFav === "Y") {
                        performOwnTransfer();
                    } else {
                        callRequestOTP();
                    }
                } else {
                    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.sendMoney.fullNameLengthHintConfirm"), popupCommonAlertDimiss, "");
                }
            } else {
                //>>
                callRequestOTP();
            }
        } else performOwnTransfer();
    } else {
        kony.print("Show");
        popupCommonAlertDimiss();
        gblToOTPFrom = "OwnAccounts";
        ShowLoadingScreen();
        if (gblTModule == "send") {
            //Nart Rawashdeh 01/06/2021<<
            if (gblSelectedBene.benType === "DTB" || gblSelectedBene.benType === "ITB") {
                checkFullName();
                if (fullNameFlagConfirm === true) {
                    if (gblSelectedBene.isFav === "Y") {
                        performOwnTransfer();
                    } else {
                        callRequestOTP();
                    }
                } else {
                    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.sendMoney.fullNameLengthHintConfirm"), popupCommonAlertDimiss, "");
                }
            } else {
                //>>
                callRequestOTP();
            }
        } else performOwnTransfer();
    }

    function onClickYesConfirm() {
        popupCommonAlertDimiss();
        gblToOTPFrom = "OwnAccounts";
        ShowLoadingScreen();
        if (gblTModule == "send") {
            if (gblSelectedBene.isFav === "Y") performOwnTransfer();
            else callRequestOTP();
        } else performOwnTransfer();
    }

    function onClickNoConfirm() {
        popupCommonAlertDimiss();
        frmNewTransferKA.show();
    }
    //Nart Rawashdeh 01/06/2021 <<
    function checkFullName() {
        fullNameFlagConfirm = false;
        var nameSyllablesValidationConfirm = 0;
        var remainingTextConfirm = frmConfirmTransferKA.lblBenName.text;
        kony.print("remainingTextConfirm :" + remainingTextConfirm);
        var spaceCharConfirm = remainingTextConfirm.trim();
        for (var x = 0; x <= remainingTextConfirm.length + 1; x++) {
            spaceCharConfirm = remainingTextConfirm.trim();
            spaceCharConfirm = spaceCharConfirm.search(" ");
            if (nameSyllablesValidationConfirm === 2) {
                fullNameFlagConfirm = true;
                break;
            } else if (spaceCharConfirm === -1 && nameSyllablesValidationConfirm !== 2) {
                fullNameFlagConfirm = false;
                break;
            } else {
                remainingTextConfirm = remainingTextConfirm.substring((spaceCharConfirm + 1), remainingTextConfirm.length);
                nameSyllablesValidationConfirm++;
            }
            kony.print("remainingTextConfirm LOOP :" + remainingTextConfirm);
            kony.print("spaceCharConfirm LOOP :" + spaceCharConfirm);
            kony.print("nameSyllablesValidationConfirm LOOP : " + nameSyllablesValidationConfirm);
            kony.print("fullNameFlagConfirm LOOP :" + fullNameFlagConfirm);
        }
        kony.print("remainingTextConfirm :" + remainingTextConfirm);
        kony.print("spaceCharConfirm :" + spaceCharConfirm);
        kony.print("nameSyllablesValidationConfirm : " + nameSyllablesValidationConfirm);
        kony.print("fullNameFlagConfirm :" + fullNameFlagConfirm);
    }
    //>>
}
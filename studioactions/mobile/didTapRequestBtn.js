function didTapRequestBtn(eventobject) {
    return AS_Button_df31d02470924aa6a367adf016c38ec0(eventobject);
}

function AS_Button_df31d02470924aa6a367adf016c38ec0(eventobject) {
    //Omar Alnajjar 24/02/2021 R2P
    if (frmIPSRequestToPay.btnRequest.skin === "R2PbtnSKN") {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        SetupRTPConfirmationScreen();
        frmIPSRequestToPay.flxR2PBody.setVisibility(false);
        frmIPSRequestToPay.flxConfirmRTP.setVisibility(true);
        frmIPSRequestToPay.flxIPSHeader.setVisibility(false);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.jomopay.fillDetails"), popupCommonAlertDimiss, "");
    }
    // sendRequestPayment();
}
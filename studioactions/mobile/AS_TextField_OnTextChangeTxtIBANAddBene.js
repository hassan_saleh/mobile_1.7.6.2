function AS_TextField_OnTextChangeTxtIBANAddBene(eventobject, changedtext) {
    return AS_TextField_fa18cc5354d64b0db50b2b8ab408c5cc(eventobject, changedtext);
}

function AS_TextField_fa18cc5354d64b0db50b2b8ab408c5cc(eventobject, changedtext) {
    frmAddBeneIPS.txtIBAN.text = frmAddBeneIPS.txtIBAN.text.toUpperCase().replace(/[^0-9A-Z]/g, "");
    if (frmAddBeneIPS.txtIBAN.text.match(/^JO/g) === null || frmAddBeneIPS.txtIBAN.text.match(/^JO/g) === "") {
        frmAddBeneIPS.flxIBANDiv.skin = "sknFlxOrangeLine";
        frmAddBeneIPS.lblNext.skin = "sknLblNextDisabled";
    } else {
        if (frmAddBeneIPS.txtIBAN.text.length == 30) {
            frmAddBeneIPS.flxIBANDiv.skin = "sknFlxGreenLine";
        } else {
            frmAddBeneIPS.flxIBANDiv.skin = "sknFlxOrangeLine";
            frmAddBeneIPS.lblNext.skin = "sknLblNextDisabled";
        }
    }
    validateNextAddIPSBene();
}
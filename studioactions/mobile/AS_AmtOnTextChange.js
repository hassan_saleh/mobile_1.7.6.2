function AS_AmtOnTextChange(eventobject, changedtext) {
    return AS_TextField_b751b9b023d545c8bdcbe3e5725b9a1b(eventobject, changedtext);
}

function AS_TextField_b751b9b023d545c8bdcbe3e5725b9a1b(eventobject, changedtext) {
    if (frmNewTransferKA.txtBox.text == "." || frmNewTransferKA.txtBox.text == 0 || frmNewTransferKA.txtBox.text == "NaN") {
        frmNewTransferKA.lblNext.skin = sknLblNextDisabled;
        frmNewTransferKA.lblNext.setEnabled(false);
        frmNewTransferKA.lblLine3.skin = "sknFlxOrangeLine";
        frmNewTransferKA.flxInvalidAmountField.setVisibility(true);
    } else {
        frmNewTransferKA.lblLine3.skin = "lblLine0h8b99d8a85f649";
        frmNewTransferKA.flxInvalidAmountField.setVisibility(false);
    }
    enableNext();
    frmNewTransferKA.txtBox.text = fixDecimal(frmNewTransferKA.txtBox.text, getDecimalNumForCurr(frmNewTransferKA.lblFromAccCurr.text));
    frmNewTransferKA.lblHiddenAmount.text = frmNewTransferKA.txtBox.text;
}
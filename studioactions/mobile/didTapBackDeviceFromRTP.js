function didTapBackDeviceFromRTP(eventobject) {
    return AS_Form_ee5709f35ae24de98a1c12a2b2c902ee(eventobject);
}

function AS_Form_ee5709f35ae24de98a1c12a2b2c902ee(eventobject) {
    //Omar ALnajjar R2P 23/02/2021
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), onClickYesBackRTP, onClickNoBackRTP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
}
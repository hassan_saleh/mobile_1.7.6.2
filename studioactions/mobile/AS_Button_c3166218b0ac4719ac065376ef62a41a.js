function AS_Button_c3166218b0ac4719ac065376ef62a41a(eventobject) {
    if (frmAccountsLandingKA.flxDeals.isVisible === true) {
        gblTModule = "OpenTermDeposite";
        resetForm();
        frmOpenTermDeposit.show();
    } else { /*hassan hide deals*/
        frmNewSubAccountLandingNew.lblNext.skin = "sknLblNextDisabled";
        frmNewSubAccountLandingNew.lblFre.text = geti18Value("i18n.common.accountType");
        frmNewSubAccountLandingNew.lblCurr.text = geti18Value("i18n.common.currencyType");
        frmNewSubAccountConfirm.lblCheck1.text = "q";
        frmNewSubAccountLandingNew.lblFrequency.setVisibility(false);
        frmNewSubAccountLandingNew.lblCurrHead.setVisibility(false);
        frmNewSubAccountLandingNew.lblLine4.skin = "sknFlxGreyLine"
        frmNewSubAccountLandingNew.CopylblLine0ib1721fa6b7140.skin = "sknFlxGreyLine";
        frmNewSubAccountLandingNew.show();
    }
}
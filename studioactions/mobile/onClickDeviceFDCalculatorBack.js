function onClickDeviceFDCalculatorBack(eventobject) {
    return AS_Form_j06737abc3374c49b2160602b5463550(eventobject);
}

function AS_Form_j06737abc3374c49b2160602b5463550(eventobject) {
    if (frmDepositCalculator.flxBody.isVisible === true) {
        customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), onCLickYesBackOpenDepositsCalculator, popupCommonAlertDimiss, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
    } else {
        frmDepositCalculator.flxCalculatorResults.setVisibility(false);
        frmDepositCalculator.flxBody.setVisibility(true);
    }
}
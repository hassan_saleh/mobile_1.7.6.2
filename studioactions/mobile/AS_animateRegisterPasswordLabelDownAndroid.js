function AS_animateRegisterPasswordLabelDownAndroid(eventobject, changedtext) {
    return AS_TextField_b1325229fcb14190b124c5a6eb8d6d5f(eventobject, changedtext);
}

function AS_TextField_b1325229fcb14190b124c5a6eb8d6d5f(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidPassword(frmEnrolluserLandingKA.tbxPasswordKA.text)) {
        frmEnrolluserLandingKA.flxUnderlinePassword.skin = "sknFlxGreenLine";
        frmEnrolluserLandingKA.flxPasswordValidation.isVisible = false;
        passwordFlag = true;
    } else {
        frmEnrolluserLandingKA.flxPasswordValidation.isVisible = true;
        frmEnrolluserLandingKA.flxUnderlinePassword.skin = "sknFlxOrangeLine";
        passwordFlag = false;
    }
    animateLabel("DOWN", "lblPassword", frmEnrolluserLandingKA.tbxPasswordKA.text);
}
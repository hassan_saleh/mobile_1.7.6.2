function OndoneofJomooayAmountTxt(eventobject, changedtext) {
    return AS_TextField_f0797600ac284aa3ab9f5d23f3cc5fa4(eventobject, changedtext);
}

function AS_TextField_f0797600ac284aa3ab9f5d23f3cc5fa4(eventobject, changedtext) {
    animate_NEWCARDSOPTIOS("UP", "lblstaticdescription", frmJoMoPay.txtDescription.text);
    var amount = frmJoMoPay.txtFieldAmount.text;
    var balance = parseFloat(frmJoMoPay.lblAccountBalance.text.replace(/[^0-9]/g, "")) - parseFloat(amount);
    if (frmJoMoPay.txtFieldAmount.text !== null && frmJoMoPay.txtFieldAmount.text !== "" && frmJoMoPay.txtFieldAmount.text !== "." && frmJoMoPay.txtFieldAmount.text !== "0" && frmJoMoPay.txtFieldAmount.text !== 0) {
        frmJoMoPay.txtFieldAmount.text = Number.parseFloat(amount).toFixed(3);
    }
    if (parseFloat(balance) < 0) {
        frmJoMoPay.flxBorderAmount.skin = "sknFlxOrangeLine";
        customAlertPopup(geti18Value("i18n.common.Information"), geti18Value("i18n.errorCode.54"), popupCommonAlertDimiss, "", geti18Value("i18n.NUO.OKay"), "");
        return;
    } else {
        frmJoMoPay.flxBorderAmount.skin = "skntextFieldDividerGreen";
    }
}
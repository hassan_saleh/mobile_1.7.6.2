function onDeviceBackOpenTermDeposit(eventobject) {
    return AS_Form_dcf426bf4eeb477e99ec28c5cc5177ec(eventobject);
}

function AS_Form_dcf426bf4eeb477e99ec28c5cc5177ec(eventobject) {
    //Omar Alnajjar Open deposits
    if (frmOpenTermDeposit.flxConfirmDeposite.isVisible === true) {
        frmOpenTermDeposit.flxDepositSelection.setVisibility(true);
        frmOpenTermDeposit.flxConfirmDeposite.setVisibility(false);
        frmOpenTermDeposit.btnNextDeposite.setVisibility(true);
    } else {
        customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), onCLickYesBackOpenDeposits, popupCommonAlertDimiss, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
    }
}
function AS_FlexContainer_j8425e7341bf480bbf30289f53e5d4df(eventobject) {
    if (frmFilterTransaction.flxDatePicker.isVisible == true) {
        // clearForm.filterTransactions();
        if (kony.application.getPreviousForm().id == "frmManageCardsKA") {
            searchTransation();
        } else if (frmFilterTransaction.lblDateRange.text == geti18nkey("i18n.FilterTransaction.pickadate")) {
            customAlertPopup(geti18nkey("i18n.maps.Info"), geti18nkey("i18n.common.enterdate"), popupCommonAlertDimiss, null, geti18nkey("i18n.settings.ok"), null);
        } else {
            frmFilterTransaction.flxDatePicker.isVisible = false;
            frmFilterTransaction.flxFilterTransaction.isVisible = true;
            frmFilterTransaction.lblFilterHeader.text = kony.i18n.getLocalizedString("i18n.FilterTransaction");
        }
    } else if (frmFilterTransaction.flxFilterTransaction.isVisible == true) {}
}
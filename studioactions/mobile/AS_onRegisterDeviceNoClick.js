function AS_onRegisterDeviceNoClick(eventobject) {
    return AS_Button_e4cdf6e3abac4b41a3cd06c81905d337(eventobject);
}

function AS_Button_e4cdf6e3abac4b41a3cd06c81905d337(eventobject) {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
    frmAccountsLandingKA.flxDeals.setVisibility(false);
    frmAccountsLandingKA.flxLoans.setVisibility(false);
    frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
    frmAccountsLandingKA.flxDeals.zIndex = 1;
    frmAccountsLandingKA.flxLoans.zIndex = 1;
    frmAccountsLandingKA.forceLayout();
    gblNoThanksClickSaveDatatoQB = true;
    accountDashboardDataCall();
}
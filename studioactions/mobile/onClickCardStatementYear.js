function onClickCardStatementYear(eventobject) {
    return AS_FlexContainer_e26e3a8dbcdb4c7e92220d55e21b6c76(eventobject);
}

function AS_FlexContainer_e26e3a8dbcdb4c7e92220d55e21b6c76(eventobject) {
    // create_dynamicFilterPopup();
    if (frmCardStatementKA.flxCardsSegmentOption.top != "100%") {
        animate_cardsFilterOption();
    }
    var dat = new Date().getFullYear();
    var temp = [];
    for (var i = dat, j = 0; i > dat - 10; i--, j++) temp[j] = {
        "lblValue": i.toFixed(),
        "lblTitle": "Year"
    };
    frmCardStatementKA.segCardsOptions.setData(temp);
    animate_cardsFilterOption();
}
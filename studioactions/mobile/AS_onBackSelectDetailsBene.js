function AS_onBackSelectDetailsBene(eventobject) {
    return AS_FlexContainer_ca225436b3e949ae8cbab6f142a8fd95(eventobject);
}

function AS_FlexContainer_ca225436b3e949ae8cbab6f142a8fd95(eventobject) {
    if (kony.newCARD.applyCardsOption === true) {
        frmApplyNewCardsKA.show();
    } else {
        if (gblsubaccBranchList) {
            frmOrderCheckBook.show();
        } else if (gblrequestStatementBranch) {
            frmRequestStatementAccounts.show();
        } else if (gblRequestNewPinBranchList) {
            frmRequestPinCard.show();
        } else if (gblWorkflowType === "IPS") {
            frmEPS.show();
        } else if (gblTModule === "OpenTermDeposite") {
            frmOpenTermDeposit.show();
        } //Omar Alnajjar 23/02/2021 R2P
        else if (gblWorkflowType === "IPSR2PBanks" || gblWorkflowType === "IPSR2P") {
            frmIPSRequestToPay.show();
        } //Omar ALnajjar Add Bene
        else if (gblWorkflowType === "IPSAddBeneBank") {
            frmAddBeneIPS.show();
        }
        //Omar alnajjar 21/03/2021 RTP
        else if (gblWorkflowType === "IPSAcsRejR2P") {
            frmIPSPendingRTP.show();
        }
        //Omar alnajjar 21/03/2021 Return Payment
        else if (gblWorkflowType === "IPSReturn") {
            frmIPSRequests.show();
        } else {
            frmAddExternalAccountKA.show();
            kony.boj.addBeneNextStatus();
        }
    }
}
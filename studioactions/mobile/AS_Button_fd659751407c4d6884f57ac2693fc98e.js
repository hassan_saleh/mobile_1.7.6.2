function AS_Button_fd659751407c4d6884f57ac2693fc98e(eventobject) {
    ShowLoadingScreen();
    gblToOTPFrom = "ConfirmPayBill";
    //Mai 3/21/2021 DVOC
    gblDVOC = "Bills payment";
    gblToOTPFrom = "ConfirmPayBillRegPre"; // hassan OTP Prepaid
    //callRequestOTP();
    var billerCodePre = frmConfirmPrePayBill.prBillerCode.text;
    if (frmConfirmPrePayBill.flxTotalAmount.isVisible === true) {
        var amnt = frmConfirmPrePayBill.lblTotalAmount.text;
        amnt = amnt.substring(0, amnt.length - 4);
        if (parseFloat(kony.store.getItem("BillPayfromAcc").availableBalance) >= parseFloat(amnt)) {
            if (frmConfirmPayBill.prDueAmount.text > 20) // hassan OTP Prepaid
                callRequestOTP();
            else if (billerCodePre === "83" || billerCodePre === "111" || billerCodePre === "127" || billerCodePre === "130" || billerCodePre === "183" || billerCodePre === "211" || billerCodePre === "221" || billerCodePre === "273" || billerCodePre === "274" || billerCodePre === "301") callRequestOTP(); // hassan OTP Prepaid
            else performBillPayPrePaid();
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInCardAcc"), popupCommonAlertDimiss, "");
        }
    } else {
        if (frmConfirmPrePayBill.prDueAmount.text > 20) // hassan OTP Prepaid
            callRequestOTP();
        else if (billerCodePre === "83" || billerCodePre === "111" || billerCodePre === "127" || billerCodePre === "130" || billerCodePre === "183" || billerCodePre === "211" || billerCodePre === "221" || billerCodePre === "273" || billerCodePre === "274" || billerCodePre === "301") callRequestOTP(); // hassan OTP Prepaid
        else performBillPayPrePaid();
    }
}
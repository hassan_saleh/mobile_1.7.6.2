function BackFromReturnPayment(eventobject) {
    return AS_FlexContainer_af8712eb81db40df818c6924aac898fe(eventobject);
}

function AS_FlexContainer_af8712eb81db40df818c6924aac898fe(eventobject) {
    if (frmIPSRequests.lblNext.skin === "sknLblNextEnabled") {
        gblTModule == "IPSReturn";
        // Mai 26/11/2020 if IPSRequest
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        SetupConfirmationScreen();
        //   GetAliasInfo();
        frmEPS.flxMain.setVisibility(false);
        // frmEPS.flxReturnPayment.setVisibility(false);
        frmEPS.flxConfirmEPS.setVisibility(true);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.jomopay.fillDetails"), popupCommonAlertDimiss, "");
    }
}
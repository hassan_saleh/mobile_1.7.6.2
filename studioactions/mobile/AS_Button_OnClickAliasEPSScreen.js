function AS_Button_OnClickAliasEPSScreen(eventobject) {
    return AS_Button_i384728c2537420891282a51bfcb923d(eventobject);
}

function AS_Button_i384728c2537420891282a51bfcb923d(eventobject) {
    if (!transferFromBene) {
        frmEPS.flxDetailsIBAN.setVisibility(false);
        frmEPS.flxDetailsMobileScroll.setVisibility(false);
        frmEPS.flxDetailsAliasScroll.setVisibility(true);
        if (frmEPS.btnAlias.skin === slButtonBlueFocus) {
            frmEPS.lblNext.skin = "sknLblNextDisabled";
            frmEPS.btnAlias.skin = sknOrangeBGRNDBOJ;
            frmEPS.btnIBAN.skin = slButtonBlueFocus;
            frmEPS.btnMob.skin = slButtonBlueFocus;
        } else {
            frmEPS.btnAlias.skin = sknOrangeBGRNDBOJ;
            frmEPS.btnIBAN.skin = slButtonBlueFocus;
        }
        // for reset other flows 
        // Reset Mob Details //
        frmEPS.txtAliasNameMob.text = "";
        frmEPS.txtAmountMob.text = "";
        frmEPS.lblIBANMobtxt.text = "";
        frmEPS.lblAddressMobtxt.text = "";
        frmEPS.lblBankMobAliastxt.text = "";
        frmEPS.flxBeneNameMob.setVisibility(false);
        frmEPS.flxAmountMob.setVisibility(false);
        frmEPS.flxIbanDetailsMob.setVisibility(false);
        frmEPS.flxAddressMob.setVisibility(false);
        frmEPS.flxBankMob.setVisibility(false);
        // Reset Mob Details //
        // Reset Iban Details //  
        frmEPS.txtAmountIBAN.text = "";
        frmEPS.txtIBANAlias.text = "";
        frmEPS.txtAddressAlias.text = "";
        frmEPS.txtBankAlias.text = "";
        frmEPS.tbxBankName.text = "";
        frmEPS.flxBeneNameIBAN.setVisibility(false);
        frmEPS.lblBankNameStat.setVisibility(false);
        frmEPS.tbxBankName.setVisibility(false);
        frmEPS.lblBankName.setVisibility(true);
        // Reset Iban Details // 
        // GetAliasInfo();
        // frmEPS.flxAliasInfo.setVisibility(true);
        // frmEPS.flxMain.setVisibility(false);
    }
}
function actionOnEmpTypeLogout(eventobject) {
    return AS_Button_dde69c7bfc254c52ba79adbdfbb1d96b(eventobject);
}

function AS_Button_dde69c7bfc254c52ba79adbdfbb1d96b(eventobject) {
    function SHOW_ALERT_ide_onClick_d69d3f0ff1e54c0595451556c1d0f936_True() {
        kony.print("Deleted");
    }

    function SHOW_ALERT_ide_onClick_d69d3f0ff1e54c0595451556c1d0f936_False() {}

    function SHOW_ALERT_ide_onClick_d69d3f0ff1e54c0595451556c1d0f936_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_d69d3f0ff1e54c0595451556c1d0f936_True();
        } else {
            SHOW_ALERT_ide_onClick_d69d3f0ff1e54c0595451556c1d0f936_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_d69d3f0ff1e54c0595451556c1d0f936_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}
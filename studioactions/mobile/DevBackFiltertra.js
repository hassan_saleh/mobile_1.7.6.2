function DevBackFiltertra(eventobject) {
    return AS_Form_a1730049c2654b389e0499d5f0547031(eventobject);
}

function AS_Form_a1730049c2654b389e0499d5f0547031(eventobject) {
    if (frmFilterTransaction.flxDatePicker.isVisible == true) {
        clearForm.filterTransactions();
        frmFilterTransaction.flxDatePicker.isVisible = false;
        frmFilterTransaction.flxFilterTransaction.isVisible = true;
        frmFilterTransaction.lblFilterHeader.text = kony.i18n.getLocalizedString("i18n.FilterTransaction");
    } else if (frmFilterTransaction.flxFilterTransaction.isVisible == true || frmFilterTransaction.flxCardsFilterScreen.isVisible == true) {
        kony.application.getPreviousForm().show();
        clearForm.filterTransactions();
    }
}
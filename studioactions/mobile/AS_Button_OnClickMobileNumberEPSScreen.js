function AS_Button_OnClickMobileNumberEPSScreen(eventobject) {
    return AS_Button_c0dfd3117c3f449bb29108b40418bd2a(eventobject);
}

function AS_Button_c0dfd3117c3f449bb29108b40418bd2a(eventobject) {
    if (!transferFromBene) {
        frmEPS.flxDetailsAliasScroll.setVisibility(false);
        frmEPS.flxDetailsIBAN.setVisibility(false);
        frmEPS.flxDetailsMobileScroll.setVisibility(true);
        if (frmEPS.btnMob.skin === slButtonBlueFocus) {
            frmEPS.lblNext.skin = "sknLblNextDisabled";
            frmEPS.btnMob.skin = sknOrangeBGRNDBOJ;
            frmEPS.btnAlias.skin = slButtonBlueFocus;
            frmEPS.btnIBAN.skin = slButtonBlueFocus;
        }
        //reset the other flows 
        // Reset Iban Details //  
        frmEPS.txtAmountIBAN.text = "";
        frmEPS.txtIBANAlias.text = "";
        frmEPS.txtAddressAlias.text = "";
        frmEPS.txtBankAlias.text = "";
        frmEPS.tbxBankName.text = "";
        frmEPS.flxBeneNameIBAN.setVisibility(false);
        frmEPS.lblBankNameStat.setVisibility(false);
        frmEPS.tbxBankName.setVisibility(false);
        frmEPS.lblBankName.setVisibility(true);
        // Reset Iban Details // 
        // Reset Alias Details //
        frmEPS.txtAliasName.text = "";
        frmEPS.txtAmountAlias.text = "";
        frmEPS.lblIBANAliastxt.text = "";
        frmEPS.lbAliasAddresstxt.text = "";
        frmEPS.lblAliasBanktxt.txt = "";
        frmEPS.flxBeneName.setVisibility(false);
        frmEPS.flxAmountAlias.setVisibility(false);
        frmEPS.flxIBANAliass.setVisibility(false);
        frmEPS.flxAliasAddress.setVisibility(false);
        frmEPS.flxAliasBank.setVisibility(false);
        // Reset Alias Details //
        // animateLabel("UP", "lblIBANMob",frmEPS.lblIBANMob.text);
        // animateLabel("UP", "lblAddressMob",frmEPS.lblAddressMob.text);
        // animateLabel("UP", "lblBankMob",frmEPS.lblBankMob.text);
        // GetAliasInfo();
    }
}
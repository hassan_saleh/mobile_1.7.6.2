function AS_TextField_c3b866ae003f414a98628c7dfc56a1d7(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidUsername(frmEnrolluserLandingKA.tbxUsernameKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxGreenLine";
        usernameFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxOrangeLine";
        usernameFlag = false;
    }
    if (usernameFlag === true && passwordFlag === true && confrmPasswordFlag === true) {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextEnabled";
        frmEnrolluserLandingKA.flxProgress1.left = "80%";
    } else {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextDisabled";
        frmEnrolluserLandingKA.flxProgress1.left = "65%";
    }
    isClearButtonVisible(frmEnrolluserLandingKA.tbxUsernameKA, frmEnrolluserLandingKA.lblClose);
}
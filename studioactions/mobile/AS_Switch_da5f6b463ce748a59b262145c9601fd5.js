function AS_Switch_da5f6b463ce748a59b262145c9601fd5(eventobject) {
    if (frmAuthorizationAlternatives.flxQuickBalance.height === "8%") {
        frmAuthorizationAlternatives.flxQuickBalance.height = "40%";
        addDatatoSegQuickBalance(1);
    } else {
        frmAuthorizationAlternatives.flxQuickBalance.height = "8%";
        addDatatoSegQuickBalance(0);
    }
}
function AS_preShowFrmBills(eventobject) {
    return AS_Form_b815194793774694a3b6d2ab21a12cf8(eventobject);
}

function AS_Form_b815194793774694a3b6d2ab21a12cf8(eventobject) {
    frmBills.lblType.text = billType;
    //frmBills.tbxAmount.text = "";
    animateLabel("DOWN", "lblAmount", frmBills.tbxAmount.text);
    if (billType === "PREPAID") gotoPrepaidBillForm();
    else gotoPostpaidBillForm();
    validateFrmBills();
}
function onClickFilterDone(eventobject) {
    return AS_FlexContainer_h58f754d30154631ad25eb2f6379bb6f(eventobject);
}

function AS_FlexContainer_h58f754d30154631ad25eb2f6379bb6f(eventobject) {
    if (frmFilterTransaction.flxDatePicker.isVisible == true) {
        // clearForm.filterTransactions();
        if (frmFilterTransaction.lblDateRange.text == geti18nkey("i18n.FilterTransaction.pickadate")) {
            customAlertPopup(geti18nkey("i18n.maps.Info"), geti18nkey("i18n.common.enterdate"), popupCommonAlertDimiss, null, geti18nkey("i18n.settings.ok"), null);
        } else {
            if (validate_FILTERDATESELECTION()) {
                frmFilterTransaction.flxDatePicker.isVisible = false;
                frmFilterTransaction.flxFilterTransaction.isVisible = true;
                frmFilterTransaction.lblFilterHeader.text = kony.i18n.getLocalizedString("i18n.FilterTransaction");
            } else customAlertPopup(geti18nkey("i18n.maps.Info"), geti18nkey("i18n.filter.validatedate"), popupCommonAlertDimiss, null, geti18nkey("i18n.settings.ok"), null);
        }
    } else if (frmFilterTransaction.flxFilterTransaction.isVisible == true) {
        searchTransation();
    }
}
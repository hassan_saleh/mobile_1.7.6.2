function AS_FlexContainer_d3f103ab48d648eabef86eceea0895cc(eventobject) {
    if (frmEPS.lblNext.skin === "sknLblNextEnabled") {
        if (validate_SufficientBalance()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            SetupConfirmationScreen();
            //   GetAliasInfo();
            frmEPS.flxMain.setVisibility(false);
            frmEPS.flxConfirmEPS.setVisibility(true);
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        }
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.jomopay.fillDetails"), popupCommonAlertDimiss, "");
    }
}
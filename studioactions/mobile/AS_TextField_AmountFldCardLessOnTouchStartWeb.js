function AS_TextField_AmountFldCardLessOnTouchStartWeb(eventobject, x, y) {
    return AS_TextField_bfe0bbfc64304b5780f41cb2b631a240(eventobject, x, y);
}

function AS_TextField_bfe0bbfc64304b5780f41cb2b631a240(eventobject, x, y) {
    frmCardlessTransaction.txtFieldAmount.maxTextLength = 9;
    frmCardlessTransaction.txtFieldAmount.text = frmCardlessTransaction.lblHiddenAmount.text;
}
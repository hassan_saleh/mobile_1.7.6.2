function AS_Button_gc0c88e1719145ef8015042ec2019648(eventobject, context) {
    kony.print("eventobject ::" + JSON.stringify(eventobject));
    kony.print("context ::" + JSON.stringify(context));
    if (context.widgetInfo.selectedRowItems[0].benType === "IPS") {
        frmAddBeneIPS.lblTitle.text = geti18Value("i18n.updatebene.title");
        aliasOpr = "Update"; // hassan fix delete ips alias     
        if (context.widgetInfo.selectedRowItems[0].bene_address2 === "IBAN") {
            onClickAliasTypeBtnBene("editIBAN");
        } else if (context.widgetInfo.selectedRowItems[0].bene_address2 === "mobile") {
            onClickAliasTypeBtnBene("editMobile");
        } else if (context.widgetInfo.selectedRowItems[0].bene_address2 === "alias") {
            onClickAliasTypeBtnBene("editAlias");
        }
    } else {
        gblFromModule = "updateBeneficiary";
        set_EDIT_BENEFICIARY_SCREEN(context.widgetInfo.selectedRowItems[0]);
    }
}
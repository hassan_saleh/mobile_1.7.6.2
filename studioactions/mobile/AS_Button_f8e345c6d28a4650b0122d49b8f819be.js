function AS_Button_f8e345c6d28a4650b0122d49b8f819be(eventobject) {
    //Omar ALnajjar R2P 23/02/2021
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), onClickYesBackRTP, onClickNoBackRTP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
}
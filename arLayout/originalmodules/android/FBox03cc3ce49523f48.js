function initializeFBox03cc3ce49523f48() {
    FBox03cc3ce49523f48 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "FBox03cc3ce49523f48",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    FBox03cc3ce49523f48.setDefaultUnit(kony.flex.DP);
    var lblOperationHrs = new kony.ui.Label({
        "id": "lblOperationHrs",
        "isVisible": true,
        "left": "0dp",
        "minHeight": "20dp",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.locateus.frmLocatorBranchDetailsKA.CopyatmServices00ff586c7132d45"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var lblOperationHrsValue = new kony.ui.Label({
        "id": "lblOperationHrsValue",
        "isVisible": true,
        "left": "90dp",
        "minHeight": "20dp",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.locateus.frmLocatorBranchDetailsKA.CopyatmServices00ff586c7132d45"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    FBox03cc3ce49523f48.add(lblOperationHrs, lblOperationHrsValue);
}
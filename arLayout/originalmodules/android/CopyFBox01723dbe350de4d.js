function initializeCopyFBox01723dbe350de4d() {
    CopyFBox01723dbe350de4d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "CopyFBox01723dbe350de4d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox01723dbe350de4d.setDefaultUnit(kony.flex.DP);
    var informationListIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "25dp",
        "id": "informationListIcon",
        "isVisible": true,
        "left": "5%",
        "skin": "sknslImage",
        "src": "imagedrag.png",
        "top": "0dp",
        "width": "20dp"
    }, {
        "containerWeight": 100,
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    var informationListLabel = new kony.ui.Label({
        "id": "informationListLabel",
        "isVisible": true,
        "left": "15%",
        "text": "Label",
        "top": "25dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var informationListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "informationListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": 0,
        "skin": "sknsegmentDivider",
        "top": "99dp",
        "width": "85%",
        "zIndex": 1
    }, {
        "containerWeight": 100
    }, {});
    informationListDivider.setDefaultUnit(kony.flex.DP);
    informationListDivider.add();
    var Label0bd2197c3f99146 = new kony.ui.Label({
        "id": "Label0bd2197c3f99146",
        "isVisible": true,
        "left": "15%",
        "text": "Label",
        "top": "47dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var CopyLabel00d056f230c2546 = new kony.ui.Label({
        "id": "CopyLabel00d056f230c2546",
        "isVisible": true,
        "left": "15%",
        "text": "Label",
        "top": "63dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var Label0102e827d68d940 = new kony.ui.Label({
        "centerY": "50%",
        "id": "Label0102e827d68d940",
        "isVisible": true,
        "right": "10%",
        "text": "Label",
        "top": "39dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    CopyFBox01723dbe350de4d.add(informationListIcon, informationListLabel, informationListDivider, Label0bd2197c3f99146, CopyLabel00d056f230c2546, Label0102e827d68d940);
}
function initializetmSegReleaseNote() {
    flxReleaseNotesTmp = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxReleaseNotesTmp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxReleaseNotesTmp.setDefaultUnit(kony.flex.DP);
    var imgBullet = new kony.ui.Image2({
        "height": "25dp",
        "id": "imgBullet",
        "isVisible": false,
        "left": "2%",
        "skin": "slImage",
        "src": "current_location.png",
        "top": "10dp",
        "width": "25dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblReleaseNotes = new kony.ui.Label({
        "id": "lblReleaseNotes",
        "isVisible": true,
        "left": "22%",
        "minHeight": "40dp",
        "skin": "sknLblSmallWhiteBold100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": "76%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 2],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxCircle = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8dp",
        "id": "flxCircle",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "skin": "flxsknRoundWhiteBG",
        "top": "19dp",
        "width": "8dp",
        "zIndex": 1
    }, {}, {});
    flxCircle.setDefaultUnit(kony.flex.DP);
    flxCircle.add();
    var lblReleaseNotesIcon = new kony.ui.Label({
        "height": "50dp",
        "id": "lblReleaseNotesIcon",
        "isVisible": true,
        "left": "4%",
        "skin": "sknyellowBOJ",
        "text": "E",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "20dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxReleaseNotesTmp.add(imgBullet, lblReleaseNotes, flxCircle, lblReleaseNotesIcon);
}
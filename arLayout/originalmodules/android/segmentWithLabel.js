function initializesegmentWithLabel() {
    Copycontainer09742046d0d2944 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "Copycontainer09742046d0d2944",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    Copycontainer09742046d0d2944.setDefaultUnit(kony.flex.DP);
    var lblSettingsNameKA = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSettingsNameKA",
        "isVisible": true,
        "left": "5%",
        "skin": "skn",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSettingsStatusKA = new kony.ui.Label({
        "centerY": "48.72%",
        "id": "lblSettingsStatusKA",
        "isVisible": true,
        "right": "35dp",
        "skin": "sknSegSecondLabel",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgProgressKey = new kony.ui.Image2({
        "centerY": "50%",
        "height": "20dp",
        "id": "imgProgressKey",
        "isVisible": true,
        "right": "4%",
        "skin": "sknslImage",
        "src": "right_chevron_icon.png",
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Hidden = new kony.ui.Label({
        "id": "Hidden",
        "isVisible": false,
        "left": "145dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    Copycontainer09742046d0d2944.add(lblSettingsNameKA, lblSettingsStatusKA, imgProgressKey, Hidden);
}
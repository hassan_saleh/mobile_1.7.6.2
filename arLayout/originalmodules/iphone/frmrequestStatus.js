function addWidgetsfrmrequestStatus() {
    frmrequestStatus.setDefaultUnit(kony.flex.DP);
    var flxHeaderFxRate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeaderFxRate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "CopysknslFbox0b60a9222667f44",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeaderFxRate.setDefaultUnit(kony.flex.DP);
    var lblNumberKA = new kony.ui.Label({
        "height": "20dp",
        "id": "lblNumberKA",
        "isVisible": false,
        "right": "5dp",
        "skin": "CopyslLabel0f7ca8453c24243",
        "text": "18",
        "top": "13dp",
        "width": "20dp",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxForTappingKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxForTappingKA",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "onTouchEnd": AS_FlexContainer_e7832152ea264dd19a743f59a40cf77f,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "0%",
        "zIndex": 1
    }, {}, {});
    flxForTappingKA.setDefaultUnit(kony.flex.DP);
    flxForTappingKA.add();
    var lblTitleLabel1 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "45%",
        "height": "90%",
        "id": "lblTitleLabel1",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.title.requeststatus"),
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_CHAR_WRAP
    });
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "onClick": AS_FlexContainer_ec02ef75e4d6471ba1c9abd51592900d,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 10
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblBack0c15b1f41de0b41 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "CopylblBack0c15b1f41de0b41",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBack.add(lblBackIcon, CopylblBack0c15b1f41de0b41);
    flxHeaderFxRate.add(lblNumberKA, flxForTappingKA, lblTitleLabel1, flxBack);
    var flxTab = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxTab",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "slFbox",
        "top": "12%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTab.setDefaultUnit(kony.flex.DP);
    var flxContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "40%",
        "clipBounds": true,
        "height": "65%",
        "id": "flxContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFboxOuterRing",
        "top": "0dp",
        "width": "88%",
        "zIndex": 1
    }, {}, {});
    flxContent.setDefaultUnit(kony.flex.DP);
    var btnNew = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnNew",
        "isVisible": true,
        "left": "0",
        "onClick": AS_Button_g09a2b6c58d9449aa3cb4430449e3da6,
        "skin": "slButtonWhiteTab",
        "text": kony.i18n.getLocalizedString("i18n.messages.btnNew"),
        "top": "0dp",
        "width": "33%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnProgress = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnProgress",
        "isVisible": true,
        "left": "0",
        "onClick": AS_Button_g521499a285541ada4001eedbc73929c,
        "skin": "slButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.inProgress"),
        "top": "0dp",
        "width": "33%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnCompleted = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonWhiteTabFocus",
        "height": "98%",
        "id": "btnCompleted",
        "isVisible": true,
        "left": "0",
        "onClick": AS_Button_h7010eddba354dbf886d2f74208a5c1a,
        "skin": "slButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.managepayee.btnCompleted"),
        "width": "34%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxContent.add(btnNew, btnProgress, btnCompleted);
    flxTab.add(flxContent);
    var segRequestStatus = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "lblDelDate": "Delivery Date",
            "lblDelDateValue": "New",
            "lblJoMoPay": "Pin Request",
            "lblRequestDate": "Request Date",
            "lblRequestDateVale": "New",
            "lblStatus": "Status",
            "lblStatusDesc": "New"
        }, {
            "lblDelDate": "Delivery Date",
            "lblDelDateValue": "New",
            "lblJoMoPay": "Pin Request",
            "lblRequestDate": "Request Date",
            "lblRequestDateVale": "New",
            "lblStatus": "Status",
            "lblStatusDesc": "New"
        }, {
            "lblDelDate": "Delivery Date",
            "lblDelDateValue": "New",
            "lblJoMoPay": "Pin Request",
            "lblRequestDate": "Request Date",
            "lblRequestDateVale": "New",
            "lblStatus": "Status",
            "lblStatusDesc": "New"
        }],
        "groupCells": false,
        "height": "80%",
        "id": "segRequestStatus",
        "isVisible": true,
        "left": "5%",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "sknsegCards",
        "rowSkin": "sknsegCards",
        "rowTemplate": flxtmpSegRequestStatus,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "20%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxLine": "flxLine",
            "flxtmpSegRequestStatus": "flxtmpSegRequestStatus",
            "lblDelDate": "lblDelDate",
            "lblDelDateValue": "lblDelDateValue",
            "lblJoMoPay": "lblJoMoPay",
            "lblRequestDate": "lblRequestDate",
            "lblRequestDateVale": "lblRequestDateVale",
            "lblStatus": "lblStatus",
            "lblStatusDesc": "lblStatusDesc"
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": false
    });
    frmrequestStatus.add(flxHeaderFxRate, flxTab, segRequestStatus);
};

function frmrequestStatusGlobals() {
    frmrequestStatus = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmrequestStatus,
        "enabledForIdleTimeout": true,
        "id": "frmrequestStatus",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};
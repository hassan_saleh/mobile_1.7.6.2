function initializetmpCalculateFx() {
    flxTmpCalculateFx = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "85dp",
        "id": "flxTmpCalculateFx",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxTmpCalculateFx.setDefaultUnit(kony.flex.DP);
    var flxFlag = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxFlag",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknflxroundflagBorder",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxFlag.setDefaultUnit(kony.flex.DP);
    var imgFlag = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgFlag",
        "isVisible": true,
        "skin": "slImage",
        "src": "bahrainflag.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxFlag.add(imgFlag);
    var lblConvertedValue = new kony.ui.Label({
        "id": "lblConvertedValue",
        "isVisible": true,
        "left": "22%",
        "skin": "sknlblWhitecarioRegular135",
        "top": "16%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCurrency = new kony.ui.Label({
        "id": "lblCurrency",
        "isVisible": true,
        "left": "22%",
        "skin": "sknlblWhitecariolight115",
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblActualValue = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblActualValue",
        "isVisible": true,
        "left": "58%",
        "skin": "sknlblWhitecarioRegular100",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblEqual = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblEqual",
        "isVisible": true,
        "left": "70%",
        "skin": "sknlblWhitecarioRegular135",
        "text": "=",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblEqualentValue = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblEqualentValue",
        "isVisible": true,
        "left": "76%",
        "skin": "sknlblWhitecarioRegular100",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxTmpCalculateFx.add(flxFlag, lblConvertedValue, lblCurrency, lblActualValue, lblEqual, lblEqualentValue);
}
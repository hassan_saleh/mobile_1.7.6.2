function initializetempCurrencySectionHeader() {
    flxHeadre = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeadre",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxBackgrounf"
    }, {}, {});
    flxHeadre.setDefaultUnit(kony.flex.DP);
    var lblSection = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSection",
        "isVisible": true,
        "left": "15dp",
        "skin": "sknLblWhike150",
        "top": "6dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblLine0i25ba22a14b94a = new kony.ui.Label({
        "height": "1.50%",
        "id": "CopylblLine0i25ba22a14b94a",
        "isVisible": true,
        "left": "0dp",
        "skin": "lblsknToandFromAccLine",
        "top": "97%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxHeadre.add(lblSection, CopylblLine0i25ba22a14b94a);
}
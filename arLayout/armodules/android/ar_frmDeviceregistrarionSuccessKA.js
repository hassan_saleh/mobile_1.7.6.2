//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:35 EET 2020
function addWidgetsfrmDeviceregistrarionSuccessKAAr() {
    frmDeviceregistrarionSuccessKA.setDefaultUnit(kony.flex.DP);
    var touchFeature = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "touchFeature",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    touchFeature.setDefaultUnit(kony.flex.DP);
    var FlexContainer00a3c07fadcbf4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "22%",
        "id": "FlexContainer00a3c07fadcbf4b",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "5%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer00a3c07fadcbf4b.setDefaultUnit(kony.flex.DP);
    var CopysuccessIcon0f42e785ae2344c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "35dp",
        "id": "CopysuccessIcon0f42e785ae2344c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknsuccessIcon",
        "top": "3%",
        "width": "35dp",
        "zIndex": 1
    }, {}, {});
    CopysuccessIcon0f42e785ae2344c.setDefaultUnit(kony.flex.DP);
    var CopyImage031bb4461f4b644 = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "50%",
        "id": "CopyImage031bb4461f4b644",
        "isVisible": true,
        "skin": "sknslImage",
        "src": "check.png",
        "width": "50%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    CopysuccessIcon0f42e785ae2344c.add(CopyImage031bb4461f4b644);
    var CopyLabel04e32e5c0a78845 = new kony.ui.Label({
        "id": "CopyLabel04e32e5c0a78845",
        "isVisible": true,
        "right": "8dp",
        "skin": "sknFaceIDHeader",
        "text": kony.i18n.getLocalizedString("i18n.login.deviceRegistrationSuccessfull"),
        "top": "15dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDevregistraionSuccess = new kony.ui.Label({
        "centerX": "50.03%",
        "id": "lblDevregistraionSuccess",
        "isVisible": true,
        "skin": "sknLatoRegularlbl",
        "text": "This device is now registered for Mobile Banking.",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var ChoiceLabelDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "4dp",
        "id": "ChoiceLabelDivider",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "4%",
        "skin": "sknsegmentDivider",
        "top": "30dp",
        "width": "92%",
        "zIndex": 1
    }, {}, {});
    ChoiceLabelDivider.setDefaultUnit(kony.flex.DP);
    ChoiceLabelDivider.add();
    FlexContainer00a3c07fadcbf4b.add(CopysuccessIcon0f42e785ae2344c, CopyLabel04e32e5c0a78845, lblDevregistraionSuccess, ChoiceLabelDivider);
    var flxTouchIdAndPinSelect = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxTouchIdAndPinSelect",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTouchIdAndPinSelect.setDefaultUnit(kony.flex.DP);
    var lblLoginMethod = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblLoginMethod",
        "isVisible": true,
        "skin": "sknLatoRegularlbl",
        "text": "You can change your login method",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxPinAndTouchIdChoice = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80%",
        "id": "flxPinAndTouchIdChoice",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFbox",
        "top": "5%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPinAndTouchIdChoice.setDefaultUnit(kony.flex.DP);
    var EnableTouchIdCheckBox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "49.97%",
        "clipBounds": true,
        "height": "15%",
        "id": "EnableTouchIdCheckBox",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "CopysknCopyslFbox08b870a93d2574f",
        "top": "5%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    EnableTouchIdCheckBox.setDefaultUnit(kony.flex.DP);
    var imgEnableTouchId = new kony.ui.Image2({
        "height": "40%",
        "id": "imgEnableTouchId",
        "isVisible": true,
        "onTouchEnd": AS_Image_484bbbcab31342c6b08c56317dd0cada,
        "left": "10dp",
        "skin": "slImage",
        "src": "checkbox_selected.png",
        "top": "25%",
        "width": "10%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var lblUseTouchID = new kony.ui.Label({
        "id": "lblUseTouchID",
        "isVisible": true,
        "right": "18%",
        "skin": "sknLatoRegularlbl",
        "text": "TOUCH ID",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgTouchIdKA = new kony.ui.Image2({
        "height": "35dp",
        "id": "imgTouchIdKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "touch.png",
        "top": "6dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    EnableTouchIdCheckBox.add(imgEnableTouchId, lblUseTouchID, imgTouchIdKA);
    var EnableFaceIdCheckBox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "15%",
        "id": "EnableFaceIdCheckBox",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "CopysknCopyslFbox08b870a93d2574f",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    EnableFaceIdCheckBox.setDefaultUnit(kony.flex.DP);
    var lbluseFaceId = new kony.ui.Label({
        "id": "lbluseFaceId",
        "isVisible": true,
        "right": "18%",
        "skin": "sknRegularFormlbl",
        "text": "FACE ID",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var EnableFaceIdImg = new kony.ui.Image2({
        "height": "40%",
        "id": "EnableFaceIdImg",
        "isVisible": true,
        "onTouchEnd": AS_Image_e67ee1a9029b46a6b24c7b6c4f41a01b,
        "left": "10dp",
        "skin": "slImage",
        "src": "checkbox_selected.png",
        "top": "25%",
        "width": "10%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var imgFaceIdKA = new kony.ui.Image2({
        "height": "35dp",
        "id": "imgFaceIdKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "face.png",
        "top": "6dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    EnableFaceIdCheckBox.add(lbluseFaceId, EnableFaceIdImg, imgFaceIdKA);
    var lblRetainlogin = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblRetainlogin",
        "isVisible": false,
        "right": "130dp",
        "skin": "sknLatoRegularlbl",
        "text": "You can retain your login method",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "7%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 3],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var EnablePINCheckBox = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "15%",
        "id": "EnablePINCheckBox",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "CopysknCopyslFbox08b870a93d2574f",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    EnablePINCheckBox.setDefaultUnit(kony.flex.DP);
    var lblUsePin = new kony.ui.Label({
        "id": "lblUsePin",
        "isVisible": true,
        "right": "18%",
        "skin": "sknLatoRegularlbl",
        "text": "PIN",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var EnablePinImg = new kony.ui.Image2({
        "height": "40%",
        "id": "EnablePinImg",
        "isVisible": true,
        "onTouchEnd": AS_Image_a13ab9bb1e8c44c890173dc099fec63f,
        "left": "10dp",
        "skin": "slImage",
        "src": "checkbox_selected.png",
        "top": "25%",
        "width": "10%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var imgPinKA = new kony.ui.Image2({
        "height": "35dp",
        "id": "imgPinKA",
        "isVisible": true,
        "right": "1%",
        "skin": "slImage",
        "src": "pin.png",
        "top": "6dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    EnablePINCheckBox.add(lblUsePin, EnablePinImg, imgPinKA);
    var enableTouchID = new kony.ui.Button({
        "centerX": "49.97%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "enableTouchID",
        "isVisible": true,
        "onClick": AS_Button_b01ede2c7d5e41d0b5f412e75d42e33a,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.btnContinuee"),
        "top": "7%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var noThanks = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "25dp",
        "id": "noThanks",
        "isVisible": true,
        "onClick": AS_Button_7555369e80e14c399577b5fc7b4298e3,
        "skin": "sknsecondaryAction",
        "text": "Do it Later",
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxPinAndTouchIdChoice.add(EnableTouchIdCheckBox, EnableFaceIdCheckBox, lblRetainlogin, EnablePINCheckBox, enableTouchID, noThanks);
    flxTouchIdAndPinSelect.add(lblLoginMethod, flxPinAndTouchIdChoice);
    touchFeature.add(FlexContainer00a3c07fadcbf4b, flxTouchIdAndPinSelect);
    frmDeviceregistrarionSuccessKA.add(touchFeature);
};
function frmDeviceregistrarionSuccessKAGlobalsAr() {
    frmDeviceregistrarionSuccessKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmDeviceregistrarionSuccessKAAr,
        "bounces": false,
        "enableScrolling": true,
        "enabledForIdleTimeout": true,
        "id": "frmDeviceregistrarionSuccessKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "pagingEnabled": false,
        "preShow": AS_Form_084182b0dacf46049d4a86a66fe4553c,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_136e19c6b10d4b008de0ca11c933104b,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};

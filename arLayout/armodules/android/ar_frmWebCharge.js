//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:36 EET 2020
function addWidgetsfrmWebChargeAr() {
frmWebCharge.setDefaultUnit(kony.flex.DP);
var flxBodyWeb = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxBodyWeb",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "CopyslFbox0cd4df9aea5a14b",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBodyWeb.setDefaultUnit(kony.flex.DP);
var flxWebChargeHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxWebChargeHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxWebChargeHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_hd615388afb84591af8b1ab90b5a55ff,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0h2fe661f120d45 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblBack0h2fe661f120d45",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0h2fe661f120d45);
var lblWebCharge = new kony.ui.Label({
"height": "90%",
"id": "lblWebCharge",
"isVisible": true,
"left": "20%",
"minHeight": "90%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.webcharge.title"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnNext = new kony.ui.Button({
"focusSkin": "jomopaynextDisabled",
"height": "90%",
"id": "btnNext",
"isVisible": true,
"onClick": AS_Button_jcf3d5634bc2448aa8d7658bfe79f5ca,
"right": "0.00%",
"skin": "jomopaynextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "0%",
"width": "20%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxWebChargeHeader.add(flxBack, lblWebCharge, btnNext);
var flxWebChargeBody = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxWebChargeBody",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"skin": "sknDetails",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxWebChargeBody.setDefaultUnit(kony.flex.DP);
var flxAccountDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxAccountDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "5%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccountDetails.setDefaultUnit(kony.flex.DP);
var lblCardnum = new kony.ui.Label({
"id": "lblCardnum",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": "Current balance on your card ***114",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "80%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var lblAmount = new kony.ui.Label({
"id": "lblAmount",
"isVisible": true,
"right": "0.00%",
"skin": "CopyslLabel0fac1d7db4bd14c",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCurrency = new kony.ui.Label({
"id": "lblCurrency",
"isVisible": false,
"right": "2%",
"skin": "CopysknTransferType0c1e03cb653744d",
"text": "JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAmount.add(lblAmount, lblCurrency);
flxAccountDetails.add(lblCardnum, flxAmount);
var flxAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxAccount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccount.setDefaultUnit(kony.flex.DP);
var lblAccountStaticText = new kony.ui.Label({
"height": "35%",
"id": "lblAccountStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.settings.account"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAccountType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxAccountType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "slFbox",
"top": "37%",
"width": "96%",
"zIndex": 1
}, {}, {});
flxAccountType.setDefaultUnit(kony.flex.DP);
var btnDropDownAccount = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"height": "100%",
"id": "btnDropDownAccount",
"isVisible": true,
"onClick": AS_Button_de3367f304744fbf9af77b93ef2b50c3,
"left": "6%",
"skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"text": "d",
"top": "-5%",
"width": "93%",
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxAccountNo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxAccountNo",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "1%",
"skin": "slFbox",
"top": "5%",
"width": "80%",
"zIndex": 2
}, {}, {});
flxAccountNo.setDefaultUnit(kony.flex.DP);
var lblAccountType = new kony.ui.Label({
"height": "70%",
"id": "lblAccountType",
"isVisible": true,
"right": "0%",
"skin": "sknTransferType",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountCode = new kony.ui.Label({
"height": "70%",
"id": "lblAccountCode",
"isVisible": true,
"right": "3%",
"skin": "lblsegtextsmall0b5a3b38d4be646",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblhiddenFrmAcc = new kony.ui.Label({
"height": "70%",
"id": "lblhiddenFrmAcc",
"isVisible": false,
"right": "3%",
"skin": "lblsegtextsmall0b5a3b38d4be646",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblhiddenToAcc = new kony.ui.Label({
"height": "70%",
"id": "lblhiddenToAcc",
"isVisible": false,
"right": "3%",
"skin": "lblsegtextsmall0b5a3b38d4be646",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccountNo.add( lblhiddenToAcc, lblhiddenFrmAcc, lblAccountCode,lblAccountType);
flxAccountType.add(btnDropDownAccount, flxAccountNo);
var flxBorderAccountType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "20%",
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderAccountType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDividerGreen",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBorderAccountType.setDefaultUnit(kony.flex.DP);
flxBorderAccountType.add();
flxAccount.add(lblAccountStaticText, flxAccountType, flxBorderAccountType);
var flxAmountDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxAmountDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmountDetails.setDefaultUnit(kony.flex.DP);
var lblAmountStaticText = new kony.ui.Label({
"id": "lblAmountStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblHiddenAmount = new kony.ui.Label({
"id": "lblHiddenAmount",
"isVisible": false,
"right": "15%",
"skin": "lblAccountStaticText",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "15%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var txtFieldAmount = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "45%",
"id": "txtFieldAmount",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
"right": "5%",
"maxTextLength": 9,
"onDone": AS_TextField_e59de8c02998442899f89a1dc1de84aa,
"onTextChange": AS_TextField_a00a6d6ba27147ffa6408a7e2f5d510e,
"onTouchStart": AS_TextField_bfe0bbfc64304b5780f41cb2b631a240,
"placeholder": "0.000",
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "40%",
"width": "90%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onBeginEditing": AS_TextField_hce8f31a9bc048e7bdef1ac38ab9243f,
"onEndEditing": AS_TextField_h700c696b8c94ebd87be37c7c85f6ded,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxBorderAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBorderAmount.setDefaultUnit(kony.flex.DP);
flxBorderAmount.add();
var lblCurrencyCode = new kony.ui.Label({
"id": "lblCurrencyCode",
"isVisible": true,
"right": "88%",
"skin": "lblAccountStaticText",
"text": "JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "42%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAmountDetails.add(lblAmountStaticText, lblHiddenAmount, txtFieldAmount, flxBorderAmount, lblCurrencyCode);
flxWebChargeBody.add(flxAccountDetails, flxAccount, flxAmountDetails);
flxBodyWeb.add(flxWebChargeHeader, flxWebChargeBody);
var flxConfirmPopup = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxConfirmPopup",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "CopyslFbox0b17aedfc0b4841",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmPopup.setDefaultUnit(kony.flex.DP);
var flxConfirmHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxConfirmHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmHeader.setDefaultUnit(kony.flex.DP);
var flxHeaderBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxHeaderBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_d00f0cea3a7b4b6886c49f0b83c6a7d9,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxHeaderBack.setDefaultUnit(kony.flex.DP);
var lblHeaderBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": "j",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblHeaderBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblHeaderBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeaderBack.add(lblHeaderBackIcon, lblHeaderBack);
var lblHeaderTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblHeaderTitle",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnClose = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknDeleteFocus",
"height": "80%",
"id": "btnClose",
"isVisible": false,
"left": "85%",
"onClick": AS_Button_a545ccb44f174df1980a57143ed30421,
"skin": "sknBtnBack",
"text": "O",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxConfirmHeader.add(flxHeaderBack, lblHeaderTitle, btnClose);
var flxDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "9%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDetails.setDefaultUnit(kony.flex.DP);
var flxCardBalance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxCardBalance",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCardBalance.setDefaultUnit(kony.flex.DP);
var lblStaticCardBalance = new kony.ui.Label({
"id": "lblStaticCardBalance",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": "Current Card Balance",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBalanceConfirm = new kony.ui.Label({
"id": "lblBalanceConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"text": "108.000 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCardBalance.add(lblStaticCardBalance, lblBalanceConfirm);
var flxConfirmAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxConfirmAccount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0.00%",
"skin": "slFbox",
"top": "15%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmAccount.setDefaultUnit(kony.flex.DP);
var lblStaticAccount = new kony.ui.Label({
"id": "lblStaticAccount",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.settings.account"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountTypeConfirm = new kony.ui.Label({
"id": "lblAccountTypeConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"text": "Savings Account",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxConfirmAccount.add(lblStaticAccount, lblAccountTypeConfirm);
var flxAmountConfirm = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxAmountConfirm",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0.00%",
"skin": "slFbox",
"top": "29.99%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmountConfirm.setDefaultUnit(kony.flex.DP);
var lblAmountstatic = new kony.ui.Label({
"id": "lblAmountstatic",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAmountConfirm = new kony.ui.Label({
"id": "lblAmountConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"text": "50.000 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAmountConfirm.add(lblAmountstatic, lblAmountConfirm);
var flxFinalCardBalance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxFinalCardBalance",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0.00%",
"skin": "slFbox",
"top": "45.04%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFinalCardBalance.setDefaultUnit(kony.flex.DP);
var lblStaticFinalCardBalance = new kony.ui.Label({
"id": "lblStaticFinalCardBalance",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.webcharge.cardbalanceafter"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFinalAmountConfirm = new kony.ui.Label({
"id": "lblFinalAmountConfirm",
"isVisible": true,
"right": "5%",
"skin": "skncardconfirm",
"text": "158.000 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFinalCardBalance.add(lblStaticFinalCardBalance, lblFinalAmountConfirm);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhite",
"height": "10%",
"id": "btnConfirm",
"isVisible": true,
"onClick": AS_Button_d71b09906c6644798b54816a4d974cce,
"skin": "slButtonWhite",
"text": "Confirm",
"top": "78%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxFees = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxFees",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "60%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFees.setDefaultUnit(kony.flex.DP);
var lblFeesStaticText = new kony.ui.Label({
"id": "lblFeesStaticText",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.opening_account.fees"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxFeeInside = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxFeeInside",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "95%",
"zIndex": 1
}, {}, {});
flxFeeInside.setDefaultUnit(kony.flex.DP);
var lblFee = new kony.ui.Label({
"id": "lblFee",
"isVisible": true,
"right": "0%",
"skin": "sknTransferType",
"text": "2.000",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblJOD = new kony.ui.Label({
"id": "lblJOD",
"isVisible": true,
"right": "2%",
"skin": "sknTransferType",
"text": "JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFeeInside.add( lblJOD,lblFee);
flxFees.add(lblFeesStaticText, flxFeeInside);
flxDetails.add(flxCardBalance, flxConfirmAccount, flxAmountConfirm, flxFinalCardBalance, btnConfirm, flxFees);
flxConfirmPopup.add(flxConfirmHeader, flxDetails);
frmWebCharge.add(flxBodyWeb, flxConfirmPopup);
};
function frmWebChargeGlobalsAr() {
frmWebChargeAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmWebChargeAr,
"enabledForIdleTimeout": true,
"id": "frmWebCharge",
"init": AS_Form_e12052e7e87b4c80b75bb1db3a3685be,
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_b520f9b65ebc47c9b4f8f0ba68a1ca67,
"skin": "slFormCommon"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_b04a8e05cb5847579518a85cfa0c102a,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};

//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:34 EET 2020
function addWidgetsfrmAccountDetailKAAr() {
frmAccountDetailKA.setDefaultUnit(kony.flex.DP);
var accountDetailsScrollContainer = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": false,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "accountDetailsScrollContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "CopysknslFSbox0b54a32ebce3c43",
"top": "0dp",
"verticalScrollIndicator": false,
"width": "100.00%",
"zIndex": 1
}, {}, {});
accountDetailsScrollContainer.setDefaultUnit(kony.flex.DP);
var titleBarAccountDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "38%",
"id": "titleBarAccountDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "CopysknslFbox0a36e6cb491a845",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarAccountDetails.setDefaultUnit(kony.flex.DP);
var typesOfTab = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40dp",
"id": "typesOfTab",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "40dp",
"width": "100%",
"zIndex": 2
}, {}, {});
typesOfTab.setDefaultUnit(kony.flex.DP);
var lblAccounts = new kony.ui.Label({
"centerY": "50%",
"id": "lblAccounts",
"isVisible": true,
"right": "4%",
"onTouchEnd": AS_Label_b8d5d4e30f6545269b1cd1e96772e338,
"skin": "CopyslLabel0gb87ffd1a7a049",
"text": "Accounts",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDeals = new kony.ui.Label({
"centerY": "50%",
"id": "lblDeals",
"isVisible": true,
"right": "34%",
"onTouchEnd": AS_Label_a63c46b360cf412baceecc14e759a89e,
"skin": "CopyslLabel0dcffe61f4a6d43",
"text": "Deals",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLoans = new kony.ui.Label({
"centerY": "50%",
"id": "lblLoans",
"isVisible": true,
"right": "55%",
"onTouchEnd": AS_Label_d47d329208f742928cf608a4dd3fa563,
"skin": "CopyslLabel0bd2a04fe343b46",
"text": "Loans",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
typesOfTab.add(lblAccounts, lblDeals, lblLoans);
var accountBalanceOverview = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "accountBalanceOverview",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "55dp",
"width": "100%",
"zIndex": 2
}, {}, {});
accountBalanceOverview.setDefaultUnit(kony.flex.DP);
var FlexContainer04dfa3182c5f34e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlexContainer04dfa3182c5f34e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer04dfa3182c5f34e.setDefaultUnit(kony.flex.DP);
var lblavailbalance = new kony.ui.Label({
"centerX": "50%",
"id": "lblavailbalance",
"isVisible": true,
"right": "15dp",
"skin": "sknoverviewTypeLabel",
"text": "Salary account *** 454",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var availableBalanceAmount = new kony.ui.Label({
"centerX": "50%",
"id": "availableBalanceAmount",
"isVisible": true,
"left": "137dp",
"skin": "sknCopyslLabel03fbcc1555fb34b",
"text": "2,735.210 JOD",
"top": "13%",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
FlexContainer04dfa3182c5f34e.add(lblavailbalance, availableBalanceAmount);
var flxAccountDetailsSecondary = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxAccountDetailsSecondary",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "100%",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccountDetailsSecondary.setDefaultUnit(kony.flex.DP);
var lblavailbalanceSecondary = new kony.ui.Label({
"centerX": "50%",
"id": "lblavailbalanceSecondary",
"isVisible": true,
"right": "15dp",
"skin": "sknoverviewTypeLabel",
"text": "Salary account *** 454",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var availableBalanceAmountSecondary = new kony.ui.Label({
"centerX": "50%",
"id": "availableBalanceAmountSecondary",
"isVisible": true,
"left": "137dp",
"skin": "sknCopyslLabel03fbcc1555fb34b",
"text": "2,735.210 JOD",
"top": "13%",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccountDetailsSecondary.add(lblavailbalanceSecondary, availableBalanceAmountSecondary);
accountBalanceOverview.add(FlexContainer04dfa3182c5f34e, flxAccountDetailsSecondary);
var tabsWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "90%",
"clipBounds": true,
"height": "40dp",
"id": "tabsWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "1dp",
"skin": "flexTransparent",
"width": "100%",
"zIndex": 1
}, {}, {});
tabsWrapper.setDefaultUnit(kony.flex.DP);
var scheduledTabButton = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "100%",
"id": "scheduledTabButton",
"isVisible": false,
"right": "50%",
"onClick": AS_Button_c50f55da7abc4e9bbc305b28a6a4a969,
"skin": "skntabDeselected",
"text": kony.i18n.getLocalizedString("i18n.common.cSCHEDULED"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 1,0, 0],
"paddingInPixel": false
}, {});
var recentTabButton = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "100%",
"id": "recentTabButton",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_ae94a8f889e641c0a929890f0d92c687,
"skin": "skntabSelected",
"text": kony.i18n.getLocalizedString("i18n.deposit.recent"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 1,0, 0],
"paddingInPixel": false
}, {});
var tabDeselectedIndicator = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "tabDeselectedIndicator",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknCopyslFbox00fe82e6f7cc84b",
"top": "0dp",
"width": "100%"
}, {}, {});
tabDeselectedIndicator.setDefaultUnit(kony.flex.DP);
tabDeselectedIndicator.add();
var tabSelectedIndicator = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "tabSelectedIndicator",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknCopyslFbox03fbc1653617542",
"width": "50%"
}, {}, {});
tabSelectedIndicator.setDefaultUnit(kony.flex.DP);
tabSelectedIndicator.add();
var CopylistDivider0c9d0c3b0715a44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylistDivider0c9d0c3b0715a44",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopylistDivider0c9d0c3b0715a44.setDefaultUnit(kony.flex.DP);
CopylistDivider0c9d0c3b0715a44.add();
var lblListView = new kony.ui.Label({
"centerY": "50%",
"id": "lblListView",
"isVisible": true,
"right": "15%",
"onTouchEnd": AS_Label_f7fb93421982446f86b4d899530149ae,
"skin": "lblListOn",
"text": "W",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblInfoView = new kony.ui.Label({
"centerY": "50%",
"id": "lblInfoView",
"isVisible": true,
"right": "40%",
"onTouchEnd": AS_Label_jb5dd610ee03470db5189b9306c8704e,
"skin": "lblListOff",
"text": "E",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "20%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountOptions = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblAccountOptions",
"isVisible": true,
"onTouchEnd": AS_Label_gb1a2f5c5cf441a8bfbb50dca315092f,
"left": "9%",
"skin": "lblFontSettIconsFont2",
"text": "Q",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "4dp",
"width": "20%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
tabsWrapper.add(scheduledTabButton, recentTabButton, tabDeselectedIndicator, tabSelectedIndicator, CopylistDivider0c9d0c3b0715a44, lblListView, lblInfoView, lblAccountOptions);
var accountDetailsOverview = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "250dp",
"id": "accountDetailsOverview",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknaccountDetailsHeader",
"top": "0dp",
"width": "100%"
}, {}, {});
accountDetailsOverview.setDefaultUnit(kony.flex.DP);
var gradientOverlay = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "39%",
"id": "gradientOverlay",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknaccountDetailsGradient",
"width": "100%",
"zIndex": 1
}, {}, {});
gradientOverlay.setDefaultUnit(kony.flex.DP);
gradientOverlay.add();
var flxAccntDetailButtonsKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "flxAccntDetailButtonsKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "140dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccntDetailButtonsKA.setDefaultUnit(kony.flex.DP);
var accountInfoButton = new kony.ui.Button({
"bottom": "10%",
"focusSkin": "sknsecondaryActionReversedFocus",
"height": "40dp",
"id": "accountInfoButton",
"isVisible": false,
"right": "15dp",
"onClick": AS_Button_fa7e771f72f74ddab0c58d750bea00a7,
"skin": "sknsecondaryActionReversed",
"text": kony.i18n.getLocalizedString("i18n.Accounts.DetailsC"),
"top": 0,
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnaccountstatements = new kony.ui.Button({
"bottom": "10%",
"focusSkin": "sknsecondaryActionReversedFocus",
"height": "40dp",
"id": "btnaccountstatements",
"isVisible": false,
"onClick": AS_Button_gc069ac275324a818cec5af106c77dea,
"left": "15dp",
"skin": "sknsecondaryActionReversed",
"text": "STATEMENTS",
"top": 0,
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxAccntDetailButtonsKA.add(accountInfoButton, btnaccountstatements);
var actionWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10dp",
"clipBounds": true,
"height": "45%",
"id": "actionWrapper",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "3dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "sknslFbox0ae62737fa1334d",
"top": "126dp",
"width": "100%",
"zIndex": 1
}, {}, {});
actionWrapper.setDefaultUnit(kony.flex.DP);
var AccountDetailsWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": false,
"height": "100dp",
"id": "AccountDetailsWrapper",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "skncontainerBkgNone0bb6a50826c6d49",
"width": "33%",
"zIndex": 1
}, {}, {});
AccountDetailsWrapper.setDefaultUnit(kony.flex.DP);
var accountDetailsFlx = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "accountDetailsFlx",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0ae62737fa1334d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
accountDetailsFlx.setDefaultUnit(kony.flex.DP);
var accountDetailsIcon = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "accountDetailsIcon",
"isVisible": true,
"skin": "sknslImage0c4dffbb22b0745",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var AccountDetailsLbl = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "AccountDetailsLbl",
"isVisible": true,
"skin": "skniconButtonLabel0c37db8b353724e",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
accountDetailsFlx.add(accountDetailsIcon, AccountDetailsLbl);
var buttonDivider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "buttonDivider",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 1,
"skin": "sknwhiteDivider0gdcc22aab0424e",
"width": "1dp",
"zIndex": 1
}, {}, {});
buttonDivider.setDefaultUnit(kony.flex.DP);
buttonDivider.add();
var AccountDetailsButton = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0b86a9ce9a15c4d",
"height": "100%",
"id": "AccountDetailsButton",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_b4a1c7b9c82744658680ac24ee49c50d,
"skin": "skninvisibleButtonNormal0b2be2515c86a4e",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
AccountDetailsWrapper.add(accountDetailsFlx, buttonDivider, AccountDetailsButton);
var StatementsWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100dp",
"id": "StatementsWrapper",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "skncontainerBkgNone0bb6a50826c6d49",
"width": "33.33%",
"zIndex": 1
}, {}, {});
StatementsWrapper.setDefaultUnit(kony.flex.DP);
var StatementsFlx = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "StatementsFlx",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0ae62737fa1334d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
StatementsFlx.setDefaultUnit(kony.flex.DP);
var StatementsIcon = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "StatementsIcon",
"isVisible": true,
"skin": "sknslImage0c4dffbb22b0745",
"src": "statementicon.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var StatementsLbl = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "StatementsLbl",
"isVisible": true,
"skin": "skniconButtonLabel0c37db8b353724e",
"text": kony.i18n.getLocalizedString("i18n.overview.accountStatements"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
StatementsFlx.add(StatementsIcon, StatementsLbl);
var buttonDivider2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "buttonDivider2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 1,
"skin": "sknwhiteDivider0gdcc22aab0424e",
"width": "1dp",
"zIndex": 1
}, {}, {});
buttonDivider2.setDefaultUnit(kony.flex.DP);
buttonDivider2.add();
var StatementsButton = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0b86a9ce9a15c4d",
"height": "100%",
"id": "StatementsButton",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_fdc0727cc5db40f4acb9787a4bcbf703,
"skin": "skninvisibleButtonNormal0b2be2515c86a4e",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
StatementsWrapper.add(StatementsFlx, buttonDivider2, StatementsButton);
var MoreWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100dp",
"id": "MoreWrapper",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "skncontainerBkgNone0bb6a50826c6d49",
"width": "33%",
"zIndex": 1
}, {}, {});
MoreWrapper.setDefaultUnit(kony.flex.DP);
var MoreFlx = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "MoreFlx",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0ae62737fa1334d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
MoreFlx.setDefaultUnit(kony.flex.DP);
var MoreIcon = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "MoreIcon",
"isVisible": true,
"skin": "sknslImage0c4dffbb22b0745",
"src": "moreicon.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Morelbl = new kony.ui.Label({
"bottom": "10%",
"centerX": "50%",
"height": "39dp",
"id": "Morelbl",
"isVisible": true,
"skin": "skniconButtonLabel0c37db8b353724e",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"top": "9dp",
"width": "75%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
MoreFlx.add(MoreIcon, Morelbl);
var MoreButton = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0b86a9ce9a15c4d",
"height": "97.00%",
"id": "MoreButton",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_bea0eaf428c747fdae271cba8f5ac59e,
"skin": "skninvisibleButtonNormal0b2be2515c86a4e",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
MoreWrapper.add(MoreFlx, MoreButton);
actionWrapper.add( MoreWrapper, StatementsWrapper,AccountDetailsWrapper);
var flxMore = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50.01%",
"clipBounds": true,
"id": "flxMore",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "7.84%",
"skin": "CopyslFbox0c615f21e868d4f",
"top": "140dp",
"width": "100%",
"zIndex": 10
}, {}, {});
flxMore.setDefaultUnit(kony.flex.DP);
var flxMoreOption1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "flxMoreOption1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "0dp",
"width": "33%",
"zIndex": 1
}, {}, {});
flxMoreOption1.setDefaultUnit(kony.flex.DP);
var flxm1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "flxm1",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxm1.setDefaultUnit(kony.flex.DP);
var MoreIcon1 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "MoreIcon1",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreLabel1 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "MoreLabel1",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxm1.add(MoreIcon1, MoreLabel1);
var Morebtn1 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "Morebtn1",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_i54db12ecdb840ce8f0684e06db573f5,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreHiddenKey1 = new kony.ui.Label({
"id": "MoreHiddenKey1",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMoreOption1.add(flxm1, Morebtn1, MoreHiddenKey1);
var flxMoreOption2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "flxMoreOption2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "0dp",
"width": "33%",
"zIndex": 1
}, {}, {});
flxMoreOption2.setDefaultUnit(kony.flex.DP);
var flxm2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "flxm2",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxm2.setDefaultUnit(kony.flex.DP);
var MoreIcon2 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "MoreIcon2",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreLabel2 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "MoreLabel2",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxm2.add(MoreIcon2, MoreLabel2);
var Morebtn2 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "Morebtn2",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_f0d8df876d4c43218db95ad2ea9dfe92,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreHiddenKey2 = new kony.ui.Label({
"id": "MoreHiddenKey2",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMoreOption2.add(flxm2, Morebtn2, MoreHiddenKey2);
var flxMoreOption3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "flxMoreOption3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "120dp",
"width": "33%",
"zIndex": 1
}, {}, {});
flxMoreOption3.setDefaultUnit(kony.flex.DP);
var flxm3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "flxm3",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxm3.setDefaultUnit(kony.flex.DP);
var MoreIcon3 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "MoreIcon3",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreLabel3 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "MoreLabel3",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxm3.add(MoreIcon3, MoreLabel3);
var Morebtn3 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "Morebtn3",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_ed52e33870904115b74c23556f90f40d,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreHiddenKey3 = new kony.ui.Label({
"id": "MoreHiddenKey3",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMoreOption3.add(flxm3, Morebtn3, MoreHiddenKey3);
var flxMoreOption4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "flxMoreOption4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "120dp",
"width": "33%",
"zIndex": 1
}, {}, {});
flxMoreOption4.setDefaultUnit(kony.flex.DP);
var flxm4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "flxm4",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxm4.setDefaultUnit(kony.flex.DP);
var MoreIcon4 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "MoreIcon4",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreLabel4 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "MoreLabel4",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxm4.add(MoreIcon4, MoreLabel4);
var Morebtn4 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "Morebtn4",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_ae2fec46216643fa8a0f6457d00f0eff,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreHiddenKey4 = new kony.ui.Label({
"id": "MoreHiddenKey4",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMoreOption4.add(flxm4, Morebtn4, MoreHiddenKey4);
var flxMoreOption5 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "flxMoreOption5",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "240dp",
"width": "33%",
"zIndex": 1
}, {}, {});
flxMoreOption5.setDefaultUnit(kony.flex.DP);
var flxm5 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "flxm5",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxm5.setDefaultUnit(kony.flex.DP);
var MoreIcon5 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "MoreIcon5",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreLabel5 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "MoreLabel5",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxm5.add(MoreIcon5, MoreLabel5);
var Morebtn5 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "Morebtn5",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_c0857f4c28e342b4a180ea4b43bfdf91,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreHiddenKey5 = new kony.ui.Label({
"id": "MoreHiddenKey5",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMoreOption5.add(flxm5, Morebtn5, MoreHiddenKey5);
var flxMoreOption6 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "flxMoreOption6",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "240dp",
"width": "33%",
"zIndex": 1
}, {}, {});
flxMoreOption6.setDefaultUnit(kony.flex.DP);
var flxm6 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "flxm6",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxm6.setDefaultUnit(kony.flex.DP);
var MoreIcon6 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "MoreIcon6",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreLabel6 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "MoreLabel6",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxm6.add(MoreIcon6, MoreLabel6);
var Morebtn6 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "Morebtn6",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_gcbe059d2ba54eba994daf4645c737b9,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreHiddenKey6 = new kony.ui.Label({
"id": "MoreHiddenKey6",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMoreOption6.add(flxm6, Morebtn6, MoreHiddenKey6);
var flxMoreOption7 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "flxMoreOption7",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "360dp",
"width": "33%",
"zIndex": 1
}, {}, {});
flxMoreOption7.setDefaultUnit(kony.flex.DP);
var flxm7 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "flxm7",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxm7.setDefaultUnit(kony.flex.DP);
var MoreIcon7 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "MoreIcon7",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreLabel7 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "MoreLabel7",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxm7.add(MoreIcon7, MoreLabel7);
var Morebtn7 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "Morebtn7",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_f4ef4398400b4171b691c02baff1203d,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreHiddenKey7 = new kony.ui.Label({
"id": "MoreHiddenKey7",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMoreOption7.add(flxm7, Morebtn7, MoreHiddenKey7);
var flxMoreOption8 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "flxMoreOption8",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "360dp",
"width": "33%",
"zIndex": 1
}, {}, {});
flxMoreOption8.setDefaultUnit(kony.flex.DP);
var flxm8 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "flxm8",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxm8.setDefaultUnit(kony.flex.DP);
var MoreIcon8 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "MoreIcon8",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreLabel8 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "MoreLabel8",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxm8.add(MoreIcon8, MoreLabel8);
var Morebtn8 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "Morebtn8",
"isVisible": true,
"onClick": AS_Button_c133b23f4a504a3cb1f9ca58870cb1ec,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var MoreHiddenKey8 = new kony.ui.Label({
"id": "MoreHiddenKey8",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMoreOption8.add(flxm8, Morebtn8, MoreHiddenKey8);
var lineDivider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50%",
"clipBounds": true,
"id": "lineDivider",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 150,
"skin": "sknwhiteDivider0a18e65b4e8c444",
"top": 0,
"width": "3dp",
"zIndex": 1
}, {}, {});
lineDivider.setDefaultUnit(kony.flex.DP);
lineDivider.add();
var collapseButton = new kony.ui.Button({
"bottom": "60px",
"centerX": "50%",
"centerY": "380dp",
"focusSkin": "roundCollapseButton",
"height": "50dp",
"id": "collapseButton",
"isVisible": true,
"right": 135,
"onClick": AS_Button_i2608f930790492f891625412f769468,
"skin": "roundCollapseButton",
"width": "50dp",
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxMore.add(flxMoreOption1, flxMoreOption2, flxMoreOption3, flxMoreOption4, flxMoreOption5, flxMoreOption6, flxMoreOption7, flxMoreOption8, lineDivider, collapseButton);
accountDetailsOverview.add(gradientOverlay, flxAccntDetailButtonsKA, actionWrapper, flxMore);
titleBarAccountDetails.add(typesOfTab, accountBalanceOverview, tabsWrapper, accountDetailsOverview);
var imgIndicator = new kony.ui.Image2({
"centerY": "38.60%",
"id": "imgIndicator",
"isVisible": true,
"right": "9%",
"skin": "slImage",
"src": "searcha.png",
"width": "20%",
"zIndex": 3
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxSegmentWrapper = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "60%",
"horizontalScrollIndicator": true,
"id": "flxSegmentWrapper",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "1dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknGradientbluetodark",
"top": "40%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 2
}, {}, {});
flxSegmentWrapper.setDefaultUnit(kony.flex.DP);
var resourcesLabel = new kony.ui.Label({
"height": "14.50%",
"id": "resourcesLabel",
"isVisible": false,
"right": "0dp",
"skin": "sknsecondaryHeader",
"text": kony.i18n.getLocalizedString("i18n.common.recentTransactions"),
"top": "0dp",
"width": "100%",
"zIndex": 10000
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxExtraIcons = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxExtraIcons",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxExtraIcons.setDefaultUnit(kony.flex.DP);
var lblSearch = new kony.ui.Label({
"centerY": "50%",
"id": "lblSearch",
"isVisible": true,
"right": "4%",
"skin": "sknSearchIcon",
"text": "h",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "8dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var txtSearch = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"focusSkin": "sknTxtBoxSearch",
"height": "30dp",
"id": "txtSearch",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "8%",
"maxTextLength": 30,
"onDone": AS_TextField_a2a3bd6e53b144f79cec73b66257114e,
"placeholder": kony.i18n.getLocalizedString("i18n.search.SearchTransactions"),
"secureTextEntry": false,
"skin": "sknTxtBoxSearch",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "15dp",
"width": "185dp",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "skntxtSearchPlaceholder",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblExport = new kony.ui.Label({
"height": "100%",
"id": "lblExport",
"isVisible": true,
"onTouchEnd": AS_Label_a98dd929857f442ba8989a685259746a,
"left": "12%",
"skin": "CopyslLabel0i2b4c385eb5140",
"text": "l",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFilter = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblFilter",
"isVisible": false,
"left": "4%",
"skin": "CopyslLabel0b8f2bdd739144d",
"text": "n",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblFilter0f7770a92cff247 = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblFilter0f7770a92cff247",
"isVisible": true,
"onTouchEnd": AS_Label_j818a1de1a7c4270b7ee322f068e6185,
"left": "4%",
"skin": "CopyslLabel0b8f2bdd739144d",
"text": "n",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "CopyslFbox0ab42d5f22ae345",
"top": "82%",
"width": "200dp",
"zIndex": 2
}, {}, {});
flxLine.setDefaultUnit(kony.flex.DP);
flxLine.add();
flxExtraIcons.add(lblSearch, txtSearch, lblExport, lblFilter, CopylblFilter0f7770a92cff247, flxLine);
var CopyflxLine0a4e3964e40ce45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxLine0a4e3964e40ce45",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknLineDarkBlue",
"top": "2%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyflxLine0a4e3964e40ce45.setDefaultUnit(kony.flex.DP);
CopyflxLine0a4e3964e40ce45.add();
var flxUnClearedTransactions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxUnClearedTransactions",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_fe50fb1ff5af40699315301599553287,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnClearedTransactions.setDefaultUnit(kony.flex.DP);
var lblUnclearedTransactions = new kony.ui.Label({
"centerY": "50%",
"id": "lblUnclearedTransactions",
"isVisible": true,
"right": "5%",
"skin": "sknLightBlue110perCarioSemiBold",
"text": kony.i18n.getLocalizedString("i18n.accounts.unclearedFunds"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxUnClearedFundsDownArrow = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "90%",
"id": "flxUnClearedFundsDownArrow",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "slFbox",
"width": "8%",
"zIndex": 1
}, {}, {});
flxUnClearedFundsDownArrow.setDefaultUnit(kony.flex.DP);
var lblUnClearedFundsRing = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblUnClearedFundsRing",
"isVisible": true,
"skin": "sknBOJFontLightBlue140per",
"text": "V",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxUnClearedFundsDownArrow.add(lblUnClearedFundsRing);
var lblUnClearedTransactionTotal = new kony.ui.Label({
"centerY": "50%",
"id": "lblUnClearedTransactionTotal",
"isVisible": true,
"left": "13%",
"skin": "sknLightBlue110perCarioSemiBold",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxUnClearedTransactions.add(lblUnclearedTransactions, flxUnClearedFundsDownArrow, lblUnClearedTransactionTotal);
var CopyflxLine0bf7c1444b0a84f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxLine0bf7c1444b0a84f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknLineDarkBlue",
"top": "2%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyflxLine0bf7c1444b0a84f.setDefaultUnit(kony.flex.DP);
CopyflxLine0bf7c1444b0a84f.add();
var accountDetailsTransactions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "71%",
"id": "accountDetailsTransactions",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0.00%",
"width": "100%"
}, {}, {});
accountDetailsTransactions.setDefaultUnit(kony.flex.DP);
var transactionSegment = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"ImgWithDraw": "",
"Imgcheck": "",
"chevron": "",
"lblExpiryTime": "",
"lblLastTransaction": "",
"lblNoData": "",
"lblSepKA": "",
"transactionAmount": "",
"transactionAmountSI": "",
"transactionDate": "",
"transactionName": ""
}],
"groupCells": false,
"height": "98%",
"id": "transactionSegment",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "sknsegAcc",
"rowSkin": "sknsegAcc",
"rowTemplate": CopyFlexContainer011baa9bcad7141,
"scrollingEvents": {},
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "072c4800",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer011baa9bcad7141": "CopyFlexContainer011baa9bcad7141",
"ImgWithDraw": "ImgWithDraw",
"Imgcheck": "Imgcheck",
"chevron": "chevron",
"flxImageandNameKA": "flxImageandNameKA",
"lblExpiryTime": "lblExpiryTime",
"lblLastTransaction": "lblLastTransaction",
"lblNoData": "lblNoData",
"lblSepKA": "lblSepKA",
"transactionAmount": "transactionAmount",
"transactionAmountSI": "transactionAmountSI",
"transactionDate": "transactionDate",
"transactionName": "transactionName"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var scheduledTransactionSegment = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bottom": "5dp",
"data": [{
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}],
"groupCells": false,
"id": "scheduledTransactionSegment",
"isVisible": false,
"right": "50%",
"needPageIndicator": true,
"onRowClick": AS_Segment_g9a3686e87db426d8d977b85789a9cf1,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowTemplate": CopyFlexContainer011baa9bcad7141,
"scrollingEvents": {},
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "ffffff00",
"separatorRequired": true,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer011baa9bcad7141": "CopyFlexContainer011baa9bcad7141",
"ImgWithDraw": "ImgWithDraw",
"Imgcheck": "Imgcheck",
"chevron": "chevron",
"flxImageandNameKA": "flxImageandNameKA",
"lblExpiryTime": "lblExpiryTime",
"lblLastTransaction": "lblLastTransaction",
"lblNoData": "lblNoData",
"lblSepKA": "lblSepKA",
"transactionAmount": "transactionAmount",
"transactionAmountSI": "transactionAmountSI",
"transactionDate": "transactionDate",
"transactionName": "transactionName"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var LabelNoRecordsKA = new kony.ui.Label({
"centerY": "50%",
"id": "LabelNoRecordsKA",
"isVisible": false,
"right": "0dp",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.common.NoTransactions"),
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblSheduledNoRecordsKA = new kony.ui.Label({
"centerY": "50%",
"id": "lblSheduledNoRecordsKA",
"isVisible": false,
"right": "50%",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.common.NoTransactions"),
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var segmentBorderBottom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "segmentBorderBottom",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
segmentBorderBottom.setDefaultUnit(kony.flex.DP);
segmentBorderBottom.add();
var FlexContainer0836bbc7ed5014a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100dp",
"id": "FlexContainer0836bbc7ed5014a",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "-2dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0836bbc7ed5014a.setDefaultUnit(kony.flex.DP);
FlexContainer0836bbc7ed5014a.add();
accountDetailsTransactions.add(transactionSegment, scheduledTransactionSegment, LabelNoRecordsKA, lblSheduledNoRecordsKA, segmentBorderBottom, FlexContainer0836bbc7ed5014a);
flxSegmentWrapper.add(resourcesLabel, flxExtraIcons, CopyflxLine0a4e3964e40ce45, flxUnClearedTransactions, CopyflxLine0bf7c1444b0a84f, accountDetailsTransactions);
var accountDetailsTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "9%",
"id": "accountDetailsTitleBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
accountDetailsTitleBar.setDefaultUnit(kony.flex.DP);
var accountDetailsHeader = new kony.ui.Label({
"centerX": "50%",
"height": "22dp",
"id": "accountDetailsHeader",
"isVisible": false,
"skin": "sknnavBarTitle",
"text": "John's Primary Checking Acct",
"top": "15dp",
"width": "68%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var accountDetailsTime = new kony.ui.Label({
"centerX": "50%",
"id": "accountDetailsTime",
"isVisible": false,
"skin": "sknasOfTimeLabel",
"text": " 4:33 PM",
"top": "37dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var closeButton = new kony.ui.Button({
"focusSkin": "sknCopyinvisibleButtonFocus0df746dda29534b",
"height": "50dp",
"id": "closeButton",
"isVisible": false,
"left": "0dp",
"onClick": AS_Button_fb245ecc8971403abe69c66b69b5d26f,
"skin": "sknCopyinvisibleButton046d30d34316f47",
"top": "6dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": false,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var searchButton = new kony.ui.Button({
"focusSkin": "skntitleBarSearchButtonFocus",
"height": "50dp",
"id": "searchButton",
"isVisible": false,
"left": "86.11%",
"onClick": AS_Button_cf1c1c9820b4448a91f5f3319bc36d94,
"right": "1%",
"skin": "skntitleBarSearchButton",
"top": "8dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var lblBoj = new kony.ui.Label({
"centerY": "50%",
"height": "90%",
"id": "lblBoj",
"isVisible": true,
"left": "7.50%",
"skin": "accountDealLoanBackSkin",
"text": kony.i18n.getLocalizedString("i18n.accounts.accounts"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMsg = new kony.ui.Label({
"id": "lblMsg",
"isVisible": false,
"right": "15%",
"skin": "CopyslLabel0if62558c2f444d",
"text": "Y",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "11dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblUserProfileIcon = new kony.ui.Label({
"height": "50dp",
"id": "lblUserProfileIcon",
"isVisible": false,
"onTouchEnd": AS_Label_f104be42b6b849e6b16538e64febe2a0,
"right": "0%",
"skin": "CopyslLabel0cbb44b1e4e6f4a",
"text": "F",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "40dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"height": "90%",
"id": "lblBackIcon",
"isVisible": true,
"left": "1%",
"onTouchEnd": AS_Label_c71cd08657254db5ab3ca89bb1e72c54,
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "30%",
"zIndex": 20
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [1, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var FlexContainer0b9fde1e61d3c44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlexContainer0b9fde1e61d3c44",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "slFbox",
"top": "0dp",
"width": "100dp",
"zIndex": 1
}, {}, {});
FlexContainer0b9fde1e61d3c44.setDefaultUnit(kony.flex.DP);
var btnNotification = new kony.ui.Button({
"focusSkin": "BtnNotificationMail",
"height": "45dp",
"id": "btnNotification",
"isVisible": false,
"left": "10dp",
"skin": "BtnNotificationMail",
"text": "Y",
"top": "0dp",
"width": "42dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": false,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var btnProfile = new kony.ui.Button({
"focusSkin": "btnUser",
"height": "50dp",
"id": "btnProfile",
"isVisible": true,
"left": "54dp",
"onClick": AS_Button_g367d42e106b463f954721b8e94951f4,
"skin": "btnUser",
"text": "F",
"top": "0dp",
"width": "40dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
FlexContainer0b9fde1e61d3c44.add(btnNotification, btnProfile);
accountDetailsTitleBar.add(accountDetailsHeader, accountDetailsTime, closeButton, searchButton, lblBoj, lblMsg, lblUserProfileIcon, lblBackIcon, FlexContainer0b9fde1e61d3c44);
var flxAccountOptions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"id": "flxAccountOptions",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "40%",
"width": "100%",
"zIndex": 110
}, {}, {});
flxAccountOptions.setDefaultUnit(kony.flex.DP);
var flxRequestStatement = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxRequestStatement",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 10
}, {}, {});
flxRequestStatement.setDefaultUnit(kony.flex.DP);
var CopyflxRight0d26c9310984f45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0d26c9310984f45",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_a7f78f522d994143a2ed3d7942479db3,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxRight0d26c9310984f45.setDefaultUnit(kony.flex.DP);
var CopyinterestEarnedLabel0g3277f09dbea43 = new kony.ui.Label({
"centerY": "50%",
"height": "30dp",
"id": "CopyinterestEarnedLabel0g3277f09dbea43",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.accounts.requestStatement"),
"top": "26dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblCcPaynowIcon0g68c0fdb1f2049 = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0g68c0fdb1f2049",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "S",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblEdit0cb8bdbb5180c41 = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0cb8bdbb5180c41",
"isVisible": true,
"left": "0%",
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxRight0d26c9310984f45.add(CopyinterestEarnedLabel0g3277f09dbea43, CopylblCcPaynowIcon0g68c0fdb1f2049, CopylblEdit0cb8bdbb5180c41);
var CopyflxInfoLine0h03e27421c7942 = new kony.ui.Label({
"centerY": "50%",
"id": "CopyflxInfoLine0h03e27421c7942",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "I",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxRequestStatement.add(CopyflxRight0d26c9310984f45, CopyflxInfoLine0h03e27421c7942);
var flxOrderCheckBook = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "flxOrderCheckBook",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flxsegBg",
"top": "1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOrderCheckBook.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0gafe4c0d61ef4f = new kony.ui.Label({
"centerY": "50%",
"id": "CopyflxInfoLine0gafe4c0d61ef4f",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "I",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxRight0e87efefcf3084c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxRight0e87efefcf3084c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_f7800c64f31444fb860620e18a315379,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 10
}, {}, {});
CopyflxRight0e87efefcf3084c.setDefaultUnit(kony.flex.DP);
var CopyinterestEarnedLabel0h59544fc55f948 = new kony.ui.Label({
"centerY": "50%",
"height": "30dp",
"id": "CopyinterestEarnedLabel0h59544fc55f948",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.accounts.orderchequebook"),
"top": "26dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblCcPaynowIcon0ia9bd60362544a = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0ia9bd60362544a",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "F",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblEdit0b4a3f2847e3d44 = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0b4a3f2847e3d44",
"isVisible": true,
"left": "0%",
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxRight0e87efefcf3084c.add(CopyinterestEarnedLabel0h59544fc55f948, CopylblCcPaynowIcon0ia9bd60362544a, CopylblEdit0b4a3f2847e3d44);
flxOrderCheckBook.add(CopyflxInfoLine0gafe4c0d61ef4f, CopyflxRight0e87efefcf3084c);
flxAccountOptions.add(flxRequestStatement, flxOrderCheckBook);
var imgDivSett = new kony.ui.Image2({
"centerY": "38.60%",
"id": "imgDivSett",
"isVisible": false,
"left": "9%",
"skin": "slImage",
"src": "searcha.png",
"width": "20%",
"zIndex": 110
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
accountDetailsScrollContainer.add(titleBarAccountDetails, imgIndicator, flxSegmentWrapper, accountDetailsTitleBar, flxAccountOptions, imgDivSett);
var accountLabelScroll = new kony.ui.Label({
"height": "50dp",
"id": "accountLabelScroll",
"isVisible": false,
"right": "0dp",
"skin": "sknCopysecondaryHeader07cfb9398604f4f",
"text": kony.i18n.getLocalizedString("i18n.common.recentTransactions"),
"top": "135dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDealsScreen = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxDealsScreen",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "100%",
"skin": "CopyslFbox0gb59b51361be42",
"top": "0.00%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDealsScreen.setDefaultUnit(kony.flex.DP);
var flxDealsTitleBarDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "38%",
"id": "flxDealsTitleBarDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopysknslFbox0a36e6cb491a845",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDealsTitleBarDetails.setDefaultUnit(kony.flex.DP);
var flxDealsTopBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "40dp",
"id": "flxDealsTopBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxDealsTopBar.setDefaultUnit(kony.flex.DP);
var CopyaccountDetailsHeader0g07eb6ed69a146 = new kony.ui.Label({
"centerX": "50%",
"height": "22dp",
"id": "CopyaccountDetailsHeader0g07eb6ed69a146",
"isVisible": false,
"skin": "sknnavBarTitle",
"text": "John's Primary Checking Acct",
"top": "15dp",
"width": "68%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyaccountDetailsTime0jb7b50b0fdb94d = new kony.ui.Label({
"centerX": "50%",
"id": "CopyaccountDetailsTime0jb7b50b0fdb94d",
"isVisible": false,
"skin": "sknasOfTimeLabel",
"text": " 4:33 PM",
"top": "37dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopycloseButton0b1fe6d68906246 = new kony.ui.Button({
"focusSkin": "sknCopyinvisibleButtonFocus0df746dda29534b",
"height": "50dp",
"id": "CopycloseButton0b1fe6d68906246",
"isVisible": false,
"left": "0dp",
"onClick": AS_Button_d8d7a873581b457c93cc916abca2ee8f,
"skin": "sknCopyinvisibleButton046d30d34316f47",
"top": "6dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": false,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var CopysearchButton0i615ed6f4c7e47 = new kony.ui.Button({
"focusSkin": "skntitleBarSearchButtonFocus",
"height": "50dp",
"id": "CopysearchButton0i615ed6f4c7e47",
"isVisible": false,
"left": "86.11%",
"onClick": AS_Button_h7c3386af28c4efd963b32bb1a62eb52,
"right": "1%",
"skin": "skntitleBarSearchButton",
"top": "8dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var lblDealsMsg = new kony.ui.Label({
"id": "lblDealsMsg",
"isVisible": true,
"right": "15%",
"skin": "CopyslLabel0if62558c2f444d",
"text": "Y",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "7dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDealsBank = new kony.ui.Label({
"height": "50dp",
"id": "lblDealsBank",
"isVisible": false,
"left": "4%",
"skin": "CopyslLabel0bb55fd33e1b64e",
"text": "C",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDealsProfile = new kony.ui.Label({
"id": "lblDealsProfile",
"isVisible": true,
"right": "2%",
"skin": "CopyslLabel0cbb44b1e4e6f4a",
"text": "F",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "7dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBojDeal = new kony.ui.Label({
"height": "30dp",
"id": "lblBojDeal",
"isVisible": true,
"left": "8%",
"skin": "accountDealLoanBackSkin",
"text": "Deals",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBackDealIcon = new kony.ui.Label({
"height": "50dp",
"id": "lblBackDealIcon",
"isVisible": true,
"left": "0%",
"onTouchEnd": AS_Label_a1928eebf22847b9bbcce0ec29acd050,
"skin": "backTransactionIconSkin",
"text": "j",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [1, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDealsTopBar.add(CopyaccountDetailsHeader0g07eb6ed69a146, CopyaccountDetailsTime0jb7b50b0fdb94d, CopycloseButton0b1fe6d68906246, CopysearchButton0i615ed6f4c7e47, lblDealsMsg, lblDealsBank, lblDealsProfile, lblBojDeal, lblBackDealIcon);
var flxDealsAccountBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40dp",
"id": "flxDealsAccountBar",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "40dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxDealsAccountBar.setDefaultUnit(kony.flex.DP);
var lblDealsDeals = new kony.ui.Label({
"centerY": "50%",
"id": "lblDealsDeals",
"isVisible": true,
"right": "4%",
"onTouchEnd": AS_Label_id16a7d1cdf642f6a74222ac6c6a277c,
"skin": "CopyslLabel0e2dc1030f8784c",
"text": "Deals",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDealsAccount = new kony.ui.Label({
"centerY": "50%",
"id": "lblDealsAccount",
"isVisible": true,
"right": "25%",
"onTouchEnd": AS_Label_b7ac4285daf9433bb3ca53053684dd84,
"skin": "CopyslLabel0fcccbc7f62984f",
"text": "Accounts",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDealsLoans = new kony.ui.Label({
"centerY": "50%",
"id": "lblDealsLoans",
"isVisible": true,
"right": "55%",
"onTouchEnd": AS_Label_h4266fb6dc3d444dab4c3d29efbd4dde,
"skin": "CopyslLabel0bfdb4f6e0bb547",
"text": "Loans",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDealsAccountBar.add(lblDealsDeals, lblDealsAccount, lblDealsLoans);
var flxDealsBalance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "flxDealsBalance",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "60dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxDealsBalance.setDefaultUnit(kony.flex.DP);
var flxDealsBalanceOverview = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxDealsBalanceOverview",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDealsBalanceOverview.setDefaultUnit(kony.flex.DP);
var lblDealsAccountName = new kony.ui.Label({
"centerX": "50%",
"id": "lblDealsAccountName",
"isVisible": true,
"right": "15dp",
"skin": "sknoverviewTypeLabel",
"text": "Deals account *** 236",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDealsAccountBalance = new kony.ui.Label({
"centerX": "50%",
"id": "lblDealsAccountBalance",
"isVisible": true,
"left": "137dp",
"skin": "sknCopyslLabel03fbcc1555fb34b",
"text": "5,855.670 JOD",
"top": "5dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDealsBalanceOverview.add(lblDealsAccountName, lblDealsAccountBalance);
var CopyFlexContainer0a144944d848c4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "35dp",
"id": "CopyFlexContainer0a144944d848c4b",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0a144944d848c4b.setDefaultUnit(kony.flex.DP);
var Copylblaccountbalance0ece8ca8ded544e = new kony.ui.Label({
"id": "Copylblaccountbalance0ece8ca8ded544e",
"isVisible": true,
"right": "15dp",
"skin": "sknoverviewTypeLabel",
"text": kony.i18n.getLocalizedString("i18n.overview.accountBalance"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyaccountBalanceAmount0e92f8fa33ae941 = new kony.ui.Label({
"id": "CopyaccountBalanceAmount0e92f8fa33ae941",
"isVisible": true,
"left": "15dp",
"skin": "sknCopyslLabel03fbcc1555fb34b",
"text": "- $1,529.54",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyFlexContainer0a144944d848c4b.add(Copylblaccountbalance0ece8ca8ded544e, CopyaccountBalanceAmount0e92f8fa33ae941);
flxDealsBalance.add(flxDealsBalanceOverview, CopyFlexContainer0a144944d848c4b);
var CopyaccountDetailsOverview0h78ad6c5ba654e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "250dp",
"id": "CopyaccountDetailsOverview0h78ad6c5ba654e",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknaccountDetailsHeader",
"top": "0dp",
"width": "100%"
}, {}, {});
CopyaccountDetailsOverview0h78ad6c5ba654e.setDefaultUnit(kony.flex.DP);
var CopygradientOverlay0c66dda9a1f294b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "39%",
"id": "CopygradientOverlay0c66dda9a1f294b",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknaccountDetailsGradient",
"width": "100%",
"zIndex": 1
}, {}, {});
CopygradientOverlay0c66dda9a1f294b.setDefaultUnit(kony.flex.DP);
CopygradientOverlay0c66dda9a1f294b.add();
var CopyflxAccntDetailButtonsKA0be145932176d41 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "CopyflxAccntDetailButtonsKA0be145932176d41",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "140dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxAccntDetailButtonsKA0be145932176d41.setDefaultUnit(kony.flex.DP);
var CopyaccountInfoButton0e7a58b9df11446 = new kony.ui.Button({
"bottom": "10%",
"focusSkin": "sknsecondaryActionReversedFocus",
"height": "40dp",
"id": "CopyaccountInfoButton0e7a58b9df11446",
"isVisible": true,
"right": "15dp",
"onClick": AS_Button_fe1cef3bb60e4e828f55ce2ef74f385f,
"skin": "sknsecondaryActionReversed",
"text": kony.i18n.getLocalizedString("i18n.Accounts.DetailsC"),
"top": 0,
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Copybtnaccountstatements0g4fa48e571cd41 = new kony.ui.Button({
"bottom": "10%",
"focusSkin": "sknsecondaryActionReversedFocus",
"height": "40dp",
"id": "Copybtnaccountstatements0g4fa48e571cd41",
"isVisible": true,
"onClick": AS_Button_ad09901692de45bba0e8ddbcb786b99c,
"left": "15dp",
"skin": "sknsecondaryActionReversed",
"text": "STATEMENTS",
"top": 0,
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyflxAccntDetailButtonsKA0be145932176d41.add(CopyaccountInfoButton0e7a58b9df11446, Copybtnaccountstatements0g4fa48e571cd41);
var CopyactionWrapper0d3c3d4aa2a8548 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10dp",
"clipBounds": true,
"height": "45%",
"id": "CopyactionWrapper0d3c3d4aa2a8548",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "3dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "sknslFbox0ae62737fa1334d",
"top": "126dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyactionWrapper0d3c3d4aa2a8548.setDefaultUnit(kony.flex.DP);
var CopyAccountDetailsWrapper0c341cf3e0a544b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": false,
"height": "100dp",
"id": "CopyAccountDetailsWrapper0c341cf3e0a544b",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "skncontainerBkgNone0bb6a50826c6d49",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyAccountDetailsWrapper0c341cf3e0a544b.setDefaultUnit(kony.flex.DP);
var CopyaccountDetailsFlx0g3234d5eb5c249 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "CopyaccountDetailsFlx0g3234d5eb5c249",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0ae62737fa1334d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyaccountDetailsFlx0g3234d5eb5c249.setDefaultUnit(kony.flex.DP);
var CopyaccountDetailsIcon0a642dfc2042e4e = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyaccountDetailsIcon0a642dfc2042e4e",
"isVisible": true,
"skin": "sknslImage0c4dffbb22b0745",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyAccountDetailsLbl0g123e776089144 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyAccountDetailsLbl0g123e776089144",
"isVisible": true,
"skin": "skniconButtonLabel0c37db8b353724e",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyaccountDetailsFlx0g3234d5eb5c249.add(CopyaccountDetailsIcon0a642dfc2042e4e, CopyAccountDetailsLbl0g123e776089144);
var CopybuttonDivider0gfb5e73d34eb4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "CopybuttonDivider0gfb5e73d34eb4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 1,
"skin": "sknwhiteDivider0gdcc22aab0424e",
"width": "1dp",
"zIndex": 1
}, {}, {});
CopybuttonDivider0gfb5e73d34eb4a.setDefaultUnit(kony.flex.DP);
CopybuttonDivider0gfb5e73d34eb4a.add();
var CopyAccountDetailsButton0f3c910d09abe41 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0b86a9ce9a15c4d",
"height": "100%",
"id": "CopyAccountDetailsButton0f3c910d09abe41",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_e0ab5fd36b654f61a457251a624f0e19,
"skin": "skninvisibleButtonNormal0b2be2515c86a4e",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyAccountDetailsWrapper0c341cf3e0a544b.add(CopyaccountDetailsFlx0g3234d5eb5c249, CopybuttonDivider0gfb5e73d34eb4a, CopyAccountDetailsButton0f3c910d09abe41);
var CopyStatementsWrapper0fe2c9a16a9c84b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100dp",
"id": "CopyStatementsWrapper0fe2c9a16a9c84b",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "skncontainerBkgNone0bb6a50826c6d49",
"width": "33.33%",
"zIndex": 1
}, {}, {});
CopyStatementsWrapper0fe2c9a16a9c84b.setDefaultUnit(kony.flex.DP);
var CopyStatementsFlx0c375d1f5e2b040 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "CopyStatementsFlx0c375d1f5e2b040",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0ae62737fa1334d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyStatementsFlx0c375d1f5e2b040.setDefaultUnit(kony.flex.DP);
var CopyStatementsIcon0iec45c49c39145 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyStatementsIcon0iec45c49c39145",
"isVisible": true,
"skin": "sknslImage0c4dffbb22b0745",
"src": "statementicon.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyStatementsLbl0d8341593b14e46 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyStatementsLbl0d8341593b14e46",
"isVisible": true,
"skin": "skniconButtonLabel0c37db8b353724e",
"text": kony.i18n.getLocalizedString("i18n.overview.accountStatements"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyStatementsFlx0c375d1f5e2b040.add(CopyStatementsIcon0iec45c49c39145, CopyStatementsLbl0d8341593b14e46);
var CopybuttonDivider0ecc8f178916c48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "CopybuttonDivider0ecc8f178916c48",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 1,
"skin": "sknwhiteDivider0gdcc22aab0424e",
"width": "1dp",
"zIndex": 1
}, {}, {});
CopybuttonDivider0ecc8f178916c48.setDefaultUnit(kony.flex.DP);
CopybuttonDivider0ecc8f178916c48.add();
var CopyStatementsButton0e8c3ee76c00d42 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0b86a9ce9a15c4d",
"height": "100%",
"id": "CopyStatementsButton0e8c3ee76c00d42",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_hda4d033441c4124b369a15de08223fa,
"skin": "skninvisibleButtonNormal0b2be2515c86a4e",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyStatementsWrapper0fe2c9a16a9c84b.add(CopyStatementsFlx0c375d1f5e2b040, CopybuttonDivider0ecc8f178916c48, CopyStatementsButton0e8c3ee76c00d42);
var CopyMoreWrapper0c65fcc8b23ab42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100dp",
"id": "CopyMoreWrapper0c65fcc8b23ab42",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "skncontainerBkgNone0bb6a50826c6d49",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyMoreWrapper0c65fcc8b23ab42.setDefaultUnit(kony.flex.DP);
var CopyMoreFlx0f464e9747a1e4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "CopyMoreFlx0f464e9747a1e4d",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0ae62737fa1334d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyMoreFlx0f464e9747a1e4d.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0g4a9f5226a044c = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0g4a9f5226a044c",
"isVisible": true,
"skin": "sknslImage0c4dffbb22b0745",
"src": "moreicon.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMorelbl0ebaafaa289d94a = new kony.ui.Label({
"bottom": "10%",
"centerX": "50%",
"height": "39dp",
"id": "CopyMorelbl0ebaafaa289d94a",
"isVisible": true,
"skin": "skniconButtonLabel0c37db8b353724e",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"top": "9dp",
"width": "75%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyMoreFlx0f464e9747a1e4d.add(CopyMoreIcon0g4a9f5226a044c, CopyMorelbl0ebaafaa289d94a);
var CopyMoreButton0j8d755a7dc454d = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0b86a9ce9a15c4d",
"height": "97.00%",
"id": "CopyMoreButton0j8d755a7dc454d",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_e5e543826d244105a279966acb3a1c45,
"skin": "skninvisibleButtonNormal0b2be2515c86a4e",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyMoreWrapper0c65fcc8b23ab42.add(CopyMoreFlx0f464e9747a1e4d, CopyMoreButton0j8d755a7dc454d);
CopyactionWrapper0d3c3d4aa2a8548.add( CopyMoreWrapper0c65fcc8b23ab42, CopyStatementsWrapper0fe2c9a16a9c84b,CopyAccountDetailsWrapper0c341cf3e0a544b);
var CopyflxMore0bcc6938bdc5340 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50.01%",
"clipBounds": true,
"id": "CopyflxMore0bcc6938bdc5340",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "7.84%",
"skin": "CopyslFbox0c615f21e868d4f",
"top": "140dp",
"width": "100%",
"zIndex": 10
}, {}, {});
CopyflxMore0bcc6938bdc5340.setDefaultUnit(kony.flex.DP);
var CopyflxMoreOption0b5bb49f42f3f46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0b5bb49f42f3f46",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "0dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0b5bb49f42f3f46.setDefaultUnit(kony.flex.DP);
var Copyflxm0fb000536df944f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0fb000536df944f",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0fb000536df944f.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0hc5519eedce34a = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0hc5519eedce34a",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0d408ab48bf8c4a = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0d408ab48bf8c4a",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0fb000536df944f.add(CopyMoreIcon0hc5519eedce34a, CopyMoreLabel0d408ab48bf8c4a);
var CopyMorebtn0h0886976956440 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0h0886976956440",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_d367ca8ef0304d98aebf43b26b63b15e,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0b5a8d4d25e8d48 = new kony.ui.Label({
"id": "CopyMoreHiddenKey0b5a8d4d25e8d48",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0b5bb49f42f3f46.add(Copyflxm0fb000536df944f, CopyMorebtn0h0886976956440, CopyMoreHiddenKey0b5a8d4d25e8d48);
var CopyflxMoreOption0gb90006d10cc4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0gb90006d10cc4d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "0dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0gb90006d10cc4d.setDefaultUnit(kony.flex.DP);
var Copyflxm0fecad35f570c48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0fecad35f570c48",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0fecad35f570c48.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0c11acc4d672748 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0c11acc4d672748",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0a6e695eb434e4f = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0a6e695eb434e4f",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0fecad35f570c48.add(CopyMoreIcon0c11acc4d672748, CopyMoreLabel0a6e695eb434e4f);
var CopyMorebtn0h1b91a9f77a243 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0h1b91a9f77a243",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_c691035400684764b59c35e638253c82,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0f06b2177039344 = new kony.ui.Label({
"id": "CopyMoreHiddenKey0f06b2177039344",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0gb90006d10cc4d.add(Copyflxm0fecad35f570c48, CopyMorebtn0h1b91a9f77a243, CopyMoreHiddenKey0f06b2177039344);
var CopyflxMoreOption0g80f9581b50747 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0g80f9581b50747",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "120dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0g80f9581b50747.setDefaultUnit(kony.flex.DP);
var Copyflxm0be2b242ea3ef4e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0be2b242ea3ef4e",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0be2b242ea3ef4e.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0ad99ee8f422148 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0ad99ee8f422148",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0fac282602fe14b = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0fac282602fe14b",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0be2b242ea3ef4e.add(CopyMoreIcon0ad99ee8f422148, CopyMoreLabel0fac282602fe14b);
var CopyMorebtn0d9284642e49445 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0d9284642e49445",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_e2519f0646ed43f9ab7d80ea0b2ed1d7,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0i4ec9ed7050d4e = new kony.ui.Label({
"id": "CopyMoreHiddenKey0i4ec9ed7050d4e",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0g80f9581b50747.add(Copyflxm0be2b242ea3ef4e, CopyMorebtn0d9284642e49445, CopyMoreHiddenKey0i4ec9ed7050d4e);
var CopyflxMoreOption0d3de62e2f69444 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0d3de62e2f69444",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "120dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0d3de62e2f69444.setDefaultUnit(kony.flex.DP);
var Copyflxm0c4916451c78742 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0c4916451c78742",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0c4916451c78742.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0c1d224df96a344 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0c1d224df96a344",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0bbdc8fe958e746 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0bbdc8fe958e746",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0c4916451c78742.add(CopyMoreIcon0c1d224df96a344, CopyMoreLabel0bbdc8fe958e746);
var CopyMorebtn0c689b425c53944 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0c689b425c53944",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_dcd6fc4374bf47ec95e4360951be3428,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0a42493f340764e = new kony.ui.Label({
"id": "CopyMoreHiddenKey0a42493f340764e",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0d3de62e2f69444.add(Copyflxm0c4916451c78742, CopyMorebtn0c689b425c53944, CopyMoreHiddenKey0a42493f340764e);
var CopyflxMoreOption0fe0503387be94d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0fe0503387be94d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "240dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0fe0503387be94d.setDefaultUnit(kony.flex.DP);
var Copyflxm0a0008e8fab4b4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0a0008e8fab4b4c",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0a0008e8fab4b4c.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0f1dfd08a250642 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0f1dfd08a250642",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0b30162c953b043 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0b30162c953b043",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0a0008e8fab4b4c.add(CopyMoreIcon0f1dfd08a250642, CopyMoreLabel0b30162c953b043);
var CopyMorebtn0b68d381f471c43 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0b68d381f471c43",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_ea63927fc31c4c07aa084fbb9c85b6ba,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0a7bbfef707574d = new kony.ui.Label({
"id": "CopyMoreHiddenKey0a7bbfef707574d",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0fe0503387be94d.add(Copyflxm0a0008e8fab4b4c, CopyMorebtn0b68d381f471c43, CopyMoreHiddenKey0a7bbfef707574d);
var CopyflxMoreOption0d969621c301e45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0d969621c301e45",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "240dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0d969621c301e45.setDefaultUnit(kony.flex.DP);
var Copyflxm0c09f73038a1340 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0c09f73038a1340",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0c09f73038a1340.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0cd3911a255a246 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0cd3911a255a246",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0d407f375cde748 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0d407f375cde748",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0c09f73038a1340.add(CopyMoreIcon0cd3911a255a246, CopyMoreLabel0d407f375cde748);
var CopyMorebtn0d1ff8a40f1e04f = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0d1ff8a40f1e04f",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_dd7a8430ba984063b0023c693cda610e,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0e6b327a6978b4d = new kony.ui.Label({
"id": "CopyMoreHiddenKey0e6b327a6978b4d",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0d969621c301e45.add(Copyflxm0c09f73038a1340, CopyMorebtn0d1ff8a40f1e04f, CopyMoreHiddenKey0e6b327a6978b4d);
var CopyflxMoreOption0b3d7d4a6dd9b4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0b3d7d4a6dd9b4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "360dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0b3d7d4a6dd9b4a.setDefaultUnit(kony.flex.DP);
var Copyflxm0ae2ddeb491cf4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0ae2ddeb491cf4b",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0ae2ddeb491cf4b.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0ab4b4680be8647 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0ab4b4680be8647",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0h46f7beea2b54f = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0h46f7beea2b54f",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0ae2ddeb491cf4b.add(CopyMoreIcon0ab4b4680be8647, CopyMoreLabel0h46f7beea2b54f);
var CopyMorebtn0e31762c33df441 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0e31762c33df441",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_cff86d55242146368e7d611ec24f8fd1,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0efde9fc50ea846 = new kony.ui.Label({
"id": "CopyMoreHiddenKey0efde9fc50ea846",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0b3d7d4a6dd9b4a.add(Copyflxm0ae2ddeb491cf4b, CopyMorebtn0e31762c33df441, CopyMoreHiddenKey0efde9fc50ea846);
var CopyflxMoreOption0jde7342b28be4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0jde7342b28be4c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "360dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0jde7342b28be4c.setDefaultUnit(kony.flex.DP);
var Copyflxm0ad0b9f1e54204c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0ad0b9f1e54204c",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0ad0b9f1e54204c.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0af0db3a6d9a24c = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0af0db3a6d9a24c",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0b728a68f23e34c = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0b728a68f23e34c",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0ad0b9f1e54204c.add(CopyMoreIcon0af0db3a6d9a24c, CopyMoreLabel0b728a68f23e34c);
var CopyMorebtn0ga05785316504f = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0ga05785316504f",
"isVisible": true,
"onClick": AS_Button_f3eb16184f5f4fda91b8b6c3208f8515,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0i4e1d3ae3b604f = new kony.ui.Label({
"id": "CopyMoreHiddenKey0i4e1d3ae3b604f",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0jde7342b28be4c.add(Copyflxm0ad0b9f1e54204c, CopyMorebtn0ga05785316504f, CopyMoreHiddenKey0i4e1d3ae3b604f);
var CopylineDivider0e7ca7e1af17247 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50%",
"clipBounds": true,
"id": "CopylineDivider0e7ca7e1af17247",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 150,
"skin": "sknwhiteDivider0a18e65b4e8c444",
"top": 0,
"width": "3dp",
"zIndex": 1
}, {}, {});
CopylineDivider0e7ca7e1af17247.setDefaultUnit(kony.flex.DP);
CopylineDivider0e7ca7e1af17247.add();
var CopycollapseButton0a814bdeedcac42 = new kony.ui.Button({
"bottom": "60px",
"centerX": "50%",
"centerY": "380dp",
"focusSkin": "roundCollapseButton",
"height": "50dp",
"id": "CopycollapseButton0a814bdeedcac42",
"isVisible": true,
"right": 135,
"onClick": AS_Button_g90f06f5685c45c3a3418c8c1e9441f2,
"skin": "roundCollapseButton",
"width": "50dp",
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyflxMore0bcc6938bdc5340.add(CopyflxMoreOption0b5bb49f42f3f46, CopyflxMoreOption0gb90006d10cc4d, CopyflxMoreOption0g80f9581b50747, CopyflxMoreOption0d3de62e2f69444, CopyflxMoreOption0fe0503387be94d, CopyflxMoreOption0d969621c301e45, CopyflxMoreOption0b3d7d4a6dd9b4a, CopyflxMoreOption0jde7342b28be4c, CopylineDivider0e7ca7e1af17247, CopycollapseButton0a814bdeedcac42);
CopyaccountDetailsOverview0h78ad6c5ba654e.add(CopygradientOverlay0c66dda9a1f294b, CopyflxAccntDetailButtonsKA0be145932176d41, CopyactionWrapper0d3c3d4aa2a8548, CopyflxMore0bcc6938bdc5340);
var flxDealsTabs = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40dp",
"id": "flxDealsTabs",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgheader",
"top": "180dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDealsTabs.setDefaultUnit(kony.flex.DP);
var CopyscheduledTabButton0a2f2f886eae247 = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "100%",
"id": "CopyscheduledTabButton0a2f2f886eae247",
"isVisible": false,
"right": "50%",
"onClick": AS_Button_j4abe7531cf5425abc3a7f70e1d7e9a5,
"skin": "skntabDeselected",
"text": kony.i18n.getLocalizedString("i18n.common.cSCHEDULED"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 1,0, 0],
"paddingInPixel": false
}, {});
var CopyrecentTabButton0b7db9fe3d78245 = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "100%",
"id": "CopyrecentTabButton0b7db9fe3d78245",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_i3ea3c7279e34816a794253b021471ad,
"skin": "skntabSelected",
"text": kony.i18n.getLocalizedString("i18n.deposit.recent"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 1,0, 0],
"paddingInPixel": false
}, {});
var CopytabDeselectedIndicator0e2c60280207f49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "CopytabDeselectedIndicator0e2c60280207f49",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknCopyslFbox00fe82e6f7cc84b",
"top": "0dp",
"width": "100%"
}, {}, {});
CopytabDeselectedIndicator0e2c60280207f49.setDefaultUnit(kony.flex.DP);
CopytabDeselectedIndicator0e2c60280207f49.add();
var CopytabSelectedIndicator0ace0c91e16c945 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "CopytabSelectedIndicator0ace0c91e16c945",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknCopyslFbox03fbc1653617542",
"width": "50%"
}, {}, {});
CopytabSelectedIndicator0ace0c91e16c945.setDefaultUnit(kony.flex.DP);
CopytabSelectedIndicator0ace0c91e16c945.add();
var CopylistDivider0ccf01f4e36ac49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylistDivider0ccf01f4e36ac49",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopylistDivider0ccf01f4e36ac49.setDefaultUnit(kony.flex.DP);
CopylistDivider0ccf01f4e36ac49.add();
var lblDealsListView = new kony.ui.Label({
"centerY": "50%",
"id": "lblDealsListView",
"isVisible": true,
"right": "20%",
"skin": "lblListOn",
"text": "W",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "50dp",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDealsInfoView = new kony.ui.Label({
"centerY": "50%",
"id": "lblDealsInfoView",
"isVisible": true,
"onTouchEnd": AS_Label_j6ef68c16fbc4452aa068075cb0131d7,
"left": "20%",
"skin": "lblListOff",
"text": "E",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "50dp",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDealsTabs.add(CopyscheduledTabButton0a2f2f886eae247, CopyrecentTabButton0b7db9fe3d78245, CopytabDeselectedIndicator0e2c60280207f49, CopytabSelectedIndicator0ace0c91e16c945, CopylistDivider0ccf01f4e36ac49, lblDealsListView, lblDealsInfoView);
flxDealsTitleBarDetails.add(flxDealsTopBar, flxDealsAccountBar, flxDealsBalance, CopyaccountDetailsOverview0h78ad6c5ba654e, flxDealsTabs);
var flxDealsLower = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "68%",
"horizontalScrollIndicator": true,
"id": "flxDealsLower",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "1dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknGradientbluetodark",
"top": "40%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxDealsLower.setDefaultUnit(kony.flex.DP);
var CopyresourcesLabel0ca900b81f6cd46 = new kony.ui.Label({
"height": "14.50%",
"id": "CopyresourcesLabel0ca900b81f6cd46",
"isVisible": false,
"right": "0dp",
"skin": "sknsecondaryHeader",
"text": kony.i18n.getLocalizedString("i18n.common.recentTransactions"),
"top": "0dp",
"width": "100%",
"zIndex": 10000
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDealsExtraTab = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxDealsExtraTab",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 10000
}, {}, {});
flxDealsExtraTab.setDefaultUnit(kony.flex.DP);
var lblDealsSearch = new kony.ui.Label({
"centerY": "50%",
"id": "lblDealsSearch",
"isVisible": true,
"right": "4%",
"skin": "CopyslLabel0j2197a197ee14d",
"text": "h",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "8dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDealsTextarea = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"height": "30dp",
"id": "lblDealsTextarea",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "8%",
"placeholder": "Search transaction",
"secureTextEntry": false,
"skin": "CopyslTextBox0f379cdd9b28441",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "13dp",
"width": "185dp",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblDealsExport = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblDealsExport",
"isVisible": true,
"onTouchEnd": AS_Label_j88b6a1982c54344877bca33750e5b6e,
"left": "15%",
"skin": "CopyslLabel0i2b4c385eb5140",
"text": "l",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDealsFilter = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblDealsFilter",
"isVisible": false,
"left": "4%",
"skin": "CopyslLabel0b8f2bdd739144d",
"text": "n",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblDealsFilter0f5b73f15da904a = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblDealsFilter0f5b73f15da904a",
"isVisible": true,
"onTouchEnd": AS_Label_fba86207b11e43b5a416a90958ce02e0,
"left": "4%",
"skin": "CopyslLabel0b8f2bdd739144d",
"text": "n",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDealsSingleLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxDealsSingleLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "CopyslFbox0ab42d5f22ae345",
"top": "40dp",
"width": "200dp",
"zIndex": 1
}, {}, {});
flxDealsSingleLine.setDefaultUnit(kony.flex.DP);
flxDealsSingleLine.add();
flxDealsExtraTab.add(lblDealsSearch, lblDealsTextarea, lblDealsExport, lblDealsFilter, CopylblDealsFilter0f5b73f15da904a, flxDealsSingleLine);
var flxDealsSegment = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "85.50%",
"id": "flxDealsSegment",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "200%"
}, {}, {});
flxDealsSegment.setDefaultUnit(kony.flex.DP);
var dealsTransactionSeg = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bottom": "5dp",
"data": [{
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}],
"groupCells": false,
"id": "dealsTransactionSeg",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowTemplate": CopyFlexContainer011baa9bcad7141,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "072c4800",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer011baa9bcad7141": "CopyFlexContainer011baa9bcad7141",
"ImgWithDraw": "ImgWithDraw",
"Imgcheck": "Imgcheck",
"chevron": "chevron",
"flxImageandNameKA": "flxImageandNameKA",
"lblExpiryTime": "lblExpiryTime",
"lblLastTransaction": "lblLastTransaction",
"lblNoData": "lblNoData",
"lblSepKA": "lblSepKA",
"transactionAmount": "transactionAmount",
"transactionAmountSI": "transactionAmountSI",
"transactionDate": "transactionDate",
"transactionName": "transactionName"
},
"width": "50%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyscheduledTransactionSegment0a11e23f6e97241 = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bottom": "5dp",
"data": [{
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}],
"groupCells": false,
"id": "CopyscheduledTransactionSegment0a11e23f6e97241",
"isVisible": false,
"right": "50%",
"needPageIndicator": true,
"onRowClick": AS_Segment_bfc100fb39be4403ab64ad39fe497641,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": CopyFlexContainer011baa9bcad7141,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "ffffff00",
"separatorRequired": true,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer011baa9bcad7141": "CopyFlexContainer011baa9bcad7141",
"ImgWithDraw": "ImgWithDraw",
"Imgcheck": "Imgcheck",
"chevron": "chevron",
"flxImageandNameKA": "flxImageandNameKA",
"lblExpiryTime": "lblExpiryTime",
"lblLastTransaction": "lblLastTransaction",
"lblNoData": "lblNoData",
"lblSepKA": "lblSepKA",
"transactionAmount": "transactionAmount",
"transactionAmountSI": "transactionAmountSI",
"transactionDate": "transactionDate",
"transactionName": "transactionName"
},
"width": "50%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabelNoRecordsKA0d0cc82039dee42 = new kony.ui.Label({
"centerY": "50%",
"id": "CopyLabelNoRecordsKA0d0cc82039dee42",
"isVisible": false,
"right": "0dp",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.common.NoTransactions"),
"top": "0dp",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblSheduledNoRecordsKA0a0b990e13af944 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblSheduledNoRecordsKA0a0b990e13af944",
"isVisible": false,
"right": "50%",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.common.NoTransactions"),
"top": "0dp",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopysegmentBorderBottom0jd3e2cce0f3c47 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopysegmentBorderBottom0jd3e2cce0f3c47",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopysegmentBorderBottom0jd3e2cce0f3c47.setDefaultUnit(kony.flex.DP);
CopysegmentBorderBottom0jd3e2cce0f3c47.add();
var CopyFlexContainer0a8517ecd193f43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100dp",
"id": "CopyFlexContainer0a8517ecd193f43",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "-2dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0a8517ecd193f43.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0a8517ecd193f43.add();
flxDealsSegment.add(dealsTransactionSeg, CopyscheduledTransactionSegment0a11e23f6e97241, CopyLabelNoRecordsKA0d0cc82039dee42, CopylblSheduledNoRecordsKA0a0b990e13af944, CopysegmentBorderBottom0jd3e2cce0f3c47, CopyFlexContainer0a8517ecd193f43);
flxDealsLower.add(CopyresourcesLabel0ca900b81f6cd46, flxDealsExtraTab, flxDealsSegment);
flxDealsScreen.add(flxDealsTitleBarDetails, flxDealsLower);
var flxLoansScreen = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxLoansScreen",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "100%",
"skin": "CopyslFbox0d83a1fe42eaa4e",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLoansScreen.setDefaultUnit(kony.flex.DP);
var flxLoansUpper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "30%",
"id": "flxLoansUpper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopysknslFbox0a36e6cb491a845",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLoansUpper.setDefaultUnit(kony.flex.DP);
var flxLoansTopBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "35dp",
"id": "flxLoansTopBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxLoansTopBar.setDefaultUnit(kony.flex.DP);
var CopyaccountDetailsHeader0ha1c9f8db7664c = new kony.ui.Label({
"centerX": "50%",
"height": "22dp",
"id": "CopyaccountDetailsHeader0ha1c9f8db7664c",
"isVisible": false,
"skin": "sknnavBarTitle",
"text": "John's Primary Checking Acct",
"top": "15dp",
"width": "68%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyaccountDetailsTime0g9681cd8f7f24e = new kony.ui.Label({
"centerX": "50%",
"id": "CopyaccountDetailsTime0g9681cd8f7f24e",
"isVisible": false,
"skin": "sknasOfTimeLabel",
"text": " 4:33 PM",
"top": "37dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopycloseButton0e53ede59182049 = new kony.ui.Button({
"focusSkin": "sknCopyinvisibleButtonFocus0df746dda29534b",
"height": "50dp",
"id": "CopycloseButton0e53ede59182049",
"isVisible": false,
"left": "0dp",
"onClick": AS_Button_c53666b52304472ab90a99f367195c32,
"skin": "sknCopyinvisibleButton046d30d34316f47",
"top": "6dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": false,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var CopysearchButton0cd3d15d6f3334f = new kony.ui.Button({
"focusSkin": "skntitleBarSearchButtonFocus",
"height": "50dp",
"id": "CopysearchButton0cd3d15d6f3334f",
"isVisible": false,
"left": "86.11%",
"onClick": AS_Button_b429455c32b8418da03ed9bf9c8fdf63,
"right": "1%",
"skin": "skntitleBarSearchButton",
"top": "8dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var lblLoansMsgIcon = new kony.ui.Label({
"id": "lblLoansMsgIcon",
"isVisible": true,
"right": "15%",
"skin": "CopyslLabel0if62558c2f444d",
"text": "Y",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "7dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLoansBankIcon = new kony.ui.Label({
"height": "50dp",
"id": "lblLoansBankIcon",
"isVisible": false,
"left": "4%",
"skin": "CopyslLabel0bb55fd33e1b64e",
"text": "C",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLoansProfileIcon = new kony.ui.Label({
"id": "lblLoansProfileIcon",
"isVisible": true,
"right": "2%",
"skin": "CopyslLabel0cbb44b1e4e6f4a",
"text": "F",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "7dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLoanTransactionIcon = new kony.ui.Label({
"height": "30dp",
"id": "lblLoanTransactionIcon",
"isVisible": true,
"left": "8%",
"skin": "accountDealLoanBackSkin",
"text": "Loans",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "7dp",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBackLoanIcon = new kony.ui.Label({
"height": "45dp",
"id": "lblBackLoanIcon",
"isVisible": true,
"left": "0%",
"onTouchEnd": AS_Label_cd213d43257c4ec98f881f67bd25ad9f,
"skin": "backTransactionIconSkin",
"text": "j",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [1, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxLoansTopBar.add(CopyaccountDetailsHeader0ha1c9f8db7664c, CopyaccountDetailsTime0g9681cd8f7f24e, CopycloseButton0e53ede59182049, CopysearchButton0cd3d15d6f3334f, lblLoansMsgIcon, lblLoansBankIcon, lblLoansProfileIcon, lblLoanTransactionIcon, lblBackLoanIcon);
var flxLoansAccountNames = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40dp",
"id": "flxLoansAccountNames",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "40dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxLoansAccountNames.setDefaultUnit(kony.flex.DP);
var lblLoansDealsAccount = new kony.ui.Label({
"centerY": "50%",
"id": "lblLoansDealsAccount",
"isVisible": true,
"right": "55%",
"onTouchEnd": AS_Label_f0a463b37c224125901d7266f4347de4,
"skin": "CopyslLabel0fcccbc7f62984f",
"text": "Deals",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLoansAccountAccount = new kony.ui.Label({
"centerY": "50%",
"id": "lblLoansAccountAccount",
"isVisible": true,
"right": "26%",
"onTouchEnd": AS_Label_d38a10e07e4e4bd68dd4dd4e5182633b,
"skin": "CopyslLabel0fcccbc7f62984f",
"text": "Accounts",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLoansLoansAccount = new kony.ui.Label({
"centerY": "50%",
"id": "lblLoansLoansAccount",
"isVisible": true,
"right": "4%",
"onTouchEnd": AS_Label_d62e5aeae4af4c7182941d68a3c08ee0,
"skin": "CopyslLabel0e2dc1030f8784c",
"text": "Loans",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxLoansAccountNames.add(lblLoansDealsAccount, lblLoansAccountAccount, lblLoansLoansAccount);
var flxLoansAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "flxLoansAccounts",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "60dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxLoansAccounts.setDefaultUnit(kony.flex.DP);
var flxLoansAvailableBalance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxLoansAvailableBalance",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLoansAvailableBalance.setDefaultUnit(kony.flex.DP);
var lblLoansAccount = new kony.ui.Label({
"centerX": "50%",
"id": "lblLoansAccount",
"isVisible": true,
"right": "15dp",
"skin": "sknoverviewTypeLabel",
"text": "Loan account ****345",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLoansAvialableBalance = new kony.ui.Label({
"centerX": "50.00%",
"id": "lblLoansAvialableBalance",
"isVisible": true,
"left": "137dp",
"skin": "sknCopyslLabel03fbcc1555fb34b",
"text": "7,568.540 JOD",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLoansRemainingBalance = new kony.ui.Label({
"bottom": "15dp",
"centerX": "50%",
"id": "lblLoansRemainingBalance",
"isVisible": true,
"skin": "sknoverviewTypeLabel",
"text": "6,592.640 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxLoansAvailableBalance.add(lblLoansAccount, lblLoansAvialableBalance, lblLoansRemainingBalance);
var CopyFlexContainer0dece3f7e088945 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "35dp",
"id": "CopyFlexContainer0dece3f7e088945",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0dece3f7e088945.setDefaultUnit(kony.flex.DP);
var Copylblaccountbalance0jcc60ca0679e47 = new kony.ui.Label({
"id": "Copylblaccountbalance0jcc60ca0679e47",
"isVisible": true,
"right": "15dp",
"skin": "sknoverviewTypeLabel",
"text": kony.i18n.getLocalizedString("i18n.overview.accountBalance"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyaccountBalanceAmount0c40e033d690041 = new kony.ui.Label({
"id": "CopyaccountBalanceAmount0c40e033d690041",
"isVisible": true,
"left": "15dp",
"skin": "sknCopyslLabel03fbcc1555fb34b",
"text": "- $1,529.54",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyFlexContainer0dece3f7e088945.add(Copylblaccountbalance0jcc60ca0679e47, CopyaccountBalanceAmount0c40e033d690041);
flxLoansAccounts.add(flxLoansAvailableBalance, CopyFlexContainer0dece3f7e088945);
var CopyaccountDetailsOverview0dd1fa608aee343 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "250dp",
"id": "CopyaccountDetailsOverview0dd1fa608aee343",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknaccountDetailsHeader",
"top": "0dp",
"width": "100%"
}, {}, {});
CopyaccountDetailsOverview0dd1fa608aee343.setDefaultUnit(kony.flex.DP);
var CopygradientOverlay0ge127ec45b044b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "39%",
"id": "CopygradientOverlay0ge127ec45b044b",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknaccountDetailsGradient",
"width": "100%",
"zIndex": 1
}, {}, {});
CopygradientOverlay0ge127ec45b044b.setDefaultUnit(kony.flex.DP);
CopygradientOverlay0ge127ec45b044b.add();
var CopyflxAccntDetailButtonsKA0jd6c5b383a2149 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "CopyflxAccntDetailButtonsKA0jd6c5b383a2149",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "140dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxAccntDetailButtonsKA0jd6c5b383a2149.setDefaultUnit(kony.flex.DP);
var CopyaccountInfoButton0i0de251732d34a = new kony.ui.Button({
"bottom": "10%",
"focusSkin": "sknsecondaryActionReversedFocus",
"height": "40dp",
"id": "CopyaccountInfoButton0i0de251732d34a",
"isVisible": true,
"right": "15dp",
"onClick": AS_Button_fb1eb98d40dd4bffac1512615ec41d7c,
"skin": "sknsecondaryActionReversed",
"text": kony.i18n.getLocalizedString("i18n.Accounts.DetailsC"),
"top": 0,
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Copybtnaccountstatements0aae8981ce25a4f = new kony.ui.Button({
"bottom": "10%",
"focusSkin": "sknsecondaryActionReversedFocus",
"height": "40dp",
"id": "Copybtnaccountstatements0aae8981ce25a4f",
"isVisible": true,
"onClick": AS_Button_c588998d5e70441aa1cb30ce21056048,
"left": "15dp",
"skin": "sknsecondaryActionReversed",
"text": "STATEMENTS",
"top": 0,
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyflxAccntDetailButtonsKA0jd6c5b383a2149.add(CopyaccountInfoButton0i0de251732d34a, Copybtnaccountstatements0aae8981ce25a4f);
var CopyactionWrapper0b8641777576440 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10dp",
"clipBounds": true,
"height": "45%",
"id": "CopyactionWrapper0b8641777576440",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "3dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "sknslFbox0ae62737fa1334d",
"top": "126dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyactionWrapper0b8641777576440.setDefaultUnit(kony.flex.DP);
var CopyAccountDetailsWrapper0b19a9177669a4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": false,
"height": "100dp",
"id": "CopyAccountDetailsWrapper0b19a9177669a4d",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "skncontainerBkgNone0bb6a50826c6d49",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyAccountDetailsWrapper0b19a9177669a4d.setDefaultUnit(kony.flex.DP);
var CopyaccountDetailsFlx0b72c3181bdd245 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "CopyaccountDetailsFlx0b72c3181bdd245",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0ae62737fa1334d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyaccountDetailsFlx0b72c3181bdd245.setDefaultUnit(kony.flex.DP);
var CopyaccountDetailsIcon0c16830d87b974d = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyaccountDetailsIcon0c16830d87b974d",
"isVisible": true,
"skin": "sknslImage0c4dffbb22b0745",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyAccountDetailsLbl0a531cf303e224c = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyAccountDetailsLbl0a531cf303e224c",
"isVisible": true,
"skin": "skniconButtonLabel0c37db8b353724e",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyaccountDetailsFlx0b72c3181bdd245.add(CopyaccountDetailsIcon0c16830d87b974d, CopyAccountDetailsLbl0a531cf303e224c);
var CopybuttonDivider0a5bf9b0ff7f347 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "CopybuttonDivider0a5bf9b0ff7f347",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 1,
"skin": "sknwhiteDivider0gdcc22aab0424e",
"width": "1dp",
"zIndex": 1
}, {}, {});
CopybuttonDivider0a5bf9b0ff7f347.setDefaultUnit(kony.flex.DP);
CopybuttonDivider0a5bf9b0ff7f347.add();
var CopyAccountDetailsButton0b67b86b9f23146 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0b86a9ce9a15c4d",
"height": "100%",
"id": "CopyAccountDetailsButton0b67b86b9f23146",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_d5dc1ae81d0c4bac8d907ae2a04f6ee0,
"skin": "skninvisibleButtonNormal0b2be2515c86a4e",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyAccountDetailsWrapper0b19a9177669a4d.add(CopyaccountDetailsFlx0b72c3181bdd245, CopybuttonDivider0a5bf9b0ff7f347, CopyAccountDetailsButton0b67b86b9f23146);
var CopyStatementsWrapper0j399a4f4d50848 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100dp",
"id": "CopyStatementsWrapper0j399a4f4d50848",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "skncontainerBkgNone0bb6a50826c6d49",
"width": "33.33%",
"zIndex": 1
}, {}, {});
CopyStatementsWrapper0j399a4f4d50848.setDefaultUnit(kony.flex.DP);
var CopyStatementsFlx0c890a1567e2447 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "CopyStatementsFlx0c890a1567e2447",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0ae62737fa1334d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyStatementsFlx0c890a1567e2447.setDefaultUnit(kony.flex.DP);
var CopyStatementsIcon0cf6ea4a8be0f44 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyStatementsIcon0cf6ea4a8be0f44",
"isVisible": true,
"skin": "sknslImage0c4dffbb22b0745",
"src": "statementicon.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyStatementsLbl0fe19df49d8a24d = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyStatementsLbl0fe19df49d8a24d",
"isVisible": true,
"skin": "skniconButtonLabel0c37db8b353724e",
"text": kony.i18n.getLocalizedString("i18n.overview.accountStatements"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyStatementsFlx0c890a1567e2447.add(CopyStatementsIcon0cf6ea4a8be0f44, CopyStatementsLbl0fe19df49d8a24d);
var CopybuttonDivider0i0eec9f495924a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "CopybuttonDivider0i0eec9f495924a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 1,
"skin": "sknwhiteDivider0gdcc22aab0424e",
"width": "1dp",
"zIndex": 1
}, {}, {});
CopybuttonDivider0i0eec9f495924a.setDefaultUnit(kony.flex.DP);
CopybuttonDivider0i0eec9f495924a.add();
var CopyStatementsButton0d24a38dadef241 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0b86a9ce9a15c4d",
"height": "100%",
"id": "CopyStatementsButton0d24a38dadef241",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_d011b72f53b4412cae322b4a3ec54e9e,
"skin": "skninvisibleButtonNormal0b2be2515c86a4e",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyStatementsWrapper0j399a4f4d50848.add(CopyStatementsFlx0c890a1567e2447, CopybuttonDivider0i0eec9f495924a, CopyStatementsButton0d24a38dadef241);
var CopyMoreWrapper0ddadc03b11e842 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100dp",
"id": "CopyMoreWrapper0ddadc03b11e842",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "skncontainerBkgNone0bb6a50826c6d49",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyMoreWrapper0ddadc03b11e842.setDefaultUnit(kony.flex.DP);
var CopyMoreFlx0beff2abb5ebe49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "CopyMoreFlx0beff2abb5ebe49",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0ae62737fa1334d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyMoreFlx0beff2abb5ebe49.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0a2f340e5fb8f45 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0a2f340e5fb8f45",
"isVisible": true,
"skin": "sknslImage0c4dffbb22b0745",
"src": "moreicon.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMorelbl0d1c297a2203c4e = new kony.ui.Label({
"bottom": "10%",
"centerX": "50%",
"height": "39dp",
"id": "CopyMorelbl0d1c297a2203c4e",
"isVisible": true,
"skin": "skniconButtonLabel0c37db8b353724e",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"top": "9dp",
"width": "75%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyMoreFlx0beff2abb5ebe49.add(CopyMoreIcon0a2f340e5fb8f45, CopyMorelbl0d1c297a2203c4e);
var CopyMoreButton0jd2d3dea1aeb40 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0b86a9ce9a15c4d",
"height": "97.00%",
"id": "CopyMoreButton0jd2d3dea1aeb40",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_dd8d8cf821b440f89dd6093da277b869,
"skin": "skninvisibleButtonNormal0b2be2515c86a4e",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyMoreWrapper0ddadc03b11e842.add(CopyMoreFlx0beff2abb5ebe49, CopyMoreButton0jd2d3dea1aeb40);
CopyactionWrapper0b8641777576440.add( CopyMoreWrapper0ddadc03b11e842, CopyStatementsWrapper0j399a4f4d50848,CopyAccountDetailsWrapper0b19a9177669a4d);
var CopyflxMore0dcd89fda4fe54d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50.01%",
"clipBounds": true,
"id": "CopyflxMore0dcd89fda4fe54d",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "7.84%",
"skin": "CopyslFbox0c615f21e868d4f",
"top": "140dp",
"width": "100%",
"zIndex": 10
}, {}, {});
CopyflxMore0dcd89fda4fe54d.setDefaultUnit(kony.flex.DP);
var CopyflxMoreOption0e148330049a044 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0e148330049a044",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "0dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0e148330049a044.setDefaultUnit(kony.flex.DP);
var Copyflxm0ba2496a4ffeb47 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0ba2496a4ffeb47",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0ba2496a4ffeb47.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0ffbcf508e6714e = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0ffbcf508e6714e",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0f19116a03c4e4d = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0f19116a03c4e4d",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0ba2496a4ffeb47.add(CopyMoreIcon0ffbcf508e6714e, CopyMoreLabel0f19116a03c4e4d);
var CopyMorebtn0d848285339314a = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0d848285339314a",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_d5686a9f90f54aaca1d1f17a32d1cdec,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0h9aa79dbd2ba47 = new kony.ui.Label({
"id": "CopyMoreHiddenKey0h9aa79dbd2ba47",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0e148330049a044.add(Copyflxm0ba2496a4ffeb47, CopyMorebtn0d848285339314a, CopyMoreHiddenKey0h9aa79dbd2ba47);
var CopyflxMoreOption0i8a60a30a7714b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0i8a60a30a7714b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "0dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0i8a60a30a7714b.setDefaultUnit(kony.flex.DP);
var Copyflxm0b1951f31a93e40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0b1951f31a93e40",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0b1951f31a93e40.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0ce92893a954d43 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0ce92893a954d43",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0ib499fd3ea7e41 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0ib499fd3ea7e41",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0b1951f31a93e40.add(CopyMoreIcon0ce92893a954d43, CopyMoreLabel0ib499fd3ea7e41);
var CopyMorebtn0d35cb2d28b0047 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0d35cb2d28b0047",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_j5986c18f245442484c6ec23f13b00b6,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0h4c36717e70941 = new kony.ui.Label({
"id": "CopyMoreHiddenKey0h4c36717e70941",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0i8a60a30a7714b.add(Copyflxm0b1951f31a93e40, CopyMorebtn0d35cb2d28b0047, CopyMoreHiddenKey0h4c36717e70941);
var CopyflxMoreOption0b5d391d43a5a41 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0b5d391d43a5a41",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "120dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0b5d391d43a5a41.setDefaultUnit(kony.flex.DP);
var Copyflxm0fa0e643eae6643 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0fa0e643eae6643",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0fa0e643eae6643.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0a5e92896ffc747 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0a5e92896ffc747",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0e7c08a89218643 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0e7c08a89218643",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0fa0e643eae6643.add(CopyMoreIcon0a5e92896ffc747, CopyMoreLabel0e7c08a89218643);
var CopyMorebtn0eb8bb7d5cca146 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0eb8bb7d5cca146",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_d34c7c8f8cd642e6bae435679a0af27a,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0e334b45a77d24c = new kony.ui.Label({
"id": "CopyMoreHiddenKey0e334b45a77d24c",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0b5d391d43a5a41.add(Copyflxm0fa0e643eae6643, CopyMorebtn0eb8bb7d5cca146, CopyMoreHiddenKey0e334b45a77d24c);
var CopyflxMoreOption0gca0166108a949 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0gca0166108a949",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "120dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0gca0166108a949.setDefaultUnit(kony.flex.DP);
var Copyflxm0c59a8f3f7a6a4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0c59a8f3f7a6a4c",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0c59a8f3f7a6a4c.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0c12ec46eab4340 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0c12ec46eab4340",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0efefb5f45caa47 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0efefb5f45caa47",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0c59a8f3f7a6a4c.add(CopyMoreIcon0c12ec46eab4340, CopyMoreLabel0efefb5f45caa47);
var CopyMorebtn0e91d6e4484ee47 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0e91d6e4484ee47",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_i017c9201984492fb131ed1146791958,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0c98e1a2958334b = new kony.ui.Label({
"id": "CopyMoreHiddenKey0c98e1a2958334b",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0gca0166108a949.add(Copyflxm0c59a8f3f7a6a4c, CopyMorebtn0e91d6e4484ee47, CopyMoreHiddenKey0c98e1a2958334b);
var CopyflxMoreOption0jb23f1a5856e4e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0jb23f1a5856e4e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "240dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0jb23f1a5856e4e.setDefaultUnit(kony.flex.DP);
var Copyflxm0b9d64a74d42340 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0b9d64a74d42340",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0b9d64a74d42340.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0h04e044816e24a = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0h04e044816e24a",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0cad7fe5eb10844 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0cad7fe5eb10844",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0b9d64a74d42340.add(CopyMoreIcon0h04e044816e24a, CopyMoreLabel0cad7fe5eb10844);
var CopyMorebtn0dd8b3bd4bcdd41 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0dd8b3bd4bcdd41",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_a919e92a7c244b9cbe1c2a9d4e10b02b,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0g5692532d4f041 = new kony.ui.Label({
"id": "CopyMoreHiddenKey0g5692532d4f041",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0jb23f1a5856e4e.add(Copyflxm0b9d64a74d42340, CopyMorebtn0dd8b3bd4bcdd41, CopyMoreHiddenKey0g5692532d4f041);
var CopyflxMoreOption0fb97a396345b44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0fb97a396345b44",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "240dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0fb97a396345b44.setDefaultUnit(kony.flex.DP);
var Copyflxm0h7173457d0144f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0h7173457d0144f",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0h7173457d0144f.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0jf496ecbeaa448 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0jf496ecbeaa448",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0g394126c5d2e45 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0g394126c5d2e45",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0h7173457d0144f.add(CopyMoreIcon0jf496ecbeaa448, CopyMoreLabel0g394126c5d2e45);
var CopyMorebtn0e2cb4034206f4c = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0e2cb4034206f4c",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_c80db98935664119a2b04f43956093e2,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0gf120e1dd6f347 = new kony.ui.Label({
"id": "CopyMoreHiddenKey0gf120e1dd6f347",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0fb97a396345b44.add(Copyflxm0h7173457d0144f, CopyMorebtn0e2cb4034206f4c, CopyMoreHiddenKey0gf120e1dd6f347);
var CopyflxMoreOption0b2846dabfe6446 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0b2846dabfe6446",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "360dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0b2846dabfe6446.setDefaultUnit(kony.flex.DP);
var Copyflxm0i816106a1a5443 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0i816106a1a5443",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0i816106a1a5443.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0hd21fbe878bf44 = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0hd21fbe878bf44",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0f0ceac6fcbd64c = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0f0ceac6fcbd64c",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0i816106a1a5443.add(CopyMoreIcon0hd21fbe878bf44, CopyMoreLabel0f0ceac6fcbd64c);
var CopyMorebtn0ba65aed5bb2944 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0ba65aed5bb2944",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_gf125b4731c84cd292011c78b11e169d,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0g74fe996bdc942 = new kony.ui.Label({
"id": "CopyMoreHiddenKey0g74fe996bdc942",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0b2846dabfe6446.add(Copyflxm0i816106a1a5443, CopyMorebtn0ba65aed5bb2944, CopyMoreHiddenKey0g74fe996bdc942);
var CopyflxMoreOption0h4e2eeea332040 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "CopyflxMoreOption0h4e2eeea332040",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "skncontainerBkgNone0b673526d5a5e4d",
"top": "360dp",
"width": "33%",
"zIndex": 1
}, {}, {});
CopyflxMoreOption0h4e2eeea332040.setDefaultUnit(kony.flex.DP);
var Copyflxm0g7406acfabee4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "Copyflxm0g7406acfabee4b",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox0db4f905a866a46",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Copyflxm0g7406acfabee4b.setDefaultUnit(kony.flex.DP);
var CopyMoreIcon0iea5399f8c6b4e = new kony.ui.Image2({
"centerX": "50%",
"height": "35dp",
"id": "CopyMoreIcon0iea5399f8c6b4e",
"isVisible": true,
"skin": "sknslImage0h21bd3728b6a46",
"src": "accountdetail.png",
"top": "10%",
"width": "35dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreLabel0jfaf167aa31d46 = new kony.ui.Label({
"centerX": "50%",
"height": "39dp",
"id": "CopyMoreLabel0jfaf167aa31d46",
"isVisible": true,
"skin": "skniconButtonLabel0c97d4ac41bba40",
"text": kony.i18n.getLocalizedString("i18n.account.AccountDetails"),
"top": "9dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxm0g7406acfabee4b.add(CopyMoreIcon0iea5399f8c6b4e, CopyMoreLabel0jfaf167aa31d46);
var CopyMorebtn0a2834e158b5c42 = new kony.ui.Button({
"focusSkin": "skninvisibleButtonFocus0efe69e4e0daf4d",
"height": "100%",
"id": "CopyMorebtn0a2834e158b5c42",
"isVisible": true,
"onClick": AS_Button_e0ad8a8ca784434ab03180183211c2c6,
"skin": "skninvisibleButtonNormal0f64100467ecc4b",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyMoreHiddenKey0a03a20488c9248 = new kony.ui.Label({
"id": "CopyMoreHiddenKey0a03a20488c9248",
"isVisible": false,
"right": "19dp",
"skin": "CopyslLabel0g7ddff62a84c47",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "32dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxMoreOption0h4e2eeea332040.add(Copyflxm0g7406acfabee4b, CopyMorebtn0a2834e158b5c42, CopyMoreHiddenKey0a03a20488c9248);
var CopylineDivider0g919817bdea14d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50%",
"clipBounds": true,
"id": "CopylineDivider0g919817bdea14d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 150,
"skin": "sknwhiteDivider0a18e65b4e8c444",
"top": 0,
"width": "3dp",
"zIndex": 1
}, {}, {});
CopylineDivider0g919817bdea14d.setDefaultUnit(kony.flex.DP);
CopylineDivider0g919817bdea14d.add();
var CopycollapseButton0f54c28f7434547 = new kony.ui.Button({
"bottom": "60px",
"centerX": "50%",
"centerY": "380dp",
"focusSkin": "roundCollapseButton",
"height": "50dp",
"id": "CopycollapseButton0f54c28f7434547",
"isVisible": true,
"right": 135,
"onClick": AS_Button_e6834779d95444c4a0fc3ef5fc34ce8b,
"skin": "roundCollapseButton",
"width": "50dp",
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyflxMore0dcd89fda4fe54d.add(CopyflxMoreOption0e148330049a044, CopyflxMoreOption0i8a60a30a7714b, CopyflxMoreOption0b5d391d43a5a41, CopyflxMoreOption0gca0166108a949, CopyflxMoreOption0jb23f1a5856e4e, CopyflxMoreOption0fb97a396345b44, CopyflxMoreOption0b2846dabfe6446, CopyflxMoreOption0h4e2eeea332040, CopylineDivider0g919817bdea14d, CopycollapseButton0f54c28f7434547);
CopyaccountDetailsOverview0dd1fa608aee343.add(CopygradientOverlay0ge127ec45b044b, CopyflxAccntDetailButtonsKA0jd6c5b383a2149, CopyactionWrapper0b8641777576440, CopyflxMore0dcd89fda4fe54d);
var flxLoansListInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40dp",
"id": "flxLoansListInfo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgheader",
"top": "165dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLoansListInfo.setDefaultUnit(kony.flex.DP);
var CopyscheduledTabButton0f2be9f13f83a4d = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "100%",
"id": "CopyscheduledTabButton0f2be9f13f83a4d",
"isVisible": false,
"right": "50%",
"onClick": AS_Button_j70c6a9f797d4f0f9f35990f4c75728d,
"skin": "skntabDeselected",
"text": kony.i18n.getLocalizedString("i18n.common.cSCHEDULED"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 1,0, 0],
"paddingInPixel": false
}, {});
var CopyrecentTabButton0b7bfd9ba709745 = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "100%",
"id": "CopyrecentTabButton0b7bfd9ba709745",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_f622759993404e18a0ea462b7b5119e3,
"skin": "skntabSelected",
"text": kony.i18n.getLocalizedString("i18n.deposit.recent"),
"top": "0dp",
"width": "50%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 1,0, 0],
"paddingInPixel": false
}, {});
var CopytabDeselectedIndicator0h1ec5110dd9b47 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "CopytabDeselectedIndicator0h1ec5110dd9b47",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknCopyslFbox00fe82e6f7cc84b",
"top": "0dp",
"width": "100%"
}, {}, {});
CopytabDeselectedIndicator0h1ec5110dd9b47.setDefaultUnit(kony.flex.DP);
CopytabDeselectedIndicator0h1ec5110dd9b47.add();
var CopytabSelectedIndicator0f6e9acfec3af4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "CopytabSelectedIndicator0f6e9acfec3af4b",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknCopyslFbox03fbc1653617542",
"width": "50%"
}, {}, {});
CopytabSelectedIndicator0f6e9acfec3af4b.setDefaultUnit(kony.flex.DP);
CopytabSelectedIndicator0f6e9acfec3af4b.add();
var CopylistDivider0f0b0729d7b0f4e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylistDivider0f0b0729d7b0f4e",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopylistDivider0f0b0729d7b0f4e.setDefaultUnit(kony.flex.DP);
CopylistDivider0f0b0729d7b0f4e.add();
var lblLoansListIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblLoansListIcon",
"isVisible": true,
"right": "20%",
"skin": "lblListOn",
"text": "W",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "70dp",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLoansInfoIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblLoansInfoIcon",
"isVisible": true,
"onTouchEnd": AS_Label_b382d52720d9477f8d924d12180bf0dc,
"left": "20%",
"skin": "lblListOff",
"text": "E",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxLoansListInfo.add(CopyscheduledTabButton0f2be9f13f83a4d, CopyrecentTabButton0b7bfd9ba709745, CopytabDeselectedIndicator0h1ec5110dd9b47, CopytabSelectedIndicator0f6e9acfec3af4b, CopylistDivider0f0b0729d7b0f4e, lblLoansListIcon, lblLoansInfoIcon);
flxLoansUpper.add(flxLoansTopBar, flxLoansAccountNames, flxLoansAccounts, CopyaccountDetailsOverview0dd1fa608aee343, flxLoansListInfo);
var flxLoansLower = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "60%",
"horizontalScrollIndicator": true,
"id": "flxLoansLower",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "1dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknGradientbluetodark",
"top": "40%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxLoansLower.setDefaultUnit(kony.flex.DP);
var CopyresourcesLabel0ff9c4baa95684d = new kony.ui.Label({
"height": "14.50%",
"id": "CopyresourcesLabel0ff9c4baa95684d",
"isVisible": false,
"right": "0dp",
"skin": "sknsecondaryHeader",
"text": kony.i18n.getLocalizedString("i18n.common.recentTransactions"),
"top": "0dp",
"width": "100%",
"zIndex": 10000
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxLoansSearchScreen = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxLoansSearchScreen",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 10000
}, {}, {});
flxLoansSearchScreen.setDefaultUnit(kony.flex.DP);
var lblLoansSearchIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblLoansSearchIcon",
"isVisible": true,
"right": "4%",
"skin": "CopyslLabel0j2197a197ee14d",
"text": "h",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "8dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLoansSearchArea = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"height": "30dp",
"id": "lblLoansSearchArea",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "8%",
"placeholder": "Search transaction",
"secureTextEntry": false,
"skin": "CopyslTextBox0f379cdd9b28441",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "13dp",
"width": "185dp",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblLoansExportIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblLoansExportIcon",
"isVisible": true,
"onTouchEnd": AS_Label_d10f105b9a994ffbb9368c805737626c,
"left": "15%",
"skin": "CopyslLabel0i2b4c385eb5140",
"text": "l",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLoansFilterIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblLoansFilterIcon",
"isVisible": false,
"left": "4%",
"skin": "CopyslLabel0b8f2bdd739144d",
"text": "n",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblLoansFilterIcon0bdce4598dc274d = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblLoansFilterIcon0bdce4598dc274d",
"isVisible": true,
"onTouchEnd": AS_Label_fd9eda376e0b4905b82fb5a11cccdb7d,
"left": "4%",
"skin": "CopyslLabel0b8f2bdd739144d",
"text": "n",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flexLoanSingleLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flexLoanSingleLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "CopyslFbox0ab42d5f22ae345",
"top": "40dp",
"width": "200dp",
"zIndex": 1
}, {}, {});
flexLoanSingleLine.setDefaultUnit(kony.flex.DP);
flexLoanSingleLine.add();
flxLoansSearchScreen.add(lblLoansSearchIcon, lblLoansSearchArea, lblLoansExportIcon, lblLoansFilterIcon, CopylblLoansFilterIcon0bdce4598dc274d, flexLoanSingleLine);
var CopyflxDealsSegment0db0d143bfe7644 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "85.50%",
"id": "CopyflxDealsSegment0db0d143bfe7644",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "200%"
}, {}, {});
CopyflxDealsSegment0db0d143bfe7644.setDefaultUnit(kony.flex.DP);
var loanTransactionSeg = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bottom": "5dp",
"data": [{
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}],
"groupCells": false,
"id": "loanTransactionSeg",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowTemplate": CopyFlexContainer011baa9bcad7141,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "072c4800",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer011baa9bcad7141": "CopyFlexContainer011baa9bcad7141",
"ImgWithDraw": "ImgWithDraw",
"Imgcheck": "Imgcheck",
"chevron": "chevron",
"flxImageandNameKA": "flxImageandNameKA",
"lblExpiryTime": "lblExpiryTime",
"lblLastTransaction": "lblLastTransaction",
"lblNoData": "lblNoData",
"lblSepKA": "lblSepKA",
"transactionAmount": "transactionAmount",
"transactionAmountSI": "transactionAmountSI",
"transactionDate": "transactionDate",
"transactionName": "transactionName"
},
"width": "50%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyscheduledTransactionSegment0c9cb6e68f4c04d = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bottom": "5dp",
"data": [{
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "Payment to City of Austin",
"lblSepKA": "Label",
"transactionAmount": "$155.00",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}],
"groupCells": false,
"id": "CopyscheduledTransactionSegment0c9cb6e68f4c04d",
"isVisible": false,
"right": "50%",
"needPageIndicator": true,
"onRowClick": AS_Segment_cd8fe135219740e3acab134e2a565a40,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": CopyFlexContainer011baa9bcad7141,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "ffffff00",
"separatorRequired": true,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer011baa9bcad7141": "CopyFlexContainer011baa9bcad7141",
"ImgWithDraw": "ImgWithDraw",
"Imgcheck": "Imgcheck",
"chevron": "chevron",
"flxImageandNameKA": "flxImageandNameKA",
"lblExpiryTime": "lblExpiryTime",
"lblLastTransaction": "lblLastTransaction",
"lblNoData": "lblNoData",
"lblSepKA": "lblSepKA",
"transactionAmount": "transactionAmount",
"transactionAmountSI": "transactionAmountSI",
"transactionDate": "transactionDate",
"transactionName": "transactionName"
},
"width": "50%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabelNoRecordsKA0e9ab92e918c244 = new kony.ui.Label({
"centerY": "50%",
"id": "CopyLabelNoRecordsKA0e9ab92e918c244",
"isVisible": false,
"right": "0dp",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.common.NoTransactions"),
"top": "0dp",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblSheduledNoRecordsKA0da28d23e1ef344 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblSheduledNoRecordsKA0da28d23e1ef344",
"isVisible": false,
"right": "50%",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.common.NoTransactions"),
"top": "0dp",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopysegmentBorderBottom0c77212a03f204b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopysegmentBorderBottom0c77212a03f204b",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopysegmentBorderBottom0c77212a03f204b.setDefaultUnit(kony.flex.DP);
CopysegmentBorderBottom0c77212a03f204b.add();
var CopyFlexContainer0d2744687061946 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100dp",
"id": "CopyFlexContainer0d2744687061946",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "-2dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0d2744687061946.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0d2744687061946.add();
CopyflxDealsSegment0db0d143bfe7644.add(loanTransactionSeg, CopyscheduledTransactionSegment0c9cb6e68f4c04d, CopyLabelNoRecordsKA0e9ab92e918c244, CopylblSheduledNoRecordsKA0da28d23e1ef344, CopysegmentBorderBottom0c77212a03f204b, CopyFlexContainer0d2744687061946);
flxLoansLower.add(CopyresourcesLabel0ff9c4baa95684d, flxLoansSearchScreen, CopyflxDealsSegment0db0d143bfe7644);
flxLoansScreen.add(flxLoansUpper, flxLoansLower);
var lblNoResult = new kony.ui.Label({
"centerX": "50%",
"centerY": "60%",
"id": "lblNoResult",
"isVisible": false,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.commomerror.dataNotFound"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxPageIndicator = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": false,
"bounces": false,
"centerX": "51%",
"centerY": "4%",
"clipBounds": true,
"enableScrolling": true,
"height": "4%",
"horizontalScrollIndicator": false,
"id": "flxPageIndicator",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_HORIZONTAL,
"skin": "slFSbox",
"verticalScrollIndicator": true,
"width": "50%",
"zIndex": 3
}, {}, {});
flxPageIndicator.setDefaultUnit(kony.flex.DP);
var flxPageIndicator0 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "20%",
"id": "flxPageIndicator0",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknPageIndicatorFlexnoOpc",
"width": "8%",
"zIndex": 1
}, {}, {});
flxPageIndicator0.setDefaultUnit(kony.flex.DP);
flxPageIndicator0.add();
flxPageIndicator.add(flxPageIndicator0);
var flxAccountdetailsSortContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"focusSkin": "sknflxTransprnt",
"height": "92%",
"id": "flxAccountdetailsSortContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "100%",
"width": "100%",
"zIndex": 3
}, {}, {});
flxAccountdetailsSortContainer.setDefaultUnit(kony.flex.DP);
var flxAccountDetailsSort = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30%",
"id": "flxAccountDetailsSort",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknWhiteBGroundedBorder",
"top": "70%",
"width": "100%",
"zIndex": 3
}, {}, {});
flxAccountDetailsSort.setDefaultUnit(kony.flex.DP);
var lblSortby = new kony.ui.Label({
"centerX": "50%",
"centerY": "10%",
"id": "lblSortby",
"isVisible": true,
"skin": "sknCairoBold120",
"text": kony.i18n.getLocalizedString("i18n.common.sortby"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxLine1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "22%",
"clipBounds": true,
"height": "1%",
"id": "flxLine1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknGreyLine50Opc",
"top": 42,
"width": "80%",
"zIndex": 1
}, {}, {});
flxLine1.setDefaultUnit(kony.flex.DP);
flxLine1.add();
var lblSortDate = new kony.ui.Label({
"centerX": "40%",
"centerY": "40.00%",
"id": "lblSortDate",
"isVisible": true,
"skin": "sknCarioSemiBold130perDRKBluefont80Opc",
"text": kony.i18n.getLocalizedString("i18n.accounts.date"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxSortDateAsc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "60%",
"centerY": "40%",
"clipBounds": true,
"height": "17%",
"id": "flxSortDateAsc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_j6801224b8544ce9bb7a340ed45cdf53,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortDateAsc.setDefaultUnit(kony.flex.DP);
var imgSortDateAsc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortDateAsc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_straight_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortDateAsc.add(imgSortDateAsc);
var flxSortDateDesc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "68%",
"centerY": "40%",
"clipBounds": true,
"height": "17%",
"id": "flxSortDateDesc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_fdf977484fac4bae83f19cf0dbdea5c8,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortDateDesc.setDefaultUnit(kony.flex.DP);
var imgSortDateDesc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortDateDesc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_down_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortDateDesc.add(imgSortDateDesc);
var lblSortAmount = new kony.ui.Label({
"centerX": "40%",
"centerY": "65%",
"id": "lblSortAmount",
"isVisible": true,
"skin": "sknCarioSemiBold130perDRKBluefont80Opc",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxSortAmountAsc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "60%",
"centerY": "65%",
"clipBounds": true,
"height": "17%",
"id": "flxSortAmountAsc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_d2919aeed5ba4e98956e843adbab8f5b,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortAmountAsc.setDefaultUnit(kony.flex.DP);
var imgSortAmountAsc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortAmountAsc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_straight_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortAmountAsc.add(imgSortAmountAsc);
var flxSortAmountDesc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "68%",
"centerY": "65%",
"clipBounds": true,
"height": "17%",
"id": "flxSortAmountDesc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_b61193b754714114b3a95c7b7effb557,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortAmountDesc.setDefaultUnit(kony.flex.DP);
var imgSortAmountDesc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortAmountDesc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_down_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortAmountDesc.add(imgSortAmountDesc);
flxAccountDetailsSort.add(lblSortby, flxLine1, lblSortDate, flxSortDateAsc, flxSortDateDesc, lblSortAmount, flxSortAmountAsc, flxSortAmountDesc);
flxAccountdetailsSortContainer.add(flxAccountDetailsSort);
frmAccountDetailKA.add(accountDetailsScrollContainer, accountLabelScroll, flxDealsScreen, flxLoansScreen, lblNoResult, flxPageIndicator, flxAccountdetailsSortContainer);
};
function frmAccountDetailKAGlobalsAr() {
frmAccountDetailKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmAccountDetailKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"footers": [footerBack],
"id": "frmAccountDetailKA",
"init": AS_Form_bcfc048b07974938b46a8457b03339aa,
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_afb9a294c4414f1bb28199ae6f312417,
"preShow": AS_Form_be1d5e3a340845749d2d333a89ba9606,
"skin": "CopysknSuccessBkg0abb718ccac584f"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"inTransitionConfig": {
"formAnimation": 2
},
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_46436bbe18f84abe9f63a73e3230bcb7,
"outTransitionConfig": {
"formAnimation": 0
},
"retainScrollPosition": false,
"titleBar": false,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};

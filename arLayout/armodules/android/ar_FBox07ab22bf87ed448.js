//Do not Modify!! This is an auto generated module for 'android'. Generated on Mon Dec 21 10:10:34 EET 2020
function initializeFBox07ab22bf87ed448Ar() {
    FBox07ab22bf87ed448Ar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "FBox07ab22bf87ed448",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    FBox07ab22bf87ed448Ar.setDefaultUnit(kony.flex.DP);
    var lblOperationHrs = new kony.ui.Label({
        "id": "lblOperationHrs",
        "isVisible": true,
        "right": "0dp",
        "minHeight": "20dp",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.locateus.frmLocatorBranchDetailsKA.CopyatmServices00ff586c7132d45"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    FBox07ab22bf87ed448Ar.add(lblOperationHrs);
}

//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmPreferredAccountsKAAr() {
    frmPreferredAccountsKA.setDefaultUnit(kony.flex.DP);
    var iosTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "iosTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    iosTitleBar.setDefaultUnit(kony.flex.DP);
    var userSettingsLabel = new kony.ui.Label({
        "centerX": "50%",
        "id": "userSettingsLabel",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.settings.preferredAccounts"),
        "top": "15dp",
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var backButton = new kony.ui.Button({
        "focusSkin": "sknleftBackButtonFocus",
        "height": "50dp",
        "id": "backButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_c6f957b50025464382aaec59fadae38f,
        "skin": "sknleftBackButtonNormal",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    iosTitleBar.add(userSettingsLabel, backButton);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var androidSettings = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "androidSettings",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidSettings.setDefaultUnit(kony.flex.DP);
    var lbltransferHeader = new kony.ui.Label({
        "height": "35dp",
        "id": "lbltransferHeader",
        "isVisible": true,
        "right": "5%",
        "skin": "skn30363f110KA",
        "text": "For Transfers",
        "width": "64.16%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var ForTransfers = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "HiddenLbl": "",
            "imgicontick": "",
            "lblPageNameKA": "Saving Account 2354"
        }, {
            "HiddenLbl": "",
            "imgicontick": "",
            "lblPageNameKA": "Checking Account 6587"
        }],
        "groupCells": false,
        "id": "ForTransfers",
        "isVisible": true,
        "right": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_971b946f471b45a5b6118b9465ab254a,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": container,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": false,
        "separatorThickness": 0,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "HiddenLbl": "HiddenLbl",
            "container": "container",
            "imgicontick": "imgicontick",
            "lblPageNameKA": "lblPageNameKA"
        },
        "width": "100%"
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": false
    });
    var segmentBorderBottomAndroid = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "segmentBorderBottomAndroid",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "-1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    segmentBorderBottomAndroid.setDefaultUnit(kony.flex.DP);
    segmentBorderBottomAndroid.add();
    androidSettings.add(lbltransferHeader, ForTransfers, segmentBorderBottomAndroid);
    mainContent.add(androidSettings);
    frmPreferredAccountsKA.add(iosTitleBar, mainContent);
};
function frmPreferredAccountsKAGlobalsAr() {
    frmPreferredAccountsKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPreferredAccountsKAAr,
        "bounces": false,
        "enabledForIdleTimeout": true,
        "id": "frmPreferredAccountsKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": true,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false
    });
};

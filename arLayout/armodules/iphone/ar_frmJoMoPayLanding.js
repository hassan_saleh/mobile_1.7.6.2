//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmJoMoPayLandingAr() {
frmJoMoPayLanding.setDefaultUnit(kony.flex.DP);
var flxJoMoPayHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "23%",
"id": "flxJoMoPayHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFlxHeaderImg",
"top": "0%",
"width": "100%",
"zIndex": 5
}, {}, {});
flxJoMoPayHeader.setDefaultUnit(kony.flex.DP);
var flxHeadMain1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "34%",
"id": "flxHeadMain1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 3
}, {}, {});
flxHeadMain1.setDefaultUnit(kony.flex.DP);
var flxBackLanding = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxBackLanding",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_bc7bdd6dbef142e99e18969ae884c33b,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBackLanding.setDefaultUnit(kony.flex.DP);
var CopylblBackIcon0ia1d1aefbd8d4c = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "CopylblBackIcon0ia1d1aefbd8d4c",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblBack0e1c5acde1ca046 = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "CopylblBack0e1c5acde1ca046",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBackLanding.add(CopylblBackIcon0ia1d1aefbd8d4c, CopylblBack0e1c5acde1ca046);
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "25%",
"id": "lblBack",
"isVisible": false,
"left": "8%",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnReg = new kony.ui.Button({
"focusSkin": "CopyslButtonGlosssBlue0hcfda7e5f61c48",
"height": "100%",
"id": "btnReg",
"isVisible": true,
"onClick": AS_Button_j0446ba9d958486385caf7565e3dcf6a,
"right": "11%",
"skin": "CopyslButtonGlosssBlue0hcfda7e5f61c48",
"text": "Q",
"top": "0%",
"width": "11%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var btnAdd = new kony.ui.Button({
"focusSkin": "CopyslButtonGlosssBlue0eff25d8e6d2f4d",
"height": "100%",
"id": "btnAdd",
"isVisible": true,
"onClick": AS_Button_g0d2f17d9fa740eab689b862f3b4c95f,
"right": "0%",
"skin": "CopyslButtonGlosssBlue0eff25d8e6d2f4d",
"text": "u",
"top": "0%",
"width": "11%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var btnBack = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
"height": "50%",
"id": "btnBack",
"isVisible": false,
"left": "0.01%",
"onClick": AS_Button_eaba936138494c19ac8027e67eb3a444,
"skin": "CopyslButtonGlossBlue0e73a02c4810645",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0.00%",
"width": "15%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [2, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var lblJoMoPay = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "100%",
"id": "lblJoMoPay",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.jomopay.jomopaytitle"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxHeadMain1.add(flxBackLanding, lblBack, btnReg, btnAdd, btnBack, lblJoMoPay);
var flxSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "28%",
"id": "flxSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxSearch",
"top": "62%",
"width": "100%",
"zIndex": 3
}, {}, {});
flxSearch.setDefaultUnit(kony.flex.DP);
var txtSearch1 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"height": "60%",
"id": "txtSearch1",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "10%",
"onDone": AS_TextField_ad7459f1814543e1bdaf62b5ac660924,
"placeholder": kony.i18n.getLocalizedString("i18n.transfer.searchPlh"),
"secureTextEntry": false,
"skin": "CopyslTextBox0c1d969ae9edf4b",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "5%",
"width": "90%",
"zIndex": 2
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"leftViewImage": "search_empty.png",
"pasteboardType": constants.TEXTBOX_PASTE_BOARD_TYPE_SYSTEM_LEVEL,
"placeholderSkin": "sknPlaceHolder",
"showClearButton": true,
"showCloseButton": false,
"showProgressIndicator": false,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxBorderLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "90%",
"width": "90%",
"zIndex": 2
}, {}, {});
flxBorderLine.setDefaultUnit(kony.flex.DP);
flxBorderLine.add();
var imgSearch = new kony.ui.Image2({
"height": "60%",
"id": "imgSearch",
"isVisible": false,
"right": "7%",
"onTouchEnd": AS_Image_dc6a815418e649069104a650fe9c3100,
"skin": "slImage",
"src": "search_icon.png",
"top": "5%",
"width": "5%",
"zIndex": 2
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var txtSearch = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "CopyslTextBox0c1d969ae9edf4b",
"height": "85%",
"id": "txtSearch",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "13%",
"maxTextLength": 35,
"onDone": AS_TextField_e8e7876ef2c846eaa754f3f7a30ff9ba,
"onTextChange": AS_TextField_e8e7876ef2c846eaa754f3f7a30ff9ba,
"placeholder": kony.i18n.getLocalizedString("i18n.transfer.searchPlh"),
"secureTextEntry": false,
"skin": "CopyslTextBox0c1d969ae9edf4b",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0%",
"width": "70%",
"zIndex": 2
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceHolder",
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var btnSearch = new kony.ui.Button({
"focusSkin": "sknSearch",
"height": "96%",
"id": "btnSearch",
"isVisible": true,
"right": "5%",
"onClick": AS_Button_a6b8ccee7579411ba5191945fd3e4728,
"skin": "sknSearch",
"text": "h",
"top": "0%",
"width": "8%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxSearch.add(txtSearch1, flxBorderLine, imgSearch, txtSearch, btnSearch);
var FlxJomopayTab = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "28%",
"id": "FlxJomopayTab",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "34%",
"width": "100%",
"zIndex": 3
}, {}, {});
FlxJomopayTab.setDefaultUnit(kony.flex.DP);
var flxContent = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "68%",
"id": "flxContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFboxOuterRing",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxContent.setDefaultUnit(kony.flex.DP);
var btnBene = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnBene",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_f3ccdc83ba3348538172008c901d48aa,
"skin": "slButtonWhiteTab",
"text": kony.i18n.getLocalizedString("i18n.jomopay.beneficiary"),
"top": "0dp",
"width": "50%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnTrans = new kony.ui.Button({
"centerY": "47%",
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnTrans",
"isVisible": true,
"right": "0",
"onClick": AS_Button_e2cc375953f14254aca9b9ba1ea7188e,
"skin": "slButtonWhiteTabDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.transactions"),
"top": "0dp",
"width": "50%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxContent.add( btnTrans,btnBene);
FlxJomopayTab.add(flxContent);
flxJoMoPayHeader.add(flxHeadMain1, flxSearch, FlxJomopayTab);
var flxPaymentHistory = new kony.ui.FlexScrollContainer({
"accessibilityConfig": {
"a11yNavigationMode": constants.ACCESSIBILITY_NAVIGATION_MODE_NATIVE
},
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": false,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxPaymentHistory",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknflxScrollBlue",
"top": "0dp",
"verticalScrollIndicator": false,
"width": "100.00%",
"zIndex": 1
}, {}, {});
flxPaymentHistory.setDefaultUnit(kony.flex.DP);
var titleBarAccountDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "9%",
"id": "titleBarAccountDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "s",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarAccountDetails.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "0dp",
"onClick": AS_FlexContainer_b304e71e5bee4f6b86fead9b8ce20daf,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
var lblBoj = new kony.ui.Label({
"centerX": "50%",
"height": "90%",
"id": "lblBoj",
"isVisible": true,
"skin": "sknLblWhite",
"text": "Payment History",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
titleBarAccountDetails.add(flxBack, lblBoj);
var flxSegmentWrapper = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "flxSegmentWrapper",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknGradientbluetodark",
"top": "9%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 2
}, {}, {});
flxSegmentWrapper.setDefaultUnit(kony.flex.DP);
var resourcesLabel = new kony.ui.Label({
"height": "14.50%",
"id": "resourcesLabel",
"isVisible": false,
"right": "0dp",
"skin": "sknsecondaryHeader",
"text": kony.i18n.getLocalizedString("i18n.common.recentTransactions"),
"top": "0dp",
"width": "100%",
"zIndex": 10000
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxExtraIcons = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxExtraIcons",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxExtraIcons.setDefaultUnit(kony.flex.DP);
var lblSearch = new kony.ui.Label({
"centerY": "50%",
"id": "lblSearch",
"isVisible": true,
"right": "4%",
"skin": "CopyslLabel0j2197a197ee14d",
"text": "h",
"top": "8dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopytxtSearch0a0ed2251565c45 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"focusSkin": "sknCarioRegular125OPc50",
"height": "30dp",
"id": "CopytxtSearch0a0ed2251565c45",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "8%",
"maxTextLength": 30,
"placeholder": kony.i18n.getLocalizedString("i18n.search.SearchTransactions"),
"secureTextEntry": false,
"skin": "sknCarioRegular125OPc50",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "13dp",
"width": "185dp",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoCorrect": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"pasteboardType": constants.TEXTBOX_PASTE_BOARD_TYPE_NO_PASTE_BOARD,
"placeholderSkin": "sknPlaceHolder",
"showClearButton": true,
"showCloseButton": true,
"showProgressIndicator": true,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblExport = new kony.ui.Label({
"height": "100%",
"id": "lblExport",
"isVisible": true,
"onTouchEnd": AS_Label_f404d10e250f4b60b7f9676211bed896,
"left": "15%",
"skin": "CopyslLabel0i2b4c385eb5140",
"text": "l",
"top": "0%",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopylblFilter0f7770a92cff247 = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblFilter0f7770a92cff247",
"isVisible": true,
"onTouchEnd": AS_Label_d803739e56b54fdc941e5e36180f1e5d,
"left": "4%",
"skin": "CopyslLabel0b8f2bdd739144d",
"text": "n",
"top": "0dp",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "CopyslFbox0ab42d5f22ae345",
"top": "82%",
"width": "200dp",
"zIndex": 2
}, {}, {});
flxLine.setDefaultUnit(kony.flex.DP);
flxLine.add();
flxExtraIcons.add(lblSearch, CopytxtSearch0a0ed2251565c45, lblExport, CopylblFilter0f7770a92cff247, flxLine);
var CopyflxLine0a4e3964e40ce45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxLine0a4e3964e40ce45",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknLineDarkBlue",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyflxLine0a4e3964e40ce45.setDefaultUnit(kony.flex.DP);
CopyflxLine0a4e3964e40ce45.add();
var flxUnClearedTransactions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxUnClearedTransactions",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnClearedTransactions.setDefaultUnit(kony.flex.DP);
var lblUnclearedTransactions = new kony.ui.Label({
"centerY": "50%",
"id": "lblUnclearedTransactions",
"isVisible": true,
"right": "5%",
"skin": "sknLightBlue110perCarioSemiBold",
"text": kony.i18n.getLocalizedString("i18n.accounts.unclearedFunds"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxUnClearedFundsDownArrow = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "90%",
"id": "flxUnClearedFundsDownArrow",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "slFbox",
"width": "8%",
"zIndex": 1
}, {}, {});
flxUnClearedFundsDownArrow.setDefaultUnit(kony.flex.DP);
var lblUnClearedFundsRing = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblUnClearedFundsRing",
"isVisible": true,
"skin": "sknBOJFontLightBlue140per",
"text": "V",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxUnClearedFundsDownArrow.add(lblUnClearedFundsRing);
var lblUnClearedTransactionTotal = new kony.ui.Label({
"centerY": "50%",
"id": "lblUnClearedTransactionTotal",
"isVisible": true,
"left": "13%",
"skin": "sknLightBlue110perCarioSemiBold",
"text": "-130.200 JOD",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxLineUnclearedTransaction = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "2%",
"id": "flxLineUnclearedTransaction",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"skin": "sknBOJblueBG",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLineUnclearedTransaction.setDefaultUnit(kony.flex.DP);
flxLineUnclearedTransaction.add();
flxUnClearedTransactions.add(lblUnclearedTransactions, flxUnClearedFundsDownArrow, lblUnClearedTransactionTotal, flxLineUnclearedTransaction);
var accountDetailsTransactions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "accountDetailsTransactions",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0.00%",
"width": "100%"
}, {}, {});
accountDetailsTransactions.setDefaultUnit(kony.flex.DP);
accountDetailsTransactions.add();
var transactionSegment = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bottom": "5dp",
"data": [
[{
"lbltmpTitle": "Label"
},
[{
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "",
"lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
"lblSepKA": "Label",
"lblTransRef": "1234",
"lblTransType": "",
"transactionAmount": "",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "",
"lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
"lblSepKA": "Label",
"lblTransRef": "1234",
"lblTransType": "",
"transactionAmount": "",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "",
"lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
"lblSepKA": "Label",
"lblTransRef": "1234",
"lblTransType": "",
"transactionAmount": "",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}]
],
[{
"lbltmpTitle": "Label"
},
[{
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "",
"lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
"lblSepKA": "Label",
"lblTransRef": "1234",
"lblTransType": "",
"transactionAmount": "",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "",
"lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
"lblSepKA": "Label",
"lblTransRef": "1234",
"lblTransType": "",
"transactionAmount": "",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"ImgWithDraw": "pending.png",
"Imgcheck": "checkf.png",
"chevron": "right_chevron_icon.png",
"lblExpiryTime": "21h:22m",
"lblLastTransaction": "",
"lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
"lblSepKA": "Label",
"lblTransRef": "1234",
"lblTransType": "",
"transactionAmount": "",
"transactionAmountSI": "$155.00",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}]
]
],
"groupCells": false,
"id": "transactionSegment",
"isVisible": true,
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "sknsegAcc",
"rowSkin": "sknsegAcc",
"rowTemplate": flxtmpPaymentHistoryNewKA,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"sectionHeaderTemplate": flxTmpHeader,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "072c4800",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"ImgWithDraw": "ImgWithDraw",
"Imgcheck": "Imgcheck",
"chevron": "chevron",
"flxImageandNameKA": "flxImageandNameKA",
"flxTmpHeader": "flxTmpHeader",
"flxUnderline": "flxUnderline",
"flxtmpPaymentHistoryNewKA": "flxtmpPaymentHistoryNewKA",
"lblExpiryTime": "lblExpiryTime",
"lblLastTransaction": "lblLastTransaction",
"lblNoData": "lblNoData",
"lblSepKA": "lblSepKA",
"lblTransRef": "lblTransRef",
"lblTransType": "lblTransType",
"lbltmpTitle": "lbltmpTitle",
"transactionAmount": "transactionAmount",
"transactionAmountSI": "transactionAmountSI",
"transactionDate": "transactionDate",
"transactionName": "transactionName"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": true
});
var lblNoResult = new kony.ui.Label({
"centerY": "50%",
"id": "lblNoResult",
"isVisible": false,
"right": "0dp",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.common.NoTransactions"),
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxSegmentWrapper.add(resourcesLabel, flxExtraIcons, CopyflxLine0a4e3964e40ce45, flxUnClearedTransactions, accountDetailsTransactions, transactionSegment, lblNoResult);
var flxAccountdetailsSortContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"focusSkin": "sknflxTransprnt",
"height": "92%",
"id": "flxAccountdetailsSortContainer",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "100%",
"width": "100%",
"zIndex": 3
}, {}, {});
flxAccountdetailsSortContainer.setDefaultUnit(kony.flex.DP);
var flxAccountDetailsSort = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30%",
"id": "flxAccountDetailsSort",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknWhiteBGroundedBorder",
"top": "70%",
"width": "100%",
"zIndex": 3
}, {}, {});
flxAccountDetailsSort.setDefaultUnit(kony.flex.DP);
var lblSortby = new kony.ui.Label({
"centerX": "50%",
"centerY": "10%",
"id": "lblSortby",
"isVisible": true,
"skin": "sknCairoBold120",
"text": kony.i18n.getLocalizedString("i18n.common.sortby"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxLine1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "22%",
"clipBounds": true,
"height": "1%",
"id": "flxLine1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknGreyLine50Opc",
"top": 42,
"width": "80%",
"zIndex": 1
}, {}, {});
flxLine1.setDefaultUnit(kony.flex.DP);
flxLine1.add();
var lblSortDate = new kony.ui.Label({
"centerX": "40%",
"centerY": "40.00%",
"id": "lblSortDate",
"isVisible": true,
"skin": "sknCarioSemiBold130perDRKBluefont80Opc",
"text": kony.i18n.getLocalizedString("i18n.accounts.date"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxSortDateAsc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "60%",
"centerY": "40%",
"clipBounds": true,
"height": "17%",
"id": "flxSortDateAsc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_i91ccfe93cea405cb3c77fcc5648b5b8,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortDateAsc.setDefaultUnit(kony.flex.DP);
var imgSortDateAsc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortDateAsc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_straight_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortDateAsc.add(imgSortDateAsc);
var flxSortDateDesc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "68%",
"centerY": "40%",
"clipBounds": true,
"height": "17%",
"id": "flxSortDateDesc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_i0c3c7e31623425dbe981e5b0ecdd9cf,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortDateDesc.setDefaultUnit(kony.flex.DP);
var imgSortDateDesc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortDateDesc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_down_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortDateDesc.add(imgSortDateDesc);
var lblSortAmount = new kony.ui.Label({
"centerX": "40%",
"centerY": "65%",
"id": "lblSortAmount",
"isVisible": true,
"skin": "sknCarioSemiBold130perDRKBluefont80Opc",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxSortAmountAsc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "60%",
"centerY": "65%",
"clipBounds": true,
"height": "17%",
"id": "flxSortAmountAsc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_e3bab6ac2b5a4231868465005103c207,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortAmountAsc.setDefaultUnit(kony.flex.DP);
var imgSortAmountAsc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortAmountAsc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_straight_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortAmountAsc.add(imgSortAmountAsc);
var flxSortAmountDesc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "68%",
"centerY": "65%",
"clipBounds": true,
"height": "17%",
"id": "flxSortAmountDesc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_dafa2cff9f634e3e8b9ec6ae7853df72,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortAmountDesc.setDefaultUnit(kony.flex.DP);
var imgSortAmountDesc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortAmountDesc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_down_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortAmountDesc.add(imgSortAmountDesc);
flxAccountDetailsSort.add(lblSortby, flxLine1, lblSortDate, flxSortDateAsc, flxSortDateDesc, lblSortAmount, flxSortAmountAsc, flxSortAmountDesc);
flxAccountdetailsSortContainer.add(flxAccountDetailsSort);
var footerBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": false,
"height": "10%",
"id": "footerBack",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "skncontainerBkgWhite",
"top": "90%",
"width": "100%"
}, {}, {});
footerBack.setDefaultUnit(kony.flex.DP);
var footerBackground = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "footerBackground",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "menu",
"top": "0",
"width": "100%"
}, {}, {});
footerBackground.setDefaultUnit(kony.flex.DP);
var FlxAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxAccounts",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknfocusmenu",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxAccounts.setDefaultUnit(kony.flex.DP);
var img1 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img1",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_accounts_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label03174bff69bb54c = new kony.ui.Label({
"centerX": "50%",
"id": "Label03174bff69bb54c",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnAccounts = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnUser",
"height": "50dp",
"id": "btnAccounts",
"isVisible": true,
"onClick": AS_Button_a7bc5540955d45ef9e50f6d9d87db20b,
"skin": "btnUser",
"text": "H",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlxAccounts.add(img1, Label03174bff69bb54c, btnAccounts);
var FlxTranfers = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxTranfers",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxTranfers.setDefaultUnit(kony.flex.DP);
var img2 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img2",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_t_and_p_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label02bec01fd5baf4c = new kony.ui.Label({
"centerX": "50%",
"id": "Label02bec01fd5baf4c",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.Payments"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopybtnAccounts0ia3b1ff37c304b = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0ia3b1ff37c304b",
"isVisible": true,
"onClick": AS_Button_f966a395ea004d248ce601391db1ef8b,
"skin": "btnCard",
"text": "I",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlxTranfers.add(img2, Label02bec01fd5baf4c, CopybtnAccounts0ia3b1ff37c304b);
var FlxBot = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxBot",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 2
}, {}, {});
FlxBot.setDefaultUnit(kony.flex.DP);
var imgBot = new kony.ui.Image2({
"centerX": "50%",
"height": "40dp",
"id": "imgBot",
"isVisible": false,
"right": "13dp",
"skin": "slImage",
"src": "chaticonactive.png",
"top": "4dp",
"width": "40dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnAccounts0ff48f9feb0aa42 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0ff48f9feb0aa42",
"isVisible": true,
"onClick": AS_Button_f8edc738592e46f4bb3dfc3fe7641857,
"skin": "btnCard",
"text": "i",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlxBot.add(imgBot, CopybtnAccounts0ff48f9feb0aa42);
var FlxDeposits = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxDeposits",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxDeposits.setDefaultUnit(kony.flex.DP);
var img3 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img3",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_deposits_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label04221a71494e848 = new kony.ui.Label({
"centerX": "50%",
"id": "Label04221a71494e848",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.deposits"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopybtnAccounts0bc68a97c14fb49 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0bc68a97c14fb49",
"isVisible": true,
"skin": "btnCard",
"text": "J",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlxDeposits.add(img3, Label04221a71494e848, CopybtnAccounts0bc68a97c14fb49);
var FlxMore = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxMore",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxMore.setDefaultUnit(kony.flex.DP);
var img4 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img4",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_more_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label0e5331028c2ef41 = new kony.ui.Label({
"centerX": "50%",
"id": "Label0e5331028c2ef41",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var CopybtnAccounts0ec4d23080f0146 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0ec4d23080f0146",
"isVisible": true,
"skin": "btnCard",
"text": "K",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
FlxMore.add(img4, Label0e5331028c2ef41, CopybtnAccounts0ec4d23080f0146);
footerBackground.add( FlxMore, FlxDeposits, FlxBot, FlxTranfers,FlxAccounts);
footerBack.add(footerBackground);
flxPaymentHistory.add(titleBarAccountDetails, flxSegmentWrapper, flxAccountdetailsSortContainer, footerBack);
var flxCheckRegJomopay = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "88%",
"id": "flxCheckRegJomopay",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxBackgrounf",
"top": "12%",
"width": "100%",
"zIndex": 6
}, {}, {});
flxCheckRegJomopay.setDefaultUnit(kony.flex.DP);
var btnRege = new kony.ui.Button({
"centerX": "50.04%",
"focusSkin": "slButtonWhite",
"height": "8%",
"id": "btnRege",
"isVisible": true,
"onClick": AS_Button_jb3e76caf5fa4bb9bfd874cff207cf3e,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.jomopay.createaccount"),
"top": "85%",
"width": "70%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": false
});
var imgBeneficiary = new kony.ui.Image2({
"centerX": "50%",
"height": "35%",
"id": "imgBeneficiary",
"isVisible": true,
"skin": "slImage",
"src": "icon_beneficiary.png",
"top": "10%",
"width": "90%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblMessageTitle = new kony.ui.Label({
"centerX": "50%",
"id": "lblMessageTitle",
"isVisible": true,
"skin": "sknLblMsg",
"text": kony.i18n.getLocalizedString("i18n.externalAccount.Sendmoneytobeneficiary"),
"top": "50%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblMessage = new kony.ui.Label({
"centerX": "50%",
"id": "lblMessage",
"isVisible": true,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.regestrationMsg"),
"textStyle": {},
"top": "60%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var btnAddBene = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "8%",
"id": "btnAddBene",
"isVisible": false,
"onClick": AS_Button_g0d2f17d9fa740eab689b862f3b4c95f,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.Bene.Addbeneficiary"),
"top": "85%",
"width": "70%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxCheckRegJomopay.add(btnRege, imgBeneficiary, lblMessageTitle, lblMessage, btnAddBene);
var flxJomoPayList = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "78%",
"id": "flxJomoPayList",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "22%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJomoPayList.setDefaultUnit(kony.flex.DP);
var segJomopayDetails = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}, {
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}, {
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}, {
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}, {
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}, {
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}, {
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}, {
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}, {
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}, {
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}, {
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}, {
"btnPic": "",
"lblAmount": "",
"lblCurrency": "",
"lblPayeeName": "",
"lblSeparator": "Label",
"lblTransactiondate": ""
}],
"groupCells": false,
"height": "100%",
"id": "segJomopayDetails",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "Copyseg0c15349e52ca249",
"rowSkin": "Copyseg0c15349e52ca249",
"rowTemplate": flxJomoPayPastDetails,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": false,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"btnPic": "btnPic",
"flxJomoPayPastDetails": "flxJomoPayPastDetails",
"flxProfilePic": "flxProfilePic",
"flxTAmount": "flxTAmount",
"flxUserDetails": "flxUserDetails",
"lblAmount": "lblAmount",
"lblCurrency": "lblCurrency",
"lblDescription": "lblDescription",
"lblPayeeName": "lblPayeeName",
"lblSeparator": "lblSeparator",
"lblTransactiondate": "lblTransactiondate"
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": true
});
var lblNoHistory = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblNoHistory",
"isVisible": false,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.jomopay.nosearch"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxJomoPayList.add(segJomopayDetails, lblNoHistory);
var flxJomopayBeneficiary = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "78%",
"id": "flxJomopayBeneficiary",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "22%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJomopayBeneficiary.setDefaultUnit(kony.flex.PERCENTAGE);
var flxJomoBeneList = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "75%",
"horizontalScrollIndicator": true,
"id": "flxJomoBeneList",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxJomoBeneList.setDefaultUnit(kony.flex.DP);
var segJomopayBeneficiary = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [
[{
"lbltmpTitle": "Label"
},
[{
"BenificiaryName": "",
"accountNumber": "",
"btnDelete": "",
"btnFav": "",
"lblInitial": "",
"lblSeparator": ""
}]
],
[{
"lbltmpTitle": "Label"
},
[{
"BenificiaryName": "",
"accountNumber": "",
"btnDelete": "",
"btnFav": "",
"lblInitial": "",
"lblSeparator": ""
}]
]
],
"groupCells": false,
"id": "segJomopayBeneficiary",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"onRowClick": AS_Button_b49a60fcf6df4c69a3ec1c476ab73e33,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "Copyseg0c15349e52ca249",
"rowSkin": "Copyseg0c15349e52ca249",
"rowTemplate": flxSegJMPBene,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"sectionHeaderTemplate": flxTmpHeader,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "1e415b00",
"separatorRequired": true,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"BenificiaryName": "BenificiaryName",
"accountNumber": "accountNumber",
"btnDelete": "btnDelete",
"btnFav": "btnFav",
"contactListDivider": "contactListDivider",
"flxAnimateBenf": "flxAnimateBenf",
"flxButtonHolder": "flxButtonHolder",
"flxDetails": "flxDetails",
"flxIcon1": "flxIcon1",
"flxSegJMPBene": "flxSegJMPBene",
"flxTmpHeader": "flxTmpHeader",
"flxUnderline": "flxUnderline",
"lblInitial": "lblInitial",
"lblSeparator": "lblSeparator",
"lbltmpTitle": "lbltmpTitle"
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": false,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": true
});
var lblNoBeneficiary = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblNoBeneficiary",
"isVisible": false,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.bene.noBeneError"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxJomoBeneList.add(segJomopayBeneficiary, lblNoBeneficiary);
flxJomopayBeneficiary.add(flxJomoBeneList);
var flxScanMerchantQR = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"clipBounds": true,
"height": "10%",
"id": "flxScanMerchantQR",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"width": "100%",
"zIndex": 7
}, {}, {});
flxScanMerchantQR.setDefaultUnit(kony.flex.DP);
var btnScanMerchantQR = new kony.ui.Button({
"bottom": "10%",
"centerX": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "80%",
"id": "btnScanMerchantQR",
"isVisible": true,
"onClick": AS_Button_a8d3b9a3aff644329f6cc789089a46ba,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.jomopay.scanMerchantQR"),
"width": "70%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var btnQRCodeReader = new kony.ui.Button({
"focusSkin": "sknSearch",
"height": "100%",
"id": "btnQRCodeReader",
"isVisible": false,
"onClick": AS_Button_e9e4c0f013fe4fd8b78936ce8bb16990,
"left": "5%",
"skin": "sknSearch",
"top": "0%",
"width": "9%",
"zIndex": 5
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var imgQRIcon = new kony.ui.Image2({
"height": "100%",
"id": "imgQRIcon",
"isVisible": true,
"left": "19.44%",
"skin": "slImage",
"src": "qrcode.png",
"top": "0.00%",
"width": "9%",
"zIndex": 2
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxScanMerchantQR.add(btnScanMerchantQR, btnQRCodeReader, imgQRIcon);
var flxAddBene = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "10%",
"id": "flxAddBene",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"width": "100%",
"zIndex": 7
}, {}, {});
flxAddBene.setDefaultUnit(kony.flex.DP);
var btnAddJomoPayBene = new kony.ui.Button({
"bottom": "10%",
"centerX": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "80%",
"id": "btnAddJomoPayBene",
"isVisible": true,
"onClick": AS_Button_e40483c53dfd4e8d873ad5b73a137f5d,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.jomopay.sendtoother"),
"width": "70%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
flxAddBene.add(btnAddJomoPayBene);
frmJoMoPayLanding.add(flxJoMoPayHeader, flxPaymentHistory, flxCheckRegJomopay, flxJomoPayList, flxJomopayBeneficiary, flxScanMerchantQR, flxAddBene);
};
function frmJoMoPayLandingGlobalsAr() {
frmJoMoPayLandingAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmJoMoPayLandingAr,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmJoMoPayLanding",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_ceb2c3f5781344109e1c85e3f4ac0cd5,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};

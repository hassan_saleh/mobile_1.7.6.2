//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function addWidgetsfrmMyAccountSettingsKAAr() {
frmMyAccountSettingsKA.setDefaultUnit(kony.flex.DP);
var flxMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxTransprnt",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxSettingsHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxSettingsHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "CopyslFbox0f07559cdcd2e4e",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSettingsHeader.setDefaultUnit(kony.flex.DP);
var flxTitle = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "flxTitle",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "sknflxTransprnt",
"width": "100%",
"zIndex": 2
}, {}, {});
flxTitle.setDefaultUnit(kony.flex.DP);
var lblSettingsTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"id": "lblSettingsTitle",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.myaccountsettings"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblAccNumber = new kony.ui.Label({
"centerY": "50%",
"id": "lblAccNumber",
"isVisible": false,
"skin": "lblAmountCurrency",
"text": " ***777",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxTitle.add(lblSettingsTitle, lblAccNumber);
var btnBack = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
"height": "90%",
"id": "btnBack",
"isVisible": true,
"left": "0%",
"onClick": AS_Button_e36a8f2af41e4d5f9a345d1b4399891e,
"skin": "CopyslButtonGlossBlue0e73a02c4810645",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0.00%",
"width": "20%",
"zIndex": 4
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [2, 0, 0, 0],
"paddingInPixel": false
}, {
"showProgressIndicator": true
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "45%",
"id": "lblBack",
"isVisible": true,
"left": "8%",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxSettingsHeader.add(flxTitle, btnBack, lblBack);
var FlexScrollContainer0aaefdaacee1040 = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "FlexScrollContainer0aaefdaacee1040",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknflxScrollBlue",
"top": "9%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
FlexScrollContainer0aaefdaacee1040.setDefaultUnit(kony.flex.DP);
var lblAddNickNameDesc = new kony.ui.Label({
"id": "lblAddNickNameDesc",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.accountSetting.chooseAccountsSettings"),
"top": "3%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLinewhiteOp",
"top": "3%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLine.setDefaultUnit(kony.flex.DP);
flxLine.add();
var segChooseAccounts = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [{
"Symbol": "o",
"lblACCHideShow": "Salary Account",
"lblAccName": "Salary Account",
"lblAccNickname": "Salary Account",
"lblAccountName": "Salary Account",
"lblAccountNumber": "***777",
"lblAmount": "2,7453 JOD",
"lblDefaultAccPayment": "Balance",
"lblDefaultAccTransfer": "Salary Account"
}, {
"Symbol": "o",
"lblACCHideShow": "Salary Account",
"lblAccName": "Salary Account",
"lblAccNickname": "Salary Account",
"lblAccountName": "Savings Account",
"lblAccountNumber": "***145",
"lblAmount": "2,7453 JOD",
"lblDefaultAccPayment": "Balance",
"lblDefaultAccTransfer": "Salary Account"
}, {
"Symbol": "o",
"lblACCHideShow": "Salary Account",
"lblAccName": "Salary Account",
"lblAccNickname": "Salary Account",
"lblAccountName": "Current Account",
"lblAccountNumber": "***0014",
"lblAmount": "2,7453 JOD",
"lblDefaultAccPayment": "Balance",
"lblDefaultAccTransfer": "Salary Account"
}],
"groupCells": false,
"id": "segChooseAccounts",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"onRowClick": AS_Segment_bf63ae0736d5495a8572d59e7bb49193,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "sknsegAcc",
"rowSkin": "sknsegAcc",
"rowTemplate": flxSegChooseAccounts,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "ffffff50",
"separatorRequired": true,
"separatorThickness": 2,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Symbol": "Symbol",
"flxSegChooseAccounts": "flxSegChooseAccounts",
"lblACCHideShow": "lblACCHideShow",
"lblAccName": "lblAccName",
"lblAccNickname": "lblAccNickname",
"lblAccountName": "lblAccountName",
"lblAccountNumber": "lblAccountNumber",
"lblAmount": "lblAmount",
"lblDefaultAccPayment": "lblDefaultAccPayment",
"lblDefaultAccTransfer": "lblDefaultAccTransfer"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"bounces": true,
"editStyle": constants.SEGUI_EDITING_STYLE_NONE,
"enableDictionary": false,
"indicator": constants.SEGUI_NONE,
"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
"showProgressIndicator": false
});
var LabelNoRecordsKA = new kony.ui.Label({
"id": "LabelNoRecordsKA",
"isVisible": false,
"right": "16%",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.alerts.NoAccountsMsg"),
"top": "20dp",
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
FlexScrollContainer0aaefdaacee1040.add(lblAddNickNameDesc, flxLine, segChooseAccounts, LabelNoRecordsKA);
flxMain.add(flxSettingsHeader, FlexScrollContainer0aaefdaacee1040);
frmMyAccountSettingsKA.add(flxMain);
};
function frmMyAccountSettingsKAGlobalsAr() {
frmMyAccountSettingsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmMyAccountSettingsKAAr,
"enabledForIdleTimeout": true,
"id": "frmMyAccountSettingsKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_b06f99cc19d64d84844d34bb8ac13dad,
"skin": "slFormCommon"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"configureExtendBottom": false,
"configureExtendTop": false,
"configureStatusBarStyle": false,
"footerOverlap": false,
"formTransparencyDuringPostShow": "100",
"headerOverlap": false,
"inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
"needsIndicatorDuringPostShow": false,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar"
});
};

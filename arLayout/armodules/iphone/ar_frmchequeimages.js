//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:02 EET 2020
function addWidgetsfrmchequeimagesAr() {
    frmchequeimages.setDefaultUnit(kony.flex.DP);
    var titleBarWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarWrapper.setDefaultUnit(kony.flex.DP);
    var iosTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "iosTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    iosTitleBar.setDefaultUnit(kony.flex.DP);
    var transferPayTitleLabel = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "22dp",
        "id": "transferPayTitleLabel",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.deposit.chequeImages"),
        "width": "72%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var backButton = new kony.ui.Button({
        "focusSkin": "sknleftBackButtonFocus",
        "height": "50dp",
        "id": "backButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_9ed59c0146d2491bad650a899b9c6118,
        "skin": "sknleftBackButtonNormal",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": false
    });
    iosTitleBar.add(transferPayTitleLabel, backButton);
    titleBarWrapper.add(iosTitleBar);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkgGray",
        "top": "50dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var searchSegmentedControllerWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "56dp",
        "id": "searchSegmentedControllerWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    searchSegmentedControllerWrapper.setDefaultUnit(kony.flex.DP);
    var imgradiobtn = new kony.ui.RadioButtonGroup({
        "centerY": "50%",
        "height": "60%",
        "id": "imgradiobtn",
        "isVisible": true,
        "right": "5%",
        "masterData": [["back", kony.i18n.getLocalizedString("i18n.deposit.back")],["front", kony.i18n.getLocalizedString("i18n.deposit.front")]],
        "onSelection": AS_RadioButtonGroup_c9ebe7999a824b5d99d802d0ce5c9115,
        "selectedKey": "front",
        "selectedKeyValue": ["front", "Front"],
        "skin": "sknslRadioButtonGroup",
        "width": "90%"
    }, {
        "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "groupCells": false,
        "viewConfig": {
            "toggleViewConfig": {
                "enableTintColor": true,
                "equalSegments": true,
                "tintColor": "1a298000"
            }
        },
        "viewType": constants.RADIOGROUP_VIEW_TYPE_TOGGLEVIEW
    });
    searchSegmentedControllerWrapper.add(imgradiobtn);
    var FlexContainer04824959abde740 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "80%",
        "id": "FlexContainer04824959abde740",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "30dp",
        "left": "0dp",
        "skin": "slFbox",
        "top": "15dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    FlexContainer04824959abde740.setDefaultUnit(kony.flex.DP);
    var frontImage = new kony.ui.Image2({
        "height": "100%",
        "id": "frontImage",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "check_front.png",
        "top": "0dp",
        "width": "100%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var backImage = new kony.ui.Image2({
        "height": "100%",
        "id": "backImage",
        "isVisible": false,
        "right": "0dp",
        "skin": "slImage",
        "src": "check_front.png",
        "top": "0dp",
        "width": "100%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer04824959abde740.add(frontImage, backImage);
    mainContent.add(searchSegmentedControllerWrapper, FlexContainer04824959abde740);
    frmchequeimages.add(titleBarWrapper, mainContent);
};
function frmchequeimagesGlobalsAr() {
    frmchequeimagesAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmchequeimagesAr,
        "allowHorizontalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmchequeimages",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient",
        "statusBarHidden": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": true,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "inTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "needsIndicatorDuringPostShow": false,
        "outTransitionConfig": {
            "transitionDirection": "none",
            "transitionEffect": "none"
        },
        "retainScrollPosition": false,
        "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
        "titleBar": false
    });
};

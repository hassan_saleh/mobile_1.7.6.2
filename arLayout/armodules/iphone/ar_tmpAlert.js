//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function initializetmpAlertAr() {
    flxAlertDetailAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxAlertDetail",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFbox"
    }, {}, {});
    flxAlertDetailAr.setDefaultUnit(kony.flex.DP);
    var lblDate = new kony.ui.Label({
        "id": "lblDate",
        "isVisible": true,
        "right": "10%",
        "skin": "sknLblAccNum",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAlertDetails = new kony.ui.Label({
        "bottom": "10dp",
        "id": "lblAlertDetails",
        "isVisible": true,
        "right": "10%",
        "skin": "CopysknLblWhike0g2a89378ebae48",
        "top": "5dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxAlertDetailAr.add(lblDate, lblAlertDetails);
}

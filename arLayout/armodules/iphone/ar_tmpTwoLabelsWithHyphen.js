//Do not Modify!! This is an auto generated module for 'iphone'. Generated on Mon Dec 21 09:49:03 EET 2020
function initializetmpTwoLabelsWithHyphenAr() {
flxWrapperKAAr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "flxWrapperKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skncontainerFAFAFAKA"
}, {}, {});
flxWrapperKAAr.setDefaultUnit(kony.flex.DP);
var flxInnerRound = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"focusSkin": "sknFocusRoundedFlexf0f0f0KA",
"height": "80.20%",
"id": "flxInnerRound",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "sknRoundedFlexNormalfafafaKA",
"top": "7dp",
"zIndex": 1
}, {}, {});
flxInnerRound.setDefaultUnit(kony.flex.DP);
var flxStrtKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "36dp",
"id": "flxStrtKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "slFbox",
"top": "0dp",
"width": "35%",
"zIndex": 1
}, {}, {});
flxStrtKA.setDefaultUnit(kony.flex.DP);
var lblStrtKA = new kony.ui.Label({
"centerY": "50%",
"id": "lblStrtKA",
"isVisible": true,
"right": "30%",
"skin": "sknLatoNUOccccccKA",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var lblExperience = new kony.ui.Label({
"centerY": "60%",
"id": "lblExperience",
"isVisible": false,
"right": "40%",
"skin": "sknLatoNUOccccccKA",
"text": "Label",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxStrtKA.add(lblStrtKA, lblExperience);
var lblhyphenKA = new kony.ui.Label({
"centerY": "50%",
"id": "lblhyphenKA",
"isVisible": true,
"right": "0%",
"skin": "sknLatoNUOccccccKA",
"text": "-",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
var flxEndKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "36dp",
"id": "flxEndKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "slFbox",
"top": "0dp",
"width": "35%",
"zIndex": 1
}, {}, {});
flxEndKA.setDefaultUnit(kony.flex.DP);
var lblEndKA = new kony.ui.Label({
"centerY": "50%",
"id": "lblEndKA",
"isVisible": true,
"right": "10%",
"left": "80%",
"skin": "sknLatoNUOccccccKA",
"width": "85%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false,
"wrapping": constants.WIDGET_TEXT_WORD_WRAP
});
flxEndKA.add(lblEndKA);
flxInnerRound.add( flxEndKA, lblhyphenKA,flxStrtKA);
flxWrapperKAAr.add(flxInnerRound);
}

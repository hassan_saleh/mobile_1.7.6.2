package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.kony.custom.pdf.CustomPDF;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_NSPDFViewer extends JSLibrary {

 
 
	public static final String savepdf = "savepdf";
 
 
	public static final String openpdf = "openpdf";
 
	String[] methods = { savepdf, openpdf };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_NSPDFViewer(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 2){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String base64String0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 base64String0 = (java.lang.String)params[0];
 }
 java.lang.String pdfName0 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 pdfName0 = (java.lang.String)params[1];
 }
 ret = this.savepdf( base64String0, pdfName0 );
 
 			break;
 		case 1:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String pdfname1 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 pdfname1 = (java.lang.String)params[0];
 }
 ret = this.openpdf( pdfname1 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "NSPDFViewer";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] savepdf( java.lang.String inputKey0, java.lang.String inputKey1 ){
 
		Object[] ret = null;
 Double val = new Double(com.kony.custom.pdf.CustomPDF.savePDF( inputKey0
 , inputKey1
 ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] openpdf( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 Double val = new Double(com.kony.custom.pdf.CustomPDF.openPDF( inputKey0
 ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
};

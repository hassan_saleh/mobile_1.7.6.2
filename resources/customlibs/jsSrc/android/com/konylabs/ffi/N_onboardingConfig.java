package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.pst.labibaonbeardingsetup.LabibaConfig;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_onboardingConfig extends JSLibrary {

 
 
	public static final String startActivity = "startActivity";
 
	String[] methods = { startActivity };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_onboardingConfig(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 Boolean isArabic0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 isArabic0 = (Boolean)params[0];
 }
 ret = this.startActivity( isArabic0 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "onboardingConfig";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] startActivity( Boolean inputKey0 ){
 
		Object[] ret = null;
 java.lang.String val = com.pst.labibaonbeardingsetup.LabibaConfig.connLabiba( inputKey0.booleanValue() );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
};

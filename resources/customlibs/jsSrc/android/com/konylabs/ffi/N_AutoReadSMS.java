package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.cpcg.smsreader.SmsReceiver;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_AutoReadSMS extends JSLibrary {

 
	String[] methods = { };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[1];
 libs[0] = new SmsReader();
 return libs;
 }



	public N_AutoReadSMS(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "AutoReadSMS";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 


class SmsReader extends JSLibrary {

 
 
	public static final String registerListener = "registerListener";
 
 
	public static final String deregisterListener = "deregisterListener";
 
	String[] methods = { registerListener, deregisterListener };

	public Object createInstance(final Object[] params) {
 return new com.cpcg.smsreader.SmsReceiver(
 (com.konylabs.vm.Function)params[0] , (java.lang.String)params[1] );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.registerListener(params[0]
 );
 
 			break;
 		case 1:
 if (paramLen < 0 || paramLen > 1){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 ret = this.deregisterListener(params[0]
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "SmsReader";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] registerListener( Object self ){
 
		Object[] ret = null;
 ((com.cpcg.smsreader.SmsReceiver)self).registerListener( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] deregisterListener( Object self ){
 
		Object[] ret = null;
 ((com.cpcg.smsreader.SmsReceiver)self).deregisterListener( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}

};

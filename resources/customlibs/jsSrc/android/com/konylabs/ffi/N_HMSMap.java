package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.bankofjordan.mobileapp.WrapperClass;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_HMSMap extends JSLibrary {

 
 
	public static final String invokeHMSMap = "invokeHMSMap";
 
 
	public static final String isHmsAvailable = "isHmsAvailable";
 
	String[] methods = { invokeHMSMap, isHmsAvailable };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_HMSMap(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 2){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String locale0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 locale0 = (java.lang.String)params[0];
 }
 com.konylabs.vm.LuaTable jsLocationArray0 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 jsLocationArray0 = (com.konylabs.vm.LuaTable)params[1];
 }
 ret = this.invokeHMSMap( locale0, jsLocationArray0 );
 
 			break;
 		case 1:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.isHmsAvailable( );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "HMSMap";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] invokeHMSMap( java.lang.String inputKey0, com.konylabs.vm.LuaTable inputKey1 ){
 
		Object[] ret = null;
 com.bankofjordan.mobileapp.WrapperClass.invokeHMSMap( inputKey0
 , (java.util.Vector)TableLib.convertToList(inputKey1)
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] isHmsAvailable( ){
 
		Object[] ret = null;
 Boolean val = new Boolean(com.bankofjordan.mobileapp.WrapperClass.isHmsAvailable( ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
};
